<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
if (env('APP_ENV') === 'production') {
    URL::forceSchema('https');
}

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/main', ['as' => 'home', 'uses' => 'HomeController@index']);

// Route::get('/post', function() {
//     $a = [
//         [
//             'title' => 'Foo',
//             'author'    => 'vue'
//         ],
//     ];
//     return $a;
// });

Auth::routes();

// Telegram test
// use \App\Notifications\TelegramNotification;
// Route::post('/', function () {
//     Auth::user()->notify(new TelegramNotification());
// });

//User Management
Route::group(['prefix' => 'main', 'middleware' => 'auth', 'namespace' => 'Main'], function()
{
	//Home
  if (env('APP_ENV') === 'production') {
      URL::forceSchema('https');
  }
  Route::get('/', ['as' => 'main.index', 'uses' => 'MainController@index']);
	Route::get('/main', ['as' => 'main.index', 'uses' => 'MainController@index']);
	Route::get('/downloadExcel', ['as' => 'main.index', 'uses' => 'MainController@downloadExcel']);
	//Notif
	Route::get('/notif', ['as' => 'main.notif.index', 'uses' => 'MainController@notif']);
	//Profile
		//detail
	Route::get('/profile', ['as' => 'main.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'main.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'main.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'main.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'main.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'main.user.index', 'uses' => 'UserController@index']);
		//create
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'main.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'main.user.store', 'uses' => 'UserController@store']);
		//edit
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'main.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'main.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'main.user.delete', 'uses' => 'UserController@delete']);

	//User
		//index
	Route::get('/userbranch', ['middleware' => ['role:userbranch|read'], 'as' => 'main.userbranch.index', 'uses' => 'UserbranchController@index']);
		//create
	Route::get('/userbranch/create', ['middleware' => ['role:userbranch|create'], 'as' => 'main.userbranch.create', 'uses' => 'UserbranchController@create']);
	Route::post('/userbranch/create', ['middleware' => ['role:userbranch|create'], 'as' => 'main.userbranch.store', 'uses' => 'UserbranchController@store']);
		//edit
	Route::get('/userbranch/{id}/edit', ['middleware' => ['role:userbranch|update'], 'as' => 'main.userbranch.edit', 'uses' => 'UserbranchController@edit']);
	Route::put('/userbranch/{id}/edit', ['middleware' => ['role:userbranch|update'], 'as' => 'main.userbranch.update', 'uses' => 'UserbranchController@update']);
		//delete
	Route::delete('/userbranch/{id}/delete', ['middleware' => ['role:userbranch|delete'], 'as' => 'main.userbranch.delete', 'uses' => 'UserbranchController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'main.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'main.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'main.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'main.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'main.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'main.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'main.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'main.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'main.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'main.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'main.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'main.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'main.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'main.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'main.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'main.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'main.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'main.privilege.delete', 'uses' => 'PrivilegeController@delete']);

	//Branch
		//index
	Route::get('/branch', ['middleware' => ['role:branch|read'], 'as' => 'main.branch.index', 'uses' => 'BranchController@index']);
	Route::get('/branch/data', ['as' => 'main.branch.data', 'uses' => 'BranchController@data']);
		//create
	Route::get('/branch/create', ['middleware' => ['role:branch|create'], 'as' => 'main.branch.create', 'uses' => 'BranchController@create']);
	Route::post('/branch/create', ['middleware' => ['role:branch|create'], 'as' => 'main.branch.store', 'uses' => 'BranchController@store']);
		//edit
	Route::get('/branch/edit/{id}', ['middleware' => ['role:branch|update'], 'as' => 'main.branch.edit', 'uses' => 'BranchController@edit']);
	Route::put('/branch/edit/{id}', ['middleware' => ['role:branch|update'], 'as' => 'main.branch.update', 'uses' => 'BranchController@update']);
		//delete
	Route::delete('/branch/delete/{id}', ['middleware' => ['role:branch|delete'], 'as' => 'main.branch.delete', 'uses' => 'BranchController@delete']);
	Route::post('/branch/deletebulk', ['middleware' => ['role:branch|delete'], 'as' => 'main.branch.deletebulk', 'uses' => 'BranchController@deletebulk']);

	//Faq
		//index
	Route::get('/faq', ['middleware' => ['role:faq|read'], 'as' => 'main.faq.index', 'uses' => 'FaqController@index']);
	Route::get('/faq/data', ['as' => 'main.faq.data', 'uses' => 'FaqController@data']);
		//create
	Route::get('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'main.faq.create', 'uses' => 'FaqController@create']);
	Route::post('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'main.faq.store', 'uses' => 'FaqController@store']);
		//edit
	Route::get('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'main.faq.edit', 'uses' => 'FaqController@edit']);
	Route::put('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'main.faq.update', 'uses' => 'FaqController@update']);
		//delete
	Route::delete('/faq/delete/{id}', ['middleware' => ['role:faq|delete'], 'as' => 'main.faq.delete', 'uses' => 'FaqController@delete']);
	Route::post('/faq/deletebulk', ['middleware' => ['role:faq|delete'], 'as' => 'main.faq.deletebulk', 'uses' => 'FaqController@deletebulk']);


	//Popup
		//index
	Route::get('/popup', ['middleware' => ['role:popup|read'], 'as' => 'main.popup.index', 'uses' => 'PopupController@index']);
		//edit
	Route::get('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'main.popup.edit', 'uses' => 'PopupController@edit']);
	Route::put('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'main.popup.update', 'uses' => 'PopupController@update']);


	//Customer Type
		//index
	Route::get('/customer-type', ['as' => 'main.customer_type.index', 'uses' => 'CustomerTypeController@index']);
	Route::get('/customer-type/data', ['as' => 'main.customer_type.data', 'uses' => 'CustomerTypeController@data']);
		//create
	Route::get('/customer-type/create', ['as' => 'main.customer_type.create', 'uses' => 'CustomerTypeController@create']);
	Route::post('/customer-type/create', ['as' => 'main.customer_type.store', 'uses' => 'CustomerTypeController@store']);
		//edit
	Route::get('/customer-type/edit/{id}', ['as' => 'main.customer_type.edit', 'uses' => 'CustomerTypeController@edit']);
	Route::put('/customer-type/edit/{id}', ['as' => 'main.customer_type.update', 'uses' => 'CustomerTypeController@update']);
		//delete
	Route::delete('/customer-type/delete/{id}', ['as' => 'main.customer_type.delete', 'uses' => 'CustomerTypeController@delete']);
	Route::post('/customer-type/bulk-change-status', ['as' => 'main.customer_type.bulk_change_status', 'uses' => 'CustomerTypeController@bulk_change_status']);


	//General Journal
		//index
	Route::get('/general-journal', ['as' => 'main.general-journal.index', 'uses' => 'GeneralJournalController@index']);
	Route::get('/general-journal/data', ['as' => 'main.general-journal.data', 'uses' => 'GeneralJournalController@data']);
		//create
	Route::get('/general-journal/create', ['as' => 'main.general-journal.create', 'uses' => 'GeneralJournalController@create']);
	Route::post('/general-journal/create', ['as' => 'main.general-journal.store', 'uses' => 'GeneralJournalController@store']);
		//edit
	Route::get('/general-journal/edit/{id}', ['as' => 'main.general-journal.edit', 'uses' => 'GeneralJournalController@edit']);
	Route::put('/general-journal/edit/{id}', ['as' => 'main.general-journal.update', 'uses' => 'GeneralJournalController@update']);
		//delete
	Route::delete('/general-journal/delete/{id}', ['as' => 'main.general-journal.delete', 'uses' => 'GeneralJournalController@delete']);

});


//Setting
Route::group(['prefix' => 'main/setting', 'middleware' => 'auth', 'namespace' => 'Main\Setting'], function()
{

  if (env('APP_ENV') === 'production') {
      URL::forceSchema('https');
  }
	//Period
		//index
	Route::get('/period', ['as' => 'main.period.index', 'uses' => 'PeriodController@index']);
	Route::get('/period/data', ['as' => 'main.period.data', 'uses' => 'PeriodController@data']);
		//create
	Route::get('/period/create', ['as' => 'main.period.create', 'uses' => 'PeriodController@create']);
	Route::post('/period/create', ['as' => 'main.period.store', 'uses' => 'PeriodController@store']);
		//edit
	Route::get('/period/edit/{id}', ['as' => 'main.period.edit', 'uses' => 'PeriodController@edit']);
	Route::put('/period/edit/{id}', ['as' => 'main.period.update', 'uses' => 'PeriodController@update']);
		//delete
	Route::delete('/period/delete/{id}', ['as' => 'main.period.delete', 'uses' => 'PeriodController@delete']);
		// archive
	Route::post('/period/bulk-change-status', ['as' => 'main.period.bulk_change_status', 'uses' => 'PeriodController@bulk_change_status']);

	//Monthly Rate
		//index
	Route::get('/monthly-rate', ['as' => 'main.monthly-rate.index', 'uses' => 'MonthlyRateController@index']);
	Route::get('/monthly-rate/data', ['as' => 'main.monthly-rate.data', 'uses' => 'MonthlyRateController@data']);
		//create
	Route::get('/monthly-rate/create', ['as' => 'main.monthly-rate.create', 'uses' => 'MonthlyRateController@create']);
	Route::post('/monthly-rate/create', ['as' => 'main.monthly-rate.store', 'uses' => 'MonthlyRateController@store']);
		//edit
	Route::get('/monthly-rate/edit/{id}', ['as' => 'main.monthly-rate.edit', 'uses' => 'MonthlyRateController@edit']);
	Route::put('/monthly-rate/edit/{id}', ['as' => 'main.monthly-rate.update', 'uses' => 'MonthlyRateController@update']);
		//delete
	Route::delete('/monthly-rate/delete/{id}', ['as' => 'main.monthly-rate.delete', 'uses' => 'MonthlyRateController@delete']);
		// archive
	Route::post('/monthly-rate/bulk-change-status', ['as' => 'main.monthly-rate.bulk_change_status', 'uses' => 'MonthlyRateController@bulk_change_status']);

	//Depreciation Type
		//index
	Route::get('/depreciation-type', ['as' => 'main.depreciation-type.index', 'uses' => 'DepreciationTypeController@index']);
	Route::get('/depreciation-type/data', ['as' => 'main.depreciation-type.data', 'uses' => 'DepreciationTypeController@data']);
		//create
	Route::get('/depreciation-type/create', ['as' => 'main.depreciation-type.create', 'uses' => 'DepreciationTypeController@create']);
	Route::post('/depreciation-type/create', ['as' => 'main.depreciation-type.store', 'uses' => 'DepreciationTypeController@store']);
		//edit
	Route::get('/depreciation-type/edit/{id}', ['as' => 'main.depreciation-type.edit', 'uses' => 'DepreciationTypeController@edit']);
	Route::put('/depreciation-type/edit/{id}', ['as' => 'main.depreciation-type.update', 'uses' => 'DepreciationTypeController@update']);
		//delete
	Route::delete('/depreciation-type/delete/{id}', ['as' => 'main.depreciation-type.delete', 'uses' => 'DepreciationTypeController@delete']);
		// archive
	Route::post('/depreciation-type/bulk-change-status', ['as' => 'main.depreciation-type.bulk_change_status', 'uses' => 'DepreciationTypeController@bulk_change_status']);


	//Deferral Distribution Type
		//index
	Route::get('/deferral-distribution-type', ['as' => 'main.deferral-distribution-type.index', 'uses' => 'DeferralDistributionTypeController@index']);
	Route::get('/deferral-distribution-type/data', ['as' => 'main.deferral-distribution-type.data', 'uses' => 'DeferralDistributionTypeController@data']);
		//create
	Route::get('/deferral-distribution-type/create', ['as' => 'main.deferral-distribution-type.create', 'uses' => 'DeferralDistributionTypeController@create']);
	Route::post('/deferral-distribution-type/create', ['as' => 'main.deferral-distribution-type.store', 'uses' => 'DeferralDistributionTypeController@store']);
		//edit
	Route::get('/deferral-distribution-type/edit/{id}', ['as' => 'main.deferral-distribution-type.edit', 'uses' => 'DeferralDistributionTypeController@edit']);
	Route::put('/deferral-distribution-type/edit/{id}', ['as' => 'main.deferral-distribution-type.update', 'uses' => 'DeferralDistributionTypeController@update']);
		//delete
	Route::delete('/deferral-distribution-type/delete/{id}', ['as' => 'main.deferral-distribution-type.delete', 'uses' => 'DeferralDistributionTypeController@delete']);
		// archive
	Route::post('/deferral-distribution-type/bulk-change-status', ['as' => 'main.deferral-distribution-type.bulk_change_status', 'uses' => 'DeferralDistributionTypeController@bulk_change_status']);


	//Deferral Profile Maintenance
		//index
	Route::get('/deferral-profile-maintenance', ['as' => 'main.deferral-profile-maintenance.index', 'uses' => 'DeferralProfileMaintenanceController@index']);
	Route::get('/deferral-profile-maintenance/data', ['as' => 'main.deferral-profile-maintenance.data', 'uses' => 'DeferralProfileMaintenanceController@data']);
		//create
	Route::get('/deferral-profile-maintenance/create', ['as' => 'main.deferral-profile-maintenance.create', 'uses' => 'DeferralProfileMaintenanceController@create']);
	Route::post('/deferral-profile-maintenance/create', ['as' => 'main.deferral-profile-maintenance.store', 'uses' => 'DeferralProfileMaintenanceController@store']);
		//edit
	Route::get('/deferral-profile-maintenance/edit/{id}', ['as' => 'main.deferral-profile-maintenance.edit', 'uses' => 'DeferralProfileMaintenanceController@edit']);
	Route::put('/deferral-profile-maintenance/edit/{id}', ['as' => 'main.deferral-profile-maintenance.update', 'uses' => 'DeferralProfileMaintenanceController@update']);
		//delete
	Route::delete('/deferral-profile-maintenance/delete/{id}', ['as' => 'main.deferral-profile-maintenance.delete', 'uses' => 'DeferralProfileMaintenanceController@delete']);
		// archive
	Route::post('/deferral-profile-maintenance/bulk-change-status', ['as' => 'main.deferral-profile-maintenance.bulk_change_status', 'uses' => 'DeferralProfileMaintenanceController@bulk_change_status']);


	//Inventory
		//index
	Route::get('/inventory', ['as' => 'main.inventory.index', 'uses' => 'InventoryController@index']);
	Route::post('/inventory/data', ['as' => 'main.inventory.data', 'uses' => 'InventoryController@data']);
  Route::get('/inventory/data-modal', ['as' => 'main.inventory.data-modal', 'uses' => 'InventoryController@data_modal']);
	Route::get('/inventory/data-modal-transfer', ['as' => 'main.inventory.data-modal-transfer', 'uses' => 'InventoryController@data_modal_transfer']);
		//create
	Route::get('/inventory/create', ['as' => 'main.inventory.create', 'uses' => 'InventoryController@create']);
	Route::post('/inventory/create', ['as' => 'main.inventory.store', 'uses' => 'InventoryController@store']);
		//edit
	Route::get('/inventory/edit/{id}', ['as' => 'main.inventory.edit', 'uses' => 'InventoryController@edit']);
	Route::put('/inventory/edit/{id}', ['as' => 'main.inventory.update', 'uses' => 'InventoryController@update']);
		//delete
	Route::delete('/inventory/delete/{id}', ['as' => 'main.inventory.delete', 'uses' => 'InventoryController@delete']);
		// archive
	Route::post('/inventory/bulk-change-status', ['as' => 'main.inventory.bulk_change_status', 'uses' => 'InventoryController@bulk_change_status']);

	//Inventory Group
		//index
	Route::get('/inventory-group', ['as' => 'main.inventory-group.index', 'uses' => 'InventoryGroupController@index']);
	Route::post('/inventory-group/data', ['as' => 'main.inventory-group.data', 'uses' => 'InventoryGroupController@data']);
		//create
	Route::get('/inventory-group/create', ['as' => 'main.inventory-group.create', 'uses' => 'InventoryGroupController@create']);
	Route::post('/inventory-group/create', ['as' => 'main.inventory-group.store', 'uses' => 'InventoryGroupController@store']);
		//edit
	Route::get('/inventory-group/edit/{id}', ['as' => 'main.inventory-group.edit', 'uses' => 'InventoryGroupController@edit']);
	Route::put('/inventory-group/edit/{id}', ['as' => 'main.inventory-group.update', 'uses' => 'InventoryGroupController@update']);
		//delete
	Route::delete('/inventory-group/delete/{id}', ['as' => 'main.inventory-group.delete', 'uses' => 'InventoryGroupController@delete']);
		// archive
	Route::post('/inventory-group/bulk-change-status', ['as' => 'main.inventory-group.bulk_change_status', 'uses' => 'InventoryGroupController@bulk_change_status']);

	//Inventory Type
		//index
	Route::get('/inventory-type', ['as' => 'main.inventory-type.index', 'uses' => 'InventoryTypeController@index']);
	Route::post('/inventory-type/data', ['as' => 'main.inventory-type.data', 'uses' => 'InventoryTypeController@data']);
		//create
	Route::get('/inventory-type/create', ['as' => 'main.inventory-type.create', 'uses' => 'InventoryTypeController@create']);
	Route::post('/inventory-type/create', ['as' => 'main.inventory-type.store', 'uses' => 'InventoryTypeController@store']);
		//edit
	Route::get('/inventory-type/edit/{id}', ['as' => 'main.inventory-type.edit', 'uses' => 'InventoryTypeController@edit']);
	Route::put('/inventory-type/edit/{id}', ['as' => 'main.inventory-type.update', 'uses' => 'InventoryTypeController@update']);
		//delete
	Route::delete('/inventory-type/delete/{id}', ['as' => 'main.inventory-type.delete', 'uses' => 'InventoryTypeController@delete']);
		// archive
	Route::post('/inventory-type/bulk-change-status', ['as' => 'main.inventory-type.bulk_change_status', 'uses' => 'InventoryTypeController@bulk_change_status']);

	//Inventory Size
		//index
	Route::get('/inventory-size', ['as' => 'main.inventory-size.index', 'uses' => 'InventorySizeController@index']);
	Route::post('/inventory-size/data', ['as' => 'main.inventory-size.data', 'uses' => 'InventorySizeController@data']);
		//create
	Route::get('/inventory-size/create', ['as' => 'main.inventory-size.create', 'uses' => 'InventorySizeController@create']);
	Route::post('/inventory-size/create', ['as' => 'main.inventory-size.store', 'uses' => 'InventorySizeController@store']);
		//edit
	Route::get('/inventory-size/edit/{id}', ['as' => 'main.inventory-size.edit', 'uses' => 'InventorySizeController@edit']);
	Route::put('/inventory-size/edit/{id}', ['as' => 'main.inventory-size.update', 'uses' => 'InventorySizeController@update']);
		//delete
	Route::delete('/inventory-size/delete/{id}', ['as' => 'main.inventory-size.delete', 'uses' => 'InventorySizeController@delete']);
		// archive
	Route::post('/inventory-size/bulk-change-status', ['as' => 'main.inventory-size.bulk_change_status', 'uses' => 'InventorySizeController@bulk_change_status']);

	//Inventory Weight
		//index
	Route::get('/inventory-weight', ['as' => 'main.inventory-weight.index', 'uses' => 'InventoryWeightController@index']);
	Route::post('/inventory-weight/data', ['as' => 'main.inventory-weight.data', 'uses' => 'InventoryWeightController@data']);
		//create
	Route::get('/inventory-weight/create', ['as' => 'main.inventory-weight.create', 'uses' => 'InventoryWeightController@create']);
	Route::post('/inventory-weight/create', ['as' => 'main.inventory-weight.store', 'uses' => 'InventoryWeightController@store']);
		//edit
	Route::get('/inventory-weight/edit/{id}', ['as' => 'main.inventory-weight.edit', 'uses' => 'InventoryWeightController@edit']);
	Route::put('/inventory-weight/edit/{id}', ['as' => 'main.inventory-weight.update', 'uses' => 'InventoryWeightController@update']);
		//delete
	Route::delete('/inventory-weight/delete/{id}', ['as' => 'main.inventory-weight.delete', 'uses' => 'InventoryWeightController@delete']);
		// archive
	Route::post('/inventory-weight/bulk-change-status', ['as' => 'main.inventory-weight.bulk_change_status', 'uses' => 'InventoryWeightController@bulk_change_status']);

	//Unit Of Measure
		//index
	Route::get('/unit-of-measure', ['as' => 'main.unit-of-measure.index', 'uses' => 'UnitOfMeasureController@index']);
	Route::post('/unit-of-measure/data', ['as' => 'main.unit-of-measure.data', 'uses' => 'UnitOfMeasureController@data']);
		//create
	Route::get('/unit-of-measure/create', ['as' => 'main.unit-of-measure.create', 'uses' => 'UnitOfMeasureController@create']);
	Route::post('/unit-of-measure/create', ['as' => 'main.unit-of-measure.store', 'uses' => 'UnitOfMeasureController@store']);
		//edit
	Route::get('/unit-of-measure/edit/{id}', ['as' => 'main.unit-of-measure.edit', 'uses' => 'UnitOfMeasureController@edit']);
	Route::put('/unit-of-measure/edit/{id}', ['as' => 'main.unit-of-measure.update', 'uses' => 'UnitOfMeasureController@update']);
		//delete
	Route::delete('/unit-of-measure/delete/{id}', ['as' => 'main.unit-of-measure.delete', 'uses' => 'UnitOfMeasureController@delete']);
		// archive
	Route::post('/unit-of-measure/bulk-change-status', ['as' => 'main.unit-of-measure.bulk_change_status', 'uses' => 'UnitOfMeasureController@bulk_change_status']);

	//Vendor
		//index
	Route::get('/vendor', ['as' => 'main.vendor.index', 'uses' => 'VendorController@index']);
	Route::post('/vendor/data', ['as' => 'main.vendor.data', 'uses' => 'VendorController@data']);
		//create
	Route::get('/vendor/create', ['as' => 'main.vendor.create', 'uses' => 'VendorController@create']);
	Route::post('/vendor/create', ['as' => 'main.vendor.store', 'uses' => 'VendorController@store']);
		//edit
	Route::get('/vendor/edit/{id}', ['as' => 'main.vendor.edit', 'uses' => 'VendorController@edit']);
	Route::put('/vendor/edit/{id}', ['as' => 'main.vendor.update', 'uses' => 'VendorController@update']);
		//delete
	Route::delete('/vendor/delete/{id}', ['as' => 'main.vendor.delete', 'uses' => 'VendorController@delete']);
		// archive
	Route::post('/vendor/bulk-change-status', ['as' => 'main.vendor.bulk_change_status', 'uses' => 'VendorController@bulk_change_status']);

	//Country
		//index
	Route::get('/country', ['as' => 'main.country.index', 'uses' => 'CountryController@index']);
	Route::post('/country/data', ['as' => 'main.country.data', 'uses' => 'CountryController@data']);
		//create
	Route::get('/country/create', ['as' => 'main.country.create', 'uses' => 'CountryController@create']);
	Route::post('/country/create', ['as' => 'main.country.store', 'uses' => 'CountryController@store']);
		//edit
	Route::get('/country/edit/{id}', ['as' => 'main.country.edit', 'uses' => 'CountryController@edit']);
	Route::put('/country/edit/{id}', ['as' => 'main.country.update', 'uses' => 'CountryController@update']);
		//delete
	Route::delete('/country/delete/{id}', ['as' => 'main.country.delete', 'uses' => 'CountryController@delete']);
		// archive
	Route::post('/country/bulk-change-status', ['as' => 'main.country.bulk_change_status', 'uses' => 'CountryController@bulk_change_status']);

	//City
		//index
	Route::get('/city', ['as' => 'main.city.index', 'uses' => 'CityController@index']);
	Route::post('/city/data', ['as' => 'main.city.data', 'uses' => 'CityController@data']);
		//create
	Route::get('/city/create', ['as' => 'main.city.create', 'uses' => 'CityController@create']);
	Route::post('/city/create', ['as' => 'main.city.store', 'uses' => 'CityController@store']);
		//edit
	Route::get('/city/edit/{id}', ['as' => 'main.city.edit', 'uses' => 'CityController@edit']);
	Route::put('/city/edit/{id}', ['as' => 'main.city.update', 'uses' => 'CityController@update']);
		//delete
	Route::delete('/city/delete/{id}', ['as' => 'main.city.delete', 'uses' => 'CityController@delete']);
		// archive
	Route::post('/city/bulk-change-status', ['as' => 'main.city.bulk_change_status', 'uses' => 'CityController@bulk_change_status']);


	//Temprature
		//index
	Route::get('/temprature', ['as' => 'main.temprature.index', 'uses' => 'TempratureController@index']);
	Route::post('/temprature/data', ['as' => 'main.temprature.data', 'uses' => 'TempratureController@data']);
		//create
	Route::get('/temprature/create', ['as' => 'main.temprature.create', 'uses' => 'TempratureController@create']);
	Route::post('/temprature/create', ['as' => 'main.temprature.store', 'uses' => 'TempratureController@store']);
		//edit
	Route::get('/temprature/edit/{id}', ['as' => 'main.temprature.edit', 'uses' => 'TempratureController@edit']);
	Route::put('/temprature/edit/{id}', ['as' => 'main.temprature.update', 'uses' => 'TempratureController@update']);
		//delete
	Route::delete('/temprature/delete/{id}', ['as' => 'main.temprature.delete', 'uses' => 'TempratureController@delete']);
		// archive
	Route::post('/temprature/bulk-change-status', ['as' => 'main.temprature.bulk_change_status', 'uses' => 'TempratureController@bulk_change_status']);


	//District
		//index
	Route::get('/district', ['as' => 'main.district.index', 'uses' => 'DistrictController@index']);
	Route::post('/district/data', ['as' => 'main.district.data', 'uses' => 'DistrictController@data']);
		//create
	Route::get('/district/create', ['as' => 'main.district.create', 'uses' => 'DistrictController@create']);
	Route::post('/district/create', ['as' => 'main.district.store', 'uses' => 'DistrictController@store']);
		//edit
	Route::get('/district/edit/{id}', ['as' => 'main.district.edit', 'uses' => 'DistrictController@edit']);
	Route::put('/district/edit/{id}', ['as' => 'main.district.update', 'uses' => 'DistrictController@update']);
		//delete
	Route::delete('/district/delete/{id}', ['as' => 'main.district.delete', 'uses' => 'DistrictController@delete']);
		// archive
	Route::post('/district/bulk-change-status', ['as' => 'main.district.bulk_change_status', 'uses' => 'DistrictController@bulk_change_status']);


	//Warehouse
		//index
	Route::get('/warehouse', ['as' => 'main.warehouse.index', 'uses' => 'WarehouseController@index']);
	Route::post('/warehouse/data', ['as' => 'main.warehouse.data', 'uses' => 'WarehouseController@data']);
		//create
	Route::get('/warehouse/create', ['as' => 'main.warehouse.create', 'uses' => 'WarehouseController@create']);
	Route::post('/warehouse/create', ['as' => 'main.warehouse.store', 'uses' => 'WarehouseController@store']);
		//edit
	Route::get('/warehouse/edit/{id}', ['as' => 'main.warehouse.edit', 'uses' => 'WarehouseController@edit']);
	Route::put('/warehouse/edit/{id}', ['as' => 'main.warehouse.update', 'uses' => 'WarehouseController@update']);
		//delete
	Route::delete('/warehouse/delete/{id}', ['as' => 'main.warehouse.delete', 'uses' => 'WarehouseController@delete']);
		// archive
	Route::post('/warehouse/bulk-change-status', ['as' => 'main.warehouse.bulk_change_status', 'uses' => 'WarehouseController@bulk_change_status']);

	//Room
		//index
	Route::get('/room', ['as' => 'main.room.index', 'uses' => 'RoomController@index']);
  Route::post('/room/data', ['as' => 'main.room.data', 'uses' => 'RoomController@data']);
	Route::get('/room/data-detail', ['as' => 'main.room.data', 'uses' => 'RoomController@data_detail']);
		//create
	Route::get('/room/create', ['as' => 'main.room.create', 'uses' => 'RoomController@create']);
  Route::post('/room/create', ['as' => 'main.room.store', 'uses' => 'RoomController@store']);
	Route::post('/room/create-detail', ['as' => 'main.room.store-detail', 'uses' => 'RoomController@store_detail']);
		//edit
	Route::get('/room/edit/{id}', ['as' => 'main.room.edit', 'uses' => 'RoomController@edit']);
  Route::put('/room/edit/{id}', ['as' => 'main.room.update', 'uses' => 'RoomController@update']);
	Route::put('/room/update-header-id/{id}', ['as' => 'main.room.update', 'uses' => 'RoomController@update_header_id']);
		//delete
  Route::delete('/room/delete/{id}', ['as' => 'main.room.delete', 'uses' => 'RoomController@delete']);
	Route::delete('/room/delete-detail/{id}', ['as' => 'main.room.delete', 'uses' => 'RoomController@delete_detail']);
		// archive
	Route::post('/room/bulk-change-status', ['as' => 'main.room.bulk_change_status', 'uses' => 'RoomController@bulk_change_status']);

	//Store_Location
		//index
	Route::get('/store-location', ['as' => 'main.store-location.index', 'uses' => 'StoreLocationController@index']);
	Route::post('/store-location/data', ['as' => 'main.store-location.data', 'uses' => 'StoreLocationController@data']);
		//create
	Route::get('/store-location/create', ['as' => 'main.store-location.create', 'uses' => 'StoreLocationController@create']);
	Route::post('/store-location/create', ['as' => 'main.store-location.store', 'uses' => 'StoreLocationController@store']);
		//edit
	Route::get('/store-location/edit/{id}', ['as' => 'main.store-location.edit', 'uses' => 'StoreLocationController@edit']);
	Route::put('/store-location/edit/{id}', ['as' => 'main.store-location.update', 'uses' => 'StoreLocationController@update']);
		//delete
	Route::delete('/store-location/delete/{id}', ['as' => 'main.store-location.delete', 'uses' => 'StoreLocationController@delete']);
		// archive
	Route::post('/store-location/bulk-change-status', ['as' => 'main.store-location.bulk_change_status', 'uses' => 'StoreLocationController@bulk_change_status']);

  //Ship From
		//index
	Route::get('/ship-from', ['as' => 'main.ship-from.index', 'uses' => 'ShipFromController@index']);
	Route::post('/ship-from/data', ['as' => 'main.ship-from.data', 'uses' => 'ShipFromController@data']);
		//create
	Route::get('/ship-from/create', ['as' => 'main.ship-from.create', 'uses' => 'ShipFromController@create']);
	Route::post('/ship-from/create', ['as' => 'main.ship-from.store', 'uses' => 'ShipFromController@store']);
		//edit
	Route::get('/ship-from/edit/{id}', ['as' => 'main.ship-from.edit', 'uses' => 'ShipFromController@edit']);
	Route::put('/ship-from/edit/{id}', ['as' => 'main.ship-from.update', 'uses' => 'ShipFromController@update']);
		//delete
	Route::delete('/ship-from/delete/{id}', ['as' => 'main.ship-from.delete', 'uses' => 'ShipFromController@delete']);
		// archive
	Route::post('/ship-from/bulk-change-status', ['as' => 'main.ship-from.bulk_change_status', 'uses' => 'ShipFromController@bulk_change_status']);

	//Rack
		//index
	Route::get('/rack', ['as' => 'main.rack.index', 'uses' => 'RackController@index']);
	Route::post('/rack/data', ['as' => 'main.rack.data', 'uses' => 'RackController@data']);
		//create
	Route::get('/rack/create', ['as' => 'main.rack.create', 'uses' => 'RackController@create']);
	Route::post('/rack/create', ['as' => 'main.rack.store', 'uses' => 'RackController@store']);
		//edit
	Route::get('/rack/edit/{id}', ['as' => 'main.rack.edit', 'uses' => 'RackController@edit']);
	Route::put('/rack/edit/{id}', ['as' => 'main.rack.update', 'uses' => 'RackController@update']);
		//delete
	Route::delete('/rack/delete/{id}', ['as' => 'main.rack.delete', 'uses' => 'RackController@delete']);
		// archive
	Route::post('/rack/bulk-change-status', ['as' => 'main.rack.bulk_change_status', 'uses' => 'RackController@bulk_change_status']);

	//Bay
		//index
	Route::get('/bay', ['as' => 'main.bay.index', 'uses' => 'BayController@index']);
	Route::post('/bay/data', ['as' => 'main.bay.data', 'uses' => 'BayController@data']);
		//create
	Route::get('/bay/create', ['as' => 'main.bay.create', 'uses' => 'BayController@create']);
	Route::post('/bay/create', ['as' => 'main.bay.store', 'uses' => 'BayController@store']);
		//edit
	Route::get('/bay/edit/{id}', ['as' => 'main.bay.edit', 'uses' => 'BayController@edit']);
	Route::put('/bay/edit/{id}', ['as' => 'main.bay.update', 'uses' => 'BayController@update']);
		//delete
	Route::delete('/bay/delete/{id}', ['as' => 'main.bay.delete', 'uses' => 'BayController@delete']);
		// archive
	Route::post('/bay/bulk-change-status', ['as' => 'main.bay.bulk_change_status', 'uses' => 'BayController@bulk_change_status']);

	//Pallet
		//index
	Route::get('/pallet', ['as' => 'main.pallet.index', 'uses' => 'PalletController@index']);
	Route::post('/pallet/data', ['as' => 'main.pallet.data', 'uses' => 'PalletController@data']);
		//create
	Route::get('/pallet/create', ['as' => 'main.pallet.create', 'uses' => 'PalletController@create']);
	Route::post('/pallet/create', ['as' => 'main.pallet.store', 'uses' => 'PalletController@store']);
		//edit
	Route::get('/pallet/edit/{id}', ['as' => 'main.pallet.edit', 'uses' => 'PalletController@edit']);
	Route::put('/pallet/edit/{id}', ['as' => 'main.pallet.update', 'uses' => 'PalletController@update']);
		//delete
	Route::delete('/pallet/delete/{id}', ['as' => 'main.pallet.delete', 'uses' => 'PalletController@delete']);
		// archive
	Route::post('/pallet/bulk-change-status', ['as' => 'main.pallet.bulk_change_status', 'uses' => 'PalletController@bulk_change_status']);

	//Container
	//index
	Route::get('/container', ['as' => 'main.container.index', 'uses' => 'ContainerController@index']);
	Route::post('/container/data', ['as' => 'main.container.data', 'uses' => 'ContainerController@data']);
		//create
	Route::get('/container/create', ['as' => 'main.container.create', 'uses' => 'ContainerController@create']);
	Route::post('/container/create', ['as' => 'main.container.store', 'uses' => 'ContainerController@store']);
		//edit
	Route::get('/container/edit/{id}', ['as' => 'main.container.edit', 'uses' => 'ContainerController@edit']);
	Route::put('/container/edit/{id}', ['as' => 'main.container.update', 'uses' => 'ContainerController@update']);
		//delete
	Route::delete('/container/delete/{id}', ['as' => 'main.container.delete', 'uses' => 'ContainerController@delete']);
		// archive
	Route::post('/container/bulk-change-status', ['as' => 'main.container.bulk_change_status', 'uses' => 'ContainerController@bulk_change_status']);

});


//Warehouse
Route::group(['prefix' => 'main/warehouse', 'middleware' => 'auth', 'namespace' => 'Main\Warehouse'], function()
{
	//Delivery Order
	// OPEN
	Route::get('/delivery-order/open', ['as' => 'main.delivery-order.open', 'uses' => 'DeliveryOrderController@index_open']);
	Route::get('/delivery-order/data-open', ['as' => 'main.delivery-order.data-open', 'uses' => 'DeliveryOrderController@data_open']);
	// data modal
	Route::post('/delivery-order/data-modal', ['as' => 'main.delivery-order.data-modal', 'uses' => 'DeliveryOrderController@data_modal']);
	//detail open
	Route::get('/delivery-order/open/edit/{id}', ['as' => 'main.delivery-order.open.edit', 'uses' => 'DeliveryOrderController@edit_open']);
	Route::put('/delivery-order/open/edit/{id}', ['as' => 'main.delivery-order.open.store', 'uses' => 'DeliveryOrderController@store_open']);
	// data modal
	Route::get('/delivery-order/data-modal', ['as' => 'main.delivery-order.data-modal', 'uses' => 'DeliveryOrderController@data_modal']);
	Route::post('/delivery-order/pick/{id}', ['as' => 'main.delivery-order.pick', 'uses' => 'DeliveryOrderController@pick']);

	// FORM
	//index
	Route::get('/delivery-order', ['as' => 'main.delivery-order.index', 'uses' => 'DeliveryOrderController@index']);
	Route::post('/delivery-order/data', ['as' => 'main.delivery-order.data', 'uses' => 'DeliveryOrderController@data']);
	Route::post('/delivery-order/eloquent/details-data/{id}', ['as' => 'main.delivery-order.details-data', 'uses' => 'DeliveryOrderController@getDetailsData']);
	//create
	Route::get('/delivery-order/create', ['as' => 'main.delivery-order.create', 'uses' => 'DeliveryOrderController@create']);
	Route::post('/delivery-order/create', ['as' => 'main.delivery-order.store', 'uses' => 'DeliveryOrderController@store']);
	//edit
	Route::get('/delivery-order/edit/{id}', ['as' => 'main.delivery-order.edit', 'uses' => 'DeliveryOrderController@edit']);
	Route::put('/delivery-order/edit/{id}', ['as' => 'main.delivery-order.update', 'uses' => 'DeliveryOrderController@update']);
	//delete
	Route::delete('/delivery-order/delete/{id}', ['as' => 'main.delivery-order.delete', 'uses' => 'DeliveryOrderController@delete']);
	// archive
	Route::post('/delivery-order/bulk-change-status', ['as' => 'main.delivery-order.bulk_change_status', 'uses' => 'DeliveryOrderController@bulk_change_status']);
	//get detail for API
	Route::get('/delivery-order/data-detail/{id}', ['as' => 'main.delivery-order.data-detail', 'uses' => 'DeliveryOrderController@data_detail']);
	// Detail Receive
		// get
	Route::post('/delivery-order/get-detail/{id}', ['as' => 'main.delivery-order.get-detail', 'uses' => 'DeliveryOrderController@get_detail']);
		// post
	Route::post('/delivery-order/post-detail/{id}', ['as' => 'main.delivery-order.post-detail', 'uses' => 'DeliveryOrderController@post_detail']);
		// put
	Route::put('/delivery-order/put-detail/{id}', ['as' => 'main.delivery-order.put-detail', 'uses' => 'DeliveryOrderController@put_detail']);
		// delete
	Route::delete('/delivery-order/delete-detail/{id}', ['as' => 'main.delivery-order.delete-detail', 'uses' => 'DeliveryOrderController@delete_detail']);
		// post SAP
  Route::post('/delivery-order/post-response/{id}', ['as' => 'main.delivery-order.post-response', 'uses' => 'DeliveryOrderController@post_response']);
    // post header
  Route::post('/delivery-order/post-response-header/{id}', ['as' => 'main.delivery-order.post-response-header', 'uses' => 'DeliveryOrderController@post_response_header']);
		// cancel
	Route::post('/delivery-order/cancel/{id}', ['as' => 'main.delivery-order.cancel', 'uses' => 'DeliveryOrderController@cancel']);
  // SLIP
  //	index
	Route::get('/delivery-order/slip/{id}', ['as' => 'main.delivery-order.slip', 'uses' => 'DeliveryOrderController@slip']);
	// REPORT
	//	index
	Route::get('/delivery-order/report', ['as' => 'main.delivery-order-report.index', 'uses' => 'DeliveryOrderController@report']);
	// data report
	Route::post('/delivery-order/data-report', ['as' => 'main.delivery-order.data-report', 'uses' => 'DeliveryOrderController@data_report']);
	// print report
	Route::get('/delivery-order/report/print/{id}/{id1}', ['as' => 'main.delivery-order.report-print', 'uses' => 'DeliveryOrderController@report_print']);
	// get po SAP
	Route::post('/delivery-order/sap-cancelgido', ['as' => 'main.delivery-order.sap-cancelgido', 'uses' => 'DeliveryOrderController@sap_cancelgido']);
	Route::post('/delivery-order/sap-exegido', ['as' => 'main.delivery-order.sap-exegido', 'uses' => 'DeliveryOrderController@sap_exegido']);
	Route::post('/delivery-order/sap-gido', ['as' => 'main.delivery-order.sap-gido', 'uses' => 'DeliveryOrderController@sap_gido']);
	Route::post('/delivery-order/sap-gido-new/{id}', ['as' => 'main.delivery-order.sap-gido-new', 'uses' => 'DeliveryOrderController@sap_gido_new']);

	//MaterialUsed -
		//index
	Route::get('/material-used', ['as' => 'main.material-used.index', 'uses' => 'MaterialUsedController@index']);
	Route::get('/material-used/data', ['as' => 'main.material-used.data', 'uses' => 'MaterialUsedController@data']);
		//create
	Route::get('/material-used/create', ['as' => 'main.material-used.create', 'uses' => 'MaterialUsedController@create']);
	Route::post('/material-used/create', ['as' => 'main.material-used.store', 'uses' => 'MaterialUsedController@store']);
		//edit
	Route::get('/material-used/edit/{id}', ['as' => 'main.material-used.edit', 'uses' => 'MaterialUsedController@edit']);
	Route::put('/material-used/edit/{id}', ['as' => 'main.material-used.update', 'uses' => 'MaterialUsedController@update']);
		//delete
	Route::delete('/material-used/delete/{id}', ['as' => 'main.material-used.delete', 'uses' => 'MaterialUsedController@delete']);
		// archive
	Route::post('/material-used/bulk-change-status', ['as' => 'main.material-used.bulk_change_status', 'uses' => 'MaterialUsedController@bulk_change_status']);
	// Detail Used
		// get
	Route::get('/material-used/get-detail/{id}', ['as' => 'main.material-used.get-detail', 'uses' => 'MaterialUsedController@get_detail']);
		// post
	Route::post('/material-used/post-detail/{id}', ['as' => 'main.material-used.post-detail', 'uses' => 'MaterialUsedController@post_detail']);
		// put
	Route::put('/material-used/put-detail/{id}', ['as' => 'main.material-used.put-detail', 'uses' => 'MaterialUsedController@put_detail']);
		// delete
	Route::delete('/material-used/delete-detail/{id}', ['as' => 'main.material-used.delete-detail', 'uses' => 'MaterialUsedController@delete_detail']);

	//DOReturn -
	//index
	Route::get('/gr-return', ['as' => 'main.gr-return.index', 'uses' => 'GRReturnController@index']);
	Route::post('/gr-return/data', ['as' => 'main.gr-return.data', 'uses' => 'GRReturnController@data']);
	//create
	Route::get('/gr-return/create', ['as' => 'main.gr-return.create', 'uses' => 'GRReturnController@create']);
	Route::post('/gr-return/create', ['as' => 'main.gr-return.store', 'uses' => 'GRReturnController@store']);
	//edit
	Route::get('/gr-return/edit/{id}', ['as' => 'main.gr-return.edit', 'uses' => 'GRReturnController@edit']);
	Route::put('/gr-return/edit/{id}', ['as' => 'main.gr-return.update', 'uses' => 'GRReturnController@update']);
	//delete
	Route::delete('/gr-return/delete/{id}', ['as' => 'main.gr-return.delete', 'uses' => 'GRReturnController@delete']);
	// archive
	Route::post('/gr-return/bulk-change-status', ['as' => 'main.gr-return.bulk_change_status', 'uses' => 'GRReturnController@bulk_change_status']);
	// SAP response
	Route::post('/gr-return/post-response/{id}', ['as' => 'main.gr-return.post-response', 'uses' => 'GRReturnController@post_response']);
	// Detail Used
	// get
	Route::post('/gr-return/get-detail/{id}', ['as' => 'main.gr-return.get-detail', 'uses' => 'GRReturnController@get_detail']);
	// post
	Route::post('/gr-return/post-detail/{id}', ['as' => 'main.gr-return.post-detail', 'uses' => 'GRReturnController@post_detail']);
	// put
	Route::put('/gr-return/put-detail/{id}', ['as' => 'main.gr-return.put-detail', 'uses' => 'GRReturnController@put_detail']);
	// delete
	Route::delete('/gr-return/delete-detail/{id}', ['as' => 'main.gr-return.delete-detail', 'uses' => 'GRReturnController@delete_detail']);
	//get detail for API
	Route::get('/gr-return/data-detail/{id}', ['as' => 'main.gr-return.data-detail', 'uses' => 'GRReturnController@data_detail']);
	//open item
	Route::post('/gr-return/data-modal', ['as' => 'main.gr-return.data-modal', 'uses' => 'GRReturnController@data_modal']);
	// SAP data
	Route::get('/gr-return/get-empty-nodo', ['as' => 'main.gr-return.get-empty-nodo', 'uses' => 'GRReturnController@get_empty_nodo']);
	Route::post('/gr-return/get-data-sap', ['as' => 'main.gr-return.get-data-sap', 'uses' => 'GRReturnController@get_data_sap']);
	Route::post('/gr-return/pick/{id}', ['as' => 'main.gr-return.pick', 'uses' => 'GRReturnController@pick']);
	// cancel
	Route::post('/gr-return/cancel/{id}', ['as' => 'main.gr-return.cancel', 'uses' => 'GRReturnController@cancel']);
  // get po SAP
  Route::post('/gr-return/sap-exe-tambah-item', ['as' => 'main.gr-return.sap-exe-tambah-item', 'uses' => 'GRReturnController@sap_exe_tambah_item']);
  Route::post('/gr-return/sap-tambahitem', ['as' => 'main.gr-return.sap-tambahitem', 'uses' => 'GRReturnController@sap_tambahitem']);
  Route::post('/gr-return/sap-returndo', ['as' => 'main.gr-return.sap-returndo', 'uses' => 'GRReturnController@sap_returndo']);

  //DOReturn Manual -
  //index
  Route::get('/gr-return-manual', ['as' => 'main.gr-return-manual.index', 'uses' => 'GRReturnManualController@index']);
  Route::post('/gr-return-manual/data', ['as' => 'main.gr-return-manual.data', 'uses' => 'GRReturnManualController@data']);
  //create
  Route::get('/gr-return-manual/create', ['as' => 'main.gr-return-manual.create', 'uses' => 'GRReturnManualController@create']);
  Route::post('/gr-return-manual/create', ['as' => 'main.gr-return-manual.store', 'uses' => 'GRReturnManualController@store']);
  Route::get('/gr-return-manual/data-detail/{id}', ['as' => 'main.gr-return-manual.data-detail', 'uses' => 'GRReturnManualController@data_detail']);
  //edit
  Route::get('/gr-return-manual/edit/{id}', ['as' => 'main.gr-return-manual.edit', 'uses' => 'GRReturnManualController@edit']);
  Route::put('/gr-return-manual/edit/{id}', ['as' => 'main.gr-return-manual.update', 'uses' => 'GRReturnManualController@update']);
  //delete
  Route::delete('/gr-return-manual/delete/{id}', ['as' => 'main.gr-return-manual.delete', 'uses' => 'GRReturnManualController@delete']);
  // archive
  Route::post('/gr-return-manual/bulk-change-status', ['as' => 'main.gr-return-manual.bulk_change_status', 'uses' => 'GRReturnManualController@bulk_change_status']);
   // Detail Used
  // get
  Route::post('/gr-return-manual/get-detail/{id}', ['as' => 'main.gr-return-manual.get-detail', 'uses' => 'GRReturnManualController@get_detail']);
  // post
  Route::post('/gr-return-manual/post-detail/{id}', ['as' => 'main.gr-return-manual.post-detail', 'uses' => 'GRReturnManualController@post_detail']);
  // put
  Route::put('/gr-return-manual/put-detail/{id}', ['as' => 'main.gr-return-manual.put-detail', 'uses' => 'GRReturnManualController@put_detail']);
  // delete
  Route::delete('/gr-return-manual/delete-detail/{id}', ['as' => 'main.gr-return-manual.delete-detail', 'uses' => 'GRReturnManualController@delete_detail']);
  // cancel
  Route::post('/gr-return-manual/cancel/{id}', ['as' => 'main.gr-return-manual.cancel', 'uses' => 'GRReturnManualController@cancel']);
  //open item
  Route::post('/gr-return-manual/data-modal', ['as' => 'main.gr-return-manual.data-modal', 'uses' => 'GRReturnManualController@data_modal']);
  // SAP data
  Route::get('/gr-return-manual/get-empty-nodo', ['as' => 'main.gr-return-manual.get-empty-nodo', 'uses' => 'GRReturnManualController@get_empty_nodo']);
  Route::post('/gr-return-manual/get-data-sap', ['as' => 'main.gr-return-manual.get-data-sap', 'uses' => 'GRReturnManualController@get_data_sap']);
  Route::post('/gr-return-manual/pick/{id}', ['as' => 'main.gr-return-manual.pick', 'uses' => 'GRReturnManualController@pick']);
  // cancel
  Route::post('/gr-return-manual/cancel/{id}', ['as' => 'main.gr-return-manual.cancel', 'uses' => 'GRReturnManualController@cancel']);
  // get po SAP
  Route::post('/gr-return-manual/sap-exe-tambah-item', ['as' => 'main.gr-return-manual.sap-exe-tambah-item', 'uses' => 'GRReturnManualController@sap_exe_tambah_item']);
  Route::post('/gr-return-manual/sap-tambahitem', ['as' => 'main.gr-return-manual.sap-tambahitem', 'uses' => 'GRReturnManualController@sap_tambahitem']);
  Route::post('/gr-return-manual/sap-returndo', ['as' => 'main.gr-return-manual.sap-returndo', 'uses' => 'GRReturnManualController@sap_returndo']);
  Route::post('/gr-return-manual/sap-cancelgido', ['as' => 'main.gr-return-manual.sap-cancelgido', 'uses' => 'GRReturnManualController@sap_cancelgido']);
  Route::post('/gr-return-manual/sap-cancelgr', ['as' => 'main.gr-return-manual.sap-cancelgr', 'uses' => 'GRReturnManualController@sap_cancelgr']);

	//StockTransfer
	//index
	Route::get('/po-revision', ['as' => 'main.po-revision.index', 'uses' => 'PoRevisionController@index']);
	Route::get('/po-revision/data', ['as' => 'main.po-revision.data', 'uses' => 'PoRevisionController@data']);
	//create
	Route::get('/po-revision/create', ['as' => 'main.po-revision.create', 'uses' => 'PoRevisionController@create']);
	Route::post('/po-revision/create', ['as' => 'main.po-revision.store', 'uses' => 'PoRevisionController@store']);
	//edit
	Route::get('/po-revision/edit/{id}', ['as' => 'main.po-revision.edit', 'uses' => 'PoRevisionController@edit']);
	Route::put('/po-revision/edit/{id}', ['as' => 'main.po-revision.update', 'uses' => 'PoRevisionController@update']);
	//delete
	Route::delete('/po-revision/delete/{id}', ['as' => 'main.po-revision.delete', 'uses' => 'PoRevisionController@delete']);
	// archive
	Route::post('/po-revision/bulk-change-status', ['as' => 'main.po-revision.bulk_change_status', 'uses' => 'PoRevisionController@bulk_change_status']);
	// Detail Revision
	// get
	Route::get('/po-revision/get-detail/{id}', ['as' => 'main.po-revision.get-detail', 'uses' => 'PoRevisionController@get_detail']);
	// post
	Route::post('/po-revision/post-detail/{id}', ['as' => 'main.po-revision.post-detail', 'uses' => 'PoRevisionController@post_detail']);
	// put
	Route::put('/po-revision/put-detail/{id}', ['as' => 'main.po-revision.put-detail', 'uses' => 'PoRevisionController@put_detail']);
	// delete
	Route::delete('/po-revision/delete-detail/{id}', ['as' => 'main.po-revision.delete-detail', 'uses' => 'PoRevisionController@delete_detail']);
	// REPORT
	//index
	Route::get('/po-revision/report', ['as' => 'main.po-revision-report.index', 'uses' => 'PoRevisionController@report']);

	//GRRevision
	//index
	Route::get('/gr-revision', ['as' => 'main.gr-revision.index', 'uses' => 'GrRevisionController@index']);
	Route::get('/gr-revision/data', ['as' => 'main.gr-revision.data', 'uses' => 'GrRevisionController@data']);
	//create
	Route::get('/gr-revision/create', ['as' => 'main.gr-revision.create', 'uses' => 'GrRevisionController@create']);
	Route::post('/gr-revision/create', ['as' => 'main.gr-revision.store', 'uses' => 'GrRevisionController@store']);
	//edit
	Route::get('/gr-revision/edit/{id}', ['as' => 'main.gr-revision.edit', 'uses' => 'GrRevisionController@edit']);
	Route::put('/gr-revision/edit/{id}', ['as' => 'main.gr-revision.update', 'uses' => 'GrRevisionController@update']);
	//delete
	Route::delete('/gr-revision/delete/{id}', ['as' => 'main.gr-revision.delete', 'uses' => 'GrRevisionController@delete']);
	// archive
	Route::post('/gr-revision/bulk-change-status', ['as' => 'main.gr-revision.bulk_change_status', 'uses' => 'GrRevisionController@bulk_change_status']);
	// Detail Revision
	// get
	Route::get('/gr-revision/get-detail/{id}', ['as' => 'main.gr-revision.get-detail', 'uses' => 'GrRevisionController@get_detail']);
	// post
	Route::post('/gr-revision/post-detail/{id}', ['as' => 'main.gr-revision.post-detail', 'uses' => 'GrRevisionController@post_detail']);
	// put
	Route::put('/gr-revision/put-detail/{id}', ['as' => 'main.gr-revision.put-detail', 'uses' => 'GrRevisionController@put_detail']);
	// delete
	Route::delete('/gr-revision/delete-detail/{id}', ['as' => 'main.gr-revision.delete-detail', 'uses' => 'GrRevisionController@delete_detail']);
	// REPORT
	//index
	Route::get('/gr-revision/report', ['as' => 'main.gr-revision-report.index', 'uses' => 'GrRevisionController@report']);
	// modal
	Route::post('/gr-revision/data-modal', ['as' => 'main.gr-revision.data-modal', 'uses' => 'GrRevisionController@data_modal']);

	//Goods Receive
	// OPEN
	//open
	Route::get('/goods-receive/open', ['as' => 'main.goods-receive.open', 'uses' => 'GoodsReceiveController@index_open']);
	Route::get('/goods-receive/data-open', ['as' => 'main.goods-receive.data-open', 'uses' => 'GoodsReceiveController@data_open']);
	//open item
	Route::get('/goods-receive/data-modal', ['as' => 'main.goods-receive.data-modal', 'uses' => 'GoodsReceiveController@data_modal']);
	Route::post('/goods-receive/pick/{id}', ['as' => 'main.goods-receive.pick', 'uses' => 'GoodsReceiveController@pick']);

	//detail open
	Route::get('/goods-receive/open/edit/{id}', ['as' => 'main.goods-receive.open.edit', 'uses' => 'GoodsReceiveController@edit_open']);
	Route::put('/goods-receive/open/edit/{id}', ['as' => 'main.goods-receive.open.store', 'uses' => 'GoodsReceiveController@store_open']);
	// data modal
	Route::post('/goods-receive/data-modal', ['as' => 'main.goods-receive.data-modal', 'uses' => 'GoodsReceiveController@data_modal']);
	// FORM
	//index
	Route::get('/goods-receive', ['as' => 'main.goods-receive.index', 'uses' => 'GoodsReceiveController@index']);
	Route::post('/goods-receive/data', ['as' => 'main.goods-receive.data', 'uses' => 'GoodsReceiveController@data']);
	//create
	Route::get('/goods-receive/create', ['as' => 'main.goods-receive.create', 'uses' => 'GoodsReceiveController@create']);
	Route::post('/goods-receive/create', ['as' => 'main.goods-receive.store', 'uses' => 'GoodsReceiveController@store']);
	//edit
	Route::get('/goods-receive/edit/{id}', ['as' => 'main.goods-receive.edit', 'uses' => 'GoodsReceiveController@edit']);
	Route::put('/goods-receive/edit/{id}', ['as' => 'main.goods-receive.update', 'uses' => 'GoodsReceiveController@update']);
	//delete
	Route::delete('/goods-receive/delete/{id}', ['as' => 'main.goods-receive.delete', 'uses' => 'GoodsReceiveController@delete']);
	// archive
	Route::post('/goods-receive/bulk-change-status', ['as' => 'main.goods-receive.bulk_change_status', 'uses' => 'GoodsReceiveController@bulk_change_status']);
	//get header for API
	Route::get('/goods-receive/data-header/{id}', ['as' => 'main.goods-receive.data-header', 'uses' => 'GoodsReceiveController@data_header']);
	//get detail for API
	Route::get('/goods-receive/data-detail/{id}', ['as' => 'main.goods-receive.data-detail', 'uses' => 'GoodsReceiveController@data_detail']);
	// SAP response
  Route::post('/goods-receive/post-response/{id}', ['as' => 'main.goods-receive.post-response', 'uses' => 'GoodsReceiveController@post_response']);
	Route::post('/goods-receive/post-response-detail/{id}', ['as' => 'main.goods-receive.post-response-detail', 'uses' => 'GoodsReceiveController@post_response_detail']);
  // post header only
  Route::post('/goods-receive/post-response-date/{id}', ['as' => 'main.goods-receive.post-response-date', 'uses' => 'GoodsReceiveController@post_response_date']);

	// cancel
	Route::post('/goods-receive/cancel/{id}', ['as' => 'main.goods-receive.cancel', 'uses' => 'GoodsReceiveController@cancel']);
	// Detail Receive
		// get
	Route::post('/goods-receive/get-detail/{id}', ['as' => 'main.goods-receive.get-detail', 'uses' => 'GoodsReceiveController@get_detail']);
		// post
	Route::post('/goods-receive/post-detail/{id}', ['as' => 'main.goods-receive.post-detail', 'uses' => 'GoodsReceiveController@post_detail']);
		// put
	Route::put('/goods-receive/put-detail/{id}', ['as' => 'main.goods-receive.put-detail', 'uses' => 'GoodsReceiveController@put_detail']);
		// delete
	Route::delete('/goods-receive/delete-detail/{id}', ['as' => 'main.goods-receive.delete-detail', 'uses' => 'GoodsReceiveController@delete_detail']);
	// REPORT
	//	index
	Route::get('/goods-receive/report', ['as' => 'main.goods-receive-report.index', 'uses' => 'GoodsReceiveController@report']);
	// data report
	Route::post('/goods-receive/data-report', ['as' => 'main.goods-receive.data-report', 'uses' => 'GoodsReceiveController@data_report']);
	Route::post('/goods-receive/post-po-revision', ['as' => 'main.goods-receive.post-po-revision', 'uses' => 'GoodsReceiveController@post_po_revision']);
	// print report
	Route::get('/goods-receive/report/print/{id}/{id1}', ['as' => 'main.goods-receive.report-print', 'uses' => 'GoodsReceiveController@report_print']);

	//	index
	Route::get('/goods-receive/report-outstanding', ['as' => 'main.goods-receive-report-outstanding.index', 'uses' => 'GoodsReceiveController@report_outstanding']);
	// data report
	Route::post('/goods-receive/data-report-outstanding', ['as' => 'main.goods-receive.data-report-outstanding', 'uses' => 'GoodsReceiveController@data_report_outstanding']);
	//	index
	Route::get('/goods-receive/report-outstanding-summary', ['as' => 'main.goods-receive-report-outstanding-summary.index', 'uses' => 'GoodsReceiveController@report_outstanding_summary']);
	// data report
	Route::post('/goods-receive/data-report-outstanding-summary', ['as' => 'main.goods-receive.data-report-outstanding-summary', 'uses' => 'GoodsReceiveController@data_report_outstanding_summary']);
	// get po sap
	Route::get('/goods-receive/get-po-sap/{id}', ['as' => 'main.goods-receive.get-po-sap', 'uses' => 'GoodsReceiveController@get_po_sap']);

  // get po sap
	Route::post('/goods-receive/sap-gr-new/{id}', ['as' => 'main.goods-receive.sap-gr-new', 'uses' => 'GoodsReceiveController@sap_gr_new']);
	Route::post('/goods-receive/sap-gr', ['as' => 'main.goods-receive.sap-gr', 'uses' => 'GoodsReceiveController@sap_gr']);
	Route::post('/goods-receive/sap-exegr', ['as' => 'main.goods-receive.sap-exegr', 'uses' => 'GoodsReceiveController@sap_exegr']);
	Route::post('/goods-receive/sap-cancelgr', ['as' => 'main.goods-receive.sap-cancelgr', 'uses' => 'GoodsReceiveController@sap_cancelgr']);
	
	// post data sap new
	Route::post('/goods-receive/post-data-sap-new/{id}', ['as' => 'main.goods-receive.post-data-sap-new', 'uses' => 'GoodsReceiveController@post_data_sap_new']);

  //Stock Adjustment
		//index
	Route::get('/stock-adjustment', ['as' => 'main.stock-adjustment.index', 'uses' => 'StockAdjustmentController@index']);
	Route::post('/stock-adjustment/data', ['as' => 'main.stock-adjustment.data', 'uses' => 'StockAdjustmentController@data']);
		//create
	Route::get('/stock-adjustment/create', ['as' => 'main.stock-adjustment.create', 'uses' => 'StockAdjustmentController@create']);
	Route::post('/stock-adjustment/create', ['as' => 'main.stock-adjustment.store', 'uses' => 'StockAdjustmentController@store']);
		//edit
	Route::get('/stock-adjustment/edit/{id}', ['as' => 'main.stock-adjustment.edit', 'uses' => 'StockAdjustmentController@edit']);
	Route::put('/stock-adjustment/edit/{id}', ['as' => 'main.stock-adjustment.update', 'uses' => 'StockAdjustmentController@update']);
		//delete
	Route::delete('/stock-adjustment/delete/{id}', ['as' => 'main.stock-adjustment.delete', 'uses' => 'StockAdjustmentController@delete']);
		// archive
	Route::post('/stock-adjustment/bulk-change-status', ['as' => 'main.stock-adjustment.bulk_change_status', 'uses' => 'StockAdjustmentController@bulk_change_status']);
  // SAP response
  Route::post('/stock-adjustment/post-response/{id}', ['as' => 'main.stock-adjustment.post-response', 'uses' => 'StockAdjustmentController@post_response']);
	Route::post('/stock-adjustment/save-header/{id}', ['as' => 'main.stock-adjustment.save-header', 'uses' => 'StockAdjustmentController@save_header']);
  // cancel
	Route::post('/stock-adjustment/cancel/{id}', ['as' => 'main.stock-adjustment.cancel', 'uses' => 'StockAdjustmentController@cancel']);
	// Detail Adjustment
		// get
	Route::post('/stock-adjustment/get-detail/{id}', ['as' => 'main.stock-adjustment.get-detail', 'uses' => 'StockAdjustmentController@get_detail']);
		// post
	Route::post('/stock-adjustment/post-detail/{id}', ['as' => 'main.stock-adjustment.post-detail', 'uses' => 'StockAdjustmentController@post_detail']);
		// put
	Route::put('/stock-adjustment/put-detail/{id}', ['as' => 'main.stock-adjustment.put-detail', 'uses' => 'StockAdjustmentController@put_detail']);
		// delete
	Route::delete('/stock-adjustment/delete-detail/{id}', ['as' => 'main.stock-adjustment.delete-detail', 'uses' => 'StockAdjustmentController@delete_detail']);
  //open item
	Route::post('/stock-adjustment/data-modal', ['as' => 'main.stock-adjustment.data-modal', 'uses' => 'StockAdjustmentController@data_modal']);
  //get detail for API
  Route::get('/stock-adjustment/data-detail/{id}', ['as' => 'main.stock-adjustment.data-detail', 'uses' => 'StockAdjustmentController@data_detail']);

  // post SAP
  Route::post('/stock-adjustment/sap-exeadjustminus', ['as' => 'main.stock-adjustment.sap-exeadjustminus', 'uses' => 'StockAdjustmentController@sap_exeadjustminus']);
  Route::post('/stock-adjustment/sap-adjustminus', ['as' => 'main.stock-adjustment.sap-adjustminus', 'uses' => 'StockAdjustmentController@sap_adjustminus']);
  Route::post('/stock-adjustment/sap-exeadjustplus', ['as' => 'main.stock-adjustment.sap-exeadjustplus', 'uses' => 'StockAdjustmentController@sap_exeadjustplus']);
  Route::post('/stock-adjustment/sap-adjustplus', ['as' => 'main.stock-adjustment.sap-adjustplus', 'uses' => 'StockAdjustmentController@sap_adjustplus']);

  //Stock Decreasing
		//index
	Route::get('/stock-decreasing', ['as' => 'main.stock-decreasing.index', 'uses' => 'StockDecreasingController@index']);
	Route::post('/stock-decreasing/data', ['as' => 'main.stock-decreasing.data', 'uses' => 'StockDecreasingController@data']);
		//create
	Route::get('/stock-decreasing/create', ['as' => 'main.stock-decreasing.create', 'uses' => 'StockDecreasingController@create']);
	Route::post('/stock-decreasing/create', ['as' => 'main.stock-decreasing.store', 'uses' => 'StockDecreasingController@store']);
		//edit
	Route::get('/stock-decreasing/edit/{id}', ['as' => 'main.stock-decreasing.edit', 'uses' => 'StockDecreasingController@edit']);
	Route::put('/stock-decreasing/edit/{id}', ['as' => 'main.stock-decreasing.update', 'uses' => 'StockDecreasingController@update']);
		//delete
	Route::delete('/stock-decreasing/delete/{id}', ['as' => 'main.stock-decreasing.delete', 'uses' => 'StockDecreasingController@delete']);
		// archive
	Route::post('/stock-decreasing/bulk-change-status', ['as' => 'main.stock-decreasing.bulk_change_status', 'uses' => 'StockDecreasingController@bulk_change_status']);
  // SAP response
  Route::post('/stock-decreasing/post-response/{id}', ['as' => 'main.stock-decreasing.post-response', 'uses' => 'StockDecreasingController@post_response']);
	Route::post('/stock-decreasing/save-header/{id}', ['as' => 'main.stock-decreasing.save-header', 'uses' => 'StockDecreasingController@save_header']);
  // cancel
	Route::post('/stock-decreasing/cancel/{id}', ['as' => 'main.stock-decreasing.cancel', 'uses' => 'StockDecreasingController@cancel']);


	// Detail Decreasing
		// get
	Route::post('/stock-decreasing/get-detail/{id}', ['as' => 'main.stock-decreasing.get-detail', 'uses' => 'StockDecreasingController@get_detail']);
		// post
	Route::post('/stock-decreasing/post-detail/{id}', ['as' => 'main.stock-decreasing.post-detail', 'uses' => 'StockDecreasingController@post_detail']);
		// put
	Route::put('/stock-decreasing/put-detail/{id}', ['as' => 'main.stock-decreasing.put-detail', 'uses' => 'StockDecreasingController@put_detail']);
		// delete
	Route::delete('/stock-decreasing/delete-detail/{id}', ['as' => 'main.stock-decreasing.delete-detail', 'uses' => 'StockDecreasingController@delete_detail']);
  //open item
	Route::post('/stock-decreasing/data-modal', ['as' => 'main.stock-decreasing.data-modal', 'uses' => 'StockDecreasingController@data_modal']);
  //get detail for API
  Route::get('/stock-decreasing/data-detail/{id}', ['as' => 'main.stock-decreasing.data-detail', 'uses' => 'StockDecreasingController@data_detail']);

  // post SAP
  Route::post('/stock-decreasing/sap-exesusut', ['as' => 'main.stock-decreasing.sap-exesusut', 'uses' => 'StockDecreasingController@sap_exesusut']);
  Route::post('/stock-decreasing/sap-susut', ['as' => 'main.stock-decreasing.sap-susut', 'uses' => 'StockDecreasingController@sap_susut']);

	//Stock Opname
		//index
	Route::get('/stock-opname', ['as' => 'main.stock-opname.index', 'uses' => 'StockOpnameController@index']);
	Route::post('/stock-opname/data', ['as' => 'main.stock-opname.data', 'uses' => 'StockOpnameController@data']);
		//create
	Route::get('/stock-opname/create', ['as' => 'main.stock-opname.create', 'uses' => 'StockOpnameController@create']);
	Route::post('/stock-opname/create', ['as' => 'main.stock-opname.store', 'uses' => 'StockOpnameController@store']);
		//edit
	Route::get('/stock-opname/edit/{id}', ['as' => 'main.stock-opname.edit', 'uses' => 'StockOpnameController@edit']);
	Route::put('/stock-opname/edit/{id}', ['as' => 'main.stock-opname.update', 'uses' => 'StockOpnameController@update']);
		//delete
	Route::delete('/stock-opname/delete/{id}', ['as' => 'main.stock-opname.delete', 'uses' => 'StockOpnameController@delete']);
		// archive
	Route::post('/stock-opname/bulk-change-status', ['as' => 'main.stock-opname.bulk_change_status', 'uses' => 'StockOpnameController@bulk_change_status']);
		//post stock
  	Route::post('/stock-opname/stock', ['as' => 'main.stock-opname.store-stock', 'uses' => 'StockOpnameController@store_stock']);
	Route::post('/stock-opname/generate/{id}', ['as' => 'main.stock-opname.generate', 'uses' => 'StockOpnameController@generate']);
	// Detail Opname
		// get
	Route::post('/stock-opname/get-detail/{id}', ['as' => 'main.stock-opname.get-detail', 'uses' => 'StockOpnameController@get_detail']);
		// post
	Route::post('/stock-opname/post-detail/{id}', ['as' => 'main.stock-opname.post-detail', 'uses' => 'StockOpnameController@post_detail']);
		// put
	Route::put('/stock-opname/put-detail/{id}', ['as' => 'main.stock-opname.put-detail', 'uses' => 'StockOpnameController@put_detail']);
		// delete
	Route::delete('/stock-opname/delete-detail/{id}', ['as' => 'main.stock-opname.delete-detail', 'uses' => 'StockOpnameController@delete_detail']);
		// approve or not approve
	Route::post('/stock-opname/approve-detail/{id}', ['as' => 'main.stock-opname.approve-detail', 'uses' => 'StockOpnameController@approve_detail']);
	Route::post('/stock-opname/not-approve-detail/{id}', ['as' => 'main.stock-opname.not-approve-detail', 'uses' => 'StockOpnameController@not_approve_detail']);
	Route::post('/stock-opname/restore-detail/{id}', ['as' => 'main.stock-opname.restore-detail', 'uses' => 'StockOpnameController@restore_detail']);
		//edit
	Route::get('/stock-opname/edit-detail/{id}', ['as' => 'main.stock-opname.edit-detail', 'uses' => 'StockOpnameController@edit_detail']);
	// post to SAP
	Route::post('/stock-opname/sap-stockopname', ['as' => 'main.stock-opname.sap-stockopname', 'uses' => 'StockOpnameController@sap_stockopname']);
	Route::post('/stock-opname/sap-exestockopname', ['as' => 'main.stock-opname.sap-exestockopname', 'uses' => 'StockOpnameController@sap_exestockopname']);



  //Stock OpnameCollect
		//index
	Route::get('/stock-opname-collect', ['as' => 'main.stock-opname-collect.index', 'uses' => 'StockOpnameCollectController@index']);
	Route::post('/stock-opname-collect/data', ['as' => 'main.stock-opname-collect.data', 'uses' => 'StockOpnameCollectController@data']);
		//create
	Route::get('/stock-opname-collect/create', ['as' => 'main.stock-opname-collect.create', 'uses' => 'StockOpnameCollectController@create']);
	Route::post('/stock-opname-collect/create', ['as' => 'main.stock-opname-collect.store', 'uses' => 'StockOpnameCollectController@store']);
		//edit
	Route::get('/stock-opname-collect/edit/{id}', ['as' => 'main.stock-opname-collect.edit', 'uses' => 'StockOpnameCollectController@edit']);
	Route::put('/stock-opname-collect/edit/{id}', ['as' => 'main.stock-opname-collect.update', 'uses' => 'StockOpnameCollectController@update']);
		//delete
	Route::delete('/stock-opname-collect/delete/{id}', ['as' => 'main.stock-opname-collect.delete', 'uses' => 'StockOpnameCollectController@delete']);
		// archive
	Route::post('/stock-opname-collect/bulk-change-status', ['as' => 'main.stock-opname-collect.bulk_change_status', 'uses' => 'StockOpnameCollectController@bulk_change_status']);
	// SAP response
	Route::post('/stock-opname-collect/post-response/{id}', ['as' => 'main.stock-opname-collect.post-response', 'uses' => 'StockOpnameCollectController@post_response']);
	Route::post('/stock-opname-collect/save-header/{id}', ['as' => 'main.stock-opname-collect.save-header', 'uses' => 'StockOpnameCollectController@save_header']);
	// cancel
	Route::post('/stock-opname-collect/cancel/{id}', ['as' => 'main.stock-opname-collect.cancel', 'uses' => 'StockOpnameCollectController@cancel']);

	// Detail OpnameCollect
		// get
	Route::post('/stock-opname-collect/get-detail/{id}', ['as' => 'main.stock-opname-collect.get-detail', 'uses' => 'StockOpnameCollectController@get_detail']);
		// post
	Route::post('/stock-opname-collect/post-detail/{id}', ['as' => 'main.stock-opname-collect.post-detail', 'uses' => 'StockOpnameCollectController@post_detail']);
		// put
	Route::put('/stock-opname-collect/put-detail/{id}', ['as' => 'main.stock-opname-collect.put-detail', 'uses' => 'StockOpnameCollectController@put_detail']);
		// delete
	Route::delete('/stock-opname-collect/delete-detail/{id}', ['as' => 'main.stock-opname-collect.delete-detail', 'uses' => 'StockOpnameCollectController@delete_detail']);
	//open item
	Route::post('/stock-opname-collect/data-modal', ['as' => 'main.stock-opname-collect.data-modal', 'uses' => 'StockOpnameCollectController@data_modal']);
	Route::post('/stock-opname-collect/data-modal-item', ['as' => 'main.stock-opname-collect.data-modal-item', 'uses' => 'StockOpnameCollectController@data_modal_item']);
	//get detail for API
	Route::get('/stock-opname-collect/data-detail/{id}', ['as' => 'main.stock-opname-collect.data-detail', 'uses' => 'StockOpnameCollectController@data_detail']);
	// post SAP
	Route::post('/stock-opname-collect/sap-so-new/{id}', ['as' => 'main.stock-opname-collect.sap-so-new', 'uses' => 'StockOpnameCollectController@sap_so_new']);

//Stock Transfer
		//index
	Route::get('/stock-transfer', ['as' => 'main.stock-transfer.index', 'uses' => 'StockTransferController@index']);
	Route::get('/stock-transfer/data', ['as' => 'main.stock-transfer.data', 'uses' => 'StockTransferController@data']);
		//create
	Route::get('/stock-transfer/create', ['as' => 'main.stock-transfer.create', 'uses' => 'StockTransferController@create']);
	Route::post('/stock-transfer/create', ['as' => 'main.stock-transfer.store', 'uses' => 'StockTransferController@store']);
		//edit
	Route::get('/stock-transfer/edit/{id}', ['as' => 'main.stock-transfer.edit', 'uses' => 'StockTransferController@edit']);
	Route::put('/stock-transfer/edit/{id}', ['as' => 'main.stock-transfer.update', 'uses' => 'StockTransferController@update']);
		//delete
	Route::delete('/stock-transfer/delete/{id}', ['as' => 'main.stock-transfer.delete', 'uses' => 'StockTransferController@delete']);
		// archive
	Route::post('/stock-transfer/bulk-change-status', ['as' => 'main.stock-transfer.bulk_change_status', 'uses' => 'StockTransferController@bulk_change_status']);

	// Detail Transfer
		// get
	Route::get('/stock-transfer/get-detail/{id}', ['as' => 'main.stock-transfer.get-detail', 'uses' => 'StockTransferController@get_detail']);
		// post
	Route::post('/stock-transfer/post-detail/{id}', ['as' => 'main.stock-transfer.post-detail', 'uses' => 'StockTransferController@post_detail']);
		// put
	Route::put('/stock-transfer/put-detail/{id}', ['as' => 'main.stock-transfer.put-detail', 'uses' => 'StockTransferController@put_detail']);
		// delete
	Route::delete('/stock-transfer/delete-detail/{id}', ['as' => 'main.stock-transfer.delete-detail', 'uses' => 'StockTransferController@delete_detail']);

	//Maintenance Flag
	//index
	Route::get('/maintenance-flag', ['as' => 'main.maintenance-flag.index', 'uses' => 'MaintenanceFlagController@index']);
	Route::post('/maintenance-flag/data', ['as' => 'main.maintenance-flag.data', 'uses' => 'MaintenanceFlagController@data']);
		//create
	Route::get('/maintenance-flag/create', ['as' => 'main.maintenance-flag.create', 'uses' => 'MaintenanceFlagController@create']);
	Route::post('/maintenance-flag/create', ['as' => 'main.maintenance-flag.store', 'uses' => 'MaintenanceFlagController@store']);
		//edit
	Route::get('/maintenance-flag/edit/{id}', ['as' => 'main.maintenance-flag.edit', 'uses' => 'MaintenanceFlagController@edit']);
	Route::put('/maintenance-flag/edit/{id}', ['as' => 'main.maintenance-flag.update', 'uses' => 'MaintenanceFlagController@update']);
		//delete
	Route::delete('/maintenance-flag/delete/{id}', ['as' => 'main.maintenance-flag.delete', 'uses' => 'MaintenanceFlagController@delete']);
	Route::delete('/maintenance-flag/delete-tr', ['as' => 'main.maintenance-flag.delete-tr', 'uses' => 'MaintenanceFlagController@delete_tr']);
		// get transaction data
	Route::post('/maintenance-flag/get-transaction-data', ['as' => 'main.maintenance-flag.get-transaction-data', 'uses' => 'MaintenanceFlagController@get_transaction_data']);
		// add data from stock
	Route::post('/maintenance-flag/add-data', ['as' => 'main.maintenance-flag.add-data', 'uses' => 'MaintenanceFlagController@add_data']);
		//open item
	Route::post('/maintenance-flag/data-modal', ['as' => 'main.maintenance-flag.data-modal', 'uses' => 'MaintenanceFlagController@data_modal']);

	//Report Stock
	//stock ending
	Route::get('/report-stock', ['as' => 'main.report-stock.index', 'uses' => 'ReportStockController@index']);
	Route::post('/report-stock/data', ['as' => 'main.report-stock.data', 'uses' => 'ReportStockController@data']);

	// Stock Backup
	Route::get('/stock-backup', ['as' => 'main.stock-backup.index', 'uses' => 'StockBackupController@index']);
	Route::get('/export-stock', ['as' => 'main.export-stock.index', 'uses' => 'StockBackupController@export_stock']);
	Route::get('/delete-stock/{id}', ['as' => 'main.stock-backup.delete_stock', 'uses' => 'StockBackupController@delete_stock']);

	//stock ending
	Route::get('/report-stock-my-fruit', ['as' => 'main.report-stock-my-fruit.index', 'uses' => 'ReportStockMyFruitController@index']);
	Route::post('/report-stock-my-fruit/data', ['as' => 'main.report-stock-my-fruit.data', 'uses' => 'ReportStockMyFruitController@data']);
	// get data sap my fruit
	Route::get('/get-my-fruit', ['as' => 'main.get-my-fruit.index', 'uses' => 'ReportStockMyFruitController@get_my_fruit']);

	//stock movement
	Route::get('/report-stock-movement', ['as' => 'main.report-stock-movement.index', 'uses' => 'ReportStockController@index_movement']);
	Route::post('/report-stock-movement/data', ['as' => 'main.report-stock-movement.data', 'uses' => 'ReportStockController@data_movement']);

	//stock ledger
	Route::get('/report-stock-ledger', ['as' => 'main.report-stock-ledger.index', 'uses' => 'ReportStockController@index_ledger']);
	Route::post('/report-stock-ledger/data', ['as' => 'main.report-stock-ledger.data', 'uses' => 'ReportStockController@data_ledger']);

  //stock ledger
  Route::get('/report-stock-ledger-picking', ['as' => 'main.report-stock-ledger-picking.index', 'uses' => 'ReportStockController@index_ledger_picking']);
  Route::post('/report-stock-ledger-picking/data', ['as' => 'main.report-stock-ledger-picking.data', 'uses' => 'ReportStockController@data_ledger_picking']);

  //stock ending summary
  Route::get('/report-stock-summary', ['as' => 'main.report-stock-summary.index', 'uses' => 'ReportStockSummaryController@index']);
  Route::post('/report-stock-summary/data', ['as' => 'main.report-stock-summary.data', 'uses' => 'ReportStockSummaryController@data']);

  //stock ending warehouse summary
  Route::get('/report-stock-warehouse-summary', ['as' => 'main.report-stock-warehouse-summary.index', 'uses' => 'ReportStockSummaryController@index_warehouse']);
  Route::post('/report-stock-warehouse-summary/data', ['as' => 'main.report-stock-warehouse-summary.data', 'uses' => 'ReportStockSummaryController@data_warehouse']);

  //stock movement summary
  Route::get('/report-stock-movement-summary', ['as' => 'main.report-stock-movement-summary.index', 'uses' => 'ReportStockSummaryController@index_movement']);
  Route::post('/report-stock-movement-summary/data', ['as' => 'main.report-stock-movement-summary.data', 'uses' => 'ReportStockSummaryController@data_movement']);

  //Report Stock Branch
  //stock ending
  Route::get('/report-stock-branch', ['as' => 'main.report-stock-branch.index', 'uses' => 'ReportStockBranchController@index']);
  Route::get('/report-stock-branch/data', ['as' => 'main.report-stock-branch.data', 'uses' => 'ReportStockBranchController@data']);

  //stock-branch movement
  Route::get('/report-stock-branch-movement', ['as' => 'main.report-stock-branch-movement.index', 'uses' => 'ReportStockBranchController@index_movement']);
  Route::post('/report-stock-branch-movement/data', ['as' => 'main.report-stock-branch-movement.data', 'uses' => 'ReportStockBranchController@data_movement']);

  //stock-branch ledger
  Route::get('/report-stock-branch-ledger', ['as' => 'main.report-stock-branch-ledger.index', 'uses' => 'ReportStockBranchController@index_ledger']);
  Route::post('/report-stock-branch-ledger/data', ['as' => 'main.report-stock-branch-ledger.data', 'uses' => 'ReportStockBranchController@data_ledger']);

  //Report Stock Picking
  //stock ending
  Route::get('/report-stock-picking', ['as' => 'main.report-stock-picking.index', 'uses' => 'ReportStockPickingController@index']);
  Route::get('/report-stock-picking/data', ['as' => 'main.report-stock-picking.data', 'uses' => 'ReportStockPickingController@data']);

  //stock-picking movement
  Route::get('/report-stock-picking-movement', ['as' => 'main.report-stock-picking-movement.index', 'uses' => 'ReportStockPickingController@index_movement']);
  Route::post('/report-stock-picking-movement/data', ['as' => 'main.report-stock-picking-movement.data', 'uses' => 'ReportStockPickingController@data_movement']);

  //stock-picking ledger
  Route::get('/report-stock-picking-ledger', ['as' => 'main.report-stock-picking-ledger.index', 'uses' => 'ReportStockPickingController@index_ledger']);
  Route::post('/report-stock-picking-ledger/data', ['as' => 'main.report-stock-picking-ledger.data', 'uses' => 'ReportStockPickingController@data_ledger']);

  //stock movement container
  Route::get('/report-stock-movement-container', ['as' => 'main.report-stock-movement-container.index', 'uses' => 'ReportStockContainerController@index_movement']);
  Route::post('/report-stock-movement-container/data', ['as' => 'main.report-stock-movement-container.data', 'uses' => 'ReportStockContainerController@data_movement']);

  //Report Stock Opname Raw
  //stock ending
  Route::get('/report-stock-opname-raw', ['as' => 'main.report-stock-opname-raw.index', 'uses' => 'ReportStockOpnameRawController@index']);
  Route::post('/report-stock-opname-raw/data', ['as' => 'main.report-stock-opname-raw.data', 'uses' => 'ReportStockOpnameRawController@data']);

	//PostingTransfer -
	//index
	Route::get('/posting-transfer', ['as' => 'main.posting-transfer.index', 'uses' => 'PostingTransferController@index']);
	Route::post('/posting-transfer/data', ['as' => 'main.posting-transfer.data', 'uses' => 'PostingTransferController@data']);
	//create
	Route::get('/posting-transfer/create', ['as' => 'main.posting-transfer.create', 'uses' => 'PostingTransferController@create']);
	Route::post('/posting-transfer/create', ['as' => 'main.posting-transfer.store', 'uses' => 'PostingTransferController@store']);
	//edit
	Route::get('/posting-transfer/edit/{id}', ['as' => 'main.posting-transfer.edit', 'uses' => 'PostingTransferController@edit']);
	Route::put('/posting-transfer/edit/{id}', ['as' => 'main.posting-transfer.update', 'uses' => 'PostingTransferController@update']);
	//delete
	Route::delete('/posting-transfer/delete/{id}', ['as' => 'main.posting-transfer.delete', 'uses' => 'PostingTransferController@delete']);
	// archive
	Route::post('/posting-transfer/bulk-change-status', ['as' => 'main.posting-transfer.bulk_change_status', 'uses' => 'PostingTransferController@bulk_change_status']);
	// SAP response
  Route::post('/posting-transfer/post-response/{id}', ['as' => 'main.posting-transfer.post-response', 'uses' => 'PostingTransferController@post_response']);
	Route::post('/posting-transfer/save-header/{id}', ['as' => 'main.posting-transfer.save-header', 'uses' => 'PostingTransferController@save_header']);
	// Detail Used
	// get
	Route::post('/posting-transfer/get-detail/{id}', ['as' => 'main.posting-transfer.get-detail', 'uses' => 'PostingTransferController@get_detail']);
	// post
	Route::post('/posting-transfer/post-detail/{id}', ['as' => 'main.posting-transfer.post-detail', 'uses' => 'PostingTransferController@post_detail']);
	// put
	Route::put('/posting-transfer/put-detail/{id}', ['as' => 'main.posting-transfer.put-detail', 'uses' => 'PostingTransferController@put_detail']);
	// delete
	Route::delete('/posting-transfer/delete-detail/{id}', ['as' => 'main.posting-transfer.delete-detail', 'uses' => 'PostingTransferController@delete_detail']);
	//get detail for API
	Route::get('/posting-transfer/data-detail/{id}', ['as' => 'main.posting-transfer.data-detail', 'uses' => 'PostingTransferController@data_detail']);
	//open item
	Route::post('/posting-transfer/data-modal', ['as' => 'main.posting-transfer.data-modal', 'uses' => 'PostingTransferController@data_modal']);
	// SAP data
	Route::get('/posting-transfer/get-empty-nodo', ['as' => 'main.posting-transfer.get-empty-nodo', 'uses' => 'PostingTransferController@get_empty_nodo']);
	Route::post('/posting-transfer/get-data-sap', ['as' => 'main.posting-transfer.get-data-sap', 'uses' => 'PostingTransferController@get_data_sap']);
	Route::post('/posting-transfer/pick/{id}', ['as' => 'main.posting-transfer.pick', 'uses' => 'PostingTransferController@pick']);
	// cancel
	Route::post('/posting-transfer/cancel/{id}', ['as' => 'main.posting-transfer.cancel', 'uses' => 'PostingTransferController@cancel']);

  // post SAP
  Route::post('/posting-transfer/sap-exetrfposting', ['as' => 'main.posting-transfer.sap-exetrfposting', 'uses' => 'PostingTransferController@sap_exetrfposting']);
  Route::post('/posting-transfer/sap-trfposting', ['as' => 'main.posting-transfer.sap-trfposting', 'uses' => 'PostingTransferController@sap_trfposting']);

	//District
	//index
	Route::get('/data-stream-layout', ['as' => 'main.data-stream-layout.index', 'uses' => 'DataStreamLayoutController@index']);
	Route::post('/data-stream-layout-detail', ['as' => 'main.data-stream-layout-detail.index', 'uses' => 'DataStreamLayoutController@detail']);

	Route::get('/data-stream-layout/data', ['as' => 'main.data-stream-layout.data', 'uses' => 'DataStreamLayoutController@data']);
	Route::post('/data-stream-layout/data-detail', ['as' => 'main.data-stream-layout.data_detail', 'uses' => 'DataStreamLayoutController@data_detail']);

	//Stock Adjustment
	//index
	Route::get('/test-api', ['as' => 'main.test-api.index', 'uses' => 'TestAPIController@index']);
});


//Ledger
Route::group(['prefix' => 'main/ledger', 'middleware' => 'auth', 'namespace' => 'Main\Ledger'], function()
{

	 //Account
		//index
	Route::get('/account', ['as' => 'main.account.index', 'uses' => 'AccountController@index']);
	Route::get('/account/data', ['as' => 'main.account.data', 'uses' => 'AccountController@data']);
		//create
	Route::get('/account/create', ['as' => 'main.account.create', 'uses' => 'AccountController@create']);
	Route::post('/account/create', ['as' => 'main.account.store', 'uses' => 'AccountController@store']);
		//edit
	Route::get('/account/edit/{id}', ['as' => 'main.account.edit', 'uses' => 'AccountController@edit']);
	Route::put('/account/edit/{id}', ['as' => 'main.account.update', 'uses' => 'AccountController@update']);
		//delete
	Route::delete('/account/delete/{id}', ['as' => 'main.account.delete', 'uses' => 'AccountController@delete']);
		// archive
	Route::post('/account/bulk-change-status', ['as' => 'main.account.bulk_change_status', 'uses' => 'AccountController@bulk_change_status']);

});

//Setting
Route::group(['prefix' => 'main/rms', 'middleware' => 'auth', 'namespace' => 'Main\Rms'], function()
{
	//Module
		//index
	Route::get('/module', ['as' => 'main.module.index', 'uses' => 'ModuleController@index']);
	Route::get('/module/data', ['as' => 'main.module.data', 'uses' => 'ModuleController@data']);
		//create
	Route::get('/module/create', ['as' => 'main.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['as' => 'main.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/edit/{id}', ['as' => 'main.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/edit/{id}', ['as' => 'main.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/delete/{id}', ['as' => 'main.module.delete', 'uses' => 'ModuleController@delete']);
		// archive
	Route::post('/module/bulk-change-status', ['as' => 'main.module.bulk_change_status', 'uses' => 'ModuleController@bulk_change_status']);


	//Company
		//index
	Route::get('/company', ['as' => 'main.company.index', 'uses' => 'CompanyController@index']);
	Route::post('/company/data', ['as' => 'main.company.data', 'uses' => 'CompanyController@data']);
		//create
	Route::get('/company/create', ['as' => 'main.company.create', 'uses' => 'CompanyController@create']);
	Route::post('/company/create', ['as' => 'main.company.store', 'uses' => 'CompanyController@store']);
		//edit
	Route::get('/company/edit/{id}', ['as' => 'main.company.edit', 'uses' => 'CompanyController@edit']);
	Route::put('/company/edit/{id}', ['as' => 'main.company.update', 'uses' => 'CompanyController@update']);
		//delete
	Route::delete('/company/delete/{id}', ['as' => 'main.company.delete', 'uses' => 'CompanyController@delete']);
		// archive
	Route::post('/company/bulk-change-status', ['as' => 'main.company.bulk_change_status', 'uses' => 'CompanyController@bulk_change_status']);

	//Role
		//index
	Route::get('/role', ['as' => 'main.role.index', 'uses' => 'RoleController@index']);
	Route::post('/role/data', ['as' => 'main.role.data', 'uses' => 'RoleController@data']);
		//create
	Route::get('/role/create', ['as' => 'main.role.create', 'uses' => 'RoleController@create']);
	Route::post('/role/create', ['as' => 'main.role.store', 'uses' => 'RoleController@store']);
		//edit
	Route::get('/role/edit/{id}', ['as' => 'main.role.edit', 'uses' => 'RoleController@edit']);
	Route::put('/role/edit/{id}', ['as' => 'main.role.update', 'uses' => 'RoleController@update']);
		//delete
	Route::delete('/role/delete/{id}', ['as' => 'main.role.delete', 'uses' => 'RoleController@delete']);
		// archive
	Route::post('/role/bulk-change-status', ['as' => 'main.role.bulk_change_status', 'uses' => 'RoleController@bulk_change_status']);

	//Role Permission
		//index
	Route::get('/role-permission', ['as' => 'main.role-permission.index', 'uses' => 'RolePermissionController@index']);
	Route::post('/role-permission/data', ['as' => 'main.role-permission.data', 'uses' => 'RolePermissionController@data']);
		//create
	Route::get('/role-permission/create', ['as' => 'main.role-permission.create', 'uses' => 'RolePermissionController@create']);
	Route::post('/role-permission/create', ['as' => 'main.role-permission.store', 'uses' => 'RolePermissionController@store']);
		//edit
	Route::get('/role-permission/{id}/edit', ['as' => 'main.role-permission.edit', 'uses' => 'RolePermissionController@edit']);
	Route::put('/role-permission/{id}/edit', ['as' => 'main.role-permission.update', 'uses' => 'RolePermissionController@update']);
	 	//delete
	Route::delete('/role-permission/delete/{id}', ['as' => 'main.role-permission.delete', 'uses' => 'RolePermissionController@delete']);
		// archive
	Route::post('/role-permission/bulk-change-status', ['as' => 'main.role-permission.bulk_change_status', 'uses' => 'RolePermissionController@bulk_change_status']);

	//User
	//index
	Route::get('/user', ['as' => 'main.user.index', 'uses' => 'UserController@index']);
	Route::post('/user/data', ['as' => 'main.user.data', 'uses' => 'UserController@data']);
		//create
	Route::get('/user/create', ['as' => 'main.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['as' => 'main.user.store', 'uses' => 'UserController@store']);
		//edit
	Route::get('/user/edit/{id}', ['as' => 'main.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/edit/{id}', ['as' => 'main.user.update', 'uses' => 'UserController@update']);
		//edit
	Route::get('/user/edit-password/{id}', ['as' => 'main.user.edit-password', 'uses' => 'UserController@edit_password']);
	Route::put('/user/edit-password/{id}', ['as' => 'main.user.update-password', 'uses' => 'UserController@update_password']);
		//delete
	Route::delete('/user/delete/{id}', ['as' => 'main.user.delete', 'uses' => 'UserController@delete']);
		// archive
	Route::post('/user/bulk-change-status', ['as' => 'main.user.bulk_change_status', 'uses' => 'UserController@bulk_change_status']);

	//User
	//index
	Route::get('/user-log', ['as' => 'main.user-log.index', 'uses' => 'UserLogController@index']);
	//data
	Route::post('/user-log/data', ['as' => 'main.user-log.data', 'uses' => 'UserLogController@data']);

});

//Preference
Route::group(['prefix' => 'main/preference', 'middleware' => 'auth', 'namespace' => 'Main\Preference'], function()
{
  //Preference
	//index
  Route::get('/preference', ['as' => 'main.preference.index', 'uses' => 'PreferenceController@index']);
	Route::get('/preference/backup-database', ['as' => 'main.preference.backup_database', 'uses' => 'PreferenceController@backup_database']);
  // update
  Route::put('/preference/update/{id}', ['as' => 'main.preference.update', 'uses' => 'PreferenceController@update']);
});

//API
Route::group(['prefix' => 'api', 'namespace' => 'API'], function()
{
	// Auth
	//login
	Route::post('auth/login', ['middleware' => 'cors', 'as' => 'api.auth.login', 'uses' => 'AuthController@login']);

  //login
	Route::post('auth/change-password', ['middleware' => 'cors', 'as' => 'api.auth.change-password', 'uses' => 'AuthController@change_password']);

	// Rack
	//get rack
	Route::get('rack/index', ['middleware' => 'cors', 'as' => 'api.rack.index', 'uses' => 'RackController@index'])->middleware('auth.apikey');

	// Warehouse
	Route::get('warehouse/index', ['middleware' => 'cors', 'as' => 'api.warehouse.index', 'uses' => 'WarehouseController@index'])->middleware('auth.apikey');

	// Stock
	// pallet
	Route::get('stock/get-pallet/{id}', ['middleware' => 'cors', 'as' => 'api.stock.get-pallet', 'uses' => 'StockController@get_pallet'])->middleware('auth.apikey');
	Route::get('stock/get-rack/{id}', ['middleware' => 'cors', 'as' => 'api.stock.get-rack', 'uses' => 'StockController@get_rack'])->middleware('auth.apikey');
	Route::post('stock/get-empty-rack', ['middleware' => 'cors', 'as' => 'api.stock.get-empty-rack', 'uses' => 'StockController@get_empty_rack'])->middleware('auth.apikey');
	Route::post('stock/get-empty-rack-dummy', ['middleware' => 'cors', 'as' => 'api.stock.get-empty-rack-dummy', 'uses' => 'StockController@get_empty_rack_dummy'])->middleware('auth.apikey');

	// Realtime Stock
	// index
	Route::get('realtime-stock/index', ['middleware' => 'cors', 'as' => 'api.realtime-stock.index', 'uses' => 'RealtimeStockController@index']);
	Route::post('realtime-stock/search', ['middleware' => 'cors', 'as' => 'api.realtime-stock.search', 'uses' => 'RealtimeStockController@search_manual']);
	Route::post('realtime-stock/search/{id}', ['middleware' => 'cors', 'as' => 'api.realtime-stock.search', 'uses' => 'RealtimeStockController@search_id']);

	// Inventory
	//search inventory
	Route::post('inventory/get-inventory-search', ['as' => 'api.inventory.get-polist-search', 'uses' => 'InventoryController@get_inventory_search'])->middleware('auth.apikey');
	//post submit data inventory receive
	Route::post('inventory/create', ['middleware' => 'cors', 'as' => 'api.inventory.post', 'uses' => 'InventoryController@post'])->middleware('auth.apikey');

	// Stock Movement
	// post
	Route::post('stock-movement/create', ['middleware' => 'cors', 'as' => 'api.stock-movement.post', 'uses' => 'StockMovementController@post'])->middleware('auth.apikey');

	// Move pallet to pallet
	Route::get('stock-movement/get-pallet-pallet/{id}', ['middleware' => 'cors', 'as' => 'api.stock-movement.get-pallet-pallet', 'uses' => 'StockMovementController@get_palet_palet'])->middleware('auth.apikey');

	// Move warehouse to warehouse
	//index
	Route::get('stock-movement/get-warehouse-warehouse/{id}', ['middleware' => 'cors', 'as' => 'api.stock-movement.get-warehouse-warehouse', 'uses' => 'StockMovementController@get_warehouse_warehouse'])->middleware('auth.apikey');
	//index
	Route::get('stock-movement/get-rack-master/{id}', ['middleware' => 'cors', 'as' => 'api.stock-movement.get-rack-master', 'uses' => 'StockMovementController@get_rack_master'])->middleware('auth.apikey');
	//post
	Route::post('stock-movement/get-warehouse-warehouse/create', ['middleware' => 'cors', 'as' => 'api.stock-movement.post-warehouse-warehouse', 'uses' => 'StockMovementController@post_warehouse_warehouse'])->middleware('auth.apikey');

	// Room
	Route::get('room/index', ['middleware' => 'cors', 'as' => 'api.room.index', 'uses' => 'RoomController@index'])->middleware('auth.apikey');
	Route::post('room/rack-index', ['middleware' => 'cors', 'as' => 'api.room.rack-index', 'uses' => 'RoomController@rack_index'])->middleware('auth.apikey');

	// Inventory Receive
	//get po list receive process
	Route::get('inventory-receive/get-polist', ['middleware' => 'cors', 'as' => 'api.inventory-receive.get-polist', 'uses' => 'InventoryReceiveController@get_polist'])->middleware('auth.apikey');
	Route::post('inventory-receive/get-polist-search', ['as' => 'api.inventory-receive.get-polist-search', 'uses' => 'InventoryReceiveController@get_polist_search'])->middleware('auth.apikey');
	//post submit data inventory receive
  Route::post('inventory-receive/create', ['middleware' => 'cors', 'as' => 'api.inventory-receive.post', 'uses' => 'InventoryReceiveController@post'])->middleware('auth.apikey');
	Route::post('inventory-receive/create-manual', ['middleware' => 'cors', 'as' => 'api.inventory-receive.post-manual', 'uses' => 'InventoryReceiveController@post_manual'])->middleware('auth.apikey');
	Route::post('inventory-receive/test-content', ['middleware' => 'cors', 'as' => 'api.test-content.post', 'uses' => 'InventoryReceiveController@test_content'])->middleware('auth.apikey');
	//post submit data inventory receive
	Route::post('inventory-receive/create-receive-dummy/{id}', ['middleware' => 'cors', 'as' => 'api.inventory-receive.post-receive-dummy', 'uses' => 'InventoryReceiveController@post_receive_dummy'])->middleware('auth.apikey');
	//update inventory receive
	Route::put('inventory-receive/update/{id}', ['middleware' => 'cors', 'as' => 'api.inventory-receive.update', 'uses' => 'InventoryReceiveController@update'])->middleware('auth.apikey');
	//delete inventory receive
	Route::delete('inventory-receive/delete/{id}', ['middleware' => 'cors', 'as' => 'api.inventory-receive.delete', 'uses' => 'InventoryReceiveController@delete'])->middleware('auth.apikey');

	// Sales Return
	//post submit data sales return process
	Route::post('sales-return/create', ['middleware' => 'cors', 'as' => 'api.sales-return.store', 'uses' => 'SalesReturnController@post'])->middleware('auth.apikey');
	//post submit data manual sales return process
	Route::post('sales-return/create-manual', ['middleware' => 'cors', 'as' => 'api.sales-return.store', 'uses' => 'SalesReturnController@post_manual'])->middleware('auth.apikey');

	// Purchase Order
	// Put Away Receive
	//get grouping pallet putaway process
	Route::get('putaway-receive/get-pallet', ['middleware' => 'cors', 'as' => 'api.putaway-receive.get-pallet', 'uses' => 'PutawayReceiveController@get_pallet'])->middleware('auth.apikey');
	Route::post('putaway-receive/get-pallet-search', ['middleware' => 'cors', 'as' => 'api.putaway-receive.get-pallet-search', 'uses' => 'PutawayReceiveController@get_pallet_search'])->middleware('auth.apikey');
	//get grouping room & rack putaway process
	Route::get('putaway-receive/get-room/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-receive.get-room', 'uses' => 'PutawayReceiveController@get_room'])->middleware('auth.apikey');
	//update status putaway process
	Route::post('putaway-receive/confirm/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-receive.confirm', 'uses' => 'PutawayReceiveController@confirm'])->middleware('auth.apikey');
	//update submit putaway process
	Route::post('putaway-receive/submit/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-receive.submit', 'uses' => 'PutawayReceiveController@submit'])->middleware('auth.apikey');

	// Put Away Return
	//get grouping pallet putaway return process
	Route::get('putaway-return/get-pallet', ['middleware' => 'cors', 'as' => 'api.putaway-return.get-pallet', 'uses' => 'PutawayReturnController@get_pallet'])->middleware('auth.apikey');
	Route::post('putaway-return/get-pallet-search', ['middleware' => 'cors', 'as' => 'api.putaway-return.get-pallet-search', 'uses' => 'PutawayReturnController@get_pallet_search'])->middleware('auth.apikey');
	//update status putaway return process
	Route::post('putaway-return/confirm/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-return.confirm', 'uses' => 'PutawayReturnController@confirm'])->middleware('auth.apikey');
	//update submit putaway return process
	Route::post('putaway-return/submit/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-return.submit', 'uses' => 'PutawayReturnController@submit'])->middleware('auth.apikey');
	// Route::post('putaway-return/submit/{id}', ['middleware' => 'cors', 'as' => 'api.putaway-return.submit', 'uses' => 'PutawayReturnController@submit'])->middleware('auth.apikey');

	// Picking
	//get picking list
	Route::get('picking/get-picking-list/{id}', ['middleware' => 'cors', 'as' => 'api.picking.get-picking-list', 'uses' => 'PickingController@get_picking_list'])->middleware('auth.apikey');
	//get picking list
	Route::post('picking/picking-list', ['middleware' => 'cors', 'as' => 'api.picking.picking-list', 'uses' => 'PickingController@picking_list'])->middleware('auth.apikey');
	//get picking detail
	Route::get('picking/get-picking-detail/{id}', ['middleware' => 'cors', 'as' => 'api.picking.get-picking-detail', 'uses' => 'PickingController@get_picking_detail'])->middleware('auth.apikey');
	//call pickup
	Route::post('picking/create/{id}', ['middleware' => 'cors', 'as' => 'api.picking.store', 'uses' => 'PickingController@store'])->middleware('auth.apikey');
	//submit
	Route::post('picking/create-submit/{id}', ['middleware' => 'cors', 'as' => 'api.picking.post', 'uses' => 'PickingController@post'])->middleware('auth.apikey');
    //store manual
	Route::post('picking/create-manual/{id}', ['middleware' => 'cors', 'as' => 'api.picking.store-manual', 'uses' => 'PickingController@store_manual'])->middleware('auth.apikey');
	//update status
	Route::post('picking/confirm/{id}', ['middleware' => 'cors', 'as' => 'api.picking.confirm', 'uses' => 'PickingController@confirm'])->middleware('auth.apikey');
	//get pickup list detail
	Route::get('picking/get-pickup-list', ['middleware' => 'cors', 'as' => 'api.picking.get-pickup-list', 'uses' => 'PickingController@get_pickup_list'])->middleware('auth.apikey');
	//put picking detail
	Route::post('picking/update_item_detail', ['middleware' => 'cors', 'as' => 'api.picking.update_item_detail', 'uses' => 'PickingController@update_item_detail'])->middleware('auth.apikey');


  // Stock Opname
	//get po list receive process
	Route::get('stock-opname/index', ['middleware' => 'cors', 'as' => 'api.stock-opname.index', 'uses' => 'StockOpnameController@index'])->middleware('auth.apikey');
	Route::post('stock-opname/create', ['as' => 'api.stock-opname.create', 'uses' => 'StockOpnameController@post'])->middleware('auth.apikey');
	Route::post('stock-opname/search', ['middleware' => 'cors', 'as' => 'api.stock-opname.search', 'uses' => 'StockOpnameController@search_stock'])->middleware('auth.apikey');
});
