<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GoodsReceiveDetail extends Model
{
	use SoftDeletes;
	protected $table = 'goods_receive_detail';
	protected $dates = ['deleted_at'];  

}
