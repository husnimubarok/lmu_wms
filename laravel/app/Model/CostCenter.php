<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  CostCenter extends Model
{
	use SoftDeletes;
	protected $table = 'cost_center';
	protected $dates = ['deleted_at'];  

}
