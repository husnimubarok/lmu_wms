<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  DeferralDistributionType extends Model
{
	use SoftDeletes;
	protected $table = 'deferral_distribution_type';
	protected $dates = ['deleted_at'];  

}
