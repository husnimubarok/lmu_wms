<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PoRevision extends Model
{
	use SoftDeletes;
	protected $table = 'po_revision';
	protected $dates = ['deleted_at'];  

}
