<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Room extends Model
{
	use SoftDeletes;
	protected $table = 'room';
	protected $dates = ['deleted_at'];  

}
