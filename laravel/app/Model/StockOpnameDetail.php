<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockOpnameDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_opname_detail';
	protected $dates = ['deleted_at'];  

}
