<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GRReturn extends Model
{
	use SoftDeletes;
	protected $table = 'gr_return';
	protected $dates = ['deleted_at'];  

}
