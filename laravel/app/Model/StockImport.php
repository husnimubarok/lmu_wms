<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockImport extends Model
{
	use SoftDeletes;
	protected $guarded = [];
	protected $table = 'stock_import';
	protected $fillable = ['inventory',
							'pallet_number',
							'warehouse',
							'rack',
							'room',
							'bay',
							'batch',
							'unit',
							'quantity',
							'status'];

}
