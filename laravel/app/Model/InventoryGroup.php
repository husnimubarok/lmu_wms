<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  InventoryGroup extends Model
{
	use SoftDeletes;
	protected $table = 'inventory_group';
	protected $dates = ['deleted_at'];  

}
