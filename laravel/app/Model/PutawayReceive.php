<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PutawayReceive extends Model
{
	use SoftDeletes;
	protected $table = 'putaway_receive';
	protected $dates = ['deleted_at'];  

}
