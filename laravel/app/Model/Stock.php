<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Stock extends Model
{
	use SoftDeletes;
	protected $table = 'stock';
	protected $dates = ['deleted_at'];  

}
