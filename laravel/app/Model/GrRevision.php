<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrRevision extends Model
{
	use SoftDeletes;
	protected $table = 'gr_revision';
	protected $dates = ['deleted_at'];  

}
