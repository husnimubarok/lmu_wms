<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostingTransferDetail extends Model
{
	use SoftDeletes;
	protected $table = 'posting_transfer_detail';
	protected $dates = ['deleted_at'];  

}
