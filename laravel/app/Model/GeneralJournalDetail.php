<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GeneralJournalDetail extends Model
{
	use SoftDeletes;
	protected $table = 'general_journal_detail';
	protected $dates = ['deleted_at'];  

}
