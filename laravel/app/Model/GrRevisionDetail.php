<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrRevisionDetail extends Model
{
	use SoftDeletes;
	protected $table = 'gr_revision_detail';
	protected $dates = ['deleted_at'];  

}
