<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  DeliveryOrder extends Model
{
	use SoftDeletes;
	protected $table = 'delivery_order';
	protected $dates = ['deleted_at'];  

}
