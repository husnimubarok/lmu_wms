<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permission';
    protected $guarded = [];

    public function rolePermission()
    {
    	return $this->hasMany('App\Model\RolePermission');	
    }
}
