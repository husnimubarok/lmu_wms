<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMobile extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users_mobile'; 
}
