<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOpnameMobile extends Model
{
	use SoftDeletes;
	protected $table = 'stock_opname_mobile';
	protected $dates = ['deleted_at'];

}
