<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GoodsReceive extends Model
{
	use SoftDeletes;
	protected $table = 'goods_receive';
	protected $dates = ['deleted_at'];  

}
