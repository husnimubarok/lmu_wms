<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  SalesReturn extends Model
{
	use SoftDeletes;
	protected $table = 'sales_return';
	protected $dates = ['deleted_at'];  

}
