<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockPicking extends Model
{
	use SoftDeletes;
	protected $table = 'stock_picking';
	protected $dates = ['deleted_at'];

}
