<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  InventorySize extends Model
{
	use SoftDeletes;
	protected $table = 'inventory_size';
	protected $dates = ['deleted_at'];  

}
