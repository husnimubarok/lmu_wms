<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockMovement extends Model
{
	use SoftDeletes;
	protected $table = 'stock_movement';
	protected $dates = ['deleted_at'];  

}
