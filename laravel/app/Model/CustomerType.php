<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  CustomerType extends Model
{
	use SoftDeletes;
	protected $table = 'customer_type';
	protected $dates = ['deleted_at'];  

}
