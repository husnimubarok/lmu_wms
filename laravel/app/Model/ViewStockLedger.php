<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewStockLedger extends Model
{
	use SoftDeletes;
	protected $table = 'view_stock_ledger';
	protected $dates = ['deleted_at'];

}
