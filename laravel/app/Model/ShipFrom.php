<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShipFrom extends Model
{
	use SoftDeletes;
	protected $table = 'ship_from';
	protected $dates = ['deleted_at'];

}
