<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model\Setting;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Period extends Model
{
	use SoftDeletes;
	protected $table = 'period';
	protected $dates = ['deleted_at'];  

}
