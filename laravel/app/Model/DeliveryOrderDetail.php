<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  DeliveryOrderDetail extends Model
{
	use SoftDeletes;
	protected $table = 'delivery_order_detail';
	protected $dates = ['deleted_at'];  

}
