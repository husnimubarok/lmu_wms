<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'company_id', 'created_by', 'updated_by', 'deleted_by', 'maxpoamount', 'role_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public $timestamps = true;

    public function role()
    {
        return $this->belongsTo('App\Model\Role');
    }

    public function company()
    {
        return $this->belongsTo('App\Model\Company');
    }

    public function userLogs()
    {
        return $this->hasMany('App\Model\UserLog');
    }
}
