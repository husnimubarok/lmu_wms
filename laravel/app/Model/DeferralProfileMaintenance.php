<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  DeferralProfileMaintenance extends Model
{
	use SoftDeletes;
	protected $table = 'deferral_profile_maintenance';
	protected $dates = ['deleted_at'];  

}
