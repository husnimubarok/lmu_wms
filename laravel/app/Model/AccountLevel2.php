<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Period extends Model
{
	use SoftDeletes;
	protected $table = 'period';
	protected $dates = ['deleted_at'];  

}
