<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  BsRack extends Model
{
	use SoftDeletes;
	protected $table = 'rack_bs_hidden';
	protected $dates = ['deleted_at'];  

}
