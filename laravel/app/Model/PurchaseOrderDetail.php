<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PurchaseOrderDetail extends Model
{
	use SoftDeletes;
	protected $table = 'purchase_order_detail';
	protected $dates = ['deleted_at'];  

}
