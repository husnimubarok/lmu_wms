<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  UnitOfMeasure extends Model
{
	use SoftDeletes;
	protected $table = 'unit_of_measure';
	protected $dates = ['deleted_at'];  

}
