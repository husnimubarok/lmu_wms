<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockExport extends Model
{
	use SoftDeletes;
	protected $table = 'stock_export';
	protected $dates = ['deleted_at'];  

}
