<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PickingDetail extends Model
{
	use SoftDeletes;
	protected $table = 'picking_detail';
	protected $dates = ['deleted_at'];  

}
