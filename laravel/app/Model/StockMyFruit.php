<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockMyFruit extends Model
{
	use SoftDeletes;
	protected $table = 'stock_my_fruit';
	protected $dates = ['deleted_at'];  

}
