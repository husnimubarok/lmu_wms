<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  InventoryReceiveDetail extends Model
{
	use SoftDeletes;
	protected $table = 'inventory_receive_detail';
	protected $dates = ['deleted_at'];  

}
