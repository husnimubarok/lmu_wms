<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockAdjustmentDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_adjustment_detail';
	protected $dates = ['deleted_at'];  

}
