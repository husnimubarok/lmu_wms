<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockOpname extends Model
{
	use SoftDeletes;
	protected $table = 'stock_opname';
	protected $dates = ['deleted_at'];  

}
