<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockMovementDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_movement_detail';
	protected $dates = ['deleted_at'];  

}
