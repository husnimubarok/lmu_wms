<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockDecreasing extends Model
{
	use SoftDeletes;
	protected $table = 'stock_decreasing';
	protected $dates = ['deleted_at'];

}
