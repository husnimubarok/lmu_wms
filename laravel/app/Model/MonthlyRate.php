<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  MonthlyRate extends Model
{
	use SoftDeletes;
	protected $table = 'monthly_rate';
	protected $dates = ['deleted_at'];  

}
