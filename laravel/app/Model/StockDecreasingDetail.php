<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockDecreasingDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_decreasing_detail';
	protected $dates = ['deleted_at'];

}
