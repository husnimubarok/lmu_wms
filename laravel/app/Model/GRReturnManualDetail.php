<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GRReturnManualDetail extends Model
{
	use SoftDeletes;
	protected $table = 'gr_return_manual_detail';
	protected $dates = ['deleted_at'];

}
