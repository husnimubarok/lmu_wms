<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];
    
    public $timestamps = true;

    public function rolePermissions()
    {
    	return $this->hasMany('App\Model\RolePermission');	
    }

    public function users()
    {
        return $this->hasMany('App\Model\Users');
    }
}
