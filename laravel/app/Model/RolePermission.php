<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = 'role_permission';
    protected $guarded = [];
    
    public $timestamps = true;

    public function role()
    {
    	return $this->belongsTo('App\Model\Role');	
    }

    public function permission()
    {
    	return $this->belongsTo('App\Model\Permission');	
    }
}
