<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GLAccount extends Model
{
	use SoftDeletes;
	protected $table = 'gl_account';
	protected $dates = ['deleted_at'];  

}
