<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  UserLog extends Model
{
	use SoftDeletes;
	protected $table = 'user_log';
	protected $dates = ['deleted_at'];  

}
