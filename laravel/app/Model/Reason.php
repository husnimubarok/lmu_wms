<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Reason extends Model
{
	use SoftDeletes;
	protected $table = 'return_reason';
	protected $dates = ['deleted_at'];  

}
