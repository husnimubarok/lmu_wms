<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StoreLocation extends Model
{
	use SoftDeletes;
	protected $table = 'store_location';
	protected $dates = ['deleted_at'];  

}
