<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Bay extends Model
{
	use SoftDeletes;
	protected $table = 'bay';
	protected $dates = ['deleted_at'];  

}
