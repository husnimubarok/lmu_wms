<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  DepreciationType extends Model
{
	use SoftDeletes;
	protected $table = 'depreciation_type';
	protected $dates = ['deleted_at'];  

}
