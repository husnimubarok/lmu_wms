<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  InventoryType extends Model
{
	use SoftDeletes;
	protected $table = 'inventory_type';
	protected $dates = ['deleted_at'];  

}
