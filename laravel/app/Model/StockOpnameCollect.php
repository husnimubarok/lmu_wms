<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOpnameCollect extends Model
{
	use SoftDeletes;
	protected $table = 'stock_opname_collect';
	protected $dates = ['deleted_at'];

}
