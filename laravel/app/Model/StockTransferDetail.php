<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  StockTransferDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_transfer_detail';
	protected $dates = ['deleted_at'];  

}
