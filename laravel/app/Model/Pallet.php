<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Pallet extends Model
{
	use SoftDeletes;
	protected $table = 'pallet';
	protected $dates = ['deleted_at'];  

}
