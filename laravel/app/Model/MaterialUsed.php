<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  MaterialUsed extends Model
{
	use SoftDeletes;
	protected $table = 'material_used';
	protected $dates = ['deleted_at'];  

}
