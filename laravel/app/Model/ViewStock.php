<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  ViewStock extends Model
{
	use SoftDeletes;
	protected $table = 'view_stock';
	protected $dates = ['deleted_at'];  

}
