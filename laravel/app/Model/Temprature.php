<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Temprature extends Model
{
	use SoftDeletes;
	protected $table = 'temprature';
	protected $dates = ['deleted_at'];  

}
