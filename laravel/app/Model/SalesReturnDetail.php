<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  SalesReturnDetail extends Model
{
	use SoftDeletes;
	protected $table = 'sales_return_detail';
	protected $dates = ['deleted_at'];  

}
