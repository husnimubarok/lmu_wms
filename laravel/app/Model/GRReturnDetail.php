<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  GRReturnDetail extends Model
{
	use SoftDeletes;
	protected $table = 'gr_return_detail';
	protected $dates = ['deleted_at'];  

}
