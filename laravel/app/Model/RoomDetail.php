<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomDetail extends Model
{
	use SoftDeletes;
	protected $table = 'room_detail';
	protected $dates = ['deleted_at'];

}
