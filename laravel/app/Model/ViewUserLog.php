<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewUserLog extends Model
{
	use SoftDeletes;
	protected $table = 'view_user_log';
	protected $dates = ['deleted_at'];  

}
