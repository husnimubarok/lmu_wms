<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  InventoryWeight extends Model
{
	use SoftDeletes;
	protected $table = 'inventory_weight';
	protected $dates = ['deleted_at'];  

}
