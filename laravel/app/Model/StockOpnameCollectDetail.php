<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOpnameCollectDetail extends Model
{
	use SoftDeletes;
	protected $table = 'stock_opname_collect_detail';
	protected $dates = ['deleted_at'];

}
