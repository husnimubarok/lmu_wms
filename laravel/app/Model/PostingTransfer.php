<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PostingTransfer extends Model
{
	use SoftDeletes;
	protected $table = 'posting_transfer';
	protected $dates = ['deleted_at'];  

}
