<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Picking extends Model
{
	use SoftDeletes;
	protected $table = 'picking';
	protected $dates = ['deleted_at'];  

}
