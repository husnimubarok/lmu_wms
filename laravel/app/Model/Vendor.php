<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Vendor extends Model
{
	use SoftDeletes;
	protected $table = 'supplier';
	protected $dates = ['deleted_at'];  

}
