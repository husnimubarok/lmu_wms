<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userbranch extends Model
{
    protected $table = 'user_branch';
    protected $dates = ['deleted_at'];

}
