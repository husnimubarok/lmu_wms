<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  MaterialUsedDetail extends Model
{
	use SoftDeletes;
	protected $table = 'material_used_detail';
	protected $dates = ['deleted_at'];  

}
