<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use App\Model\StockExport;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\StockMyFruit;
use App\Model\Inventory;
use App\Model\InventoryGroup;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            $prefix = Carbon::now()->format("Y-m-d h:i:s");
            (new FastExcel(StockExport::all()))->export(public_path('stock/'.$prefix.'-stock.csv'), function ($user) {
                return [
                    'Warehouse Name' => $user->warehouse_name,
                    'Room' => $user->room_name,
                    'Bay' => $user->bay_name,
                    'Rack' => $user->rack_name,
                    'Pallet No' => $user->pallet_number,
                    'Batch' => $user->batch,
                    'Inventory Code' => $user->inventory_code,
                    'Inventory Name' => $user->inventory_name,
                    'Unit' => $user->unit,
                    'Stock' => $user->quantity,
                ];
            });
        })->dailyAt('06:00');;


        $schedule->call(function () {
            $group_null = Inventory::whereNull('inventory_group')->whereNull('inventory_group_id')->get();
            foreach($group_null as $group_nulls)
            {
              $post_null = Inventory::where('id', $group_nulls->id)->first();
              $post_null->inventory_group_id = 513;
              $post_null->save();
            };

            $inventory = Inventory::whereNull('inventory_group_id')->get();

            foreach($inventory as $inventorys)
            {
                $inventory_group = InventoryGroup::where('inventory_group_code', $inventorys->inventory_group)->first();

                // dd($inventory_group);

                if(isset($inventory_group))
                {
                  $post = Inventory::where('inventory_group', $inventory_group->inventory_group_code)->whereNull('inventory_group_id')->first();
                  // dd($post);
                  $post->inventory_group_id = $inventory_group->id;
                  $post->save();
                }else{
                  $post = new InventoryGroup;
                  $post->inventory_group_code = $inventorys->inventory_group;
                  $post->inventory_group_name = $inventorys->inventory_group;
                  $post->temprature = 0;
                  $post->temprature_id = 0;
                  $post->description = $inventorys->inventory_group;
                  $post->status = 0;
                  $post->save();
                }

            };
        })->everyTenMinutes();


        // my fruit
        $schedule->call(function () {
            StockMyFruit::query()->truncate();

            $myfruit = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/my-fruit');
            $myfruit_decode = json_decode($myfruit, true);

            // dd($myfruit_decode['IT_FRUIT']);
            StockMyFruit::query()->truncate();

            if(isset($myfruit_decode['inidatasap']['IT_FRUIT']))
            {
                foreach ($myfruit_decode['inidatasap']['IT_FRUIT'] as $field => $value) {

                $inventory = Inventory::where('inventory_code', $value['MATNR'])->first();

                if(isset($inventory))
                {
                    $post = New StockMyFruit;
                    $post->inventory_id = $inventory->id;
                    $post->inventory_code = $value['MATNR'];
                    $post->inventory_name = $value['MAKTX'];
                    $post->quantity = str_replace(".", "", $value['QTY_STOCK']);
                    $post->unit = $value['UNIT'];
                    $post->pallet_number = "PLT-MYFR01";
                    $post->warehouse_id = 9;
                    $post->rack_id = 2220;
                    $post->room_id = 12;
                    $post->bay_id = 0;
                    $post->batch = 0;
                    $post->save();
                };
                };
            };

        })->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
