<?php 

namespace App\HUSNI\RMS;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Facade;
use App\Model\Privilege;
use App\Model\Module;
use App\Model\Action;

use App\Model\Role;
use App\Model\Permission;
use App\Model\RolePermission;

class RoleManager extends Facade
{
    public static function user()
    {
        return Auth::guard()->id();
    }

    // for middleware

    public static function role($value1, $value2)
    {
        $access = false;
        $module_id = Module::where('name', $value1)->pluck('id')->first();
        $action_id = Action::where('name', $value2)->pluck('id')->first();
        $privilege = new Privilege;
        $user_access = $privilege->where('user_id', self::user())->where('module_id', $module_id)->where('action_id', $action_id)->get();
        if($user_access->count() > 0)
        {
            $access = true;
        }
        return $access;
    }

    // end of middleware

    public static function hasModule()
    {
    	$privilege = new Privilege;
    	$user_access = $privilege->where('user_id', self::user())->get();
    	$module = NULL;
        $module_id = NULL;
        foreach ($user_access as $uhm) {
    		if($module_id != $uhm->module_id)
				$module[] = $uhm->modules->name;
    		$module_id = $uhm->module_id;
    	}
    	return $module;
    }

    public static function hasAction($value)
    {
    	$module = Module::where('name', $value)->first();
      if ($module) {
        $module_id = $module->id;
      	$privilege = new Privilege;
      	$moduleasActions = $privilege->where('user_id', self::user())->where('module_id', $module_id)->get();
      	$action = NULL;
      	foreach ($moduleasActions as $mhs) {
      		$action[] = $mhs->action->name;
      	}
        return $action;
      }else {
        return FALSE;
      }
    }

    public static function moduleStart($value)
    {
        $access = false;
        $hasModule = self::hasModule();
        if(!empty($hasModule))
        {
            if(in_array($value, $hasModule))
              $access = true;
        }
        return $access;
    }
 

    /* ===================================== NEW PREVILEGE ===================================== */
    /* start : 30 October 2017 */

    public static function currentUser()
    {
        return Auth::user();
    }

    public static function actionStart($keyname, $action = [])
    {
        $user = self::currentUser();

        $role = $user->role_id;
        $access = false;
        $permission = Permission::where('keyname', $keyname)->first();
        if ($permission) {
            $rolePermissions = $permission->rolePermission()->where('permission_id', $permission->id)
                                            ->where('role_id', $role)
                                            ->first();
            if ($rolePermissions) {
                $actions = explode('|', $rolePermissions->action);

                foreach ($actions as $act) {
                    if (in_array($act, $action)) {
                        $access = true;
                        break;
                    }
                }
            }

            return $access;
        }

        return false;
    }
}
