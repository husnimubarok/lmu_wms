<?php

namespace App\Http\Middleware;

use Closure;
use RoleManager;

class Privilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $privilege)
    {
        $privilege_unpack = explode("|", $privilege);
        $module = $privilege_unpack[0];
        $action = $privilege_unpack[1];
        if (\Auth::check() && \RoleManager::role($module, $action)) {
            return $next($request);
        }
        else
        {
            // return abort(404);
            return redirect('404');
        }
    }
}
