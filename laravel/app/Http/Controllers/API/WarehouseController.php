<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse; 
use App\Model\Rack; 

class WarehouseController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function index()
  { 
    $warehouse = Warehouse::all(); 

    if(count($warehouse)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success"; 
        foreach($warehouse as $key => $warehouses)
        { 
            $json['data'][$key]['warehouse_id'] = $warehouses->id;
            $json['data'][$key]['warehouse_name'] = $warehouses->warehouse_name;   
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  } 

}
