<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\UserLog;
use App\Model\Inventory;


class InventoryController extends  Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function get_inventory_search(Request $request)
  {
    $keyword = $request['keyword'];

    $sql_data = 'SELECT
                    *
                  FROM
                    inventory
                  ';
      $data = DB::table(DB::raw("(" . $sql_data . ") as rs_sql"))->where(DB::raw('concat(inventory_code," ",inventory_name)') , 'LIKE' , '%'.$keyword.'%')->orderBy('inventory_name', 'ASC')->Limit(30)->get();

      // dd($data);
      if(count($data)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data as $h_key => $datas) {

          $json['data'][$h_key]['inventory_id'] = $datas->id;
          $json['data'][$h_key]['inventory_group_id'] = $datas->inventory_group_id;
          $json['data'][$h_key]['inventory_code'] = $datas->inventory_code;
          $json['data'][$h_key]['inventory_name'] = $datas->inventory_name;

      }
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
  return response()->json($json);
}

public function post(Request $request)
  {

    try {
          $post_header = new Inventory;
          $post_header->inventory_code = $request['inventory_code'];
          $post_header->inventory_name = $request['inventory_name'];
          $post_header->created_by = $request['created_by'];
          $post_header->save();

          $user = User::where('username', $request['created_by'])->first();
          if(isset($user))
          {
            $user_log = new UserLog;
            $user_log->user_id = $user->id;
            $user_log->scope = 'INVENTORY';
            $user_log->data = json_encode([
                                  'action' => 'create',
                                  'description' => $request['inventory_name'],
                                  'transaction_number' => $request['inventory_code'],
                                  'source' => 'Mobile'
                              ]);
            $user_log->save();
          }

          return response()->json([
            'code' => 1,
            'msg' => 'Success',
            'data' => response()->json($request->all())
          ]);

    } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
    }
    return response()->json($json);

  }

}
