<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
// use Response;
use Illuminate\Http\Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryReceive;
use App\Model\InventoryReceiveDetail;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;
use App\User;
use App\Model\UserLog;
use App\Model\Inventory;
use App\Model\SalesReturn;
use App\Model\Stock;
use DateTime;

class InventoryReceiveController extends  Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function get_polist()
  {

    $sql_header = 'SELECT
                    purchase_order.id,
                    purchase_order.po_number,
                    purchase_order.doc_date,
                    purchase_order.po_status,
                    container.prefix,
                    container.container_no,
                    container.container_name
                  FROM
                    purchase_order
                  LEFT JOIN container ON purchase_order.container_id = container.id';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();


      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data_header as $h_key => $data_headers) {
            $sql_detail = 'SELECT
                            purchase_order_detail.id,
                            purchase_order_detail.inventory_code,
                            purchase_order_detail.order_qty,
                            purchase_order_detail.purchase_order_id,
                            inventory.inventory_name,
                            inventory.id AS inventory_id
                          FROM
                            purchase_order_detail
                          INNER JOIN inventory ON purchase_order_detail.inventory_code = inventory.inventory_code
                          WHERE purchase_order_detail.purchase_order_id='.$data_headers->id.'';

            $data_detail = DB::table(DB::raw("(" . $sql_detail . ") as rs_sql"))->get();

            $json['data'][$h_key]['header_id'] = $data_headers->id;
            $json['data'][$h_key]['po_number'] = $data_headers->po_number;
            $json['data'][$h_key]['container_no'] = $data_headers->container_no;
            $json['data'][$h_key]['container_name'] = $data_headers->container_name;
            $json['data'][$h_key]['doc_date'] = $data_headers->doc_date;
            $json['data'][$h_key]['po_status'] = $data_headers->po_status;

        foreach ($data_detail as $d_key => $data_details)
        {
            $json['data'][$h_key]['detail'][$d_key]['detail_id'] = $data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id_key'] = 'inventory_id_' .$data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['quantity_key'] =  'quantity_' .$data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_code'] = $data_details->inventory_code;
            $json['data'][$h_key]['detail'][$d_key]['inventory_name'] = $data_details->inventory_name;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id'] = $data_details->inventory_id;
            $json['data'][$h_key]['detail'][$d_key]['order_qty'] = $data_details->order_qty;
            $json['data'][$h_key]['detail'][$d_key]['purchase_order_id'] = $data_details->purchase_order_id;
        }
      }
    }elseif(count($data_header)==0){
      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    };
    return response()->json($json);
  }

  public function get_polist_search(Request $request)
  {
    $keyword = $request['keyword'];

    $sql_header = 'SELECT
                      purchase_order.id,
                      purchase_order.po_number,
                      purchase_order.doc_date,
                      purchase_order.po_status,
                      container.prefix,
                      purchase_order.container_no,
                      container.container_name,
                      purchase_order.plant,
                      purchase_order.store_loc AS store_location,
                      purchase_order.code_vendor AS supplier_code,
                      supplier.supplier_name AS vendor_name
                    FROM
                      purchase_order
                    LEFT JOIN (
                      SELECT
                        purchase_order.id,
                        sum(
                          purchase_order_detail.order_qty - ifnull(
                            inventory_receive_detail.quantity,
                            0
                          )
                        ) AS remain_qty
                      FROM
                        inventory_receive_detail
                      LEFT JOIN purchase_order ON inventory_receive_detail.po_number = purchase_order.po_number
                      LEFT JOIN purchase_order_detail ON purchase_order.po_number = purchase_order_detail.no_po
                      GROUP BY
                        purchase_order.id
                      HAVING
                        SUM(
                          purchase_order_detail.order_qty - ifnull(
                            inventory_receive_detail.quantity,
                            0
                          ) > 0
                        )
                    ) rcv ON purchase_order.id = rcv.id
                    LEFT JOIN container ON purchase_order.container_id = container.id
                    LEFT JOIN supplier ON purchase_order.code_vendor = supplier.supplier_code';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->where(DB::raw('concat(po_number," ",container_no)') , 'LIKE' , '%'.$keyword.'%')->get();

      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data_header as $h_key => $data_headers) {

            $sql_detail = 'SELECT
                          purchase_order_detail.id,
                          purchase_order_detail.inventory_code,
                          purchase_order_detail.order_qty - ifnull(rcv.qty_receive, 0) AS order_qty,
                          purchase_order_detail.purchase_order_id,
                          rcv.container_no,
                          -- purchase_order_detail.uom,
                          inventory.inventory_name,
                          inventory.unit_of_measure AS uom,
                          inventory.id AS inventory_id,
                          inventory.inventory_group_id AS inventory_group_id,
                          purchase_order_detail.ebelp AS item_line
                        FROM
                          (
                            SELECT
                              purchase_order_detail.id,
                              purchase_order_detail.purchase_order_id,
                              purchase_order_detail.ebelp,
                              purchase_order_detail.order_qty,
                              purchase_order_detail.uom,
                              purchase_order_detail.nett_weight,
                              purchase_order_detail.gr_qty,
                              purchase_order_detail.`status`,
                              purchase_order_detail.store_loc,
                              purchase_order_detail.no_po,
                              purchase_order_detail.sap_insert_date,
                              inventory.id AS inventory_id, 
                              inventory.inventory_code AS inventory_code
                            FROM
                              purchase_order_detail
                            INNER JOIN inventory ON purchase_order_detail.inventory_code = inventory.inventory_code
                          ) AS 
                          purchase_order_detail
                        INNER JOIN inventory ON purchase_order_detail.inventory_id = inventory.id
                        LEFT JOIN (
                                SELECT
                                  purchase_order.id,
                                  purchase_order.container_no,
                                  inventory_receive_detail.inventory_code,
                                  inventory_receive_detail.inventory_id,
                                  SUM(
                                    inventory_receive_detail.quantity
                                  ) AS qty_receive
                                FROM
                                  inventory_receive_detail
                                INNER JOIN purchase_order ON inventory_receive_detail.po_number = purchase_order.po_number
                                WHERE
                                  purchase_order.po_number = '.$data_headers->po_number.'
                                GROUP BY
                                  purchase_order.id,
                                  purchase_order.container_no,
                                  inventory_receive_detail.inventory_code,
                                  inventory_receive_detail.inventory_id
                                HAVING
                                  SUM(
                                    inventory_receive_detail.quantity
                                  )
                        ) rcv ON purchase_order_detail.inventory_code = rcv.inventory_code
                        WHERE
                          purchase_order_detail.no_po = '.$data_headers->po_number.'';

            $data_detail = DB::table(DB::raw("(" . $sql_detail . ") as rs_sql"))->orderBy('item_line', 'asc')->get();

            $json['data'][$h_key]['header_id'] = $data_headers->id;
            $json['data'][$h_key]['po_number'] = $data_headers->po_number;
            $json['data'][$h_key]['container_no'] = $data_headers->container_no;
            $json['data'][$h_key]['container_name'] = $data_headers->container_name;
            $json['data'][$h_key]['doc_date'] = $data_headers->doc_date;
            $json['data'][$h_key]['po_status'] = $data_headers->po_status;
            $json['data'][$h_key]['plant'] = $data_headers->plant;
            $json['data'][$h_key]['store_location'] = $data_headers->store_location;
            $json['data'][$h_key]['supplier_code'] = $data_headers->supplier_code;
            $json['data'][$h_key]['vendor_name'] = $data_headers->vendor_name;

        $i = 1;
        foreach ($data_detail as $d_key => $data_details)
        {
            $ii = $i++;

            $json['data'][$h_key]['detail'][$d_key]['detail_id'] = $data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id_key'] = 'inventory_id_'.$ii;
            $json['data'][$h_key]['detail'][$d_key]['quantity_key'] =  'quantity_'.$ii;
            $json['data'][$h_key]['detail'][$d_key]['inventory_code'] = $data_details->inventory_code;
            $json['data'][$h_key]['detail'][$d_key]['inventory_name'] = $data_details->inventory_name;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id'] = $data_details->inventory_id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_group_id'] = $data_details->inventory_group_id;
            $json['data'][$h_key]['detail'][$d_key]['order_qty'] = $data_details->order_qty;
            $json['data'][$h_key]['detail'][$d_key]['purchase_order_id'] = $data_details->purchase_order_id;
            $json['data'][$h_key]['detail'][$d_key]['item_line'] = $data_details->item_line;
            $json['data'][$h_key]['detail'][$d_key]['uom'] = $data_details->uom;

            $po_emodal_alloc = DB::connection('mysql2')->select('SELECT
                                                                	purchase_order.po_number,
                                                                	item.item_code,
                                                                	purchase_order_detail.lmu_jakarta,
                                                                	purchase_order_detail.lmu_surabaya,
                                                                	purchase_order_detail.lmu_semarang,
                                                                	purchase_order_detail.lmu_palembang,
                                                                	purchase_order_detail.lmu_makassar,
                                                                	purchase_order_detail.lmu_manado,
                                                                	purchase_order_detail.lmu_bali,
                                                                	purchase_order_detail.lmu_samarinda,
                                                                	purchase_order_detail.lmu_kupang,
                                                                	purchase_order_detail.lmu_jakartaspm,
                                                                	purchase_order_detail.lmu_banjarmasin
                                                                FROM
                                                                	purchase_order
                                                                INNER JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                                                                INNER JOIN item ON purchase_order_detail.item_id = item.id
                                                                WHERE purchase_order.po_number = "'.$data_headers->po_number.'" AND item.item_code = "'.$data_details->inventory_code.'" ');
          if(count($po_emodal_alloc)>0)
          {
            foreach ($po_emodal_alloc as $allocation_key => $po_emodal_allocs)
            {
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_jakarta'] = $po_emodal_allocs->lmu_jakarta;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_surabaya'] = $po_emodal_allocs->lmu_surabaya;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_semarang'] = $po_emodal_allocs->lmu_semarang;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_palembang'] = $po_emodal_allocs->lmu_palembang;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_makassar'] = $po_emodal_allocs->lmu_makassar;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_manado'] = $po_emodal_allocs->lmu_manado;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_bali'] = $po_emodal_allocs->lmu_bali;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_samarinda'] = $po_emodal_allocs->lmu_samarinda;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_kupang'] = $po_emodal_allocs->lmu_kupang;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_kupang'] = $po_emodal_allocs->lmu_kupang;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_jakartaspm'] = $po_emodal_allocs->lmu_jakartaspm;
              $json['data'][$h_key]['detail'][$d_key]['allocation'][$allocation_key]['lmu_banjarmasin'] = $po_emodal_allocs->lmu_banjarmasin;
            };
          }else{
            $json['data'][$h_key]['detail'][$d_key]['allocation'] = [];
          }
        }
      }
    }elseif(count($data_header)==0){
      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    };
  return response()->json($json);
}

public function post(Request $request)
  {

    try {
          // $warehouse_active = $request['warehouse_id'];

          $date = new DateTime;
          $date->modify('-40 minutes');
          $formatted_date = $date->format('Y-m-d H:i:s');

          $rack_inventory_receive = InventoryReceive::where('rack_id', $request['rack_id'])->where('created_at','>=',$formatted_date)->where('rack_id','!=',2218)->where('rack_id','!=',2219)->where('rack_id','!=',2221)->first();
          
          if(isset($rack_inventory_receive)){
            $json['code'] = 0;
            $json['msg'] = "Field";

            return response()->json('Rack has been book');

          }else{

            if($request['warehouse_id'] == 5 || $request['warehouse_id'] == 4)
            {
              // Gudang kering & my fruit
              $pallet_exist = InventoryReceive::where('pallet_number', 'NOT')->first();
            }else{
              $pallet_exist = InventoryReceive::where('pallet_number', $request['pallet_number'])->first();
            };
  
            if(isset($pallet_exist))
            {
              return response()->json([
                'code' => 0,
                'msg' => 'Pallet number already exists',
                'data' => response()->json($request->all())
              ]);

            }else{

              if($request['warehouse_id'] == 5)
              {
                // Gudang kering
                $post_header = new InventoryReceive;
                $post_header->warehouse_id = $request['warehouse_id'];
                $post_header->room_id = $request['room_id'];
                $post_header->bay_id = $request['bay_id'];
                $post_header->rack_id = $request['rack_id'];
                $post_header->pallet_number = 'PLT-GDKR01';
                $post_header->branch_item = $request['branch_item'];
                $post_header->qc = $request['qc'];
              }elseif($request['warehouse_id'] == 4){
                $post_header = new InventoryReceive;
                $post_header->warehouse_id = $request['warehouse_id'];
                $post_header->room_id = $request['room_id'];
                $post_header->bay_id = $request['bay_id'];
                $post_header->rack_id = $request['rack_id'];
                $post_header->pallet_number = 'PLT-VMYF01';
                $post_header->branch_item = $request['branch_item'];
                $post_header->qc = $request['qc'];
              }else{
                $post_header = new InventoryReceive;
                $post_header->warehouse_id = $request['warehouse_id'];
                $post_header->room_id = $request['room_id'];
                $post_header->bay_id = $request['bay_id'];
                $post_header->rack_id = $request['rack_id'];
                $post_header->pallet_number = $request['pallet_number'];
                $post_header->branch_item = $request['branch_item'];
                $post_header->qc = $request['qc'];
              };
              

              if($request['warehouse_id'] == 10)
              {
                $post_header->branch = 1;
              }else{
                $post_header->branch = $request['branch'];
              };
              // $post_header->po_revision = $request['po_revision'];
              $post_header->created_by = $request['created_by'];
              $post_header->save();

              // $data_detail = PurchaseOrderDetail::where('purchase_order_id', $request['po_id'])->get();

                $warehouse = Warehouse::where('id', $request['warehouse_id'])->first();

                // foreach($data_detail as $data_details){
                $count_detail = $request['max_count'];
                for($i = 0; $i < $count_detail; $i++)
                {
                    // print_r($i+1);
                    // object
                    $ii = $i+1;

                    $inventory_id = $request['inventory_id_'.$ii];
                    $quantity = $request['quantity_'.$ii];
                    $item_line = $request['item_line_'.$ii];
                    $uom = $request['uom_'.$ii];


                    if($request['po_revision'] == 1)
                    {
                      $inventory = Inventory::where('id', $inventory_id)->first();

                      if(isset($inventory))
                      {
                        $post_detail = new InventoryReceiveDetail;
                        $post_detail->transaction_id = $post_header->id;
                        $post_detail->inventory_code = $inventory->inventory_code;
                        $post_detail->inventory_id = $request['inventory_id_'.$ii];
                        $post_detail->quantity = $request['quantity_'.$ii];
                        $post_detail->po_number = $request['po_number'];
                        $post_detail->doc_date = $request['doc_date'];
                        $post_detail->supplier_code = $request['supplier_code'];
                        $post_detail->plant = $request['plant'];
                      }
                      
                      
                      if(isset($warehouse->store_location))
                      {
                        $post_detail->store_location = $warehouse->store_location;

                      }else{
                        $post_detail->store_location = $request['store_location'];
                      };

                      $post_detail->revision = $request['po_revision'];
                      $post_detail->item_line = $request['item_line_'.$ii];
                      $post_detail->uom = $request['uom_'.$ii];
                      // $post_detail->store_location = $request['store_location'];
                      $post_detail->save();
                    }else{
                      if($request['quantity_'.$ii] > 0)
                        {

                          $inventory = Inventory::where('id', $inventory_id)->first();

                          if(isset($inventory))
                          {
                            
                            $post_detail = new InventoryReceiveDetail;
                            $post_detail->transaction_id = $post_header->id;
                            $post_detail->inventory_id = $request['inventory_id_'.$ii];
                            $post_detail->quantity = $request['quantity_'.$ii];
                            $post_detail->po_number = $request['po_number'];
                            $post_detail->doc_date = $request['doc_date'];
                            $post_detail->supplier_code = $request['supplier_code'];
                            $post_detail->plant = $request['plant'];
                            $post_detail->inventory_code = $inventory->inventory_code;
                            
                            if(isset($warehouse->store_location))
                            {
                              $post_detail->store_location = $warehouse->store_location;

                            }else{
                              $post_detail->store_location = $request['store_location'];
                            };

                            $post_detail->revision = $request['po_revision'];
                            $post_detail->item_line = $request['item_line_'.$ii];
                            $post_detail->uom = $request['uom_'.$ii];
                            // $post_detail->store_location = $request['store_location'];
                            $post_detail->save();
                          };
                        };
                    };
                    // action
                };

                // new
                $get_po_revision_exists = InventoryReceiveDetail::where('po_number', $request['po_number'])->where('revision', '1')->first();

                if(isset($get_po_revision_exists))
                {
                  $get_po_revision = InventoryReceiveDetail::where('po_number', $request['po_number'])->get();
                  foreach ($get_po_revision as $get_po_revisions) {
                    // $revision = InventoryReceive::where('transaction_id', $get_po_revisions->transaction_id)->first();
                    $revision = InventoryReceiveDetail::where('id', $get_po_revisions->id)->first();
                    $revision->revision = 1;
                    $revision->save();
                  };
                };

                $user = User::where('username', $request['created_by'])->first();
                if(isset($user))
                {
                  $user_log = new UserLog;
                  $user_log->user_id = $user->id;
                  $user_log->scope = 'INVENTORY RECEIVE';
                  $user_log->data = json_encode([
                                        'action' => 'create',
                                        'description' => 'Submit Receive',
                                        'transaction_number' => $request['pallet_number'],
                                        'source' => 'Mobile'
                                    ]);
                  $user_log->save();
                };

              return response()->json([
                'code' => 1,
                'msg' => 'Success',
                'data' => response()->json($request->all())
              ]);
            }
          }
        } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
        }
    // return response()->json([
    //   'code' => 1,
    //   'msg' => 'test',
    //   'data' => response()->json($request->all())
    // ]);

  }


  public function post_manual(Request $request)
  {

    try {

          $date = new DateTime;
          $date->modify('-40 minutes');
          $formatted_date = $date->format('Y-m-d H:i:s');

          $rack_inventory_receive = InventoryReceive::where('rack_id', $request['rack_id'])->where('created_at','>=',$formatted_date)->where('rack_id','!=',2218)->where('rack_id','!=',2219)->where('rack_id','!=',2221)->first();
          
          if(isset($rack_inventory_receive)){
            $json['code'] = 0;
            $json['msg'] = "Field";

            return response()->json('Rack has been book');

          }else{


            if($request['warehouse_id'] == 5 || $request['warehouse_id'] == 4)
            {
                    // Gudang kering & my fruit
              $pallet_exist = InventoryReceive::where('pallet_number', 'NOT')->first();
            }else{
              $pallet_exist = InventoryReceive::where('pallet_number', $request['pallet_number'])->first();
            };


            if(isset($pallet_exist))
            {
              return response()->json([
                'code' => 0,
                'msg' => 'Pallet number already exists',
                'data' => response()->json($request->all())
              ]);

            }else{

                  // start
                  if($request['warehouse_id'] == 5)
                  {
                    // Gudang kering
                    $post_header = new InventoryReceive;
                    $post_header->warehouse_id = $request['warehouse_id'];
                    $post_header->room_id = $request['room_id'];
                    $post_header->bay_id = $request['bay_id'];
                    $post_header->rack_id = $request['rack_id'];
                    $post_header->pallet_number = 'PLT-GDKR01';
                    $post_header->branch_item = $request['branch_item'];
                    $post_header->qc = $request['qc'];
                  }else{
                    $post_header = new InventoryReceive;
                    $post_header->warehouse_id = $request['warehouse_id'];
                    $post_header->room_id = $request['room_id'];
                    $post_header->bay_id = $request['bay_id'];
                    $post_header->rack_id = $request['rack_id'];
                    $post_header->pallet_number = $request['pallet_number'];
                    $post_header->branch_item = $request['branch_item'];
                    $post_header->qc = $request['qc'];
                  };
                  
                  if($request['warehouse_id'] == 10)
                    {
                      $post_header->branch = 1;
                    }else{
                      $post_header->branch = $request['branch'];
                    };
                  // $post_header->branch = $request['branch'];
                  // $post_header->po_revision = $request['po_revision'];
                  $post_header->created_by = $request['created_by'];
                  $post_header->manual = 1;
                  $post_header->save();

                  // $data_detail = PurchaseOrderDetail::where('purchase_order_id', $request['po_id'])->get();
                  $warehouse = Warehouse::where('id', $request['warehouse_id'])->first();
                    // foreach($data_detail as $data_details){
                    $count_detail = $request['max_count'];
                    for($i = 0; $i < $count_detail; $i++)
                    {
                        // print_r($i+1);
                        // object
                        $ii = $i+1;

                        $inventory_id = $request['inventory_id_'.$ii];
                        $quantity = $request['quantity_'.$ii];
                        $item_line = $request['item_line_'.$ii];
                        $uom = $request['uom_'.$ii];

                        $inventory = Inventory::where('id', $request['inventory_id_'.$ii])->first();

                        // action
                        $post_detail = new InventoryReceiveDetail;
                        $post_detail->transaction_id = $post_header->id;
                        $post_detail->inventory_code = $inventory->inventory_code;
                        $post_detail->inventory_id = $request['inventory_id_'.$ii];
                        $post_detail->quantity = $request['quantity_'.$ii];
                        $post_detail->po_number = $request['po_number'];
                        $post_detail->doc_date = $request['doc_date'];
                        $post_detail->supplier_code = $request['supplier_code'];
                        $post_detail->plant = $request['plant'];
                        if(isset($warehouse->store_location))
                            {
                              $post_detail->store_location = $warehouse->store_location;

                            }else{
                              $post_detail->store_location = $request['store_location'];
                            };
                        // $post_detail->store_location = $request['store_location'];
                        $post_detail->revision = $request['po_revision'];
                        $post_detail->item_line = $request['item_line_'.$ii];
                        $post_detail->uom = $inventory->unit_of_measure;
                        // $post_detail->store_location = $request['store_location'];
                        $post_detail->save();
                    };

                    // new
                    $get_po_revision_exists = InventoryReceiveDetail::where('po_number', $request['po_number'])->where('revision', '1')->first();

                    if(isset($get_po_revision_exists))
                    {
                      $get_po_revision = InventoryReceiveDetail::where('po_number', $request['po_number'])->get();
                      foreach ($get_po_revision as $get_po_revisions) {
                        // $revision = InventoryReceive::where('transaction_id', $get_po_revisions->transaction_id)->first();
                        $revision = InventoryReceiveDetail::where('id', $get_po_revisions->id)->first();
                        $revision->revision = 1;
                        $revision->save();
                      };
                    };

                    $user = User::where('username', $request['created_by'])->first();
                    if(isset($user))
                    {
                      $user_log = new UserLog;
                      $user_log->user_id = $user->id;
                      $user_log->scope = 'INVENTORY RECEIVE';
                      $user_log->data = json_encode([
                                            'action' => 'create',
                                            'description' => 'Submit Receive Manual',
                                            'transaction_number' => $request['pallet_number'],
                                            'source' => 'Mobile'
                                        ]);
                      $user_log->save();
                    };

                    // stop
              };

            return response()->json([
              'code' => 1,
              'msg' => 'Success',
              'data' => response()->json($request->all())
            ]);
          };
        } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
        }
    // return response()->json([
    //   'code' => 1,
    //   'msg' => 'test',
    //   'data' => response()->json($request->all())
    // ]);

  }

  public function test_content(Request $request, Response $response)
  {
    // $bodyContent = $request->getContent();
    // return response()->json($bodyContent);
    // dd($bodyContent);
    // $request = Request::instance();
    // Now we can get the content from it
    // $content = $request->getContent();

    //  $content = $response->getContent();
    //  $statusCode = $response->getStatusCode();
    //  $headers = $response->headers;
    //  return serialize(compact('content', 'statusCode', 'headers'));
    // print_r($content);

    return response()->json([
      'code' => 1,
      'msg' => 'Success',
      'data' => response()->json($request->all())
    ]);

  }

  public function post_receive_dummy(Request $request, $id)
  {

    try {
          // action
          $post_detail = new InventoryReceiveDetail;
          $post_detail->transaction_id = $id;
          $post_detail->inventory_id = $request['inventory_id'];
          $post_detail->quantity = $request['quantity'];
          $post_detail->po_number = $request['po_number'];
          $post_detail->doc_date = $request['doc_date'];
          $post_detail->created_by = $request['created_by'];
          $post_detail->save();

          $user = User::where('username', $request['created_by'])->first();
          if(isset($user))
          {
            $user_log = new UserLog;
            $user_log->user_id = $user->id;
            $user_log->scope = 'INVENTORY RECEIVE DUMMY';
            $user_log->data = json_encode([
                                  'action' => 'create',
                                  'description' => 'Create Master Item from Receive',
                                  'transaction_number' => '-',
                                  'source' => 'Mobile'
                              ]);
            $user_log->save();
          };

          return response()->json([
            'code' => 1,
            'msg' => 'Success',
            'data' => response()->json($request->all())
          ]);

        } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
        }
    return response()->json($json);

  }

}
