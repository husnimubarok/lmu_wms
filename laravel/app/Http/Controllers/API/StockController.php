<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;
use App\Model\InventoryGroup;


class StockController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }


  public function get_rack(Request $request, $id)
  {
    // $pallet_number = $request['pallet_number'];

    $sql_header = 'SELECT
                      rack.rack_name,
                      stock.rack_id,
                      stock.pallet_number,
                      stock.room_id,
                      room.room_name
                    FROM
                      stock
                    LEFT JOIN rack ON stock.rack_id = rack.id
                    LEFT JOIN warehouse ON stock.warehouse_id = warehouse.id
                    LEFT JOIN room ON stock.room_id = room.id
                    WHERE rack.rack_name = "'.$id.'"
                    AND
                    stock.quantity > 0
                    GROUP BY
                    rack.rack_name,
                    stock.rack_id,
                    stock.pallet_number,
                    stock.room_id,
                    room.room_name';
    $stock = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

    if(count($stock)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success";
        foreach($stock as $key => $stocks)
        {
            $json['data'][$key]['rack_id'] = $stocks->rack_id;
            $json['data'][$key]['room_id'] = $stocks->room_id;
            $json['data'][$key]['room_name'] = $stocks->room_name;
            $json['data'][$key]['rack_name'] = $stocks->rack_name;
            $json['data'][$key]['pallet_number'] = $stocks->pallet_number;
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  }

  public function get_empty_rack(Request $request)
  {
    // try {
        // $pallet_number = $request['pallet_number'];
        $warehouse_id = $request['warehouse_id'];
        $inventory_group_id = $request['inventory_group_id'];

        // if($warehouse_id == 1)
        // {
        //   $warehouse = 1;
        // }else {
        //   $warehouse = 6;
        // };

        $strinng_inventory_group_id = '('. $inventory_group_id .')';



        $sql_header = 'SELECT
                        rack.id,
                        rack.room_id,
                        rack.rack_name
                      FROM
                        rack
                      INNER JOIN room ON rack.room_id = room.id
                      INNER JOIN room_detail ON room.id = room_detail.room_id
                      WHERE
                        (
                          rack.id NOT IN (
                            SELECT
                              stock.rack_id
                            FROM
                              stock
                            INNER JOIN room_detail ON stock.room_id = room_detail.room_id
                            WHERE
                              room_detail.warehouse_id = '.$warehouse_id.'
                            GROUP BY
                              stock.rack_id
                          )
                          AND rack.id NOT IN (
                            SELECT
                              inventory_receive.rack_id
                            FROM
                              inventory_receive
                            INNER JOIN room_detail ON inventory_receive.room_id = room_detail.room_id
                            WHERE
                              room_detail.warehouse_id = '.$warehouse_id.'
                            GROUP BY
                              inventory_receive.rack_id
                          )
                          AND rack.id NOT IN (
                            SELECT
                              sales_return.rack_id
                            FROM
                              sales_return
                            INNER JOIN room_detail ON sales_return.room_id = room_detail.room_id
                            WHERE
                              room_detail.warehouse_id = '.$warehouse_id.'
                            GROUP BY
                              sales_return.rack_id
                          )
                          AND room_detail.warehouse_id = '.$warehouse_id.'
                          AND rack.hide_rack != 1
                          AND room_detail.temprature_id = CASE
                          WHEN room_detail.temprature_id NOT IN (
                            SELECT
                              inventory_group.temprature_id
                            FROM
                              inventory
                            INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                            WHERE
                              inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                            GROUP BY
                              inventory_group.temprature_id
                          ) THEN
                            1
                          ELSE
                            (
                              SELECT
                                inventory_group.temprature_id
                              FROM
                                inventory
                              INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                              WHERE
                                inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                              GROUP BY
                                inventory_group.temprature_id
                            )
                          END
                        )
                      UNION ALL
                        SELECT
                          rack.id,
                          rack.room_id,
                          rack.rack_name
                        FROM
                          rack
                        INNER JOIN room ON rack.room_id = room.id
                        INNER JOIN room_detail ON room.id = room_detail.room_id
                        WHERE
                          (
                            rack.id IN (
                              SELECT
                                stock.rack_id
                              FROM
                                stock
                              INNER JOIN room_detail ON stock.room_id = room_detail.room_id
                              WHERE
                                stock.quantity = 0
                              AND room_detail.warehouse_id = '.$warehouse_id.'
                              GROUP BY
                                stock.rack_id
                            )
                            AND room_detail.warehouse_id = '.$warehouse_id.'
                            AND rack.hide_rack != 1
                            AND room_detail.temprature_id = CASE
                            WHEN room_detail.temprature_id NOT IN (
                              SELECT
                                inventory_group.temprature_id
                              FROM
                                inventory
                              INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                              WHERE
                                inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                              GROUP BY
                                inventory_group.temprature_id
                            ) THEN
                              1
                            ELSE
                              (
                                SELECT
                                  inventory_group.temprature_id
                                FROM
                                  inventory
                                INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                                WHERE
                                  inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                                GROUP BY
                                  inventory_group.temprature_id
                              )
                            END
                          )';
        $stock = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

        $warehouse_other_id = 7;

        $sql_header_other = 'SELECT
                              rack.id,
                              rack.room_id,
                              rack.rack_name
                            FROM
                              rack
                            INNER JOIN room ON rack.room_id = room.id
                            INNER JOIN room_detail ON room.id = room_detail.room_id
                            WHERE
                              (
                                rack.id NOT IN (
                                  SELECT
                                    stock.rack_id
                                  FROM
                                    stock
                                  INNER JOIN room_detail ON stock.room_id = room_detail.room_id
                                  WHERE
                                    room_detail.warehouse_id = '.$warehouse_id.'
                                  GROUP BY
                                    stock.rack_id
                                )
                                AND rack.id NOT IN (
                                  SELECT
                                    inventory_receive.rack_id
                                  FROM
                                    inventory_receive
                                  INNER JOIN room_detail ON inventory_receive.room_id = room_detail.room_id
                                  WHERE
                                    room_detail.warehouse_id = '.$warehouse_id.'
                                  GROUP BY
                                    inventory_receive.rack_id
                                )
                                AND rack.id NOT IN (
                                  SELECT
                                    sales_return.rack_id
                                  FROM
                                    sales_return
                                  INNER JOIN room_detail ON sales_return.room_id = room_detail.room_id
                                  WHERE
                                    room_detail.warehouse_id = '.$warehouse_id.'
                                  GROUP BY
                                    sales_return.rack_id
                                )
                                AND room_detail.warehouse_id = '.$warehouse_id.'
                                AND rack.hide_rack != 1
                                AND room_detail.temprature_id = CASE
                                WHEN room_detail.temprature_id NOT IN (
                                  SELECT
                                    inventory_group.temprature_id
                                  FROM
                                    inventory
                                  INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                                  WHERE
                                    inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                                  GROUP BY
                                    inventory_group.temprature_id
                                ) THEN
                                  1
                                ELSE
                                  (
                                    SELECT
                                      inventory_group.temprature_id
                                    FROM
                                      inventory
                                    INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                                    WHERE
                                      inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                                    GROUP BY
                                      inventory_group.temprature_id
                                  )
                                END
                              )
                            UNION ALL
                              SELECT
                                rack.id,
                                rack.room_id,
                                rack.rack_name
                              FROM
                                rack
                              INNER JOIN room ON rack.room_id = room.id
                              INNER JOIN room_detail ON room.id = room_detail.room_id
                              WHERE
                                (
                                  rack.id IN (
                                    SELECT
                                      stock.rack_id
                                    FROM
                                      stock
                                    INNER JOIN room_detail ON stock.room_id = room_detail.room_id
                                    WHERE
                                      stock.quantity = 0
                                    AND room_detail.warehouse_id = '.$warehouse_id.'
                                    GROUP BY
                                      stock.rack_id
                                  )
                                  AND room_detail.warehouse_id = '.$warehouse_id.'
                                  AND rack.hide_rack != 1
                                  AND room_detail.temprature_id = CASE
                                  WHEN room_detail.temprature_id NOT IN (
                                    SELECT
                                      inventory_group.temprature_id
                                    FROM
                                      inventory
                                    INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                                    WHERE
                                      inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                                    GROUP BY
                                      inventory_group.temprature_id
                                  ) THEN
                                    1
                                  ELSE
                                    (
                                      SELECT
                                        inventory_group.temprature_id
                                      FROM
                                        inventory
                                      INNER JOIN inventory_group ON inventory.inventory_group_id = inventory_group.id
                                      WHERE
                                        inventory.inventory_group_id in '.$strinng_inventory_group_id.'
                                      GROUP BY
                                        inventory_group.temprature_id
                                    )
                                  END
                                )';
        $stock_other = DB::table(DB::raw("(" . $sql_header_other . ") as rs_sql"))->get();

        if(isset($inventory_group_id))
        {

            if(count($stock)>0)
            {

              // check same temprature
              $pecah = explode(",", $inventory_group_id);

              $i = 0;
              foreach ($pecah as $key => $pecahs) {
                $test[$key]  = InventoryGroup::where('id', $pecahs)->first();

                // print_r($i++);
                if($key > 0)
                {
                  // print_r('temprature '.$test[$key-1]->temprature_id);

                  if($test[$key-1]->temprature_id != $test[$key]->temprature_id)
                  {
                    $temperature_not_same = 1;
                    break;
                  }
                }

              };

              if(!isset($temperature_not_same))
              {
                $json['code'] = 1;
                $json['msg'] = "Success";
                  foreach($stock as $key => $stocks)
                  {
                      $json['data'][$key]['rack_id'] = $stocks->id;
                      $json['data'][$key]['room_id'] = $stocks->room_id;
                      $json['data'][$key]['rack_name'] = $stocks->rack_name;
                      // $json['data'][$key]['warehouse_id'] = $stocks->id;
                      // $json['data'][$key]['length'] = $stocks->length;
                      // $json['data'][$key]['height'] = $stocks->height;
                      // $json['data'][$key]['tolerance'] = $stocks->tolerance;
                  };
                }else{
                  $json['code'] = 0;
                  $json['msg'] = "Field";
                    // foreach($stock_other as $key => $stock_others)
                    // {
                    //     $json['data'][$key]['rack_id'] = $stock_others->id;
                    //     $json['data'][$key]['room_id'] = $stock_others->room_id;
                    //     $json['data'][$key]['rack_name'] = $stock_others->rack_name;
                        // $json['data'][$key]['warehouse_id'] = $stocks->id;
                        // $json['data'][$key]['length'] = $stocks->length;
                        // $json['data'][$key]['height'] = $stocks->height;
                        // $json['data'][$key]['tolerance'] = $stocks->tolerance;
                    // };
                };
              }else{
                $json['code'] = 3;
                $json['msg'] = "Suhu tidak sama";
              }

        }else{
              $json['code'] = 2;
              $json['msg'] = "item belum diisi";
        };

        // return response()->json($json);

    // } catch (\Exception $e) {
    //
    //
    //     $json['code'] = 0;
    //     $json['msg'] = "Failed";
    // }
    return response()->json($json);

  }


  public function get_empty_rack_dummy(Request $request)
  {
    try {
        // $pallet_number = $request['pallet_number'];
        $warehouse_id = $request['warehouse_id'];

        // if($warehouse_id == 1)
        // {
        //   $warehouse = 1;
        // }else {
        //   $warehouse = 6;
        // };

        $sql_header = 'SELECT
                        rack.id,
                        rack.room_id,
                        rack.rack_name
                      FROM
                        rack
                      INNER JOIN room ON rack.room_id = room.id
                      INNER JOIN room_detail ON room.id = room_detail.room_id
                      WHERE
                        rack.hide_rack != 1
                        AND room_detail.warehouse_id = '.$warehouse_id.'';
        $stock = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

        $warehouse_other_id = 7;

        $sql_header_other = 'SELECT
                              rack.id,
                              rack.room_id,
                              rack.rack_name
                            FROM
                              rack
                            INNER JOIN room ON rack.room_id = room.id
                            INNER JOIN room_detail ON room.id = room_detail.room_id
                            WHERE
                            rack.hide_rack != 1
                            AND room_detail.warehouse_id ='.$warehouse_id.'';
        $stock_other = DB::table(DB::raw("(" . $sql_header_other . ") as rs_sql"))->get();

        if(count($stock)>0)
        {
          $json['code'] = 1;
          $json['msg'] = "Success";
            foreach($stock as $key => $stocks)
            {
                $json['data'][$key]['rack_id'] = $stocks->id;
                $json['data'][$key]['room_id'] = $stocks->room_id;
                $json['data'][$key]['rack_name'] = $stocks->rack_name;
                // $json['data'][$key]['warehouse_id'] = $stocks->id;
                // $json['data'][$key]['length'] = $stocks->length;
                // $json['data'][$key]['height'] = $stocks->height;
                // $json['data'][$key]['tolerance'] = $stocks->tolerance;
            };
          }else{
                $json['code'] = 0;
                $json['msg'] = "Failed";
              }
          // else{
          //   $json['code'] = 1;
          //   $json['msg'] = "Success";
          //     foreach($stock_other as $key => $stock_others)
          //     {
          //         $json['data'][$key]['rack_id'] = $stock_others->id;
          //         $json['data'][$key]['room_id'] = $stock_others->room_id;
          //         $json['data'][$key]['rack_name'] = $stock_others->rack_name;
          //         // $json['data'][$key]['warehouse_id'] = $stocks->id;
          //         // $json['data'][$key]['length'] = $stocks->length;
          //         // $json['data'][$key]['height'] = $stocks->height;
          //         // $json['data'][$key]['tolerance'] = $stocks->tolerance;
          //     };
          // };

    } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
    }

    return response()->json($json);
  }


  public function get_pallet(Request $request, $id)
  {
    // $pallet_number = $request['pallet_number'];

    $sql_header = 'SELECT
                      stock.id,
                      inventory.id AS inventory_id,
                      inventory.inventory_code,
                      inventory.inventory_name,
                      stock.pallet_number,
                      stock.quantity,
                      rack.rack_name,
                      stock.rack_id,
                      warehouse.warehouse_name,
                      room.room_name,
                      stock.warehouse_id,
                      stock.room_id
                    FROM
                      stock
                    INNER JOIN inventory ON stock.inventory_id = inventory.id
                    LEFT JOIN rack ON stock.rack_id = rack.id
                    LEFT JOIN warehouse ON stock.warehouse_id = warehouse.id
                    LEFT JOIN room ON stock.room_id = room.id
                    WHERE stock.pallet_number = "'.$id.'"';
    $stock = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

    if(count($stock)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success";
        foreach($stock as $key => $stocks)
        {
            $json['data'][$key]['inventory_id_key'] = 'inventory_id_'.$stocks->inventory_id;
            $json['data'][$key]['quantity_key'] = 'quantity_'.$stocks->id;
            $json['data'][$key]['warehouse_id'] = $stocks->warehouse_id;
            $json['data'][$key]['warehouse_name'] = $stocks->warehouse_name;
            $json['data'][$key]['room_id'] = $stocks->room_id;
            $json['data'][$key]['room_name'] = $stocks->room_name;
            $json['data'][$key]['rack_id'] = $stocks->rack_id;
            $json['data'][$key]['rack_name'] = $stocks->rack_name;
            $json['data'][$key]['pallet_number'] = $stocks->pallet_number;
            $json['data'][$key]['inventory_code'] = $stocks->inventory_code;
            $json['data'][$key]['inventory_name'] = $stocks->inventory_name;
            $json['data'][$key]['stock'] = $stocks->quantity;
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  }

}
