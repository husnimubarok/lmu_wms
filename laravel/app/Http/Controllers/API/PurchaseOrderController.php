<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PurchaseOrder; 
use App\Model\PurchaseOrderDetail; 
use App\Model\Inventory; 


class PurchaseOrderController extends  Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function index()
  {
    $sql_header = 'SELECT
                  purchase_order.po_number,
                  inventory.inventory_code,
                  inventory.inventory_name,
                  purchase_order_detail.inventory_id,
                  purchase_order_detail.order_qty,
                  purchase_order_detail.purchase_order_id,
                  purchase_order.id
                  FROM
                  purchase_order
                  INNER JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                  INNER JOIN inventory ON purchase_order_detail.inventory_id = inventory.id';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get(); 


      foreach ($data_header as $h_key => $data_headers) 
      {

          $json[$h_key]['id'] = $data_headers->id;
          $json[$h_key]['po_number'] = $data_headers->po_number;
          $json[$h_key]['inventory_code'] = $data_headers->inventory_code;
          $json[$h_key]['inventory_name'] = $data_headers->inventory_name;
          $json[$h_key]['inventory_id'] = $data_headers->inventory_id;
          $json[$h_key]['order_qty'] = $data_headers->order_qty;
          $json[$h_key]['purchase_order_id'] = $data_headers->purchase_order_id;
      } 
      return response()->json($json);
  } 

}
