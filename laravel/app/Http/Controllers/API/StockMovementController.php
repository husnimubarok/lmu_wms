<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\StockMovement;
use App\Model\StockMovementDetail;
use App\Model\Stock;
use App\Model\StockPicking;
use App\Model\Rack;
use App\User;
use App\Model\UserLog;
use Auth;


class StockMovementController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

public function get_rack_master(Request $request, $id)
  {
    $sql_header = 'SELECT
                  rack.id,
                  rack.rack_name,
                  rack.room_id
                  FROM
                  rack
                  WHERE rack_name = "'.$id.'"';
    $rack = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

    if(count($rack)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success";
        foreach($rack as $key => $racks)
        {
            $json['data'][$key]['rack_id'] = $racks->id;
            $json['data'][$key]['room_id'] = $racks->room_id;
            $json['data'][$key]['rack_name'] = $racks->rack_name;
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  }

  public function get_palet_palet($id)
  {

      $sql_header = 'SELECT
                        stock_picking.pallet_number,
                        rack.rack_name,
                        stock_picking.rack_id,
                        warehouse.warehouse_name,
                        room.room_name,
                        stock_picking.warehouse_id,
                        stock_picking.room_id
                      FROM
                        stock_picking
                      LEFT JOIN rack ON stock_picking.rack_id = rack.id
                      LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
                      LEFT JOIN room ON stock_picking.room_id = room.id
                      WHERE 
                      stock_picking.quantity > 0
                      AND stock_picking.pallet_number = "'.$id.'"';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

      // dd($data_header);

      if(isset($data_header))
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        // foreach ($data_header as $h_key => $data_headers) {

          $sql_detail = 'SELECT
                            stock_picking.id,
                            inventory.id AS inventory_id,
                            inventory.inventory_code,
                            inventory.inventory_name,
                            stock_picking.pallet_number,
                            stock_picking.quantity,
                            rack.rack_name,
                            stock_picking.rack_id,
                            warehouse.warehouse_name,
                            room.room_name,
                            stock_picking.warehouse_id,
                            stock_picking.room_id,
                            RIGHT(stock_picking.batch,8) AS batch,
                            stock_picking.unit
                          FROM
                            stock_picking
                          INNER JOIN inventory ON stock_picking.inventory_id = inventory.id
                          LEFT JOIN rack ON stock_picking.rack_id = rack.id
                          LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
                          LEFT JOIN room ON stock_picking.room_id = room.id
                          WHERE stock_picking.pallet_number = "'.$id.'" AND stock_picking.quantity > 0';
            $data_detail = DB::table(DB::raw("(" . $sql_detail . ") as rs_sql"))->get();

            $json['data']['warehouse_id'] = $data_header->warehouse_id;
            $json['data']['warehouse_name'] = $data_header->warehouse_name;
            $json['data']['room_id'] = $data_header->room_id;
            $json['data']['room_name'] = $data_header->room_name;
            $json['data']['rack_id'] = $data_header->rack_id;
            $json['data']['rack_name'] = $data_header->rack_name;
            $json['data']['pallet_number'] = $data_header->pallet_number;

        $i = 1;
        foreach ($data_detail as $d_key => $data_details)
        {
            $ii = $i++;

            $json['data']['detail'][$d_key]['detail_id'] = $data_details->id;
            $json['data']['detail'][$d_key]['inventory_id_key'] = 'inventory_id_'.$ii;
            $json['data']['detail'][$d_key]['quantity_key'] =  'quantity_'.$ii;
            $json['data']['detail'][$d_key]['inventory_code'] = $data_details->inventory_code;
            $json['data']['detail'][$d_key]['inventory_name'] = $data_details->inventory_name;
            $json['data']['detail'][$d_key]['inventory_id'] = $data_details->inventory_id;
            $json['data']['detail'][$d_key]['quantity'] = $data_details->quantity;
            $json['data']['detail'][$d_key]['batch'] = $data_details->batch;
            $json['data']['detail'][$d_key]['unit'] = $data_details->unit;
        }
      // }
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);
  }

  public function get_warehouse_warehouse($id)
  {

      $sql_header = 'SELECT
                        stock_picking.warehouse_id,
                        warehouse.warehouse_name,
                        stock_picking.room_id,
                        room.room_name,
                        stock_picking.bay_id,
                        bay.bay_name,
                        stock_picking.rack_id,
                        rack.rack_name,
                        stock_picking.pallet_number
                      FROM
                        stock_picking
                      LEFT JOIN rack ON stock_picking.rack_id = rack.id
                      LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
                      LEFT JOIN room ON stock_picking.room_id = room.id
                      LEFT JOIN bay ON stock_picking.bay_id = bay.id
                      WHERE stock_picking.pallet_number = "'.$id.'"';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

      // dd($data_header);

      if(isset($data_header))
      {
        $json['code'] = 1;
        $json['msg'] = "Success";

        $json['data']['warehouse_id'] = $data_header->warehouse_id;
        $json['data']['warehouse_name'] = $data_header->warehouse_name;
        $json['data']['room_id'] = $data_header->room_id;
        $json['data']['room_name'] = $data_header->room_name;
        $json['data']['bay_id'] = $data_header->bay_id;
        $json['data']['bay_name'] = $data_header->bay_name;
        $json['data']['rack_id'] = $data_header->rack_id;
        $json['data']['rack_name'] = $data_header->rack_name;
        $json['data']['pallet_number'] = $data_header->pallet_number;

    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);
  }

  public function post_warehouse_warehouse(Request $request)
  {

    // try {
          // dd('test');

          $user = User::where('username', $request['created_by'])->first();
            // if(isset($user))
          $data_detail_rack = StockPicking::where('rack_id', $request['rack_id_to'])->where('quantity', '>', 0)->get();

          $data_detail = StockPicking::where('warehouse_id', $request['warehouse_id_from'])->get();

          // dd($request['warehouse_id_from']);

          // return response()->json([
          //   $data_detail
          // ]);


          $rack_x = 1;
          foreach ($data_detail_rack  as $data_details_rack) {
            $rack_xx = $rack_x++;
            $sql_header_rack = 'SELECT
                                  *
                                FROM
                                  stock_picking
                                WHERE
                                stock_picking.rack_id = '.$request['rack_id_to'].'
                                AND stock_picking.quantity > 0
                                AND stock_picking.rack_id != 2218
                                AND stock_picking.rack_id != 2219
                                AND stock_picking.rack_id != 2221';
            $return_data_rack = DB::table(DB::raw("(" . $sql_header_rack . ") as rs_sql_rack"))->first();

            // dd($return_data_rack);

            if(isset($return_data_rack))
            {
              $data_header_rack['value'][] = $return_data_rack;
            };
          }

          // $pallet_x = 1;
          // foreach($data_detail_pallet as $data_detail_pallets){
          //   $pallet_xx = $pallet_x++;
           // object
            $sql_header = 'SELECT
                              stock_picking.pallet_number,
                              sum(stock_picking.quantity) AS quantity
                            FROM
                              stock_picking
                            WHERE
                              stock_picking.pallet_number = "'.$request['pallet_number_from'].'"
                            AND stock_picking.quantity < 1
                            GROUP BY
                              stock_picking.pallet_number
                            HAVING
                              sum(stock_picking.quantity)';
            $return_data = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

            if(isset($return_data))
            {
              $data_header['value'][] = $return_data;
            };
          // }

          if(isset($data_header['value']) != NULL)
          {
            // dd('ok');
            return response()->json([
              'code' => 0,
              'msg' => 'Stock not available!',
              'data' => response()->json($request->all())
            ]);
          }elseif(isset($data_header_rack) != NULL)
          {
            return response()->json([
              'code' => 0,
              'msg' => 'Rack not available!',
              'data' => response()->json($request->all())
            ]);
          }else{
            $userid= Auth::id();
            $code_transaction = 'STMG';

            $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $month = $array_month[date('n')];

            DB::insert("INSERT INTO stock_movement (code_transaction, no_transaction, transaction_date, status, created_by, created_at)
              SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_movement.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
              FROM
              stock_movement WHERE stock_movement.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_movement.transaction_date, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

            // $data_detail = Stock::where('rack_id', $id)->get();
            $today = Carbon::today();
            $lastInsertedID = DB::table('stock_movement')->max('id');
            $pallet_number = $request['pallet_number_from'];
            $warehouse = $request['warehouse_id_to'];

            // $stock_movement = new StockMovement;
            // $stock_movement->transaction_date = $today;
            // $stock_movement->save();
            // dd($data_detail);
            // if(count(data_detail)>0)
            // {

            $stock = StockPicking::where('pallet_number', $pallet_number)->get();

            foreach($stock as $stocks){

                  // action
                  $post_detail = new StockMovementDetail;
                  $post_detail->inventory_id = $stocks->inventory_id;
                  $post_detail->transaction_id = $lastInsertedID;
                  $post_detail->quantity = $stocks->quantity;
                  $post_detail->warehouse_id_from = $stocks->warehouse_id;
                  $post_detail->warehouse_id_to = $request['warehouse_id_to'];
                  $post_detail->room_id_from = $stocks->room_id;
                  $post_detail->room_id_to = $request['room_id_to'];
                  $post_detail->bay_id_from = $stocks->bay_id;
                  $post_detail->bay_id_to = $request['bay_id_to'];
                  $post_detail->rack_id_from = $stocks->rack_id;
                  $post_detail->rack_id_to = $request['rack_id_to'];
                  $post_detail->pallet_number_from = $stocks->pallet_number;
                  $post_detail->pallet_number_to = $request['pallet_number_to'];
                  $post_detail->description = $request['description'];
                  $post_detail->flag = $request['flag'];
                  $post_detail->created_by = $request['created_by'];
                  $post_detail->batch = $stocks->batch;
                  $post_detail->unit_of_measure_code = $stocks->unit;
                  $post_detail->save();
              }
            // }

            $user = User::where('username', $request['created_by'])->first();
            if(isset($user))
            {
              $user_log = new UserLog;
              $user_log->user_id = $user->id;
              $user_log->scope = 'STOCK MOVEMENT WAREHOUSE';
              $user_log->data = json_encode([
                                    'action' => 'create',
                                    'description' => $request['pallet_number_from'] .' ke '. $request['pallet_number_to'],
                                    'transaction_number' => '-',
                                    'source' => 'Mobile'
                                ]);
              $user_log->save();
            };

          return response()->json([
            'code' => 1,
            'msg' => 'Success',
            'data' => response()->json($request->all())
          ]);
      };
    // }catch (\Exception $e) {
    //   $json['code'] = 0;
    //   $json['msg'] = "Failed";
    // }
    return response()->json($json);
  }

  public function post(Request $request)
    {
      // try {
          // dd($data_detail);
          // if(count(data_detail)>0)
          // {
            // $userid= Auth::id();
            // $code_transaction = 'STMV';

            // $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            // $month = $array_month[date('n')];

            // DB::insert("INSERT INTO stock_movement (code_transaction, no_transaction, transaction_date, status, created_by, created_at)
            //   SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_movement.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
            //   FROM
            //   stock_movement WHERE stock_movement.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_movement.transaction_date, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");


            //     $today = Carbon::today();
            //     $lastInsertedID = DB::table('stock_movement')->max('id');

            // $stock_movement = new StockMovement;
            // $stock_movement->transaction_date = $today;
            // $stock_movement->save();


            // $data_detail = Stock::where('pallet_number', $request['pallet_number_from'])->get();

            // $i = 1;
            // foreach($data_detail as $data_details){

            //     // print_r($i++);
            //     // echo($request['inventory_id_'$'']);

            //     $ii = $i++;

            //     // object
            //     $inventory_id = $request['inventory_id_'.$ii];
            //     $quantity = $request['quantity_'.$ii];
            //     $batch = $request['batch_'.$ii];
            //     $unit_of_measure_code = $request['unit_of_measure_code_'.$ii];

            //     // dd($i);
            //     // action
            //     $post_detail = new StockMovementDetail;
            //     $post_detail->transaction_id = $lastInsertedID;
            //     $post_detail->warehouse_id_from = $request['warehouse_id_from'];
            //     $post_detail->warehouse_id_to = $request['warehouse_id_to'];
            //     $post_detail->room_id_from = $request['room_id_from'];
            //     $post_detail->room_id_to = $request['room_id_to'];
            //     $post_detail->bay_id_from = $request['bay_id_from'];
            //     $post_detail->bay_id_to = $request['bay_id_to'];
            //     $post_detail->rack_id_from = $request['rack_id_from'];
            //     $post_detail->rack_id_to = $request['rack_id_to'];
            //     $post_detail->pallet_number_from = $request['pallet_number_from'];
            //     $post_detail->pallet_number_to = $request['pallet_number_to'];
            //     $post_detail->description = $request['description'];
            //     $post_detail->flag = $request['flag'];
            //     $post_detail->inventory_id = $inventory_id;
            //     $post_detail->quantity = $quantity;
            //     $post_detail->created_by = $request['created_by'];
            //     $post_detail->batch = $batch;
            //     $post_detail->unit_of_measure_code = $unit_of_measure_code;
            //     $post_detail->save();

            // }

          // }
        $user = User::where('username', $request['created_by'])->first();
          // if(isset($user))
        $data_detail = StockPicking::where('pallet_number', $request['pallet_number_from'])->get();
        // ->where('quantity', '>', 0)

        // dd($data_detail);

        // $x = 1;
        // foreach($data_detail as $data_details)
        $count_detail_stock = $request['max_count'];
        for($x = 0; $x < $count_detail_stock; $x++)
        {
        // object
          // $xx = $x++;
          $xx = $x+1;

          $inventory_id_st = $request['inventory_id_'.$xx];
          $pallet_number_st = $request['pallet_number_from'];
          $quantity_st = $request['quantity_'.$xx];
          $warehouse_id_st = $request['warehouse_id_from'];
          $rack_id_st = $request['rack_id_from'];
          $batch_st = $request['batch'];

          // dd($quantity_st);


          $sql_header = 'SELECT
                            *
                          FROM
                            stock_picking
                          WHERE
                          stock_picking.inventory_id = '.$inventory_id_st.' AND stock_picking.warehouse_id = '.$warehouse_id_st.' AND stock_picking.pallet_number = "'.$pallet_number_st.'" AND stock_picking.batch = "'.$batch_st.'" AND stock_picking.rack_id = '.$rack_id_st.'
                          AND stock_picking.quantity < '.$quantity_st.' ';
          $return_data = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

          if(isset($return_data))
          {
            $data_header['value'][] = $return_data;
          };
        };

        if(isset($data_header['value']) != NULL)
        {
          // dd('ok');
          return response()->json([
            'code' => 0,
            'msg' => 'Stock not available!',
            'data' => response()->json($request->all())
          ]);
        }else{

          $userid= Auth::id();
          $code_transaction = 'STMV';

          $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
          $month = $array_month[date('n')];

          DB::insert("INSERT INTO stock_movement (code_transaction, no_transaction, transaction_date, status, created_by, created_at)
            SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_movement.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
            FROM
            stock_movement WHERE stock_movement.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_movement.transaction_date, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");


              $today = Carbon::today();
              $lastInsertedID = DB::table('stock_movement')->max('id');

              // $stock_movement = new StockMovement;
              // $stock_movement->transaction_date = $today;
              // $stock_movement->save();


              // $i = 1;
              // foreach($data_detail as $data_details)
              $count_detail = $request['max_count'];
              for($i = 0; $i < $count_detail; $i++)
              {

                  // print_r($i++);
                  // echo($request['inventory_id_'$'']);

                  // $ii = $i++;
                $ii = $i+1;

                  // object
                  $inventory_id = $request['inventory_id_'.$ii];
                  $quantity = $request['quantity_'.$ii];
                  $batch = $request['batch_'.$ii];
                  $unit_of_measure_code = $request['unit_of_measure_code_'.$ii];

                  response()->json($inventory_id);


                  // dd($i);
                  // action
                  $post_detail = new StockMovementDetail;
                  $post_detail->transaction_id = $lastInsertedID;
                  $post_detail->warehouse_id_from = $request['warehouse_id_from'];
                  $post_detail->warehouse_id_to = $request['warehouse_id_to'];
                  $post_detail->room_id_from = $request['room_id_from'];
                  $post_detail->room_id_to = $request['room_id_to'];
                  $post_detail->bay_id_from = $request['bay_id_from'];
                  $post_detail->bay_id_to = $request['bay_id_to'];
                  $post_detail->rack_id_from = $request['rack_id_from'];
                  $post_detail->rack_id_to = $request['rack_id_to'];
                  $post_detail->pallet_number_from = $request['pallet_number_from'];
                  $post_detail->pallet_number_to = $request['pallet_number_to'];
                  $post_detail->inventory_id = $inventory_id;
                  $post_detail->quantity = $quantity;
                  $post_detail->created_by = $request['created_by'];
                  $post_detail->batch = $batch;
                  $post_detail->unit_of_measure_code = $unit_of_measure_code;
                  $post_detail->save();

              }

            // }
            $user = User::where('username', $request['created_by'])->first();
            if(isset($user))
            {
              $user_log = new UserLog;
              $user_log->user_id = $user->id;
              $user_log->scope = 'STOCK MOVEMENT ITEM';
              $user_log->data = json_encode([
                                    'action' => 'create',
                                    'description' => $request['pallet_number_from'] .' ke '. $request['pallet_number_to'],
                                    'transaction_number' => '-',
                                    'source' => 'Mobile'
                                ]);
              $user_log->save();
            };
            return response()->json([
              'code' => 1,
              'msg' => 'Success',
              'data' => response()->json($request->all())
            ]);
        };
    // } catch (\Exception $e) {
    //   $json['code'] = 0;
    //   $json['msg'] = "Failed";
    // }
    return response()->json($json);

  }

}
