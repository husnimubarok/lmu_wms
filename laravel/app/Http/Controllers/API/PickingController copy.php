<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryReceive;
use App\Model\InventoryReceiveDetail;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;
use App\Model\Picking;
use App\Model\PickingDetail;
use App\Model\Stock;
use App\Model\ViewStock;
use App\User;
use App\Model\UserLog;


class PickingController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function get_picking_list($id)
  {
    try {

      $picking = file_get_contents('http://lmu.co.id:3000/m/v1/wms/picking/'.$id.'');
      $picking_decode = json_decode($picking, true);

      $json['code'] = 1;
      $json['msg'] = "Success";

      if(isset($picking_decode['items'][0]['no_do']))
      {
        $json['data']['do_no'] = $picking_decode['items'][0]['no_do'];
        $json['data']['doc_date'] = $picking_decode['items'][0]['doc_date'];

        foreach ($picking_decode['items'] as $field => $value) {
          $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
          $json['data']['detail'][$field]['item_code'] = $value['item_code'];
          $json['data']['detail'][$field]['item_name'] = $value['item_name'];
          $json['data']['detail'][$field]['qty'] = $value['qty'];
        };
      }else{
          $json['data'] = [];
      };

      return response()->json($json);

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }

  public function get_picking_detail($id)
  {
    try {

      $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
      $picking_decode = json_decode($picking, true);

      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];

      if(isset($picking_decode['items'][0]['no_do']))
      {
        $json['data']['do_no'] = $picking_decode['items'][0]['no_do'];
        $json['data']['doc_date'] = $picking_decode['items'][0]['doc_date'];

        foreach ($picking_decode['items'] as $field => $value) {
          $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
          $json['data']['detail'][$field]['item_code'] = $value['item_code'];
          $json['data']['detail'][$field]['item_name'] = $value['item_name'];
          $json['data']['detail'][$field]['qty'] = $value['qty'];

          $sql = 'SELECT
                    inventory.inventory_code,
                    warehouse.warehouse_name,
                    stock.pallet_number,
                    room.room_name,
                    rack.rack_name,
                    bay.bay_name,
                    stock.unit,
                    stock.quantity,
                    stock.warehouse_id,
                    stock.rack_id,
                    stock.room_id,
                    stock.batch,
                    inventory.inventory_name,
                    inventory.id AS inventory_id,
                    IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack,
                    IFNULL(inventory.volume,1) AS volume_inventory,
                    (stock.quantity * IFNULL(inventory.volume,1)) / IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1)  AS volume_rack_avail
                  FROM
                    inventory
                  INNER JOIN stock ON inventory.id = stock.inventory_id
                  INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
                  INNER JOIN room ON stock.room_id = room.id
                  INNER JOIN rack ON stock.rack_id = rack.id
                  LEFT OUTER JOIN bay ON stock.bay_id = bay.id
                  WHERE stock.quantity > 0
                  AND room.warehouse_id = 1
                  AND inventory.inventory_code = '.$value['item_code'].'';
          $stock = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->get();

          // dd($stock);

          if(count($stock)>0){
            $total_qty_stock = 0;
            $total_qty_stock_last = 0;
            $total_qty_pick = 0;
            foreach ($stock as $d_key => $stocks)
            {
                $total_qty_stock += $stocks->quantity;
                $total_qty_stock_last += $total_qty_stock- $stocks->quantity;

                // print_r($total_qty_stock_last);

                $total_qty_pick = $value['qty'];

                // print_r($total_qty_pick);

                if(($total_qty_stock - $stocks->quantity) < $total_qty_pick)
                {
                   // $volume_rack = $stocks->volume_rack;

                  // $volume_inventory = $stock->volume_inventory;
                  // $quantity = $stock->quantity;
                  // $total_volume_inventory = $volume_inventory * $quantity;

                  // $presentage_volume_rack_avail = $volume_inventory / $volume_rack * 100;

                  $json['data']['detail'][$field]['stock'][$d_key]['inventory_id'] = $stocks->inventory_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['warehouse_name'] = $stocks->warehouse_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['warehouse_id'] = $stocks->warehouse_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['room_name'] = $stocks->room_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['room_id'] = $stocks->room_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['bay_name'] = $stocks->bay_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['rack_name'] = $stocks->rack_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['rack_id'] = $stocks->rack_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['pallet_number'] = $stocks->pallet_number;
                  // $json['data']['detail'][$field]['stock'][$d_key]['volume_rack_avail'] = $stocks->volume_rack_avail;
                  $json['data']['detail'][$field]['stock'][$d_key]['batch'] = $stocks->batch;
                  $json['data']['detail'][$field]['stock'][$d_key]['stock'] = $stocks->quantity;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_stock_last'] = $total_qty_stock_last;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_stock'] = $total_qty_stock;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_pick'] = $total_qty_pick;
                  // $json['data']['detail'][$field]['stock'][$d_key]['presentage_volume_rack_avail'] = $stocks->presentage_volume_rack_avail;
                };

            }

          }else{
            $json['data']['detail'][$field]['stock'] = [];
          };

        };
      }
      // }else{
      //     $json['stock'] = [];
      // };

      return response()->json($json);

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
        $json['data'] = [];
      }
  }

  // public function get_picking_detail($id)
  // {
  //   try {

  //     $picking = file_get_contents('https://webapp.lmu.co.id:3000/m/v1/wms/picking/'.$id.'');
  //     $picking_decode = json_decode($picking, true);

  //     $json['code'] = 1;
  //     $json['msg'] = "Success";
  //     $json['data'] = [];

  //     if(isset($picking_decode['items'][0]['no_do']))
  //     {
  //       $json['data']['do_no'] = $picking_decode['items'][0]['no_do'];
  //       $json['data']['doc_date'] = $picking_decode['items'][0]['doc_date'];

  //       foreach ($picking_decode['items'] as $field => $value) {
  //         $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
  //         $json['data']['detail'][$field]['item_code'] = $value['item_code'];
  //         $json['data']['detail'][$field]['item_name'] = $value['item_name'];
  //         $json['data']['detail'][$field]['qty'] = $value['qty'];


  //         $sql = 'SELECT
  //                   inventory.inventory_code,
  //                   warehouse.warehouse_name,
  //                   stock.pallet_number,
  //                   room.room_name,
  //                   rack.rack_name,
  //                   bay.bay_name,
  //                   stock.unit,
  //                   stock.quantity,
  //                   stock.warehouse_id,
  //                   stock.rack_id,
  //                   stock.room_id,
  //                   inventory.inventory_name,
  //                   inventory.id AS inventory_id
  //                 FROM
  //                   inventory
  //                 INNER JOIN stock ON inventory.id = stock.inventory_id
  //                 INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
  //                 INNER JOIN room ON stock.room_id = room.id
  //                 INNER JOIN rack ON stock.rack_id = rack.id
  //                 LEFT JOIN bay ON stock.bay_id = bay.id
  //                 WHERE
  //                 inventory.inventory_code = '.$value['item_code'].'';
  //         $stock = DB::table(DB::raw("($sql) as rs_sql"))->get();

  //         // dd($stock);

  //         if(count($stock)>0){
  //           foreach ($stock as $d_key => $stocks)
  //           {
  //               $json['data']['detail'][$field]['stock'][$d_key]['inventory_id'] = $stocks->inventory_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['warehouse_name'] = $stocks->warehouse_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['warehouse_id'] = $stocks->warehouse_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['room_name'] = $stocks->room_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['room_id'] = $stocks->room_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['bay_name'] = $stocks->bay_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['rack_name'] = $stocks->rack_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['rack_id'] = $stocks->rack_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['pallet_number'] = $stocks->pallet_number;
  //               $json['data']['detail'][$field]['stock'][$d_key]['stock'] = $stocks->quantity;
  //           }
  //         }else{
  //           $json['data']['detail'][$field]['stock'] = [];
  //         };

  //       };
  //     }
  //     // }else{
  //     //     $json['stock'] = [];
  //     // };

  //     return response()->json($json);

  //     } catch (\Exception $e) {
  //       $json['code'] = 0;
  //       $json['msg'] = "Failed";
  //       $json['data'] = [];
  //     }
  // }

  public function get_pickup_list()
  {

    $sql_header = 'SELECT
                    picking.id,
                    picking.picking_number,
                    picking.doc_date,
                    picking.pickup_status
                  FROM
                    picking
                  WHERE picking.pickup_status = 1 AND picking.deleted_at IS NULL';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();


      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data_header as $h_key => $data_headers) {
            $sql_detail = 'SELECT
                            picking_detail.id,
                            picking_detail.inventory_id,
                            picking_detail.pallet_number,
                            inventory.inventory_code,
                            inventory.inventory_name,
                            picking_detail.quantity,
                            picking_detail.batch,
                            room.room_name,
                            rack.rack_name,
                            warehouse.warehouse_name
                          FROM
                            picking_detail
                          INNER JOIN inventory ON picking_detail.inventory_id = inventory.id
                          LEFT JOIN rack ON picking_detail.rack_id = rack.id
                          LEFT JOIN room ON picking_detail.room_id = room.id
                          LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                          WHERE picking_detail.transaction_id='.$data_headers->id.'';

            $data_detail = DB::table(DB::raw("(" . $sql_detail . ") as rs_sql"))->get();

            $json['data'][$h_key]['header_id'] = $data_headers->id;
            $json['data'][$h_key]['picking_number'] = $data_headers->picking_number;
            $json['data'][$h_key]['doc_date'] = $data_headers->doc_date;
            $json['data'][$h_key]['pickup_status'] = $data_headers->pickup_status;

        foreach ($data_detail as $d_key => $data_details)
        {
            $json['data'][$h_key]['detail'][$d_key]['detail_id'] = $data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_code'] = $data_details->inventory_code;
            $json['data'][$h_key]['detail'][$d_key]['inventory_name'] = $data_details->inventory_name;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id'] = $data_details->inventory_id;
            $json['data'][$h_key]['detail'][$d_key]['quantity'] = $data_details->quantity;
            $json['data'][$h_key]['detail'][$d_key]['warehouse_name'] = $data_details->warehouse_name;
            $json['data'][$h_key]['detail'][$d_key]['room_name'] = $data_details->room_name;
            $json['data'][$h_key]['detail'][$d_key]['rack_name'] = $data_details->rack_name;
            $json['data'][$h_key]['detail'][$d_key]['pallet_number'] = $data_details->pallet_number;
            $json['data'][$h_key]['detail'][$d_key]['batch'] = $data_details->batch;
        }
      }
    }elseif(count($data_header)==0){
      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    };
    return response()->json($json);
  }

  public function confirm($id, Request $request)
  {
    try{
      DB::update("UPDATE picking SET `pickup_status` = 2 WHERE picking_number = ".$id."");
      $json['code'] = 1;
      $json['msg'] = "Success";

    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);

  }

  public function store($id, Request $request)
  {
    try {

      // delete header
      $picking_exists = Picking::where('picking_number', $request['picking_number'])->first();
      // dd($picking_exists);
      if(isset($picking_exists))
      {
      $picking_exists->delete();
      };

      $picking_detail_exists = PickingDetail::where('picking_number', $request['picking_number'])->get();
      foreach($picking_detail_exists AS $picking_detail_exist)
      {
        $post = PickingDetail::where('id', $picking_detail_exist->id)->first();
        if(isset($post))
        {
          $post->delete();
        };
      };

      $post_header = new Picking;
      $post_header->picking_number = $request['picking_number'];
      $post_header->doc_date = $request['doc_date'];
      $post_header->driver_name = $request['driver_name'];
      $post_header->plat_number = $request['plat_number'];
      $post_header->loading_number = $request['loading_number'];
      $post_header->description = $request['description'];
      $post_header->created_by = $request['created_by'];
      $post_header->pickup_status = $request['pickup_status'];
      $post_header->save();

      $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
      $picking_decode = json_decode($picking, true);

      if(isset($picking_decode['items'][0]['no_do']))
      {

        foreach ($picking_decode['items'] as $field => $value) {
           // print_r($picking_decode['items'][$field]['item_code']);

          $sql = 'SELECT
                  inventory.inventory_code,
                  warehouse.warehouse_name,
                  stock.pallet_number,
                  room.room_name,
                  rack.rack_name,
                  bay.bay_name,
                  stock.unit,
                  stock.quantity,
                  stock.warehouse_id,
                  stock.rack_id,
                  stock.room_id,
                  stock.batch,
                  inventory.inventory_name,
                  inventory.id AS inventory_id,
                  IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack,
                  IFNULL(inventory.volume,1) AS volume_inventory,
                  (stock.quantity * IFNULL(inventory.volume,1)) / IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1)  AS volume_rack_avail
                FROM
                  inventory
                INNER JOIN stock ON inventory.id = stock.inventory_id
                INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
                INNER JOIN room ON stock.room_id = room.id
                INNER JOIN rack ON stock.rack_id = rack.id
                LEFT OUTER JOIN bay ON stock.bay_id = bay.id
                WHERE stock.quantity > 0 AND room.warehouse_id = 1 AND
                inventory.inventory_code = '.$picking_decode['items'][$field]['item_code'].'';
        $stock = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->get();

        // print_r($stock);


          $total_qty_stock = 0;
          $total_qty_stock_last = 0;
          $total_qty_pick = 0;

          foreach ($stock as $d_key => $stocks)
            {
              if($field == 0)
              {
                $header = 0;
              }else{
                $header = $field + 1;
              };
              $count = $header+$d_key+1;
              // print_r($count);
              $inventory_code = $request['inventory_code_'.$count];
              $inventory_id = $request['inventory_id_'.$count];
              $quantity = $request['quantity_'.$count];
              $pick_qty = $request['pick_qty_'.$count];
              $stock_qty = $request['stock_qty_'.$count];
              $item_line = $request['item_line_'.$count];
              $warehouse_id = $request['warehouse_id_'.$count];
              $room_id = $request['room_id_'.$count];
              $bay_id = $request['bay_id_'.$count];
              $rack_id = $request['rack_id_'.$count];
              $pallet_number = $request['pallet_number_'.$count];
              $batch = $request['batch_'.$count];

              $total_qty_stock += $stocks->quantity;
              $total_qty_stock_last += $total_qty_stock- $stocks->quantity;

              // print_r($total_qty_stock_last);

              $total_qty_pick = $picking_decode['items'][$field]['qty'];

              // print_r($total_qty_pick);

              if($total_qty_stock_last < $total_qty_pick)
              {

                $post_detail = new PickingDetail;
                $post_detail->transaction_id = $post_header->id;
                $post_detail->inventory_code = $inventory_code;
                $post_detail->inventory_id = $inventory_id;
                $post_detail->quantity = $quantity;
                $post_detail->pick_qty = $pick_qty;
                $post_detail->stock_qty = $stock_qty;
                $post_detail->item_line = $item_line;
                $post_detail->warehouse_id = $warehouse_id;
                $post_detail->room_id = $room_id;
                $post_detail->bay_id = $bay_id;
                $post_detail->rack_id = $rack_id;
                $post_detail->pallet_number = $pallet_number;
                $post_detail->batch = $batch;
                $post_detail->picking_number = $request['picking_number'];
                $post_detail->save();

              };

            };
          };
      };

      $user = User::where('username', $request['created_by'])->first();
      if(isset($user))
      {
        $user_log = new UserLog;
        $user_log->user_id = $user->id;
        $user_log->scope = 'PICKING';
        $user_log->data = json_encode([
                              'action' => 'create',
                              'transaction_number' => $request['picking_number'],
                              'source' => 'Mobile'
                          ]);
        $user_log->save();
      };

      return response()->json([
        'code' => 1,
        'msg' => 'Success',
        'data' => response()->json($request->all())
      ]);

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }
  public function update_item_detail(Request $request)
  {
    try{
      $post_detail = PickingDetail::where('id', $request['id_detail'])->first();
      $post_detail->quantity = $request['quantity'];
      $post_detail->pick_qty = $request['pick_qty'];
      $post_detail->save();
      return response()->json([
        'code' => 1,
        'msg' => 'Success',
        'data' => response()->json($request->all())
      ]);

    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);

  }
}
