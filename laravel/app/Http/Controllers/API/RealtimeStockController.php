<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;


class RealtimeStockController extends BaseVerifier
{

  public function index(Request $request)
  {

    $sql = 'SELECT
                    inventory.inventory_code,
                    inventory.inventory_name,
                    stock.pallet_number,
                    stock.warehouse_id,
                    warehouse.warehouse_name,
                    stock.rack_id,
                    rack.rack_name,
                    stock.room_id,
                    room.room_name,
                    stock.bay_id,
                    bay.bay_name,
                    stock.unit,
                    stock.quantity
                  FROM
                    stock
                  INNER JOIN inventory ON stock.inventory_id = inventory.id
                  LEFT JOIN warehouse ON stock.warehouse_id = warehouse.id
                  LEFT JOIN rack ON stock.rack_id = rack.id
                  LEFT JOIN room ON stock.room_id = room.id
                  LEFT JOIN bay ON stock.bay_id = bay.id';
    $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get();

    if(count($stock)>0)
    {
        foreach($stock as $key => $stocks)
        {
            $json[$key]['warehouse_name'] = $stocks->warehouse_name;
            $json[$key]['room_name'] = $stocks->room_name;
            $json[$key]['bay_name'] = $stocks->bay_name;
            $json[$key]['rack_name'] = $stocks->rack_name;
            $json[$key]['pallet_number'] = $stocks->pallet_number;
            $json[$key]['inventory_code'] = $stocks->inventory_code;
            $json[$key]['inventory_name'] = $stocks->inventory_name;
            $json[$key]['unit'] = $stocks->unit;
            $json[$key]['stock'] = $stocks->quantity;
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  }


  public function search_manual(Request $request)
  {

    $keyword = $request['keyword'];


    $sql = 'SELECT
            inventory.inventory_code,
            inventory.inventory_name,
            stock_picking.inventory_id,
            stock_picking.pallet_number,
            stock_picking.warehouse_id,
            warehouse.warehouse_name,
            stock_picking.rack_id,
            rack.rack_name,
            stock_picking.room_id,
            room.room_name,
            stock_picking.bay_id,
            bay.bay_name,
            stock_picking.unit,
            stock_picking.quantity,
            stock_picking.batch
          FROM
            stock_picking
          INNER JOIN inventory ON stock_picking.inventory_id = inventory.id
          LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
          LEFT JOIN rack ON stock_picking.rack_id = rack.id
          LEFT JOIN room ON stock_picking.room_id = room.id
          LEFT JOIN bay ON stock_picking.bay_id = bay.id
          WHERE stock_picking.quantity > 0
          -- AND warehouse.hidden_picking != 1
          -- AND stock_picking.pallet_number NOT IN (
          --   SELECT
          --     inventory_receive.pallet_number AS pallet_number
          --   FROM
          --     inventory_receive
          --   WHERE
          --     inventory_receive.branch = 1
          --   GROUP BY
          --     inventory_receive.pallet_number
          -- )
          -- AND stock_picking.pallet_number NOT IN (
          --   SELECT
          --     stock_movement_detail.pallet_number_to AS pallet_number
          --   FROM
          --     stock_movement_detail
          --   WHERE
          --     stock_movement_detail.flag = 1
          --     AND stock_movement_detail.pallet_number_to IS NOT NULL
          --   GROUP BY
          --     stock_movement_detail.pallet_number_to
          -- )
          -- AND stock_picking.pallet_number NOT IN (
          --   SELECT
          --     stock_flag.pallet_number AS pallet_number
          --   FROM
          --     stock_flag
          --   GROUP BY
          --     stock_flag.pallet_number)
              ';
    // $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get();
    $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->where(DB::raw('concat(inventory_code," ",inventory_name, " ", pallet_number)') , 'LIKE' , '%'.$keyword.'%')->orderBy('batch', 'ASC')->Limit(10)->get();

    // dd($stock);

    if(count($stock)>0)
    {
        foreach($stock as $key => $stocks)
        {
            $json[$key]['inventory_id'] = $stocks->inventory_id;
            $json[$key]['warehouse_name'] = $stocks->warehouse_name;
            $json[$key]['warehouse_id'] = $stocks->warehouse_id;
            $json[$key]['room_name'] = $stocks->room_name;
            $json[$key]['room_id'] = $stocks->room_id;
            $json[$key]['bay_name'] = $stocks->bay_name;
            $json[$key]['rack_name'] = $stocks->rack_name;
            $json[$key]['rack_id'] = $stocks->rack_id;
            $json[$key]['pallet_number'] = $stocks->pallet_number;
            $json[$key]['inventory_code'] = $stocks->inventory_code;
            $json[$key]['inventory_name'] = $stocks->inventory_name;
            $json[$key]['unit'] = $stocks->unit;
            $json[$key]['batch'] = $stocks->batch;
            $json[$key]['stock'] = $stocks->quantity;
        };
      }else{
            $json['code'] = 0;
            $json['msg'] = "Stock Not Available";
          };

    return response()->json($json);
  }



  public function search_id(Request $request, $id)
  {
    try {

    $keyword = $request['keyword'];

    // $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
    // $picking_decode = json_decode($picking, true);

    // --------- new ------------

      $curl = curl_init();
      $data = array( 
                      'nodo' => $id
                    );

      curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-getlistreturndo",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                              ));

      $response = curl_exec($curl);
      $picking_decode = json_decode($response, true);
      $err = curl_error($curl);

      curl_close($curl);

      // ------ close new ------------



    // $data_msg = array(
    //               [
    //               "code" => "0",
    //               "msg" => "Failed"
    //               ]);

    if(isset($picking_decode['T_DELPICK'][0]['VBELN']))
    {

      // foreach ($picking_decode['items'] as $field => $value) {
      //   $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
      //   $json['data']['detail'][$field]['item_code'] = $value['item_code'];
      //   $json['data']['detail'][$field]['item_name'] = $value['item_name'];
      //   $json['data']['detail'][$field]['qty'] = $value['qty'];

        // $item_code = $picking_decode['items'][0]['item_code'];

      foreach ($picking_decode['T_DELPICK'] as $field => $value) {
          $json['data']['detail'][$field]['itemdo'] = $value['POSNR'];
          $json['data']['detail'][$field]['item_code'] = $value['MATNR'];
          $json['data']['detail'][$field]['item_name'] = $value['ARKTX'];
          $json['data']['detail'][$field]['qty'] = $value['QUANTITY'];

        $sql = 'SELECT
                inventory.inventory_code,
                inventory.inventory_name,
                stock_picking.inventory_id,
                stock_picking.pallet_number,
                stock_picking.warehouse_id,
                warehouse.warehouse_name,
                stock_picking.rack_id,
                rack.rack_name,
                stock_picking.room_id,
                room.room_name,
                stock_picking.bay_id,
                bay.bay_name,
                stock_picking.unit,
                stock_picking.quantity,
                stock_picking.batch
              FROM
                stock_picking
              INNER JOIN inventory ON stock_picking.inventory_id = inventory.id
              LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
              LEFT JOIN rack ON stock_picking.rack_id = rack.id
              LEFT JOIN room ON stock_picking.room_id = room.id
              LEFT JOIN bay ON stock_picking.bay_id = bay.id
              WHERE stock_picking.quantity > 0
              AND inventory.inventory_code = '.$value['MATNR'].'';
        // $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get();
        $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->where(DB::raw('concat(inventory_code," ",inventory_name, " ", pallet_number)') , 'LIKE' , '%'.$keyword.'%')->orderBy('batch', 'ASC')->Limit(10)->get();

        // dd($stock);

        if(count($stock)>0)
        {
         
             $json['code'] = 1;
             $json['msg'] = "Success";

            foreach($stock as $key => $stocks)
            {
                $json['data']['detail'][$field]['stock'][$key]['inventory_id'] = $stocks->inventory_id;
                $json['data']['detail'][$field]['stock'][$key]['warehouse_name'] = $stocks->warehouse_name;
                $json['data']['detail'][$field]['stock'][$key]['warehouse_id'] = $stocks->warehouse_id;
                $json['data']['detail'][$field]['stock'][$key]['room_name'] = $stocks->room_name;
                $json['data']['detail'][$field]['stock'][$key]['room_id'] = $stocks->room_id;
                $json['data']['detail'][$field]['stock'][$key]['bay_name'] = $stocks->bay_name;
                $json['data']['detail'][$field]['stock'][$key]['rack_name'] = $stocks->rack_name;
                $json['data']['detail'][$field]['stock'][$key]['rack_id'] = $stocks->rack_id;
                $json['data']['detail'][$field]['stock'][$key]['pallet_number'] = $stocks->pallet_number;
                $json['data']['detail'][$field]['stock'][$key]['inventory_code'] = $stocks->inventory_code;
                $json['data']['detail'][$field]['stock'][$key]['inventory_name'] = $stocks->inventory_name;
                $json['data']['detail'][$field]['stock'][$key]['unit'] = $stocks->unit;
                $json['data']['detail'][$field]['stock'][$key]['batch'] = $stocks->batch;
                $json['data']['detail'][$field]['stock'][$key]['stock'] = $stocks->quantity;
            };
          }
          else{
            $json['data']['detail'][$field]['stock'] = []; 
            // $json['data']['detail'][$field]['stock']['msg'] = 'Failed'; 
          };
    //       elseif(count($stock)==0){
    //         $json['code'] = 1;
    //         $json['msg'] = "Success";
    //         $json['data'] = [];
    //       }else{
    //   $json['code'] = 0;
    //   $json['msg'] = "Failed";
    // };
        }
    }

    return response()->json($json);
    
    } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }



  public function search(Request $request)
  {

    $keyword = $request['keyword'];

    $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$keyword.'');
    $picking_decode = json_decode($picking, true);

    $json['code'] = 1;
    $json['msg'] = "Success";

    if(isset($picking_decode['items'][0]['item_code']))
    {
    $json['data']['itemdo'] = $picking_decode['items'][0]['itemdo'];

    $item_code = $picking_decode['items'][0]['item_code'];

    $sql = 'SELECT
            inventory.inventory_code,
            inventory.inventory_name,
            stock.inventory_id,
            stock.pallet_number,
            stock.warehouse_id,
            warehouse.warehouse_name,
            stock.rack_id,
            rack.rack_name,
            stock.room_id,
            room.room_name,
            stock.bay_id,
            bay.bay_name,
            stock.unit,
            stock.quantity,
            stock.batch
          FROM
            stock
          INNER JOIN inventory ON stock.inventory_id = inventory.id
          LEFT JOIN warehouse ON stock.warehouse_id = warehouse.id
          LEFT JOIN rack ON stock.rack_id = rack.id
          LEFT JOIN room ON stock.room_id = room.id
          LEFT JOIN bay ON stock.bay_id = bay.id';
    $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get();
    // $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->where(DB::raw('concat(inventory_code," ",inventory_name, " ", pallet_number)') , 'LIKE' , '%'.$keyword.'%')->get();

    // dd($stock);

    if(count($stock)>0)
    {
        foreach($stock as $key => $stocks)
        {
            $json['data']['detail'][$key]['inventory_id'] = $stocks->inventory_id;
            $json['data']['detail'][$key]['warehouse_name'] = $stocks->warehouse_name;
            $json['data']['detail'][$key]['warehouse_id'] = $stocks->warehouse_id;
            $json['data']['detail'][$key]['room_name'] = $stocks->room_name;
            $json['data']['detail'][$key]['room_id'] = $stocks->room_id;
            $json['data']['detail'][$key]['bay_name'] = $stocks->bay_name;
            $json['data']['detail'][$key]['rack_name'] = $stocks->rack_name;
            $json['data']['detail'][$key]['rack_id'] = $stocks->rack_id;
            $json['data']['detail'][$key]['pallet_number'] = $stocks->pallet_number;
            $json['data']['detail'][$key]['inventory_code'] = $stocks->inventory_code;
            $json['data']['detail'][$key]['inventory_name'] = $stocks->inventory_name;
            $json['data']['detail'][$key]['unit'] = $stocks->unit;
            $json['data']['detail'][$key]['batch'] = $stocks->batch;
            $json['data']['detail'][$key]['stock'] = $stocks->quantity;
        };
      }
    }

    return response()->json($json);
  }



}
