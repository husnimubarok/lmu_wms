<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\StockOpnameMobile;
use App\User;
use App\Model\UserLog;


class StockOpnameController extends  Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function index()
  {
    $sql_header = 'SELECT
                  	inventory.inventory_code,
                  	inventory.inventory_name,
                  	stock_opname_mobile.id,
                  	stock_opname_mobile.unit_of_measure_code,
                  	stock_opname_mobile.plant,
                  	stock_opname_mobile.item_line,
                  	stock_opname_mobile.quantity,
                  	stock_opname_mobile.inventory_id,
                  	warehouse.warehouse_name,
                  	IFNULL(
                  		stock_opname_mobile.warehouse_id,
                  		0
                  	) AS warehouse_id,
                  	IFNULL(stock_opname_mobile.room_id, 0) AS room_id,
                  	IFNULL(stock_opname_mobile.bay_id, 0) AS bay_id,
                  	IFNULL(stock_opname_mobile.rack_id, 0) AS rack_id,
                  	rack.rack_name,
                  	bay.bay_name,
                  	room.room_name,
                  	stock_opname_mobile.pallet_number
                  FROM
                  	stock_opname_mobile
                  LEFT OUTER JOIN warehouse ON stock_opname_mobile.warehouse_id = warehouse.id
                  LEFT OUTER JOIN inventory ON stock_opname_mobile.inventory_id = inventory.id
                  LEFT OUTER JOIN rack ON stock_opname_mobile.rack_id = rack.id
                  LEFT OUTER JOIN bay ON stock_opname_mobile.bay_id = bay.id
                  LEFT OUTER JOIN room ON stock_opname_mobile.room_id = room.id
                  WHERE
                  	stock_opname_mobile.status = 0';
      $data = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();

      if(count($data)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
          foreach($data as $key => $datas)
          {
              $json['data'][$key]['inventory_code'] = $datas->inventory_code;
              $json['data'][$key]['inventory_name'] = $datas->inventory_name;
              $json['data'][$key]['unit_of_measure_code'] = $datas->unit_of_measure_code;
              $json['data'][$key]['plant'] = $datas->plant;
              $json['data'][$key]['item_line'] = $datas->item_line;
              $json['data'][$key]['quantity'] = $datas->quantity;
              $json['data'][$key]['bay_name'] = $datas->bay_name;
              $json['data'][$key]['room_name'] = $datas->room_name;
              $json['data'][$key]['pallet_number'] = $datas->pallet_number;
          };
        }else{
          $json['code'] = 0;
          $json['msg'] = "Success";
          $json['data'] = "[]";
      }

      return response()->json($json);
  }


  public function search_stock(Request $request)
  {

    $keyword = $request['keyword'];

    $sql = 'SELECT
              inventory.inventory_code,
              inventory.inventory_name,
              stock_picking.inventory_id,
              stock_picking.pallet_number,
              stock_picking.warehouse_id,
              warehouse.warehouse_name,
              stock_picking.rack_id,
              rack.rack_name,
              stock_picking.room_id,
              room.room_name,
              stock_picking.bay_id,
              bay.bay_name,
              stock_picking.unit,
              stock_picking.quantity,
              stock_picking.batch
            FROM
              stock_picking
            INNER JOIN inventory ON stock_picking.inventory_id = inventory.id
            LEFT JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
            LEFT JOIN rack ON stock_picking.rack_id = rack.id
            LEFT JOIN room ON stock_picking.room_id = room.id
            LEFT JOIN bay ON stock_picking.bay_id = bay.id
            WHERE stock_picking.quantity > 0';
    // $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get();
    $stock = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->where(DB::raw('concat(inventory_code," ",inventory_name, " ", pallet_number)') , 'LIKE' , '%'.$keyword.'%')->orderBy('batch', 'ASC')->get();

    // dd($stock);

    if(count($stock)>0)
    {
        foreach($stock as $key => $stocks)
        {
            $json[$key]['inventory_id'] = $stocks->inventory_id;
            $json[$key]['warehouse_name'] = $stocks->warehouse_name;
            $json[$key]['warehouse_id'] = $stocks->warehouse_id;
            $json[$key]['room_name'] = $stocks->room_name;
            $json[$key]['room_id'] = $stocks->room_id;
            $json[$key]['bay_name'] = $stocks->bay_name;
            $json[$key]['rack_name'] = $stocks->rack_name;
            $json[$key]['rack_id'] = $stocks->rack_id;
            $json[$key]['pallet_number'] = $stocks->pallet_number;
            $json[$key]['inventory_code'] = $stocks->inventory_code;
            $json[$key]['inventory_name'] = $stocks->inventory_name;
            $json[$key]['unit'] = $stocks->unit;
            $json[$key]['batch'] = $stocks->batch;
            $json[$key]['stock'] = $stocks->quantity;
        };
      }else{
            $json['code'] = 0;
            $json['msg'] = "Stock Not Available";
          };

    return response()->json($json);
  }


  public function post(Request $request)
  {
    // try {
          $data = $request->all();

          


          foreach($data AS $key => $datas)
          {
          //   $post = new StockOpnameMobile;
          //   $post->inventory_id = $datas->inventory_id;
          //   $post->warehouse_id = $datas->warehouse_id;
          //   $post->room_id = $datas->room_id;
          //   $post->bay_id = $datas->bay_id;
          //   $post->pallet_number = $datas->pallet_number;
          //   $post->batch = $datas->batch;
          //   $post->rack_id = $datas->rack_id;
          //   $post->item_line = $datas->item_line;
          //   $post->plant = $datas->plant;
          //   $post->store_loc = $datas->store_loc;
          //   $post->unit_of_measure_code = $datas->unit_of_measure_code;
          //   $post->quantity = $datas->quantity;
          //   $post->created_by = $datas->created_by;
          //   $post->save();
          // };

          // print_r($request['inventory_id']);

          $post = new StockOpnameMobile;
          $post->inventory_id = $data[$key]['inventory_id'];
          $post->warehouse_id = $data[$key]['warehouse_id'];
          $post->room_id = $data[$key]['room_id'];
          $post->pallet_number = $data[$key]['pallet_number'];
          $post->batch = $data[$key]['batch'];
          $post->rack_id = $data[$key]['rack_id'];
          $post->unit_of_measure_code = $data[$key]['unit'];
          $post->quantity = $data[$key]['stock'];
          $post->created_by = $data[$key]['created_by'];
          $post->status = 0;
          $post->save();
          
          };

          $user = User::where('username', $request['created_by'])->first();
          // if(isset($user))
          // {
          //   $user_log = new UserLog;
          //   $user_log->user_id = $user->id;
          //   $user_log->scope = 'STOCK OPNAME';
          //   $user_log->data = json_encode([
          //                         'action' => 'create',
          //                         'transaction_number' => 'Temporary',
          //                         'source' => 'Mobile'
          //                     ]);
          //   $user_log->save();
          // };

          return response()->json([
            'code' => 1,
            'msg' => 'Success',
            'data' => response()->json($data[0]['inventory_id'])
          ]);
      // } catch (\Exception $e) {
      //   $json['code'] = 0;
      //   $json['msg'] = "Failed";
      // }
  }


  public function sap_stockopname(Request $request)
  {
      $curl = curl_init();
      $data = array(
          'docwms' => $request->docwms,
          'item' => $request->item,
          'zldat' => $request->zldat,
          'bldat' => $request->bldat,
          'plant' => $request->plant,
          'lgort' => $request->lgort,
          'matnr' => $request->matnr,
          'charg' => $request->charg,
          'erfmg' => $request->erfmg,
          'xnull' => $request->xnull,
          'line' => $request->line);

  		curl_setopt_array($curl, array(
                            		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-stockopname",
                            		CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                            		CURLOPT_ENCODING => "",
                            		CURLOPT_MAXREDIRS => 10,
                            		CURLOPT_TIMEOUT => 120,
                            		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            		CURLOPT_CUSTOMREQUEST => "POST",
                          		));

  		$response = curl_exec($curl);
  		$err = curl_error($curl);

  		curl_close($curl);

  		if ($err) {
  		echo "cURL Error #:" . $err;
  		} else {
    		// echo $response;
        $json = json_decode($response);
        return response()->json($json);
  		}
  }


  public function sap_exestockopname(Request $request)
  {
      $curl = curl_init();
      $data = array(
          'notrx' => $request->notrx);

  		curl_setopt_array($curl, array(
                            		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exestockopname",
                            		CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                            		CURLOPT_ENCODING => "",
                            		CURLOPT_MAXREDIRS => 10,
                            		CURLOPT_TIMEOUT => 120,
                            		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            		CURLOPT_CUSTOMREQUEST => "POST",
                          		));

  		$response = curl_exec($curl);
  		$err = curl_error($curl);

  		curl_close($curl);

  		if ($err) {
  		echo "cURL Error #:" . $err;
  		} else {
    		// echo $response;
        $json = json_decode($response);
        return response()->json($json);
  		}
  }

}
