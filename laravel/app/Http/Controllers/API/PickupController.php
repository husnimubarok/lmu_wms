<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Model\InventoryReceive; 
use App\Model\InventoryReceiveDetail; 
use App\Model\PutawayReceive; 
use App\Model\warehouse;
use App\Model\Rack;
use App\Model\UserLog;
use App\User;
use Auth;


class PutawayReceiveController extends  Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }


  public function get_pallet()
  {
    $sql_header = 'SELECT
                  inventory_receive.id,
                  inventory_receive.pallet_number,
                  inventory_receive.putaway_status,
                  room.room_name,
                  rack.rack_name
                  FROM
                  inventory_receive
                  INNER JOIN room ON inventory_receive.room_id = room.id
                  INNER JOIN rack ON inventory_receive.rack_id = rack.id
                  WHERE inventory_receive.putaway_status = 0 OR inventory_receive.putaway_status is null';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->orderBy('putaway_status', 'ASC')->get(); 
      
      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success"; 
          foreach($data_header as $key => $data_headers)
          { 
              $json['data'][$key]['id'] = $data_headers->id;
              $json['data'][$key]['pallet_number'] = $data_headers->pallet_number;   
              $json['data'][$key]['putaway_status'] = $data_headers->putaway_status;   
              $json['data'][$key]['room_name'] = $data_headers->room_name;   
              $json['data'][$key]['rack_name'] = $data_headers->rack_name;    
          };
        }else{
          $json['code'] = 0;
          $json['msg'] = "Failed";
      }

      return response()->json($json);
  } 

  public function get_pallet_search(Request $request)
  {
    $keyword = $request['keyword'];
    $sql_header = 'SELECT
                  inventory_receive.id,
                  inventory_receive.pallet_number,
                  inventory_receive.putaway_status,
                  room.room_name,
                  rack.rack_name
                  FROM
                  inventory_receive
                  INNER JOIN room ON inventory_receive.room_id = room.id
                  INNER JOIN rack ON inventory_receive.rack_id = rack.id
                  WHERE
                  inventory_receive.putaway_status = 0 OR inventory_receive.putaway_status IS NULL'; 

      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->where(DB::raw('pallet_number') , 'LIKE' , '%'.$keyword.'%')->get();
      
      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success"; 
          foreach($data_header as $key => $data_headers)
          { 
              $json['data'][$key]['id'] = $data_headers->id;
              $json['data'][$key]['pallet_number'] = $data_headers->pallet_number;   
              $json['data'][$key]['putaway_status'] = $data_headers->putaway_status;   
              $json['data'][$key]['room_name'] = $data_headers->room_name;   
              $json['data'][$key]['rack_name'] = $data_headers->rack_name;    
          };
        }else{
          $json['code'] = 0;
          $json['msg'] = "Failed";
      }

      return response()->json($json);
  } 
 

  public function confirm($id, Request $request)
  {
    try{
      DB::update("UPDATE inventory_receive SET putaway_status = 1 WHERE id = ".$id.""); 
      $json['code'] = 1;
      $json['msg'] = "Success";

      // log
      $user = User::where('username', $request['created_by'])->first();
      if(isset($user))
      {
        $inventory_receive = InventoryReceive::where('id', $id)->first();

        if(isset($inventory_receive))
        {
          // $userid= Auth::id();
          $user_log = new UserLog;
          $user_log->user_id = $user->id;
          $user_log->scope = 'PICKUP';
          $user_log->data = json_encode([
                                'action' => 'post',
                                'description' => 'Confirm Pickup',
                                'transaction_number' => $inventory_receive->pallet_number,
                                'source' => 'Mobile',
                                'inventory_receive_id' => $id
                            ]);
          $user_log->save();
        };
      };

    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);
    
  }

  public function submit($id, Request $request)
  {
    $rack_name = $request['rack_name'];

    $sql_header = 'SELECT
                  inventory_receive.rack_id,
                  rack.rack_name
                  FROM
                  inventory_receive
                  INNER JOIN rack ON inventory_receive.rack_id = rack.id
                  WHERE rack.rack_name = "'.$rack_name.'"';
    $rack = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();
    
    if(isset($rack)){
      try{

        DB::update("UPDATE inventory_receive SET putaway_status = 2 WHERE rack_id = ".$rack->rack_id." AND id=".$id."");
        $json['code'] = 1;
        $json['msg'] = "Success";

      } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
        }
      }else{
        $json['code'] = 0;
          $json['msg'] = "Failed";
      }


    $user = User::where('username', $request['created_by'])->first();
    if(isset($user))
    {
        // $userid= Auth::id();
        $user_log = new UserLog;
        $user_log->user_id = $user->id;
        $user_log->scope = 'PICKUP';
        $user_log->data = json_encode([
                              'action' => 'post',
                              'description' => 'Submit Pickup',
                              'transaction_number' => $rack->rack_name,
                              'source' => 'Mobile',
                              'inventory_receive_id' => $id
                          ]);
        $user_log->save();
    };
    
    return response()->json($json);
  }


}
