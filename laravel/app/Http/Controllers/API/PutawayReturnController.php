<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SalesReturn;
use App\Model\SalesReturnDetail;
use App\Model\PutawayReceive;
use App\Model\warehouse;
use App\Model\Rack;
use App\Model\UserLog;
use App\User;
use Auth;

class PutawayReturnController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function get_pallet()
  {
    $sql_header = 'SELECT
                    sales_return.id,
                    sales_return.pallet_number,
                    sales_return.putaway_status,
                    sales_return.room_id,
                    room.room_name,
                    sales_return.rack_id,
                    rack.rack_name,
                    sales_return.warehouse_id,
                    warehouse.warehouse_name,
                    sales_return.celong,
                    sales_return.broken,
                    sales_return.unpallet
                  FROM
                    sales_return
                  LEFT JOIN room ON sales_return.room_id = room.id
                  LEFT JOIN rack ON sales_return.rack_id = rack.id
                  LEFT JOIN warehouse ON sales_return.warehouse_id = warehouse.id
                  WHERE sales_return.putaway_status = 0 OR sales_return.putaway_status is null';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->orderBy('unpallet', 'DESC')->get();

      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
          foreach($data_header as $key => $data_headers)
          {
              $json['data'][$key]['id'] = $data_headers->id;
              $json['data'][$key]['pallet_number'] = $data_headers->pallet_number;
              $json['data'][$key]['putaway_status'] = $data_headers->putaway_status;
              $json['data'][$key]['warehouse_name'] = $data_headers->warehouse_name;
              $json['data'][$key]['room_name'] = $data_headers->room_name;
              $json['data'][$key]['rack_name'] = $data_headers->rack_name;
              $json['data'][$key]['celong'] = $data_headers->celong;
              $json['data'][$key]['broken'] = $data_headers->broken;
              $json['data'][$key]['unpallet'] = $data_headers->unpallet;
          };
        }else{
          $json['code'] = 0;
          $json['msg'] = "Failed";
      }

      return response()->json($json);
  }

  public function get_pallet_search(Request $request)
  {
    $keyword = $request['keyword'];
    $sql_header = 'SELECT
                    sales_return.id,
                    sales_return.pallet_number,
                    sales_return.putaway_status,
                    sales_return.room_id,
                    room.room_name,
                    sales_return.rack_id,
                    rack.rack_name,
                    sales_return.bay_id,
                    bay.bay_name,
                    sales_return.warehouse_id,
                    warehouse.warehouse_name,
                    sales_return.celong,
                    sales_return.broken,
                    sales_return.unpallet
                  FROM
                    sales_return
                  LEFT JOIN room ON sales_return.room_id = room.id
                  LEFT JOIN bay ON sales_return.bay_id = bay.id
                  LEFT JOIN rack ON sales_return.rack_id = rack.id
                  LEFT JOIN warehouse ON sales_return.warehouse_id = warehouse.id
                  WHERE sales_return.putaway_status = 0 OR sales_return.putaway_status is null';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->orderBy('unpallet', 'DESC')->where(DB::raw('pallet_number') , 'LIKE' , '%'.$keyword.'%')->get();

      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
          foreach($data_header as $key => $data_headers)
          {
              $json['data'][$key]['id'] = $data_headers->id;
              $json['data'][$key]['pallet_number'] = $data_headers->pallet_number;
              $json['data'][$key]['putaway_status'] = $data_headers->putaway_status;
              $json['data'][$key]['warehouse_name'] = $data_headers->warehouse_name;
              $json['data'][$key]['room_name'] = $data_headers->room_name;
              $json['data'][$key]['bay_name'] = $data_headers->bay_name;
              $json['data'][$key]['rack_name'] = $data_headers->rack_name;
              $json['data'][$key]['celong'] = $data_headers->celong;
              $json['data'][$key]['broken'] = $data_headers->broken;
              $json['data'][$key]['unpallet'] = $data_headers->unpallet;
          };
        }else{
          $json['code'] = 0;
          $json['msg'] = "Failed";
      }

      return response()->json($json);
  }

  public function confirm($id, Request $request)
  {
    try{
    DB::update("UPDATE sales_return SET putaway_status = 1 WHERE id = ".$id."");
    $json['code'] = 1;
      $json['msg'] = "Success";

    $user = User::where('username', $request['created_by'])->first();
    if(isset($user))
    {
      // $userid= Auth::id();
      $user_log = new UserLog;
      $user_log->user_id = $user->id;
      $user_log->scope = 'PUTAWAY RETURN';
      $user_log->data = json_encode([
                            'action' => 'confirm',
                            'description' => 'Confirm Put Away Return',
                            'transaction_number' => '-',
                            'source' => 'Mobile',
                            'put_away_id' => $id
                        ]);
      $user_log->save();
    };


    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);
  }


  public function submit($id, Request $request)
  {
    $rack_name = $request['rack_name'];

    $sql_header = 'SELECT
                  sales_return.rack_id,
                  rack.rack_name
                  FROM
                  sales_return
                  INNER JOIN rack ON sales_return.rack_id = rack.id
                  WHERE rack.rack_name = "'.$rack_name.'"';
    $rack = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

    $user = User::where('username', $request['created_by'])->first();
    if(isset($user))
    {
      $user_log = new UserLog;
      $user_log->user_id = $user->id;
      $user_log->scope = 'PUTAWAY RETURN';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'description' => $rack->rack_name,
                            'transaction_number' => '-',
                            'source' => 'Mobile'
                        ]);
      $user_log->save();
    };

    if(isset($rack)){
      try{
      DB::update("UPDATE sales_return SET putaway_status = 2 WHERE rack_id = ".$rack->rack_id." AND id=".$id."");
      $json['code'] = 1;
        $json['msg'] = "Success";

      } catch (\Exception $e) {
          $json['code'] = 0;
          $json['msg'] = "Failed";
        }
      }else{
        $json['code'] = 0;
          $json['msg'] = "Failed";
      }
    return response()->json($json);
  }

}
