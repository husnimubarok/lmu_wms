<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SalesReturn;
use App\Model\SalesReturnDetail;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;
use App\User;
use App\Model\UserLog;
use App\Model\Inventory;


class SalesReturnController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function post(Request $request)
  {
    // try {
        // $unpallet = $request['unpallet'];
        // if($unpallet == 0)
        // {
        //     $pallet_number = $request['pallet_number'];
        // }else{
        //     $pallet_number = $request['pallet_number'];
        // };
        
          $pallet_exist = SalesReturn::where('pallet_number', $request['pallet_number'])->first();

          if(isset($pallet_exist))
          {
            return response()->json([
              'code' => 0,
              'msg' => 'Pallet number already exists',
              'data' => response()->json($request->all())
            ]);

          }else{ 
            $post_header = new SalesReturn;
            $post_header->pallet_number = $request['pallet_number'];
            $post_header->warehouse_id = 2;
            $post_header->room_id = $request['room_id'];
            $post_header->bay_id = $request['bay_id'];
            $post_header->rack_id = $request['rack_id'];
            $post_header->celong = $request['celong'];
            $post_header->broken = $request['broken'];
            $post_header->unpallet = $request['unpallet'];
            $post_header->spm = $request['spm'];
            $post_header->created_by = $request['created_by'];
            $post_header->save();

            // $url = 'https://recruit.zoho.com/ats/EmbedResult.hr?jodigest=2cV.Sr2As6VxhLMxQGuTNij*g.Fb3J7ysduDs.AC9sU-&atslocale=en_GB&rawdata=json';
            // $JSON = file_get_contents($url);

            // echo the JSON (you can echo this to JavaScript to use it there)
            // dd($JSON);

            // $data_detail = SalesReturnDetail::where('do_number', $request['do_number'])->get();
            $cont_detail = $request['count_detail'];
            // dd($cont_detail);
            //   for ($i = 1; $i <= 10; $i++) {
            //     echo $i;
            // }

            for ($i = 1; $i <= $cont_detail; $i++) {
                // dd($i++);
                // print($i+1);
                // echo $i;
              // object
                  $inventory_code = $request['inventory_code_'.$i];
                  $quantity = $request['quantity_'.$i];
                  $item_line = $request['item_line_'.$i];
                  $store_loc = $request['store_loc_'.$i];

                  $inventory = Inventory::where('inventory_code', $inventory_code)->first();

                  // action
                  $post_detail = new SalesReturnDetail;
                  $post_detail->transaction_id = $post_header->id;
                  $post_detail->inventory_id = $inventory->id;
                  // $post_detail->inventory_code = $inventory->inventory_code;
                  $post_detail->inventory_code = $request['inventory_code_'.$i];
                  $post_detail->uom = $inventory->unit_of_measure;
                  $post_detail->quantity = $request['quantity_'.$i];
                  $post_detail->item_line = $request['item_line_'.$i];
                  $post_detail->store_loc = $request['store_loc_'.$i];
                  $post_detail->plant = $request['plant'];
                  $post_detail->do_number = $request['do_number'];
                  $post_detail->doc_date = $request['doc_date'];
                  $post_detail->save();

            };

            $user = User::where('username', $request['created_by'])->first();
            if(isset($user))
            {
              $user_log = new UserLog;
              $user_log->user_id = $user->id;
              $user_log->scope = 'SALES RETURN';
              $user_log->data = json_encode([
                                    'action' => 'create',
                                    'description' => 'Tidak ada keterangan',
                                    'transaction_number' => $request['pallet_number'],
                                    'source' => 'Mobile'
                                ]);
              $user_log->save();
            };
            return response()->json([
              'code' => 1,
              'msg' => 'Success',
              'data' => response()->json($request->all())
            ]);
          };

          // } catch (\Exception $e) {
          //   $json['code'] = 0;
          //   $json['msg'] = "Failed";
          // }
      return response()->json($json);

    }



    public function post_manual(Request $request)
    {
      try {
            // $unpallet = $request['unpallet'];
            // if($unpallet == 0)
            // {
            //     $pallet_number = $request['pallet_number'];
            // }else{
            //     $pallet_number = $request['pallet_number'];
            // };

            $post_header = new SalesReturn;
            $post_header->pallet_number = $request['pallet_number'];
            $post_header->warehouse_id = 2;
            $post_header->room_id = $request['room_id'];
            $post_header->bay_id = $request['bay_id'];
            $post_header->rack_id = $request['rack_id'];
            $post_header->celong = $request['celong'];
            $post_header->broken = $request['broken'];
            $post_header->unpallet = $request['unpallet'];
            $post_header->spm = $request['spm'];
            $post_header->created_by = $request['created_by'];
            $post_header->manual = 1;
            $post_header->save();

            // $url = 'https://recruit.zoho.com/ats/EmbedResult.hr?jodigest=2cV.Sr2As6VxhLMxQGuTNij*g.Fb3J7ysduDs.AC9sU-&atslocale=en_GB&rawdata=json';
            // $JSON = file_get_contents($url);

            // echo the JSON (you can echo this to JavaScript to use it there)
            // dd($JSON);

            // $data_detail = SalesReturnDetail::where('do_number', $request['do_number'])->get();
            $cont_detail = $request['count_detail'];
            // dd($cont_detail);
          //   for ($i = 1; $i <= 10; $i++) {
          //     echo $i;
          // }

            for ($i = 1; $i <= $cont_detail; $i++) {
                // dd($i++);
                // print($i+1);
                // echo $i;
              // object
                  $inventory_code = $request['inventory_code_'.$i];
                  $quantity = $request['quantity_'.$i];
                  $item_line = $request['item_line_'.$i];
                  $store_loc = $request['store_loc_'.$i];

                  $inventory = Inventory::where('inventory_code', $inventory_code)->first();

                  // action
                  $post_detail = new SalesReturnDetail;
                  $post_detail->transaction_id = $post_header->id;
                  $post_detail->inventory_id = $inventory->id;
                  // $post_detail->inventory_code = $inventory->inventory_code;
                  $post_detail->inventory_code = $request['inventory_code_'.$i];
                  $post_detail->uom = $inventory->unit_of_measure;
                  $post_detail->quantity = $request['quantity_'.$i];
                  $post_detail->item_line = $request['item_line_'.$i];
                  $post_detail->store_loc = $request['store_loc_'.$i];
                  $post_detail->plant = $request['plant'];
                  $post_detail->do_number = $request['do_number'];
                  $post_detail->doc_date = $request['doc_date'];
                  $post_detail->save();

            };

            $user = User::where('username', $request['created_by'])->first();
            if(isset($user))
            {
              $user_log = new UserLog;
              $user_log->user_id = $user->id;
              $user_log->scope = 'SALES RETURN';
              $user_log->data = json_encode([
                                    'action' => 'create',
                                    'description' => 'Tidak ada keterangan',
                                    'transaction_number' => $request['pallet_number'],
                                    'source' => 'Mobile'
                                ]);
              $user_log->save();
            };
                return response()->json([
                  'code' => 1,
                  'msg' => 'Success',
                  'data' => response()->json($request->all())
                ]);

              } catch (\Exception $e) {
                $json['code'] = 0;
                $json['msg'] = "Failed";
              }
          return response()->json($json);

        }



  }
