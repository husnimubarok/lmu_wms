<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Period; 


class RackController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function index()
  { 
    $rack = Period::all(); 
    for($i = 0; $i < $rack->count(); $i++)
    {
      $json[$i] = 
      [ 
      'userId' => $rack[$i]->id, 
      'id' => $rack[$i]->id, 
      'title' => 'danger', 
      'body' => 'success',  
      ];
    } 
    return response()->json($json);
  } 

  public function get_pallet(Request $request, $id)
  { 
    // $pallet_number = $request['pallet_number'];

    $sql_header = 'SELECT
                      stock.id,
                      inventory.id AS inventory_id,
                      inventory.inventory_code,
                      inventory.inventory_name,
                      stock.pallet_number,
                      stock.quantity,
                      rack.rack_name,
                      stock.rack_id,
                      warehouse.warehouse_name,
                      room.room_name,
                      stock.warehouse_id,
                      stock.room_id
                    FROM
                      stock
                    INNER JOIN inventory ON stock.inventory_id = inventory.id
                    LEFT JOIN rack ON stock.rack_id = rack.id
                    LEFT JOIN warehouse ON stock.warehouse_id = warehouse.id
                    LEFT JOIN room ON stock.room_id = room.id
                    WHERE stock.pallet_number = "'.$id.'"';
    $stock = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get(); 

    if(count($stock)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success"; 
        foreach($stock as $key => $stocks)
        { 
            $json['data'][$key]['inventory_id_key'] = 'inventory_id_'.$stocks->inventory_id;   
            $json['data'][$key]['quantity_key'] = 'quantity_'.$stocks->id;   
            $json['data'][$key]['warehouse_id'] = $stocks->warehouse_id;
            $json['data'][$key]['warehouse_name'] = $stocks->warehouse_name; 
            $json['data'][$key]['room_id'] = $stocks->room_id;
            $json['data'][$key]['room_name'] = $stocks->room_name;
            $json['data'][$key]['rack_id'] = $stocks->rack_id;
            $json['data'][$key]['rack_name'] = $stocks->rack_name;
            $json['data'][$key]['inventory_code'] = $stocks->inventory_code;
            $json['data'][$key]['inventory_name'] = $stocks->inventory_name;   
            $json['data'][$key]['stock'] = $stocks->quantity;    
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  } 
}
