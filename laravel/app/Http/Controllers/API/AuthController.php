<?php
namespace App\Http\Controllers\API;

use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Users;
use App\Model\Permission;
use App\Model\UserMobile;
use App\Model\UserLog;
use App\Model\User;
use Auth;
use App\Http\Controllers\Redirect;
use Session;
use Hash;

class AuthController extends Controller
{

  public function login(Request $request)
  {
    $username = $request['username'];
    $password = $request['password'];
    // $password = md5($request['password']);



    // $auth = Users::Where('username', $username)->Where('password', $password)->Where('type_name', 'Mobile')->first();

    // dd($auth);

    if(Auth::attempt(array('username' => $username, 'password' => $password))){

        $sql = 'SELECT
              users.id,
              users.username,
              users.`password`,
              users.first_name,
              users.last_name,
              users.type_name,
              roles.`name`,
              roles.description,
              roles.id AS level_role
            FROM
              users
            INNER JOIN roles ON users.role_id = roles.id ';
          $auth = DB::table(DB::raw("($sql) as rs_sql"))->Where('username', $username)->Where('type_name', 'Mobile')->first();

        $json['code'] = 1;
        $json['msg'] = "Success";
        $json['data'] =
              [
              'id' => $auth->id,
              'username' => $auth->username,
              'first_name' => $auth->first_name,
              'last_name' => $auth->last_name
              ];

        $sql_permission = 'SELECT
                  permission.`name`,
                  users.id
                FROM
                  users
                INNER JOIN roles ON users.role_id = roles.id
                INNER JOIN role_permission ON roles.id = role_permission.role_id
                INNER JOIN permission ON role_permission.permission_id = permission.id ';
        $permission = DB::table(DB::raw("($sql_permission) as rs_sql_permission"))->Where('id', $auth->id)->get();

      foreach($permission as $permissions)
      {
        $menu[] = $permissions->name;
      }

      $json['data']['role'] =
      [
        'level' => $auth->level_role,
        'status' => $auth->name,
        'menu' => $menu
      ];

      $user_log = new UserLog;
      $user_log->user_id = $auth->id;
      $user_log->scope = 'LOGIN';
      $user_log->data = json_encode([
                            'action' => 'login',
                            'transaction_number' => '-',
                            'source' => 'Mobile'
                        ]);
      $user_log->save();

      return response()->json($json);
    }else
    {
      $json['code'] = 0;
      $json['msg'] = "Failed";
      return response()->json($json);
    }

  }

  public function change_password(Request $request)
  {
      try {
            $username = $request['username'];
            $password = $request['password'];

            $user = User::where('username', $username)->first();
            $user->password = Hash::make($password);
            $user->save();

            return response()->json([
              'code' => 1,
              'msg' => 'Success',
              'data' => response()->json($request->all())
            ]);

            $user_log = new UserLog;
            $user_log->user_id = $auth->id;
            $user_log->scope = 'Change Password';
            $user_log->data = json_encode([
                                  'action' => 'login',
                                  'transaction_number' => '-',
                                  'source' => 'Mobile'
                              ]);
            $user_log->save();
      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
   }


}
