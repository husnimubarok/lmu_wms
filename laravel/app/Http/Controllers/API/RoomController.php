<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Warehouse; 
use App\Model\Rack; 
use App\Model\Room; 


class RoomController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function index(Request $request)
  { 
    $warehouse_id = $request['warehouse_id'];

    $sql_header = 'SELECT
                      room.id AS room_id,
                      room.warehouse_id,
                      room.room_name,
                      rack.id AS rack_id,
                      rack.rack_code,
                      rack.rack_name,
                      rack.description
                    FROM
                      room
                    LEFT JOIN rack ON room.id = rack.room_id 
                    WHERE room.warehouse_id = '.$warehouse_id.'';
    $warehouse = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get(); 

    if(count($warehouse)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success"; 
        foreach($warehouse as $key => $warehouses)
        { 
            $json['data'][$key]['room_id'] = $warehouses->room_id;
            $json['data'][$key]['rack_id'] = $warehouses->rack_id;   
            $json['data'][$key]['warehouse_id'] = $warehouses->warehouse_id;     
            $json['data'][$key]['room_name'] = $warehouses->room_name;      
            $json['data'][$key]['rack_name'] = $warehouses->rack_name;   
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  } 

  public function rack_index(Request $request)
  { 
    $rack_name = $request['rack_name'];

    $sql_header = 'SELECT
                      room.id AS room_id,
                      room.room_name,
                      rack.id AS rack_id,
                      rack.rack_code,
                      rack.rack_name
                    FROM
                      room
                    LEFT JOIN rack ON room.id = rack.room_id
                    WHERE
                      rack.rack_name ="'.$rack_name.'"';
    $warehouse = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get(); 

    if(count($warehouse)>0)
    {
      $json['code'] = 1;
      $json['msg'] = "Success"; 
        foreach($warehouse as $key => $warehouses)
        { 
            $json['data'][$key]['room_id'] = $warehouses->room_id;
            $json['data'][$key]['rack_id'] = $warehouses->rack_id;        
            $json['data'][$key]['room_name'] = $warehouses->room_name;      
            $json['data'][$key]['rack_name'] = $warehouses->rack_name;   
        };
      }else{
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }

    return response()->json($json);
  } 
 
}

 