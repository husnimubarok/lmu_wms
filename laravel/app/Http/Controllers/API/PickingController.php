<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryReceive;
use App\Model\InventoryReceiveDetail;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\Warehouse;
use App\Model\Rack;
use App\Model\Room;
use App\Model\Picking;
use App\Model\PickingDetail;
use App\Model\Stock;
use App\Model\ViewStock;
use App\Model\Inventory;
use App\User;
use App\Model\UserLog;


class PickingController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }

  public function get_picking_list($id)
  {
    try {

      // $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
      // $picking_decode = json_decode($picking, true);

      // --------- new ------------

      $curl = curl_init();
      $data = array( 
                      'nodo' => $id
                    );

      curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-getlistreturndo",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                              ));

      $response = curl_exec($curl);
      $picking_decode = json_decode($response, true);
      $err = curl_error($curl);

      curl_close($curl);

      // ------ close new ------------

      $json['code'] = 1;
      $json['msg'] = "Success";

      if(isset($picking_decode['T_DELPICK'][0]['VBELN']))
      {
        $json['data']['do_no'] = $picking_decode['T_DELPICK'][0]['VBELN'];
        $json['data']['doc_date'] = $picking_decode['T_DELPICK'][0]['BLDAT'];
        $json['data']['customer'] = $picking_decode['T_DELPICK'][0]['SOLT_NAME'];
        $json['data']['marketing'] = $picking_decode['T_DELPICK'][0]['MARKETING'];

        foreach ($picking_decode['T_DELPICK'] as $field => $value) {
          $json['data']['detail'][$field]['itemdo'] = $value['POSNR'];
          $json['data']['detail'][$field]['item_code'] = $value['MATNR'];
          $json['data']['detail'][$field]['item_name'] = $value['ARKTX'];
          $json['data']['detail'][$field]['qty'] = $value['QUANTITY'];
        };
      }else{
          $json['data'] = [];
      };

      return response()->json($json);

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }

  public function get_picking_detail($id)
  {
    try {

      // $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
      // $picking_decode = json_decode($picking, true);


      // --------- new ------------

      $curl = curl_init();
      $data = array( 
                      'nodo' => $id
                    );

      curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-getlistreturndo",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                              ));

      $response = curl_exec($curl);
      $picking_decode = json_decode($response, true);
      $err = curl_error($curl);

      curl_close($curl);

      // ------ close new ------------

      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];

      if(isset($picking_decode['T_DELPICK'][0]['VBELN']))
      {
        $json['data']['do_no'] = $picking_decode['T_DELPICK'][0]['VBELN'];
        $json['data']['doc_date'] = $picking_decode['T_DELPICK'][0]['BLDAT'];
        $json['data']['customer'] = $picking_decode['T_DELPICK'][0]['SOLT_NAME'];
        $json['data']['marketing'] = $picking_decode['T_DELPICK'][0]['MARKETING'];

        foreach ($picking_decode['T_DELPICK'] as $field => $value) {
          $json['data']['detail'][$field]['itemdo'] = $value['POSNR'];
          $json['data']['detail'][$field]['item_code'] = $value['MATNR'];
          $json['data']['detail'][$field]['item_name'] = $value['ARKTX'];
          $json['data']['detail'][$field]['qty'] = $value['QUANTITY'];

          // dd($value['MATNR']);

          $sql = 'SELECT
                    inventory.inventory_code,
                    warehouse.warehouse_name,
                    stock_picking.pallet_number,
                    room.room_name,
                    rack.rack_name,
                    bay.bay_name,
                    stock_picking.unit,
                    stock_picking.quantity,
                    stock_picking.warehouse_id,
                    stock_picking.rack_id,
                    stock_picking.room_id,
                    stock_picking.batch,
                    inventory.inventory_name,
                    inventory.id AS inventory_id,
                    IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack,
                    IFNULL(inventory.volume, 1) AS volume_inventory,
                    (
                      stock_picking.quantity * IFNULL(inventory.volume, 1)
                    ) / IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack_avail
                  FROM
                    inventory
                  INNER JOIN stock_picking ON inventory.id = stock_picking.inventory_id
                  INNER JOIN warehouse ON stock_picking.warehouse_id = warehouse.id
                  INNER JOIN room ON stock_picking.room_id = room.id
                  INNER JOIN rack ON stock_picking.rack_id = rack.id
                  LEFT OUTER JOIN bay ON stock_picking.bay_id = bay.id
                  WHERE
                    stock_picking.quantity > 0 -- AND room.warehouse_id = 1
                  AND warehouse.hidden_picking != 1
                  AND rack.hide_rack != 1
                  AND stock_picking.pallet_number NOT IN (
                    SELECT
                      inventory_receive.pallet_number AS pallet_number
                    FROM
                      inventory_receive
                    WHERE
                      inventory_receive.branch = 1
                    GROUP BY
                      inventory_receive.pallet_number
                  )
                  AND stock_picking.pallet_number NOT IN (
                    SELECT
                      stock_movement_detail.pallet_number_to AS pallet_number
                    FROM
                      stock_movement_detail
                    WHERE
                      stock_movement_detail.flag = 1
                      AND stock_movement_detail.pallet_number_to IS NOT NULL
                    GROUP BY
                      stock_movement_detail.pallet_number_to
                  )
                  AND stock_picking.pallet_number NOT IN (
                    SELECT
                      stock_flag.pallet_number AS pallet_number
                    FROM
                      stock_flag
                    GROUP BY
                      stock_flag.pallet_number
                  )
                  AND inventory.inventory_code = '.$value['MATNR'].'';
          $stock = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->orderBy('pallet_number', 'ASC')->get();

          // dd($stock);

          if(count($stock)>0){
            $total_qty_stock = 0;
            $total_qty_stock_last = 0;
            $total_qty_pick = 0;
            foreach ($stock as $d_key => $stocks)
            {
                $total_qty_stock += $stocks->quantity;
                $total_qty_stock_last += $total_qty_stock- $stocks->quantity;

                // print_r($total_qty_stock_last);

                $total_qty_pick = $value['QUANTITY'];

                // print_r($total_qty_pick);

                if(($total_qty_stock - $stocks->quantity) < $total_qty_pick)
                {
                   // $volume_rack = $stocks->volume_rack;

                  // $volume_inventory = $stock->volume_inventory;
                  // $quantity = $stock->quantity;
                  // $total_volume_inventory = $volume_inventory * $quantity;

                  // $presentage_volume_rack_avail = $volume_inventory / $volume_rack * 100;

                  $json['data']['detail'][$field]['stock'][$d_key]['inventory_id'] = $stocks->inventory_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['warehouse_name'] = $stocks->warehouse_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['warehouse_id'] = $stocks->warehouse_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['room_name'] = $stocks->room_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['room_id'] = $stocks->room_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['bay_name'] = $stocks->bay_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['rack_name'] = $stocks->rack_name;
                  $json['data']['detail'][$field]['stock'][$d_key]['rack_id'] = $stocks->rack_id;
                  $json['data']['detail'][$field]['stock'][$d_key]['pallet_number'] = $stocks->pallet_number;
                  $json['data']['detail'][$field]['stock'][$d_key]['unit'] = $stocks->unit;
                  $json['data']['detail'][$field]['stock'][$d_key]['batch'] = $stocks->batch;
                  $json['data']['detail'][$field]['stock'][$d_key]['stock'] = $stocks->quantity;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_stock_last'] = $total_qty_stock_last;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_stock'] = $total_qty_stock;
                  $json['data']['detail'][$field]['stock'][$d_key]['total_qty_pick'] = $total_qty_pick;
                  // $json['data']['detail'][$field]['stock'][$d_key]['presentage_volume_rack_avail'] = $stocks->presentage_volume_rack_avail;
                };

            }

          }else{
            // $json['data']['detail'][$field]['stock']  = 0;
            $json['data']['detail'][$field]['stock'][0]['inventory_id']  = 0;
            $json['data']['detail'][$field]['stock'][0]['warehouse_name'] = "0";
            $json['data']['detail'][$field]['stock'][0]['warehouse_id']  = 0;
            $json['data']['detail'][$field]['stock'][0]['room_name']  = "0";
            $json['data']['detail'][$field]['stock'][0]['room_id']  = 0;
            $json['data']['detail'][$field]['stock'][0]['bay_name']  = 0;
            $json['data']['detail'][$field]['stock'][0]['rack_name']  = "0";
            $json['data']['detail'][$field]['stock'][0]['rack_id']  = 0;
            $json['data']['detail'][$field]['stock'][0]['pallet_number']  = "0";
            $json['data']['detail'][$field]['stock'][0]['unit']  = "0";
            $json['data']['detail'][$field]['stock'][0]['batch']  = "0";
            $json['data']['detail'][$field]['stock'][0]['stock']  = 0;
            $json['data']['detail'][$field]['stock'][0]['total_qty_stock_last']  = 0;
            $json['data']['detail'][$field]['stock'][0]['total_qty_stock']  = 0;
            $json['data']['detail'][$field]['stock'][0]['total_qty_pick']  = 0;
        };
        };
      }
      // }else{
      //     $json['stock'] = [];
      // };

      return response()->json($json);

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
        $json['data'] = [];
      }
  }

  // public function get_picking_detail($id)
  // {
  //   try {

  //     $picking = file_get_contents('http://webapp.lmu.co.id:3000/m/v1/wms/picking/'.$id.'');
  //     $picking_decode = json_decode($picking, true);

  //     $json['code'] = 1;
  //     $json['msg'] = "Success";
  //     $json['data'] = [];

  //     if(isset($picking_decode['items'][0]['no_do']))
  //     {
  //       $json['data']['do_no'] = $picking_decode['items'][0]['no_do'];
  //       $json['data']['doc_date'] = $picking_decode['items'][0]['doc_date'];

  //       foreach ($picking_decode['items'] as $field => $value) {
  //         $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
  //         $json['data']['detail'][$field]['item_code'] = $value['item_code'];
  //         $json['data']['detail'][$field]['item_name'] = $value['item_name'];
  //         $json['data']['detail'][$field]['qty'] = $value['qty'];


  //         $sql = 'SELECT
  //                   inventory.inventory_code,
  //                   warehouse.warehouse_name,
  //                   stock.pallet_number,
  //                   room.room_name,
  //                   rack.rack_name,
  //                   bay.bay_name,
  //                   stock.unit,
  //                   stock.quantity,
  //                   stock.warehouse_id,
  //                   stock.rack_id,
  //                   stock.room_id,
  //                   inventory.inventory_name,
  //                   inventory.id AS inventory_id
  //                 FROM
  //                   inventory
  //                 INNER JOIN stock ON inventory.id = stock.inventory_id
  //                 INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
  //                 INNER JOIN room ON stock.room_id = room.id
  //                 INNER JOIN rack ON stock.rack_id = rack.id
  //                 LEFT JOIN bay ON stock.bay_id = bay.id
  //                 WHERE
  //                 inventory.inventory_code = '.$value['item_code'].'';
  //         $stock = DB::table(DB::raw("($sql) as rs_sql"))->get();

  //         // dd($stock);

  //         if(count($stock)>0){
  //           foreach ($stock as $d_key => $stocks)
  //           {
  //               $json['data']['detail'][$field]['stock'][$d_key]['inventory_id'] = $stocks->inventory_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['warehouse_name'] = $stocks->warehouse_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['warehouse_id'] = $stocks->warehouse_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['room_name'] = $stocks->room_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['room_id'] = $stocks->room_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['bay_name'] = $stocks->bay_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['rack_name'] = $stocks->rack_name;
  //               $json['data']['detail'][$field]['stock'][$d_key]['rack_id'] = $stocks->rack_id;
  //               $json['data']['detail'][$field]['stock'][$d_key]['pallet_number'] = $stocks->pallet_number;
  //               $json['data']['detail'][$field]['stock'][$d_key]['stock'] = $stocks->quantity;
  //           }
  //         }else{
  //           $json['data']['detail'][$field]['stock'] = [];
  //         };

  //       };
  //     }
  //     // }else{
  //     //     $json['stock'] = [];
  //     // };

  //     return response()->json($json);

  //     } catch (\Exception $e) {
  //       $json['code'] = 0;
  //       $json['msg'] = "Failed";
  //       $json['data'] = [];
  //     }
  // }

  public function get_pickup_list()
  {

    $sql_header = 'SELECT
                    picking.id,
                    picking.picking_number,
                    picking.doc_date,
                    picking.created_by,
                    picking.pickup_status
                  FROM
                    picking
                  WHERE picking.pickup_status != 3 AND picking.deleted_at IS NULL';
      $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->get();


      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data_header as $h_key => $data_headers) {
            $sql_detail = 'SELECT
                            picking_detail.id,
                            picking_detail.inventory_id,
                            picking_detail.pallet_number,
                            inventory.inventory_code,
                            inventory.inventory_name,
                            picking_detail.pick_qty,
                            picking_detail.batch,
                            room.room_name,
                            rack.rack_name,
                            warehouse.warehouse_name
                          FROM
                            picking_detail
                          INNER JOIN inventory ON picking_detail.inventory_id = inventory.id
                          LEFT JOIN rack ON picking_detail.rack_id = rack.id
                          LEFT JOIN room ON picking_detail.room_id = room.id
                          LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                          WHERE picking_detail.transaction_id='.$data_headers->id.' AND picking_detail.stock_qty > 0';

            $data_detail = DB::table(DB::raw("(" . $sql_detail . ") as rs_sql"))->get();

            $json['data'][$h_key]['header_id'] = $data_headers->id;
            $json['data'][$h_key]['picking_number'] = $data_headers->picking_number;
            $json['data'][$h_key]['doc_date'] = $data_headers->doc_date;
            $json['data'][$h_key]['created_by'] = $data_headers->created_by;
            $json['data'][$h_key]['pickup_status'] = $data_headers->pickup_status;

        foreach ($data_detail as $d_key => $data_details)
        {
            $json['data'][$h_key]['detail'][$d_key]['detail_id'] = $data_details->id;
            $json['data'][$h_key]['detail'][$d_key]['inventory_code'] = $data_details->inventory_code;
            $json['data'][$h_key]['detail'][$d_key]['inventory_name'] = $data_details->inventory_name;
            $json['data'][$h_key]['detail'][$d_key]['inventory_id'] = $data_details->inventory_id;
            $json['data'][$h_key]['detail'][$d_key]['quantity'] = $data_details->pick_qty;
            $json['data'][$h_key]['detail'][$d_key]['warehouse_name'] = $data_details->warehouse_name;
            $json['data'][$h_key]['detail'][$d_key]['room_name'] = $data_details->room_name;
            $json['data'][$h_key]['detail'][$d_key]['rack_name'] = $data_details->rack_name;
            $json['data'][$h_key]['detail'][$d_key]['pallet_number'] = $data_details->pallet_number;
            $json['data'][$h_key]['detail'][$d_key]['batch'] = $data_details->batch;
        }
      }
    }elseif(count($data_header)==0){
      $json['code'] = 1;
      $json['msg'] = "Success";
      $json['data'] = [];
    }else{
      $json['code'] = 0;
      $json['msg'] = "Failed";
    };
    return response()->json($json);
  }


  public function picking_list(Request $request)
  {
    // try {
    $keyword = $request['keyword'];

    $sql_header = 'SELECT
                    picking.id,
                    picking.picking_number,
                    DATE_FORMAT(picking.doc_date, "%Y%m%d") AS doc_date,
                    picking.created_by,
                    picking.customer,
                    picking.marketing,
                    picking.pickup_status
                  FROM
                    picking

                  WHERE picking.pickup_status != 3 AND picking.deleted_at IS NULL';

      if(isset($keyword))
      {
        $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->where(DB::raw('ifnull(picking_number, "")') , 'LIKE' , '%'.$keyword.'%')->get();
      }else{
        $data_header = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->where('picking_number', 'KONTOL')->get();
      };

      // $picking = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/picking/'.$id.'');
      // $picking_decode = json_decode($picking, true);

      // $json['code'] = 1;
      // $json['msg'] = "Success";
      // $json['data'] = [];

      // if(isset($picking_decode['items'][0]['no_do']))
      // {
      //   $json['data']['do_no'] = $picking_decode['items'][0]['no_do'];
      //   $json['data']['doc_date'] = $picking_decode['items'][0]['doc_date'];

      //   foreach ($picking_decode['items'] as $field => $value) {
      //     $json['data']['detail'][$field]['itemdo'] = $value['itemdo'];
      //     $json['data']['detail'][$field]['item_code'] = $value['item_code'];
      //     $json['data']['detail'][$field]['item_name'] = $value['item_name'];
      //     $json['data']['detail'][$field]['qty'] = $value['qty'];
      if(count($data_header)>0)
      {
        $json['code'] = 1;
        $json['msg'] = "Success";
        foreach ($data_header as $h_key => $data_headers) {

          $sql_detail = 'SELECT
                          inventory.inventory_code,
                          inventory.inventory_name,
                          room.room_name,
                          rack.rack_name,
                          warehouse.warehouse_name,
                          picking.picking_number,
                          picking_detail.pallet_number,
                          picking_detail.inventory_id,
                          picking_detail.item_line,
                          picking_detail.warehouse_id,
                          picking_detail.bay_id,
                          picking_detail.rack_id,
                          picking_detail.room_id,
                          picking_detail.batch,
                          picking_detail.quantity,
                          picking_detail.pick_qty,
                          picking_detail.stock_qty,
                          ifnull( picking_detail.unit_of_measure_code, inventory.unit_of_measure ) AS unit_of_measure_code
                        FROM
                          picking
                        INNER JOIN picking_detail ON picking.id = picking_detail.transaction_id
                        INNER JOIN inventory ON picking_detail.inventory_id = inventory.id
                        LEFT JOIN rack ON picking_detail.rack_id = rack.id
                        LEFT JOIN room ON picking_detail.room_id = room.id
                        LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                        WHERE picking_detail.transaction_id='.$data_headers->id.'
                        AND picking_detail.deleted_at IS NULL';
          $data_detail = DB::table(DB::raw("($sql_detail) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->orderBy('pallet_number', 'ASC')->get();

            // dd($id);
            // $json['data'][$h_key]['header_id'] = $data_headers->id;
            $json['data']['do_no'] = $data_headers->picking_number;
            $json['data']['doc_date'] = $data_headers->doc_date;
            $json['data']['customer'] = $data_headers->customer;
            $json['data']['marketing'] = $data_headers->marketing;
            // $json['data'][$h_key]['created_by'] = $data_headers->created_by;
            // $json['data'][$h_key]['pickup_status'] = $data_headers->pickup_status;


            foreach ($data_detail as $d_key => $data_details)
            {

                  $json['data']['detail'][$d_key]['itemdo'] = $data_details->item_line;
                  $json['data']['detail'][$d_key]['item_code'] = $data_details->inventory_code;
                  $json['data']['detail'][$d_key]['item_name'] = $data_details->inventory_name;
                  $json['data']['detail'][$d_key]['qty'] = $data_details->pick_qty;

                  // stock
                  $json['data']['detail'][$d_key]['stock'][0]['inventory_id'] = $data_details->inventory_id;
                  $json['data']['detail'][$d_key]['stock'][0]['warehouse_name'] = $data_details->warehouse_name;
                  $json['data']['detail'][$d_key]['stock'][0]['warehouse_id'] = $data_details->warehouse_id;
                  $json['data']['detail'][$d_key]['stock'][0]['room_name'] = $data_details->room_name;
                  $json['data']['detail'][$d_key]['stock'][0]['room_id'] = $data_details->room_id;
                  $json['data']['detail'][$d_key]['stock'][0]['bay_name'] = '-';
                  $json['data']['detail'][$d_key]['stock'][0]['rack_name'] = $data_details->rack_name;
                  $json['data']['detail'][$d_key]['stock'][0]['rack_id'] = $data_details->rack_id;
                  $json['data']['detail'][$d_key]['stock'][0]['pallet_number'] = $data_details->pallet_number;
                  $json['data']['detail'][$d_key]['stock'][0]['unit'] = $data_details->unit_of_measure_code;
                  $json['data']['detail'][$d_key]['stock'][0]['batch'] = $data_details->batch;
                  $json['data']['detail'][$d_key]['stock'][0]['stock'] = $data_details->pick_qty;
                  $json['data']['detail'][$d_key]['stock'][0]['total_qty_stock_last'] = $data_details->quantity;
                  $json['data']['detail'][$d_key]['stock'][0]['total_qty_stock'] = $data_details->quantity;
                  $json['data']['detail'][$d_key]['stock'][0]['total_qty_pick'] = 0;

            }
          }
        }elseif(count($data_header)==0){
              $json['code'] = 1;
              $json['msg'] = "Success";
              $json['data'] = [];
            }else{
              $json['code'] = 0;
              $json['msg'] = "Failed";
            };


      // } catch (\Exception $e) {
      //   $json['code'] = 0;
      //   $json['msg'] = "Failed";
      //   $json['data'] = [];
      // }

      return response()->json($json);

  }



  public function confirm($id, Request $request)
  {

    try{
      DB::update("UPDATE picking SET `pickup_status` = 2 WHERE picking_number = ".$id."");
      $json['code'] = 1;
      $json['msg'] = "Success";

      // log
      $user = User::where('username', $request['created_by'])->first();
      if(isset($user))
      {
        $picking = Picking::where('picking_number', $id)->first();

        if(isset($picking))
        {
          // $userid= Auth::id();
          $user_log = new UserLog;
          $user_log->user_id = $user->id;
          $user_log->scope = 'PICKING';
          $user_log->data = json_encode([
                                'action' => 'put',
                                'description' => 'Pickup Picking',
                                'transaction_number' => $picking->picking_number,
                                'source' => 'Mobile',
                                'picking_id' => $id
                            ]);
          $user_log->save();
        };
      };

    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);

  }

  public function store($id, Request $request)
  {
    try {

      $count_detail_stock = $request['max_count'];
      for($x = 0; $x < $count_detail_stock; $x++)
      {
      // object
        $xx = $x+1;
        $inventory_id_st = $request['inventory_id_'.$xx];
        $pallet_number_st = $request['pallet_number_'.$xx];
        $quantity_st = $request['pick_qty_'.$xx];
        $warehouse_id_st = $request['warehouse_id_'.$xx];
        $rack_id_st = $request['rack_id_'.$xx];
        $batch_st = $request['batch_'.$xx];
        $room_id_st = $request['room_id_'.$xx];

        $sql_header = 'SELECT
                          *
                        FROM
                          stock_picking
                        WHERE
                        stock_picking.inventory_id = '.$inventory_id_st.' AND stock_picking.warehouse_id = '.$warehouse_id_st.'
                        AND stock_picking.rack_id = '.$rack_id_st.'
                        AND stock_picking.room_id = '.$room_id_st.'
                        AND stock_picking.pallet_number = "'.$pallet_number_st.'"
                        AND stock_picking.batch = "'.$batch_st.'"
                        AND stock_picking.quantity < '.$quantity_st.' ';
        $return_data = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

        if(isset($return_data))
        {
          $data_header['value'][] = $return_data;
        };
      };

      // dd(count($data_header['value']));

      if(isset($data_header['value']) != NULL)
      {
        // dd('ok');
        return response()->json([
          'code' => 0,
          'msg' => 'Stock not available!',
          'data' => response()->json($request->all())
        ]);
      }else{
      //   dd('not ok');
      // };

        // delete header
        $picking_exists = Picking::where('picking_number', $request['picking_number'])->first();
        // dd($picking_exists);
        if(isset($picking_exists))
        {
        $picking_exists->delete();
        };

        $picking_detail_exists = PickingDetail::where('picking_number', $request['picking_number'])->get();
        foreach($picking_detail_exists AS $picking_detail_exist)
        {
          $post = PickingDetail::where('id', $picking_detail_exist->id)->first();
          if(isset($post))
          {
            $post->delete();
          };
        };

        $post_header = new Picking;
        $post_header->picking_number = $request['picking_number'];
        $post_header->doc_date = $request['doc_date'];
        $post_header->driver_name = $request['driver_name'];
        $post_header->plat_number = $request['plat_number'];
        $post_header->loading_number = $request['loading_number'];
        $post_header->description = $request['description'];
        $post_header->created_by = $request['created_by'];
        $post_header->pickup_status = $request['pickup_status'];
        $post_header->customer = $request['customer'];
        $post_header->marketing = $request['marketing'];
        $post_header->save();

        // $picking = file_get_contents('http://webapp.lmu.co.id:3000/m/v1/wms/picking/'.$id.'');
        // $picking_decode = json_decode($picking, true);

        // dd($picking_decode);

        // if(isset($picking_decode['items'][0]['no_do']))
        // {

        //   foreach ($picking_decode['items'] as $field => $value) {
        //      // print_r($picking_decode['items'][$field]['item_code']);

        //     $sql = 'SELECT
        //             inventory.inventory_code,
        //             warehouse.warehouse_name,
        //             stock.pallet_number,
        //             room.room_name,
        //             rack.rack_name,
        //             bay.bay_name,
        //             stock.unit,
        //             stock.quantity,
        //             stock.warehouse_id,
        //             stock.rack_id,
        //             stock.room_id,
        //             stock.batch,
        //             inventory.inventory_name,
        //             inventory.id AS inventory_id,
        //             IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack,
        //             IFNULL(inventory.volume,1) AS volume_inventory,
        //             (stock.quantity * IFNULL(inventory.volume,1)) / IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1)  AS volume_rack_avail
        //           FROM
        //             inventory
        //           INNER JOIN stock ON inventory.id = stock.inventory_id
        //           INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
        //           INNER JOIN room ON stock.room_id = room.id
        //           INNER JOIN rack ON stock.rack_id = rack.id
        //           LEFT OUTER JOIN bay ON stock.bay_id = bay.id
        //           WHERE stock.quantity > 0 AND room.warehouse_id = 1 AND
        //           inventory.inventory_code = '.$picking_decode['items'][$field]['item_code'].'';
        //   $stock = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->get();

        //   // print_r($stock);


        //     $total_qty_stock = 0;
        //     $total_qty_stock_last = 0;
        //     $total_qty_pick = 0;

        //     foreach ($stock as $d_key => $stocks)
        //       {
        //         if($field == 0)
        //         {
        //           $header = 0;
        //         }else{
        //           $header = $field + 1;
        //         };
        //         $count = $header+$d_key+1;
                // print_r($count);
                $count_detail = $request['max_count'];
                for($i = 0; $i < $count_detail; $i++)
                {
                // object
                  $ii = $i+1;

                  $inventory = Inventory::where('id', $request['inventory_id_'.$ii])->first();

                  $inventory_code = $request['inventory_code_'.$ii];
                  $inventory_id = $request['inventory_id_'.$ii];
                  $quantity = $request['quantity_'.$ii];
                  $pick_qty = $request['pick_qty_'.$ii];
                  $stock_qty = $request['stock_qty_'.$ii];
                  $item_line = $request['item_line_'.$ii];
                  $warehouse_id = $request['warehouse_id_'.$ii];
                  $room_id = $request['room_id_'.$ii];
                  // $bay_id = $request['bay_id_'.$ii];
                  $bay_id = 0;
                  $rack_id = $request['rack_id_'.$ii];
                  $pallet_number = $request['pallet_number_'.$ii];
                  $batch = $request['batch_'.$ii];

                  if(isset($inventory)){
                    $unit_of_measure_code = $inventory->unit_of_measure;
                  }else{
                    $unit_of_measure_code = '';
                  };

                  // $total_qty_stock += $stocks->quantity;
                  // $total_qty_stock_last += $total_qty_stock- $stocks->quantity;

                  // print_r($total_qty_stock_last);

                  // $total_qty_pick = $picking_decode['items'][$field]['qty'];

                  // print_r($total_qty_pick);

                  // if($total_qty_stock_last < $total_qty_pick)
                  // {

                    $post_detail = new PickingDetail;
                    $post_detail->transaction_id = $post_header->id;
                    $post_detail->inventory_code = $inventory_code;
                    $post_detail->inventory_id = $inventory_id;
                    $post_detail->quantity = $quantity;
                    $post_detail->pick_qty = $pick_qty;
                    $post_detail->stock_qty = $stock_qty;
                    $post_detail->item_line = $item_line;
                    $post_detail->warehouse_id = $warehouse_id;
                    $post_detail->room_id = $room_id;
                    $post_detail->bay_id = 0;
                    $post_detail->rack_id = $rack_id;
                    $post_detail->unit_of_measure_code = $unit_of_measure_code;
                    $post_detail->pallet_number = $pallet_number;
                    $post_detail->batch = $batch;
                    $post_detail->picking_number = $request['picking_number'];
                    $post_detail->save();
                  };

                // };

        //       };
        //     };
        // };

        $user = User::where('username', $request['created_by'])->first();
        if(isset($user))
        {
          $user_log = new UserLog;
          $user_log->user_id = $user->id;
          $user_log->scope = 'PICKING';
          $user_log->data = json_encode([
                                'description' => $request['description'],
                                'action' => 'create',
                                'transaction_number' => $request['picking_number'],
                                'source' => 'Mobile'
                            ]);
          $user_log->save();
        };

        return response()->json([
          'code' => 1,
          'msg' => 'Success',
          'data' => response()->json($request->all())
        ]);
      }

      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }

  public function post($id, Request $request)
  {
    try {

        // delete header
        $picking_exists = Picking::where('picking_number', $request['picking_number'])->first();
        // dd($picking_exists);
        if(isset($picking_exists))
        {
        $picking_exists->delete();
        };

        $picking_detail_exists = PickingDetail::where('picking_number', $request['picking_number'])->get();
        foreach($picking_detail_exists AS $picking_detail_exist)
        {
          $post = PickingDetail::where('id', $picking_detail_exist->id)->first();
          if(isset($post))
          {
            $post->delete();
          };
        };

        $post_header = new Picking;
        $post_header->picking_number = $request['picking_number'];
        $post_header->doc_date = $request['doc_date'];
        $post_header->driver_name = $request['driver_name'];
        $post_header->plat_number = $request['plat_number'];
        $post_header->loading_number = $request['loading_number'];
        $post_header->description = $request['description'];
        $post_header->created_by = $request['created_by'];
        $post_header->pickup_status = 3;
        $post_header->customer = $request['customer'];
        $post_header->marketing = $request['marketing'];
        $post_header->save();


                $count_detail = $request['max_count'];
                for($i = 0; $i < $count_detail; $i++)
                {
                // object
                  $ii = $i+1;

                  $inventory = Inventory::where('id', $request['inventory_id_'.$ii])->first();

                  $inventory_code = $request['inventory_code_'.$ii];
                  $inventory_id = $request['inventory_id_'.$ii];
                  $quantity = $request['quantity_'.$ii];
                  $pick_qty = $request['pick_qty_'.$ii];
                  $stock_qty = $request['stock_qty_'.$ii];
                  $item_line = $request['item_line_'.$ii];
                  $warehouse_id = $request['warehouse_id_'.$ii];
                  $room_id = $request['room_id_'.$ii];
                  $bay_id = 0;
                  $rack_id = $request['rack_id_'.$ii];
                  $pallet_number = $request['pallet_number_'.$ii];
                  $batch = $request['batch_'.$ii];
                  $unit_of_measure_code = $inventory->unit_of_measure;



                    $post_detail = new PickingDetail;
                    $post_detail->transaction_id = $post_header->id;
                    $post_detail->inventory_code = $inventory_code;
                    $post_detail->inventory_id = $inventory_id;
                    $post_detail->quantity = $quantity;
                    $post_detail->pick_qty = $pick_qty;
                    $post_detail->stock_qty = $stock_qty;
                    $post_detail->item_line = $item_line;
                    $post_detail->warehouse_id = $warehouse_id;
                    $post_detail->room_id = $room_id;
                    $post_detail->bay_id = $bay_id;
                    $post_detail->rack_id = $rack_id;
                    $post_detail->unit_of_measure_code = $unit_of_measure_code;
                    $post_detail->pallet_number = $pallet_number;
                    $post_detail->batch = $batch;
                    $post_detail->picking_number = $request['picking_number'];
                    $post_detail->save();
                  };

                // };

        //       };
        //     };
        // };

        $user = User::where('username', $request['created_by'])->first();
        if(isset($user))
        {
          $user_log = new UserLog;
          $user_log->user_id = $user->id;
          $user_log->scope = 'PICKING';
          $user_log->data = json_encode([
                                'description' => $request['description'],
                                'action' => 'create',
                                'transaction_number' => $request['picking_number'],
                                'source' => 'Mobile'
                            ]);
          $user_log->save();
        };

        return response()->json([
          'code' => 1,
          'msg' => 'Success',
          'data' => response()->json($request->all())
        ]);


      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      }
  }

  public function store_manual($id, Request $request)
  {
    // try {

      $count_detail_stock = $request['max_count'];
      for($x = 0; $x < $count_detail_stock; $x++)
      {
      // object
        $xx = $x+1;
        $inventory_id_st = $request['inventory_id_'.$xx];
        $pallet_number_st = $request['pallet_number_'.$xx];
        $quantity_st = $request['pick_qty_'.$xx];
        $warehouse_id_st = $request['warehouse_id_'.$xx];
        $rack_id_st = $request['rack_id_'.$xx];
        $batch_st = $request['batch_'.$xx];
        $room_id_st = $request['room_id_'.$xx];

        $sql_header = 'SELECT
                          *
                        FROM
                          stock_picking
                        WHERE
                          stock_picking.inventory_id = '.$inventory_id_st.' AND stock_picking.warehouse_id = '.$warehouse_id_st.'
                          AND stock_picking.rack_id = '.$rack_id_st.'
                          AND stock_picking.room_id = '.$room_id_st.'
                          AND stock_picking.pallet_number = "'.$pallet_number_st.'"
                          AND stock_picking.batch = "'.$batch_st.'"
                          AND stock_picking.quantity < '.$quantity_st.' ';
        $return_data = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"))->first();

        if(isset($return_data))
        {
          $data_header['value'][] = $return_data;
        };
      };

      // dd(count($data_header['value']));

      if(isset($data_header['value']) != NULL)
      {
        // dd('ok');
        return response()->json([
          'code' => 0,
          'msg' => 'Stock not available!',
          'data' => response()->json($request->all())
        ]);
      }else{

      // print_r($request['picking_number']);

      // delete header
      $picking_exists = Picking::where('picking_number', $request['picking_number'])->first();
      // dd($picking_exists);
      if(isset($picking_exists))
      {
      $picking_exists->delete();
      };

      $picking_detail_exists = PickingDetail::where('picking_number', $request['picking_number'])->get();
      foreach($picking_detail_exists AS $picking_detail_exist)
      {
        $post = PickingDetail::where('id', $picking_detail_exist->id)->first();
        if(isset($post))
        {
          $post->delete();
        };
      };

      $maxValue = Picking::max('id');

      if(isset($maxValue)){
        $maxxValue = $maxValue + 1;
      }else{
        $maxxValue = 1;
      };

      $post_header = new Picking;
      $post_header->picking_number = $request['picking_number'];
      $post_header->doc_date = $request['doc_date'];
      $post_header->driver_name = $request['driver_name'];
      $post_header->plat_number = $request['plat_number'];
      $post_header->loading_number = $request['loading_number'];
      $post_header->description = $request['description'];
      $post_header->created_by = $request['created_by'];
      $post_header->pickup_status = $request['pickup_status'];
      $post_header->customer = $request['customer'];
      $post_header->marketing = $request['marketing'];
      $post_header->manual = 1;
      $post_header->save();

      // $picking = file_get_contents('http://webapp.lmu.co.id:3000/m/v1/wms/picking/'.$id.'');
      // $picking_decode = json_decode($picking, true);

      // dd($picking_decode);

      // if(isset($picking_decode['items'][0]['no_do']))
      // {

      //   foreach ($picking_decode['items'] as $field => $value) {
      //      // print_r($picking_decode['items'][$field]['item_code']);

      //     $sql = 'SELECT
      //             inventory.inventory_code,
      //             warehouse.warehouse_name,
      //             stock.pallet_number,
      //             room.room_name,
      //             rack.rack_name,
      //             bay.bay_name,
      //             stock.unit,
      //             stock.quantity,
      //             stock.warehouse_id,
      //             stock.rack_id,
      //             stock.room_id,
      //             stock.batch,
      //             inventory.inventory_name,
      //             inventory.id AS inventory_id,
      //             IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1) AS volume_rack,
      //             IFNULL(inventory.volume,1) AS volume_inventory,
      //             (stock.quantity * IFNULL(inventory.volume,1)) / IFNULL(rack.width, 1) * IFNULL(rack.length, 1) * IFNULL(rack.height, 1)  AS volume_rack_avail
      //           FROM
      //             inventory
      //           INNER JOIN stock ON inventory.id = stock.inventory_id
      //           INNER JOIN warehouse ON stock.warehouse_id = warehouse.id
      //           INNER JOIN room ON stock.room_id = room.id
      //           INNER JOIN rack ON stock.rack_id = rack.id
      //           LEFT OUTER JOIN bay ON stock.bay_id = bay.id
      //           WHERE stock.quantity > 0 AND room.warehouse_id = 1 AND
      //           inventory.inventory_code = '.$picking_decode['items'][$field]['item_code'].'';
      //   $stock = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('batch', 'ASC')->orderBy('quantity', 'DESC')->get();

      //   // print_r($stock);


      //     $total_qty_stock = 0;
      //     $total_qty_stock_last = 0;
      //     $total_qty_pick = 0;

      //     foreach ($stock as $d_key => $stocks)
      //       {
      //         if($field == 0)
      //         {
      //           $header = 0;
      //         }else{
      //           $header = $field + 1;
      //         };
      //         $count = $header+$d_key+1;
              // print_r($count);
              $count_detail = $request['max_count'];
              for($i = 0; $i < $count_detail; $i++)
              {
                $ii = $i+1;

                $inventory = Inventory::where('id', $request['inventory_id_'.$ii])->first();
              // object
                $inventory_code = $request['inventory_code_'.$ii];
                $inventory_id = $request['inventory_id_'.$ii];
                $quantity = $request['quantity_'.$ii];
                $pick_qty = $request['pick_qty_'.$ii];
                $stock_qty = $request['stock_qty_'.$ii];
                $item_line = $request['item_line_'.$ii];
                $warehouse_id = $request['warehouse_id_'.$ii];
                $room_id = $request['room_id_'.$ii];
                $bay_id = 0;
                $rack_id = $request['rack_id_'.$ii];
                $pallet_number = $request['pallet_number_'.$ii];
                $batch = $request['batch_'.$ii];
                $unit_of_measure_code = $inventory->unit_of_measure;

                // $total_qty_stock += $stocks->quantity;
                // $total_qty_stock_last += $total_qty_stock- $stocks->quantity;

                // print_r($total_qty_stock_last);

                // $total_qty_pick = $picking_decode['items'][$field]['qty'];

                // print_r($total_qty_pick);

                // if($total_qty_stock_last < $total_qty_pick)
                // {

                  $post_detail = new PickingDetail;
                  $post_detail->transaction_id = $post_header->id;
                  $post_detail->inventory_code = $inventory_code;
                  $post_detail->inventory_id = $inventory_id;
                  $post_detail->quantity = $quantity;
                  $post_detail->pick_qty = $pick_qty;
                  $post_detail->stock_qty = $stock_qty;
                  $post_detail->item_line = $item_line;
                  $post_detail->warehouse_id = $warehouse_id;
                  $post_detail->room_id = $room_id;
                  $post_detail->bay_id = $bay_id;
                  $post_detail->rack_id = $rack_id;
                  $post_detail->unit_of_measure_code = $unit_of_measure_code;
                  $post_detail->pallet_number = $pallet_number;
                  $post_detail->batch = $batch;
                  $post_detail->picking_number = $request['picking_number'];
                  $post_detail->save();
                };

              // };

      //       };
      //     };
      // };

      $user = User::where('username', $request['created_by'])->first();
      if(isset($user))
      {
        $user_log = new UserLog;
        $user_log->user_id = $user->id;
        $user_log->scope = 'PICKING';
        $user_log->data = json_encode([
                              'description' => $request['description'],
                              'action' => 'create',
                              'transaction_number' => $request['picking_number'],
                              'source' => 'Mobile'
                          ]);
        $user_log->save();
      };

      return response()->json([
        'code' => 1,
        'msg' => 'Success',
        'data' => response()->json($request->all())
      ]);
    };

      // } catch (\Exception $e) {
      //   $json['code'] = 0;
      //   $json['msg'] = "Failed";
      // }
  }

  public function update_item_detail(Request $request)
  {
    try{
      $post_detail = PickingDetail::where('id', $request['id_detail'])->first();
      $post_detail->quantity = $request['quantity'];
      $post_detail->pick_qty = $request['pick_qty'];
      $post_detail->save();
      return response()->json([
        'code' => 1,
        'msg' => 'Success',
        'data' => response()->json($request->all())
      ]);

    } catch (\Exception $e) {
      $json['code'] = 0;
      $json['msg'] = "Failed";
    }
    return response()->json($json);

  }
}
