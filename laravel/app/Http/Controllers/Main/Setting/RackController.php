<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\RackRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Rack;
use App\Model\Bay;
use App\Model\BsRack;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class RackController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'rack_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $racks = Rack::all();
      $bay_list = Bay::pluck('bay_name', 'id');
      $rack_bs_list = BsRack::pluck('bs_name', 'id');

      return view ('main.setting.rack.index', compact('racks', 'bay_list', 'rack_bs_list'));
    }

    public function data(Request $request)
    {
        // $itemdata = Rack::select(['id', 'rack_code', 'rack_name', 'description', 'status', 'created_at', 'updated_at']);
        $itemdata = DB::table('rack')
        ->leftjoin('bay', 'rack.bay_id', '=', 'bay.id')
        ->leftjoin('rack_bs_hidden', 'rack.hide_rack', '=', 'rack_bs_hidden.id')
        ->select('rack.*', 'bay.bay_name', 'rack_bs_hidden.bs_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('rack.status', $id);
            }else{
                $itemdata->where('rack.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(rack.rack_code,'-',rack.rack_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('rack', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('rack', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->rack_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->rack_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new Rack();
        $post->rack_code = $request->rack_code;
        $post->rack_name = $request->rack_name;
        $post->bay_id = $request->bay_id ;
        $post->hide_rack = $request->hide_rack ;
        // $post->length = $request->length;
        // $post->width = $request->width;
        // $post->height = $request->height;
        // $post->tolerance = $request->tolerance;
        $post->description = $request->description;
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post);
      }
    }

    public function edit($id)
    {
      $rack = Rack::Find($id);
      echo json_encode($rack);
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = Rack::Find($id);
        $post->rack_code = $request->rack_code;
        $post->rack_name = $request->rack_name;
        $post->bay_id = $request->bay_id;
        $post->hide_rack = $request->hide_rack ;
        // $post->length = $request->length;
        // $post->width = $request->width;
        // $post->height = $request->height;
        // $post->tolerance = $request->tolerance;
        $post->description = $request->description;
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post);
      }

    }

    public function delete($id)
    {
      $post =  Rack::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = Rack::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);

  }
}
