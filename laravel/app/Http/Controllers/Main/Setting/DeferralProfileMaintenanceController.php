<?php

namespace App\Http\Controllers\Main;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
// use Illuminate\Notifications\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\DeferralProfileMaintenanceRequest;
use App\Http\Controllers\Controller;
use App\Model\DeferralProfileMaintenance; 
use Validator;
use Response;
use App\Post;
use View;

class DeferralProfileMaintenanceController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'deferral_profile_maintenance_name' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $deferral_profile_maintenances = DeferralProfileMaintenance::all();
      return view ('main.deferral_profile_maintenance.index', compact('deferral_profile_maintenances'));
    }

    public function data(Request $request)
    {    
        $itemdata = DeferralProfileMaintenance::select(['id', 'deferral_profile_maintenance_code', 'deferral_profile_maintenance_name', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('deferral_profile_maintenance.status', $id);
            }else{
                $itemdata->where('deferral_profile_maintenance.status', 0);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->deferral_profile_maintenance_name."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->deferral_profile_maintenance_name."'".')"> <i class="icon-rotate-ccw"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' | '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = new DeferralProfileMaintenance(); 
        $post->deferral_profile_maintenance_code = $request->deferral_profile_maintenance_code; 
        $post->deferral_profile_maintenance_name = $request->deferral_profile_maintenance_name; 
        $post->description = $request->description; 
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $deferral_profile_maintenance = DeferralProfileMaintenance::Find($id);
      echo json_encode($deferral_profile_maintenance); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = DeferralProfileMaintenance::Find($id); 
        $post->deferral_profile_maintenance_code = $request->deferral_profile_maintenance_code; 
        $post->deferral_profile_maintenance_name = $request->deferral_profile_maintenance_name; 
        $post->description = $request->description; 
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function delete($id)
    {
      $post =  DeferralProfileMaintenance::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = DeferralProfileMaintenance::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
