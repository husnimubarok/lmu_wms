<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
// use Illuminate\Notifications\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MonthlyRateRequest;
use App\Http\Controllers\Controller;
use App\Model\MonthlyRate; 
use Validator;
use Response;
use App\Post;
use View;

class MonthlyRateController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'monthly_rate_name' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $monthly_rates = MonthlyRate::all();
      return view ('main.setting.monthly_rate.index', compact('monthly_rates'));
    }

    public function data(Request $request)
    {    
        $itemdata = MonthlyRate::select(['id', 'monthly_rate_code', 'monthly_rate_name', 'description', 'rate_amount', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('monthly_rate.status', $id);
            }else{
                $itemdata->where('monthly_rate.status', 0);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->period_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->period_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = new MonthlyRate(); 
        $post->monthly_rate_code = $request->monthly_rate_code; 
        $post->monthly_rate_name = $request->monthly_rate_name; 
        $post->description = $request->description; 
        $post->rate_amount = $request->rate_amount; 
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $monthly_rate = MonthlyRate::Find($id);
      echo json_encode($monthly_rate); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = MonthlyRate::Find($id); 
        $post->monthly_rate_code = $request->monthly_rate_code; 
        $post->monthly_rate_name = $request->monthly_rate_name; 
        $post->description = $request->description; 
        $post->rate_amount = $request->rate_amount; 
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function delete($id)
    {
      $post =  MonthlyRate::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = MonthlyRate::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
