<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLocationRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Warehouse; 
use App\Model\StoreLocation; 
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class StoreLocationController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'store_location_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $store_locations = StoreLocation::all();
      $warehouse_list = Warehouse::pluck('warehouse_name', 'id');
      return view ('main.setting.store_location.index', compact('store_locations', 'warehouse_list'));
    }

    public function data(Request $request)
    {    
        $itemdata = DB::table('store_location')
            ->leftjoin('warehouse', 'store_location.warehouse_id', '=', 'warehouse.id') 
            ->select('store_location.*', 'warehouse.warehouse_name');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('store_location.status', $id);
            }else{
                $itemdata->where('store_location.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
                $itemdata->whereRaw("CONCAT(store_location.store_location_code,'-',store_location.store_location_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->store_location_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->store_location_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new StoreLocation(); 
        $post->store_location_code = $request->store_location_code; 
        $post->store_location_name = $request->store_location_name; 
        $post->warehouse_id = $request->warehouse_id; 
        $post->description = $request->description;
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $store_location = StoreLocation::Find($id);
      echo json_encode($store_location); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        
        $post = StoreLocation::Find($id); 
        $post->store_location_code = $request->store_location_code; 
        $post->store_location_name = $request->store_location_name; 
        $post->warehouse_id = $request->warehouse_id; 
        $post->description = $request->description;
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
  
    }

    public function delete($id)
    {
      $post =  StoreLocation::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = StoreLocation::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
