<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\InventoryTypeRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\InventoryType; 
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class InventoryTypeController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'inventory_type_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $inventory_types = InventoryType::all();
      return view ('main.setting.inventory_type.index', compact('inventory_types'));
    }

    public function data(Request $request)
    {    
        $itemdata = InventoryType::select(['id', 'inventory_type_code', 'inventory_type_name', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('inventory_type.status', $id);
            }else{
                $itemdata->where('inventory_type.status', 0);
            }

            if($keyword = $request->input('keyword')) { 
                $itemdata->whereRaw("CONCAT(inventory_type.inventory_type_code,'-',inventory_type.inventory_type_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->inventory_type_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->inventory_type_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new InventoryType(); 
        $post->inventory_type_code = $request->inventory_type_code; 
        $post->inventory_type_name = $request->inventory_type_name; 
        $post->description = $request->description;
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $inventory_type = InventoryType::Find($id);
      echo json_encode($inventory_type); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        
        $post = InventoryType::Find($id); 
        $post->inventory_type_code = $request->inventory_type_code; 
        $post->inventory_type_name = $request->inventory_type_name; 
        $post->description = $request->description;
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
  
    }

    public function delete($id)
    {
      $post =  InventoryType::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = InventoryType::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
