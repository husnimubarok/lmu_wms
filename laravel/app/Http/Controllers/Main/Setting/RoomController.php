<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\RoomRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Warehouse;
use App\Model\Temprature;
use App\Model\Room;
use App\Model\RoomDetail;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class RoomController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'room_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $rooms = Room::all();
      $warehouse_list = Warehouse::all();
      $temprature_list = Temprature::all();
      return view ('main.setting.room.index', compact('rooms', 'warehouse_list', 'temprature_list'));
    }

    public function data(Request $request)
    {
        $itemdata = DB::table('room')
            ->leftjoin('warehouse', 'room.warehouse_id', '=', 'warehouse.id')
            ->leftjoin('temprature', 'room.temprature_id', '=', 'temprature.id')
            ->select('room.*', 'warehouse.warehouse_name', 'temprature.temprature_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('room.status', $id);
            }else{
                $itemdata->where('room.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(room.room_code,'-',room.room_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {

          if (RoleManager::actionStart('room', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
            $detail = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="show_detail('."'".$itemdata->id."'".');"> <i class="icon-folder-open2"></i></a>';
          }else{
            $edit = '';
            $detail = '';
          };

          if (RoleManager::actionStart('room', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->room_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->room_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.'  '.$detail.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check', 'detail'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new Room();
        $post->room_code = $request->room_code;
        $post->room_name = $request->room_name;
        $post->warehouse_id = $request->warehouse_id;
        $post->temprature_id = $request->temprature_id;
        $post->description = $request->description;
        $post->status = 0;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post);
      }
    }

    public function edit($id)
    {
      $room = Room::Find($id);
      echo json_encode($room);
    }

    public function update_header_id(Request $request, $id)
    {
      $post = Room::where('id', Auth::id())->first();
      $post->room_id = $id;
      $post->save();

      return response()->json($post);
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = Room::Find($id);
        $post->room_code = $request->room_code;
        $post->room_name = $request->room_name;
        $post->warehouse_id = $request->warehouse_id;
        $post->temprature_id = $request->temprature_id;
        $post->description = $request->description;
        $post->status = 0;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post);
      }

    }

    public function delete($id)
    {
      $post =  Room::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = Room::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
  }

  public function data_detail(Request $request)
  {
    if($request->ajax()){

       $sql = 'SELECT
                	warehouse.warehouse_code,
                	warehouse.warehouse_name,
                	room_detail.room_id,
                	room_detail.id,
                	temprature.temprature_code,
                	temprature.temprature_name,
                	room_detail.temprature_id,
                  room_detail.description,
                	room_detail.status
                FROM
                	room_detail
                INNER JOIN warehouse ON room_detail.warehouse_id = warehouse.id
                INNER JOIN temprature ON room_detail.temprature_id = temprature.id
                WHERE room_detail.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {
              $itemdata->where('rs_sql.room_id', $request->input('room_id_filter'));
      })

      ->addColumn('action', function ($itemdata) {
        return '<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_detail_id('."'".$itemdata->id."', '".$itemdata->warehouse_code."', '".$itemdata->warehouse_name."'".')"> <i class="icon-trash"></i></a>';
      })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store_detail(Request $request)
  {
    $post = new RoomDetail();
    $post->room_id = $request->room_id;
    $post->warehouse_id = $request->warehouse_id;
    $post->temprature_id = $request->temprature_id;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post);
  }

  public function delete_detail($id)
  {
    $post =  RoomDetail::Find($id);
    $post->delete();

    return response()->json($post);
  }
}
