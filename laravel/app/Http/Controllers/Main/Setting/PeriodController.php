<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PeriodRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Period; 
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class PeriodController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'period_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $periods = Period::all();
      return view ('main.setting.period.index', compact('periods'));
    }

    public function data(Request $request)
    {    
        $itemdata = Period::select(['id', 'period_code', 'period_name', 'date_from', 'date_to', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('period.status', $id);
            }else{
                $itemdata->where('period.status', 0);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->period_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->period_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->addColumn('period_name', function ($itemdata) {
          
          $period_name = explode("-", $itemdata->period_name);
          $month = $period_name[1]; 
          $year = $period_name[0];
          $dateObj   = DateTime::createFromFormat('!m', $month);
          $monthName = $dateObj->format('F'); // March

          $period_name_format = "Month: $monthName and Year: $year";

          return $period_name_format;

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check', 'period_name']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $date_from_array = explode("-",$request->input('date_from')); // split the array
        $var_day_from = $date_from_array[0]; //day seqment
        $var_month_from = $date_from_array[1]; //month segment
        $var_year_from = $date_from_array[2]; //year segment
        $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together


        $date_to_array = explode("-",$request->input('date_to')); // split the array
        $var_day_to = $date_to_array[0]; //day seqment
        $var_month_to = $date_to_array[1]; //month segment
        $var_year_to = $date_to_array[2]; //year segment
        $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

        $post = new Period(); 
        $post->period_code = $request->period_code; 
        $post->period_name = $request->period_name; 
        $post->description = $request->description; 
        $post->date_from = $date_from_format; 
        $post->date_to = $date_to_format; 
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $period = Period::Find($id);
      echo json_encode($period); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $date_from_array = explode("-",$request->input('date_from')); // split the array
        $var_day_from = $date_from_array[0]; //day seqment
        $var_month_from = $date_from_array[1]; //month segment
        $var_year_from = $date_from_array[2]; //year segment
        $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together


        $date_to_array = explode("-",$request->input('date_to')); // split the array
        $var_day_to = $date_to_array[0]; //day seqment
        $var_month_to = $date_to_array[1]; //month segment
        $var_year_to = $date_to_array[2]; //year segment
        $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

        $post = Period::Find($id); 
        $post->period_code = $request->period_code; 
        $post->period_name = $request->period_name; 
        $post->description = $request->description; 
        $post->date_from = $date_from_format; 
        $post->date_to = $date_to_format; 
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
  
    }

    public function delete($id)
    {
      $post =  Period::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = Period::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
