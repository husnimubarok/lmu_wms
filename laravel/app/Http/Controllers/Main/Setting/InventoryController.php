<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\InventoryRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Inventory;
use App\Model\InventoryGroup;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class InventoryController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'inventory_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $inventorys = Inventory::all();
      return view ('main.setting.inventory.index', compact('inventorys'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('inventory')
            ->leftjoin('inventory_group', 'inventory.inventory_group_id', '=', 'inventory_group.id')
            ->leftjoin('unit_of_measure', 'inventory.unit_of_measure_id', '=', 'unit_of_measure.id')
            ->select('inventory.*', 'inventory_group.inventory_group_name', 'unit_of_measure.unit_of_measure_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('inventory.status', $id);
            }else{
                $itemdata->where('inventory.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(inventory.inventory_code,'-',inventory.inventory_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('inventory', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="inventory/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('inventory', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->inventory_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->inventory_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function data_modal(Request $request)
    {

        $itemdata = DB::table('inventory')
            ->leftjoin('inventory_group', 'inventory.inventory_group_id', '=', 'inventory_group.id')
            ->leftjoin('unit_of_measure', 'inventory.unit_of_measure_id', '=', 'unit_of_measure.id')
            ->select('inventory.*', 'inventory_group.inventory_group_name', 'unit_of_measure.unit_of_measure_name')
            ->get();

        return Datatables::of($itemdata)

        ->addColumn('action', function ($itemdata) {
          $choose = '<a  type="button" class="btn btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';

          return ''.$choose.'';

        })

        ->rawColumns(['action'])
        ->make(true);
    }

    public function data_modal_transfer(Request $request)
    {

        $itemdata = DB::table('inventory')
            ->leftjoin('inventory_group', 'inventory.inventory_group_id', '=', 'inventory_group.id')
            ->leftjoin('unit_of_measure', 'inventory.unit_of_measure_id', '=', 'unit_of_measure.id')
            ->select('inventory.*', 'inventory_group.inventory_group_name', 'unit_of_measure.unit_of_measure_name')
            ->get();

        return Datatables::of($itemdata)

        ->addColumn('action', function ($itemdata) {
          $choose = '<a  type="button" class="btn btn-info btn-float btn-xs" href="#" onclick="addItem(this, '.$itemdata->id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';

          return ''.$choose.'';

        })

        ->rawColumns(['action'])
        ->make(true);
    }

    public function create()
    {
      $inventory_group_list = InventoryGroup::all()->pluck('inventory_group_name', 'id');
      $unit_of_measure_list = UnitOfMeasure::all()->pluck('unit_of_measure_name', 'id');

      return view ('main.setting.inventory.form', compact('inventory_group_list', 'unit_of_measure_list'));
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new Inventory();
        $post->inventory_code = $request->input('inventory_code');
        $post->inventory_name = $request->input('inventory_name');
        $post->unit_of_measure_id = $request->input('unit_of_measure_id');
        $post->description = $request->input('description');
        $post->inventory_group_id = $request->input('inventory_group_id');
        $post->volume = $request->input('volume');
        $post->volume_unit = $request->input('volume_unit');
        $post->status = $request->input('status');
        $post->created_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Setting\InventoryController@index');
      }
    }

    public function edit($id)
    {
      $inventory = Inventory::Find($id);
      $inventory_group_list = InventoryGroup::all()->pluck('inventory_group_name', 'id');
      $unit_of_measure_list = UnitOfMeasure::all()->pluck('unit_of_measure_name', 'id');

      return view ('main.setting.inventory.form', compact('inventory', 'inventory_group_list', 'unit_of_measure_list'));
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {


        $post = Inventory::Find($id);
        $post->inventory_code = $request->input('inventory_code');
        $post->inventory_name = $request->input('inventory_name');
        $post->unit_of_measure_id = $request->input('unit_of_measure_id');
        $post->description = $request->input('description');
        $post->inventory_group_id = $request->input('inventory_group_id');
        $post->volume = $request->input('volume');
        $post->volume_unit = $request->input('volume_unit');
        $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Setting\InventoryController@index');
      }

    }

    public function delete($id)
    {
      $post =  Inventory::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = Inventory::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);

  }
}
