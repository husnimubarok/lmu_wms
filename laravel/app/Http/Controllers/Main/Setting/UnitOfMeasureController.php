<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UnitOfMeasureRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class UnitOfMeasureController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'unit_of_measure_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $unit_of_measures = UnitOfMeasure::all();
      return view ('main.setting.unit_of_measure.index', compact('unit_of_measures'));
    }

    public function data(Request $request)
    {
        $itemdata = UnitOfMeasure::select(['id', 'unit_of_measure_code', 'unit_of_measure_name', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('unit_of_measure.status', $id);
            }else{
                $itemdata->where('unit_of_measure.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(unit_of_measure.unit_of_measure_code,'-',unit_of_measure.unit_of_measure_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('unit_of_measure', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('unit_of_measure', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->unit_of_measure_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->unit_of_measure_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new UnitOfMeasure();
        $post->unit_of_measure_code = $request->unit_of_measure_code;
        $post->unit_of_measure_name = $request->unit_of_measure_name;
        $post->description = $request->description;
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post);
      }
    }

    public function edit($id)
    {
      $unit_of_measure = UnitOfMeasure::Find($id);
      echo json_encode($unit_of_measure);
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = UnitOfMeasure::Find($id);
        $post->unit_of_measure_code = $request->unit_of_measure_code;
        $post->unit_of_measure_name = $request->unit_of_measure_name;
        $post->description = $request->description;
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post);
      }

    }

    public function delete($id)
    {
      $post =  UnitOfMeasure::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = UnitOfMeasure::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);

  }
}
