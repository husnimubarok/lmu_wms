<?php

namespace App\Http\Controllers\Main;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
// use Illuminate\Notifications\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\DepreciationTypeRequest;
use App\Http\Controllers\Controller;
use App\Model\DepreciationType; 
use Validator;
use Response;
use App\Post;
use View;

class DepreciationTypeController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'depreciation_type_name' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $depreciation_types = DepreciationType::all();
      return view ('main.depreciation_type.index', compact('depreciation_types'));
    }

    public function data(Request $request)
    {    
        $itemdata = DepreciationType::select(['id', 'depreciation_type_code', 'depreciation_type_name', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('depreciation_type.status', $id);
            }else{
                $itemdata->where('depreciation_type.status', 0);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->depreciation_type_name."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->depreciation_type_name."'".')"> <i class="icon-rotate-ccw"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' | '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = new DepreciationType(); 
        $post->depreciation_type_code = $request->depreciation_type_code; 
        $post->depreciation_type_name = $request->depreciation_type_name; 
        $post->description = $request->description; 
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function edit($id)
    {
      $depreciation_type = DepreciationType::Find($id);
      echo json_encode($depreciation_type); 
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
        $post = DepreciationType::Find($id); 
        $post->depreciation_type_code = $request->depreciation_type_code; 
        $post->depreciation_type_name = $request->depreciation_type_name; 
        $post->description = $request->description; 
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      }
    }

    public function delete($id)
    {
      $post =  DepreciationType::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = DepreciationType::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
