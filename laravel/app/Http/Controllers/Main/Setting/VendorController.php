<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\VendorRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Vendor;
use App\Model\Country;
use App\Model\City;
use App\Model\District;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;


class VendorController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'supplier_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $vendors = Vendor::all();
      // $itemdata = DB::table('supplier')
      //       ->leftjoin('country', 'supplier.country_id', '=', 'country.id')
      //       ->leftjoin('city', 'city_id', '=', 'city.id')
      //       ->leftjoin('district', 'district_id', '=', 'district.id')
      //       ->select('supplier.*', 'country.country_name', 'city.city_name', 'district.district_name')->get();
      // dd($itemdata);

      return view ('main.setting.vendor.index', compact('vendors'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('supplier')
            ->leftjoin('country', 'supplier.country_id', '=', 'country.id')
            ->leftjoin('city', 'city_id', '=', 'city.id')
            ->leftjoin('district', 'district_id', '=', 'district.id')
            ->select('supplier.*', 'country.country_name', 'city.city_name', 'district.district_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('supplier.status', $id);
            }else{
                $itemdata->where('supplier.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(supplier.supplier_code,'-',supplier.supplier_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('vendor', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="vendor/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('vendor', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->supplier_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->supplier_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function create()
    {
      $country_group_list = Country::all()->pluck('country_name', 'id');
      $city_group_list = City::all()->pluck('city_name', 'id');
      $district_group_list = District::all()->pluck('district_name', 'id');

      return view ('main.setting.vendor.form', compact('country_group_list', 'city_group_list', 'district_group_list'));
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new Vendor();
        $post->supplier_code = $request->input('supplier_code');
        $post->supplier_name = $request->input('supplier_name');
        $post->country_id = $request->input('country_id');
        $post->city_id = $request->input('city_id');
        $post->district_id = $request->input('district_id');
        $post->description = $request->input('description');
        $post->status = $request->input('status');
        $post->created_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Setting\VendorController@index');
      }
    }

    public function edit($id)
    {
      $vendor = Vendor::Find($id);
      $country_group_list = Country::all()->pluck('country_name', 'id');
      $city_group_list = City::all()->pluck('city_name', 'id');
      $district_group_list = District::all()->pluck('district_name', 'id');

      return view ('main.setting.vendor.form', compact('vendor', 'country_group_list', 'city_group_list', 'district_group_list'));
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {


        $post = Vendor::Find($id);
        $post->supplier_code = $request->input('supplier_code');
        $post->supplier_name = $request->input('supplier_name');
        $post->country_id = $request->input('country_id');
        $post->city_id = $request->input('city_id');
        $post->district_id = $request->input('district_id');
        $post->description = $request->input('description');
        $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Setting\VendorController@index');
      }

    }

    public function delete($id)
    {
      $post =  Vendor::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = Vendor::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);

  }
}
