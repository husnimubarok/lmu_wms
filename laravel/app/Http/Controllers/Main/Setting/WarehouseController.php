<?php

namespace App\Http\Controllers\Main\Setting;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\WarehouseRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;
use RoleManager;

use App\Model\Warehouse;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class WarehouseController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'warehouse_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $warehouses = Warehouse::all();
      return view ('main.setting.warehouse.index', compact('warehouses'));
    }

    public function data(Request $request)
    {
        $itemdata = Warehouse::select(['id', 'warehouse_code', 'rack_capacity', 'warehouse_name', 'hidden_receive', 'hidden_picking', 'description', 'store_location', 'status', 'created_at', 'updated_at']);

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('warehouse.status', $id);
            }else{
                $itemdata->where('warehouse.status', 0);
            }
            if($keyword = $request->input('keyword')) {
                $itemdata->whereRaw("CONCAT(warehouse.warehouse_code,'-',warehouse.warehouse_name) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('warehouse', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="edit('."'".$itemdata->id."'".')"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('warehouse', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->warehouse_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->warehouse_name."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('allow_flag', function ($itemdata) {
          $allow_flag = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->warehouse_name."', '".$itemdata->status."'".')"> <i class="icon-archive"></i> </a>';

          if($itemdata->hidden_picking ==1){
            return ''.$allow_flag.'';
          }else{
            return '';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('hidden_receive', function ($itemdata) {
          if($itemdata->hidden_receive == 1)
          {
            return 'Yes';
          }else{
            return 'No';
          }
        })

        ->addColumn('hidden_picking', function ($itemdata) {
          if($itemdata->hidden_picking == 1)
          {
            return 'Yes';
          }else{
            return 'No';
          }
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check', 'allow_flag'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        $post = new Warehouse();
        $post->warehouse_code = $request->warehouse_code;
        $post->warehouse_name = $request->warehouse_name;
        $post->rack_capacity = $request->rack_capacity;
        $post->description = $request->description;
        $post->store_location = $request->store_location;
        $post->hidden_receive = $request->hidden_receive;
        $post->hidden_picking = $request->hidden_picking;
        $post->status = $request->status;
        $post->created_by = Auth::id();
        $post->save();

        return response()->json($post);
      }
    }

    public function edit($id)
    {
      $warehouse = Warehouse::Find($id);
      echo json_encode($warehouse);
    }

    public function update($id, Request $request)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {

        // if($request->hidden_receive == "On'")
        // {
        //   $hidden_receive = 1;
        // }else{
        //   $hidden_receive = 0;
        // };
        //
        // if($request->hidden_picking == 'On')
        // {
        //   $hidden_picking = 1;
        // }else{
        //   $hidden_picking = 0;
        // };

        // dd($request->hidden_receive);


        $post = Warehouse::Find($id);
        $post->warehouse_code = $request->warehouse_code;
        $post->warehouse_name = $request->warehouse_name;
        $post->rack_capacity = $request->rack_capacity;
        $post->description = $request->description;
        $post->store_location = $request->store_location;
        $post->hidden_receive = $request->hidden_receive;
        $post->hidden_picking = $request->hidden_picking;
        $post->status = $request->status;
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post);
      }

    }

    public function delete($id)
    {
      $post =  Warehouse::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = Warehouse::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);

  }
}
