<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Model\Popup;
use App\Model\Bay;
use Maatwebsite\Excel\Facades\Excel;


class MainController extends Controller
{
    public function index()
    { 

    	return view ('main.index');
    }

    public function notif()
    {

    	return view ('main.notif.index');
    }

    public function downloadExcel()
    {
        return Excel::store(new UsersExport, 'users.csv', storage_path('public'));

    }
    
}
