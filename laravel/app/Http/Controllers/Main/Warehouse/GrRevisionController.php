<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\GrRevisionRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\GrRevision; 
use App\Model\GrRevisionDetail; 
use App\Model\GoodsReceive; 
use App\Model\GoodsReceiveDetail; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class GrRevisionController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'gr_revision_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $gr_revisions = GrRevision::all();
      return view ('main.warehouse.gr_revision.index', compact('gr_revisions'));
    }

    public function data(Request $request)
    {    

        $itemdata = DB::table('gr_revision')
            ->leftjoin('goods_receive', 'gr_revision.goods_receive_id', '=', 'goods_receive.id') 
            ->select('gr_revision.*', 'goods_receive.no_transaction AS gr_number', 'goods_receive.date_transaction AS gr_date');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('gr_revision.status', $id);
            }else{
                $itemdata->where('gr_revision.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(gr_revisiondate_transaction, 'gr_revision.no_transaction') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="gr-revision/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          } 
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');  

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO gr_revision (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(gr_revision.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW()) 
        FROM
        gr_revision WHERE gr_revision.code_transaction = '".$code_transaction."' AND DATE_FORMAT(gr_revision.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('gr_revision')->max('id'); 

      $gr_revision = GrRevision::where('id', $lastInsertedID)->first();  
      $gr_revision->status = 0;     
      $gr_revision->created_by = $userid;
      $gr_revision->save();

      $user_log = new UserLog;  
      $user_log->user_id = $userid;     
      $user_log->scope = 'GR REVISION';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $gr_revision->no_transaction,
                            'source' => 'WMS',
                            'gr_revision_id' => $lastInsertedID 
                        ]);
      $user_log->save();
 

      return redirect('main/warehouse/gr-revision/edit/'.$lastInsertedID.''); 
    }

    public function edit($id)
    {
      $gr_revision = GrRevision::Find($id);
      // $goods_receive_list = GoodsReceive::all()->pluck('no_transaction', 'id');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      $sql = 'SELECT
                goods_receive.id,
                CONCAT(goods_receive.no_transaction, " [", count_id, "]") as no_transaction
              FROM
                goods_receive
              INNER JOIN (
                SELECT
                  transaction_id,
                  COUNT(id) AS count_id
                FROM
                  goods_receive_detail
                WHERE
                  revision = 1
                GROUP BY
                  transaction_id
              ) AS goods_receive_detail ON goods_receive.id = goods_receive_detail.transaction_id';
      $goods_receive_list = DB::table(DB::raw("($sql) as rs_sql"))->pluck('no_transaction', 'id');

      return view ('main.warehouse.gr_revision.form', compact('gr_revision', 'goods_receive_list','inventory_list', 'uom_list'));
    }

    public function data_modal(Request $request)
    {    
        $sql = 'SELECT
                  goods_receive.id,
                  goods_receive.nodoc,
                  goods_receive.no_transaction AS notransaction,
                  goods_receive_detail.po_number AS purchasing_number,
                  goods_receive_detail.item_line AS item_number,
                  goods_receive_detail.plant,
                  goods_receive_detail.quantity,
                  goods_receive_detail.store_loc AS storage_loc,
                  goods_receive_detail.quantity AS po_qty,
                  goods_receive_detail.unit_of_measure_code AS po_unit,
                  DATE_FORMAT(goods_receive_detail.doc_date,"%d.%m.%Y") AS po_date,
                  DATE_FORMAT(goods_receive_detail.doc_date,"%d.%m.%Y") AS doc_date,
                  RIGHT(goods_receive_detail.supplier_id,7) AS supplier_id,
                  -- goods_receive_detail.batch,
                  RIGHT(goods_receive_detail.batch, 8) AS batch, 
                  goods_receive_detail.store_loc,
                  goods_receive_detail.po_number,
                  goods_receive_detail.item_line,
                  goods_receive_detail.inventory_id,
                  goods_receive_detail.id AS id_detail,
                  supplier.supplier_code as vendor_number,
                  inventory.inventory_code AS material_number,
                  inventory.inventory_code,
                  inventory.inventory_name,
                  goods_receive.description AS doc_header,
                  warehouse.warehouse_name,
                  room.room_name,
                  bay.bay_name,
                  rack.rack_name,
                  goods_receive_detail.pallet_number,
                  goods_receive_detail.revision
                FROM
                  goods_receive_detail
                LEFT OUTER JOIN goods_receive ON goods_receive.id = goods_receive_detail.transaction_id
                LEFT OUTER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
                LEFT OUTER JOIN supplier ON goods_receive_detail.supplier_id = supplier.id
                LEFT OUTER JOIN warehouse ON goods_receive_detail.warehouse_id = warehouse.id
                LEFT OUTER JOIN room ON goods_receive_detail.room_id = room.id
                LEFT OUTER JOIN bay ON goods_receive_detail.bay_id = bay.id
                LEFT OUTER JOIN rack ON goods_receive_detail.rack_id = rack.id
                WHERE goods_receive_detail.revision = 1';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            $id = $request->input('goods_receive_id');
            $itemdata->where('rs_sql.id', $id);
            
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(rs_sql.no_transaction, 'rs_sql.inventory_name') like ?", ["%{$keyword}%"]);
            }
        })

        // dd($itemdata);


        ->addColumn('action', function ($itemdata) {
          $choose = '<a  type="button" class="btn btn-info btn-float btn-xs" href="#" onclick="addValueGr(this, '.$itemdata->id_detail.', '.$itemdata->inventory_id.'); return false;" title="Edit"> <i class="icon-touch"></i></a>'; 
          return ''.$choose.'';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->rawColumns(['action', 'check']) 
        ->make(true);
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = GrRevision::Find($id); 
        $post->date_transaction = $date_transaction_format; 
        $post->description = $request->input('description');   
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\GrRevisionController@index'); 
    }

    public function delete($id)
    {
      $post =  GrRevision::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = GrRevision::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 
    }


  // detail
  public function get_detail(Request $request, $id)
  {  

   if($request->ajax()){ 

      $sql = 'SELECT
                gr_revision_detail.id,
                gr_revision_detail.transaction_id,
                inventory_gr.inventory_name AS inventory_name_gr,
                gr_revision_detail.inventory_id,
                gr_revision_detail.inventory_id_gr,
                gr_revision_detail.quantity,
                gr_revision_detail.quantity_gr,
                gr_revision_detail.`status`,
                inventory.inventory_name
              FROM
                gr_revision_detail
              LEFT OUTER JOIN inventory inventory_gr ON gr_revision_detail.inventory_id_gr = inventory_gr.id
              LEFT JOIN inventory ON gr_revision_detail.inventory_id = inventory.id
              WHERE
              gr_revision_detail.deleted_at IS NULL AND gr_revision_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function post_detail(Request $request, $id)
  {
      try {
        $statement = new GrRevisionDetail;  
        $statement->transaction_id = $id;  
        $statement->inventory_id_gr = $request->inventory_id_gr;  
        $statement->quantity_gr = $request->quantity_gr;  
        $statement->goods_receive_detail_id = $request->goods_receive_detail_id;  
        $statement->inventory_id = $request->inventory_id;  
        $statement->quantity = $request->quantity; 
        $statement->status = 0;
        $statement->save(); 

        $header = GRRevision::where('id', $id)->first();
        $header->goods_receive_id = $request->goods_receive_id;
        $header->save();

        $goods_receive = GoodsReceiveDetail::where('id', $request->goods_receive_detail_id)->first();
        $goods_receive->inventory_id = $request->inventory_id;
        $goods_receive->quantity = $request->quantity;
        $goods_receive->revision = 0;
        $goods_receive->save();
      } catch (\Exception $e) {
        $json['code'] = 0;
        $json['msg'] = "Failed";
      } 
  }

  public function put_detail(Request $request, $id)
  {   
    $statement = GrRevisionDetail::where('id', $id)->first();  
    $statement->inventory_id = $request->inventory_id; 
    $statement->save(); 

    $purchase_order_detail = GoodsReceiveDetail::where('id', $statement->purchase_order_detail_id)->first();  
    $purchase_order_detail->inventory_id = $request->inventory_id;  
    $purchase_order_detail->save(); 
  }

  public function delete_detail(Request $request, $id)
  {   
    $statement = GrRevisionDetail::where('id', $id)->first();   
    $statement->delete(); 
  }

  // =============================================== REPORT ======================================================
  public function report()
  { 
    return view ('main.warehouse.goods_receive.report');
  }
}
