<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockOpnameRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockOpname;
use App\Model\StockOpnameDetail;
use App\Model\StockOpnameMobile;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\GoodsReceiveDetail;
use App\Model\Stock;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class StockOpnameController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_opname_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $stock_opnames = StockOpname::all();
      return view ('main.warehouse.stock_opname.index', compact('stock_opnames'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('stock_opname')
            ->leftjoin('warehouse', 'stock_opname.warehouse_id', '=', 'warehouse.id')
            ->select('stock_opname.*', 'warehouse.warehouse_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('stock_opname.status', $id);
            }else{
                $itemdata->where('stock_opname.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(stock_opname.no_transaction, 'stock_opname.warehouse_id') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('stock_opname', ['delete'])) {
            $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="stock-opname/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('stock_opname', ['delete'])) {
            $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO stock_opname (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_opname.no_transaction, 3)),3))+1001,3)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        stock_opname WHERE stock_opname.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_opname.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('stock_opname')->max('id');

      $stock_opname = StockOpname::where('id', $lastInsertedID)->first();
      $stock_opname->status = 0;
      $stock_opname->created_by = $userid;
      $stock_opname->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'STOCK OPNAME';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'stock_opname_id' => $lastInsertedID
                        ]);
      $user_log->save();


      return redirect('main/warehouse/stock-opname/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $stock_opname = StockOpname::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $inventory_list = Inventory::all()->pluck('inventory_name', 'id');

      // $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();
      // $stock_list = Stock::all();

      // $stock_list = DB::table('stock')
      //     ->join('pallet', 'stock.pallet_id', '=', 'pallet.id')
      //     ->select('stock.*', 'pallet.pallet_code')
      //     ->get();

      $stock_list = DB::table('stock')
          ->join('inventory', 'stock.inventory_id', '=', 'inventory.id')
          ->select('stock.*', 'inventory.inventory_code', 'inventory.inventory_name')
          ->get();


      return view ('main.warehouse.stock_opname.form', compact('stock_opname', 'warehouse_list', 'room_list', 'rack_list', 'pallet_list', 'inventory_list', 'uom_list', 'stock_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = StockOpname::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        // $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\StockOpnameController@index');
    }


    public function generate($id, Request $request)
    {

        // $stock = Stock::all();

        // foreach($stock as $stocks)
        // {
        //   $post = new StockOpnameDetail;
        //   $post->transaction_id = $id;
        //   $post->inventory_id = $stocks->inventory_id;
        //   $post->quantity = $stocks->quantity;
        //   $post->rack_id = $stocks->rack_id;
        //   $post->room_id = $stocks->room_id;
        //   $post->bay_id = $stocks->bay_id;
        //   $post->batch = $stocks->batch;
        //   $post->updated_by = Auth::id();
        //   $post->save();
        // }

        $mobile_data = StockOpnameMobile::where('status', '0')->get();

        foreach($mobile_data AS $mobile_datas){

          $stock = Stock::where('pallet_number', $mobile_datas->pallet_number)->where('inventory_id', $mobile_datas->inventory_id)->first();
          
          $goods_receive_detail = GoodsReceiveDetail::where('pallet_number', $mobile_datas->pallet_number)->where('inventory_id', $mobile_datas->inventory_id)->first();

          if(isset($stock)){
            $stock_isset = $stock->quantity;
          }else{
            $stock_isset = 0;
          };

          if(isset($goods_receive_detail)){
            $store_loc = $goods_receive_detail->store_loc;
            $batch = $goods_receive_detail->batch;
          }else{
            $store_loc = 0;
            $batch = 0;
          };

          // post to wms
          $post = New StockOpnameDetail;
          $post->transaction_id = $id;
          $post->warehouse_id = $mobile_datas->warehouse_id;
          $post->inventory_id = $mobile_datas->inventory_id;
          $post->quantity_actual = $mobile_datas->quantity;
          $post->quantity = $stock_isset;
          $post->quantity_adjustment = $mobile_datas->quantity - $stock_isset;
          $post->rack_id = $mobile_datas->rack_id;
          $post->room_id = $mobile_datas->room_id;
          $post->bay_id = $mobile_datas->bay_id;
          $post->batch = $batch;
          $post->store_loc = $store_loc;
          $post->updated_by = Auth::id();
          $post->save();

          // update mobile data
          $mobile_data_update = StockOpnameMobile::where('id', $mobile_datas->id)->first();
          $mobile_data_update->status = 1;
          $mobile_data_update->save();

        };

        return response()->json($mobile_data);


    }

    public function delete($id)
    {
      $post =  StockOpname::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function store_stock(Request $request, $id)
    {
        $inventory = Inventory::all();

        foreach($inventory AS $inventories)
        {
          $poss = new StockOpnameDetail;
          $post->inventory_id = $inventories->id;
          $post->transaction_id = $request->transaction_id;
          $post->save();
        }

        return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = StockOpname::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail

    // detail
    public function get_detail(Request $request, $id)
    {

      if($request->ajax()){

        $sql = 'SELECT
                  stock_opname_detail.id,
                  stock_opname.no_transaction AS notransaction,
                  RIGHT(stock_opname_detail.item_line, 2) AS item_number,
                  stock_opname_detail.plant,
                  stock_opname_detail.quantity,
                  stock_opname_detail.quantity_actual,
                  stock_opname_detail.quantity_adjustment,
                  stock_opname_detail.store_loc AS storage_loc,
                  stock_opname_detail.quantity AS po_qty,
                  stock_opname_detail.unit_of_measure_code AS po_unit,
                  RIGHT(stock_opname_detail.batch, 8) AS batch,
                  stock_opname_detail.store_loc,
                  stock_opname_detail.item_line,
                  stock_opname_detail.inventory_id,
                  stock_opname_detail.status,
                  stock_opname_detail.id AS id_detail,
                  RIGHT(inventory.inventory_code, 7) AS material_number,
                  inventory.inventory_code,
                  inventory.inventory_name,
                  stock_opname.description AS doc_header,
                  warehouse.warehouse_name,
                  room.room_name,
                  bay.bay_name,
                  rack.rack_name,
                  stock_opname_detail.pallet_number
                FROM
                  stock_opname_detail
                LEFT OUTER JOIN stock_opname ON stock_opname.id = stock_opname_detail.transaction_id
                LEFT OUTER JOIN inventory ON stock_opname_detail.inventory_id = inventory.id
                LEFT OUTER JOIN warehouse ON stock_opname_detail.warehouse_id = warehouse.id
                LEFT OUTER JOIN room ON stock_opname_detail.room_id = room.id
                LEFT OUTER JOIN bay ON stock_opname_detail.bay_id = bay.id
                LEFT OUTER JOIN rack ON stock_opname_detail.rack_id = rack.id
                WHERE
                stock_opname_detail.deleted_at IS NULL AND stock_opname_detail.transaction_id = '.$id.'';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

        return Datatables::of($itemdata)

        ->addColumn('action', function ($itemdata) {
          if($itemdata->status > 0){
            return '<a  href="javascript:void(0)" title="Restore" class="btn btn-float btn-warning btn-xs" onclick="restore('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-reload-alt"></i></a>';
          }else{
            return '<a  href="javascript:void(0)" title="Approve" class="btn btn-float btn-success btn-xs" onclick="approve('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-check"></i></a> <a  href="javascript:void(0)" title="Not Approve" class="btn btn-float btn-danger btn-xs" onclick="not_approve('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-close2"></i></a> <a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id_detail."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i>';
          };
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0 || $itemdata->status == null) {
            return '<span class="label label-warning">Outstanding</span>';
          }else if ($itemdata->status == 1){
            return '<span class="label label-success">Approve</span>';
          }else if ($itemdata->status == 9){
            return '<span class="label label-danger">Not Approve</span>';
          };
        })

        ->rawColumns(['action', 'mstatus'])

        ->make(true);
        } else {
          exit("No data available");
        }
    }


  public function post_detail(Request $request, $id)
  {
    $statement = new StockOpnameDetail;
    $statement->transaction_id = $id;
    $statement->inventory_id = $request->inventory_id;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->quantity = $request->quantity;
    $statement->status = 0;
    $statement->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = StockOpnameDetail::where('id', $id)->first();
    $statement->inventory_id = $request->inventory_id;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->quantity_actual = $request->quantity_actual;
    $statement->quantity_adjustment = $request->quantity_actual - $statement->quantity;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = StockOpnameDetail::where('id', $id)->first();
    $statement->delete();
  }

  public function approve_detail(Request $request, $id)
  {
    $statement = StockOpnameDetail::where('id', $id)->first();
    $statement->status = 1;
    $statement->save();
  }

  public function not_approve_detail(Request $request, $id)
  {
    $statement = StockOpnameDetail::where('id', $id)->first();
    $statement->status = 9;
    $statement->save();
  }

  public function restore_detail(Request $request, $id)
  {
    $statement = StockOpnameDetail::where('id', $id)->first();
    $statement->status = 0;
    $statement->save();
  }

  public function edit_detail($id)
  {
    // $stock_opname = StockOpnameDetail::Find($id);
    $sql = 'SELECT
              stock_opname_detail.id,
              stock_opname.no_transaction AS notransaction,
              RIGHT(stock_opname_detail.item_line, 2) AS item_number,
              stock_opname_detail.plant,
              stock_opname_detail.quantity,
              stock_opname_detail.quantity_actual,
              stock_opname_detail.quantity_adjustment,
              stock_opname_detail.store_loc AS storage_loc,
              stock_opname_detail.quantity AS po_qty,
              stock_opname_detail.unit_of_measure_code AS po_unit,
              RIGHT(stock_opname_detail.batch, 8) AS batch,
              stock_opname_detail.store_loc,
              stock_opname_detail.item_line,
              stock_opname_detail.inventory_id,
              stock_opname_detail.status,
              stock_opname_detail.id AS id_detail,
              RIGHT(inventory.inventory_code, 7) AS material_number,
              inventory.inventory_code,
              inventory.inventory_name,
              stock_opname.description AS doc_header,
              warehouse.warehouse_name,
              room.room_name,
              bay.bay_name,
              rack.rack_name,
              stock_opname_detail.pallet_number
            FROM
              stock_opname_detail
            LEFT OUTER JOIN stock_opname ON stock_opname.id = stock_opname_detail.transaction_id
            LEFT OUTER JOIN inventory ON stock_opname_detail.inventory_id = inventory.id
            LEFT OUTER JOIN warehouse ON stock_opname_detail.warehouse_id = warehouse.id
            LEFT OUTER JOIN room ON stock_opname_detail.room_id = room.id
            LEFT OUTER JOIN bay ON stock_opname_detail.bay_id = bay.id
            LEFT OUTER JOIN rack ON stock_opname_detail.rack_id = rack.id
            WHERE
            stock_opname_detail.deleted_at IS NULL AND stock_opname_detail.id = '.$id.'';
    $stock_opname = DB::table(DB::raw("($sql) as rs_sql"))->first();
    
    echo json_encode($stock_opname);
  }
}
