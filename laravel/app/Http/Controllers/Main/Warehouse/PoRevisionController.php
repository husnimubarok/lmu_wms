<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PoRevisionRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\PoRevision; 
use App\Model\PoRevisionDetail; 
use App\Model\PurchaseOrder; 
use App\Model\PurchaseOrderDetail; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class PoRevisionController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'po_revision_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $po_revisions = PoRevision::all();
      return view ('main.warehouse.po_revision.index', compact('po_revisions'));
    }

    public function data(Request $request)
    {    

        $itemdata = DB::table('po_revision')
            ->leftjoin('purchase_order', 'po_revision.purchase_order_id', '=', 'purchase_order.id') 
            ->select('po_revision.*', 'purchase_order.po_number');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('po_revision.status', $id);
            }else{
                $itemdata->where('po_revision.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(po_revision.no_transaction, 'po_revision.warehouse_id') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="po-revision/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          } 
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');  

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO po_revision (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(po_revision.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW()) 
        FROM
        po_revision WHERE po_revision.code_transaction = '".$code_transaction."' AND DATE_FORMAT(po_revision.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('po_revision')->max('id'); 

      $po_revision = PoRevision::where('id', $lastInsertedID)->first();  
      $po_revision->status = 0;     
      $po_revision->created_by = $userid;
      $po_revision->save();

      $user_log = new UserLog;  
      $user_log->user_id = $userid;     
      $user_log->scope = 'PO REVISION';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $po_revision->no_transaction,
                            'source' => 'WMS',
                            'po_revision_id' => $lastInsertedID 
                        ]);
      $user_log->save();
 

      return redirect('main/warehouse/po-revision/edit/'.$lastInsertedID.''); 
    }

    public function edit($id)
    {
      $po_revision = PoRevision::Find($id);
      $purchase_order_list = PurchaseOrder::all()->pluck('po_number', 'id');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      return view ('main.warehouse.po_revision.form', compact('po_revision', 'purchase_order_list','inventory_list', 'uom_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = PoRevision::Find($id); 
        $post->date_transaction = $date_transaction_format; 
        $post->description = $request->input('description');  
        $post->purchase_order_id = $request->input('purchase_order_id');  
        $post->po_number_new = $request->input('po_number_new');  
        // $post->status = $request->input('status'); 
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\PoRevisionController@index'); 
    }

    public function delete($id)
    {
      $post =  PoRevision::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = PoRevision::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 
    }


  // detail
  public function get_detail(Request $request, $id)
  {  

   if($request->ajax()){ 

      $sql = 'SELECT
                po_revision_detail.id,
                po_revision_detail.transaction_id,
                inventory.inventory_name AS inventory_name,
                po_revision_detail.unit_of_measure_code AS inventory_unit_name,
                po_revision_detail.inventory_id, 
                po_revision_detail.quantity, 
                po_revision_detail.`status` 
              FROM
                po_revision_detail
              LEFT JOIN inventory ON po_revision_detail.inventory_id = inventory.id
              WHERE
              po_revision_detail.deleted_at IS NULL AND po_revision_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function post_detail(Request $request, $id)
  {
    DB::delete("DELETE FROM po_revision_detail WHERE transaction_id = ".$id."");

    // dd($request->purchase_order_id);

    $purchase_order_detail = PurchaseOrderDetail::where('purchase_order_id', $request->purchase_order_id)->get(); 
    
    foreach($purchase_order_detail AS $purchase_order_details)
    {
      $statement = new PoRevisionDetail;  
      $statement->transaction_id = $id;  
      $statement->inventory_id = $purchase_order_details->inventory_id;  
      $statement->quantity = $purchase_order_details->order_qty; 
      $statement->purchase_order_detail_id = $purchase_order_details->id; 
      $statement->status = 0;
      $statement->save(); 
    };

  }

  public function put_detail(Request $request, $id)
  {   
    $statement = PoRevisionDetail::where('id', $id)->first();  
    $statement->inventory_id = $request->inventory_id; 
    $statement->save(); 

    $purchase_order_detail = PurchaseOrderDetail::where('id', $statement->purchase_order_detail_id)->first();  
    $purchase_order_detail->inventory_id = $request->inventory_id;  
    $purchase_order_detail->save(); 
  }

  public function delete_detail(Request $request, $id)
  {   
    $statement = PoRevisionDetail::where('id', $id)->first();   
    $statement->delete(); 
  }

  // =============================================== REPORT ======================================================
  public function report()
  { 
    return view ('main.warehouse.goods_receive.report');
  }
}
