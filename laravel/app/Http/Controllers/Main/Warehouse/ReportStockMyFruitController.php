<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;
use App\Model\StockMyFruit;
use App\Model\Warehouse;
use App\Model\Inventory;
use App\Model\InventoryGroup;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\ViewStockLedger;
use App\Model\StockImport;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use File;
use Storage;


class ReportStockMyFruitController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      return view ('main.warehouse.report_stock_my_fruit.index');
    }

    public function data(Request $request)
    {

        $sql_header = 'SELECT
                        	`stock_my_fruit`.*, FORMAT(`stock_my_fruit`.quantity,0) AS stock_qty, `warehouse`.`warehouse_name`,
                        	`room`.`room_name`,
                        	`bay`.`bay_name`,
                        	`rack`.`rack_name`,
                          `warehouse`.store_location
                        FROM
                        	`stock_my_fruit`
                        LEFT JOIN `warehouse` ON `stock_my_fruit`.`warehouse_id` = `warehouse`.`id`
                        LEFT JOIN `room` ON `stock_my_fruit`.`room_id` = `room`.`id`
                        LEFT JOIN `rack` ON `stock_my_fruit`.`rack_id` = `rack`.`id`
                        LEFT JOIN `bay` ON `stock_my_fruit`.`bay_id` = `bay`.`id`
                        WHERE `stock_my_fruit`.quantity > 0
                        ORDER BY
                        	`bay_name` ASC';
        $itemdata = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"));

        return Datatables::of($itemdata)

        ->make(true);
    }


  public function get_my_fruit()
  {
      $group_null = Inventory::whereNull('inventory_group')->whereNull('inventory_group_id')->get();

      foreach($group_null as $group_nulls)
      {
        $post_null = Inventory::where('id', $group_nulls->id)->first();
        $post_null->inventory_group_id = 513;
        $post_null->save();
      };

      $inventory = Inventory::whereNull('inventory_group_id')->get();

      foreach($inventory as $inventorys)
      {
          $inventory_group = InventoryGroup::where('inventory_group_code', $inventorys->inventory_group)->first();

          // dd($inventory_group);

          if(isset($inventory_group))
          {
            $post = Inventory::where('inventory_group', $inventory_group->inventory_group_code)->whereNull('inventory_group_id')->first();
            // dd($post);
            $post->inventory_group_id = $inventory_group->id;
            $post->save();
          }else{
            $post = new InventoryGroup;
            $post->inventory_group_code = $inventorys->inventory_group;
            $post->inventory_group_name = $inventorys->inventory_group;
            $post->temprature = 0;
            $post->temprature_id = 0;
            $post->description = $inventorys->inventory_group;
            $post->status = 0;
            $post->save();
          }

      };

      //  (new FastExcel)->configureCsv(';', '#')->import(public_path('stock/stock1.csv'), function ($line) {

      //     return StockImport::create([
      //       'warehouse' => $line['Warehouse'],
      //       'room' => $line['Room'],
      //       'bay' => $line['Bay'],
      //       'rack' => $line['Rack'],
      //       'pallet_number' => $line['Pallet'],
      //       'batch' => $line['Batch'],
      //       'inventory' => $line['SKU'],
      //       'unit' => $line['Unit'],
      //       'quantity' => $line['Stock'],
      //     ]);
      // });


      $myfruit = file_get_contents('https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/my-fruit');
      $myfruit_decode = json_decode($myfruit, true);

      StockMyFruit::query()->truncate();

      if(isset($myfruit_decode['inidatasap']['IT_FRUIT']))
      {
        foreach ($myfruit_decode['inidatasap']['IT_FRUIT'] as $field => $value) {

          $inventory = Inventory::where('inventory_code', $value['MATNR'])->first();

          if(isset($inventory))
          {
            $post = New StockMyFruit;
            $post->inventory_id = $inventory->id;
            $post->inventory_code = $value['MATNR'];
            $post->inventory_name = $value['MAKTX'];
            $post->quantity = str_replace(".", "", $value['QTY_STOCK']);
            $post->unit = $value['UNIT'];
            $post->pallet_number = "PLT-MYFR01";
            $post->warehouse_id = 9;
            $post->rack_id = 2220;
            $post->room_id = 12;
            $post->bay_id = 0;
            $post->batch = 0;
            $post->save();
          };
        };
      };

  }

}
