<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryOrderRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\User;
use App\Model\DeliveryOrder;
use App\Model\DeliveryOrderDetail;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\InventoryReceive;
use App\Model\InventoryReceiveDetail;
use App\Model\Picking;
use App\Model\PickingDetail;
use App\Model\Preference;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class DeliveryOrderController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'delivery_order_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index_open()
    {
      return view ('main.warehouse.delivery_order.index_open');
    }

    public function data_open(Request $request)
    {

        //$itemdata = InventoryReceive::all();
      $sql = 'SELECT
              picking.id,
              purchase_order.picking_number,
              purchase_order.doc_date
              FROM
              picking
              INNER JOIN purchase_order ON purchase_order.id = picking.purchase_order_id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();
        return Datatables::of($itemdata)

        ->addColumn('action', function ($itemdata) {
          $edit = '<a href="open/edit/'.$itemdata->id.'" type="button" class="btn btn-xs bg-teal-400 btn-labeled"><b><i class="icon-folder"></i></b> Open</a>';
          return ''.$edit.'';
        })

        ->rawColumns(['action'])
        ->make(true);
    }

    public function edit_open(Request $request, $id)
    {

       $sql_picking_detail = 'SELECT
                                picking_detail.id,
                                inventory.inventory_code,
                                inventory.inventory_name,
                                picking_detail.quantity,
                                picking_detail.pallet_number,
                                picking_detail.inventory_id,
                                picking.picking_number,
                                picking.doc_date,
                                warehouse.warehouse_name,
                                rack.rack_name,
                                bay.bay_name,
                                room.room_name
                              FROM
                                picking_detail
                              LEFT JOIN picking ON picking_detail.transaction_id = picking.id
                              LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                              LEFT JOIN inventory ON picking_detail.inventory_id = inventory.id
                              LEFT JOIN rack ON picking_detail.rack_id = rack.id
                              LEFT JOIN bay ON picking_detail.bay_id = bay.id
                              LEFT JOIN room ON picking_detail.room_id = room.id
                              WHERE picking_detail.quantity > 0';
      $picking_detail = DB::table(DB::raw("($sql_picking_detail) as rs_sql"))->get();

      return view ('main.warehouse.delivery_order.form_open', compact('picking', 'picking_detail'));
    }

    public function store_open(Request $request)
    {
        $userid= Auth::id();
        $codetrans = $request->input('code_trans');
        // $codetrans = 'GDIS/';

        DB::insert("INSERT INTO delivery_order (code_transaction, no_transaction, date_transaction, created_by, created_at)
          SELECT
          '".$codetrans."', IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%y%m%d'),RIGHT((RIGHT(MAX(RIGHT(delivery_order.no_transaction,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%y%m%d'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW())
          FROM
          delivery_order");

        $lastInsertedID = DB::table('delivery_order')->max('id');

        // foreach($request->input('good_receive_detail') as $detail_key => $detail_value)
        // {
        //   $picking_detail = InventoryReceiveDetail::where('id', $detail_value['id'])->first();

        //   $good_receive_detail = new DeliveryOrderDetail;
        //   $good_receive_detail->transaction_id = $lastInsertedID;
        //   $good_receive_detail->inventory_id =  $detail_value['inventory_id'];
        //   $good_receive_detail->quantity =  $picking_detail->quantity;
        //   $good_receive_detail->save();
        // }

      return redirect('main/warehouse/delivery-order/edit/'.$lastInsertedID.'');
    }

    public function index()
    {
      $delivery_orders = DeliveryOrder::all();
      return view ('main.warehouse.delivery_order.index', compact('delivery_orders'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('delivery_order')
            ->whereNull('delivery_order.deleted_at')
            ->select('delivery_order.*');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($id = $request->input('filter_status')) {
                $itemdata->where('delivery_order.status', $id);
            }else{
                $itemdata->where('delivery_order.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(ifnull(delivery_order.no_transaction,''), ifnull(delivery_order.nodo,'')) like ?", ["%{$keyword}%"]);
            }
            $itemdata->whereBetween('delivery_order.date_transaction', [$date_from_format, $date_to_format]);
            $itemdata->orderBy('delivery_order.id', 'DESC');
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('goods_issue', ['update'])) {
            $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="delivery-order/edit/'.$itemdata->id.'" title="Display"> <i class="icon-pencil7"></i> </a>';
            $print = '<a type="button" target="blank_" class="btn btn-info btn-float btn-xs" href="delivery-order/slip/'.$itemdata->id.'" title="Display"> <i class="icon-printer2"></i> </a>';
          }else{
            $edit = '';
            $print = '';
          };

          if (RoleManager::actionStart('goods_issue', ['delete'])) {
            $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $archieve_disabled = '<a type="button" disabled="disabled" class="btn btn-warning btn-float btn-xs" title="Archive"> <i class="icon-archive"></i></a>';
            $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $archieve_disabled = '';
            $reactive = '';
          };
        //   if($itemdata->status ==1){
        //     return ''.$reactive.'';
        //   }else{
        //     return ''.$edit.' '.$archieve.'';
        //   }
        // })

        if($itemdata->status ==1){
          return ''.$reactive.'';
          }elseif(strlen($itemdata->nodo) > 0){
            if($itemdata->code_transaction == 'GDMN/')
            {
              return ''.$edit.' '.$archieve_disabled.' '.$print.'';
            }else{
              return ''.$edit.' '.$archieve_disabled.'';
            };
          }else{
            if($itemdata->code_transaction == 'GDMN/')
            {
              return ''.$edit.' '.$archieve.'  '.$print.'';
            }else{
              return ''.$edit.' '.$archieve.'';

            };
          }
        })

        ->addColumn('details_url', function($itemdata) {
            return url('eloquent/details-data/' . $itemdata->id);
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('picking_no', function($itemdata) {
            $picking_det = DeliveryOrderDetail::where('transaction_id', $itemdata->id)->first();

            // foreach($picking_det as $picking_dets)
            // {
            //   $picking_data = $picking_dets->picking_number;
            // };

            // $test = $picking_data;
            // print_r($test);
            if(isset($picking_det))
            {
              return $picking_det->picking_number;
            }else{
              return '-';
            };
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check', 'description'])
        ->make(true);
    }

    public function getDetailsData($id)
    {
        $posts = DeliveryOrderDetail::find($id)->posts();

        return Datatables::of($posts)->make(true);
    }

    public function data_modal(Request $request)
    {

           $sql = 'SELECT
                      picking_detail.id,
                      inventory.inventory_code,
                      inventory.inventory_name,
                      picking_detail.quantity,
                      picking_detail.pallet_number,
                      IFNULL(picking_detail.inventory_id,0) AS inventory_id,
                      picking.picking_number,
                      picking_detail.id AS id_detail,
                      picking_detail.itemdo,
                      picking_detail.item_line,
                      picking_detail.pick_qty,
                      IFNULL(picking_detail.warehouse_id,0) AS warehouse_id,
                      IFNULL(picking_detail.rack_id,0) AS rack_id,
                      IFNULL(picking_detail.bay_id,0) AS bay_id,
                      IFNULL(picking_detail.room_id,0) AS room_id,
                      IFNULL(picking_detail.batch,0) AS batch,
                      picking.doc_date,
                      picking.manual,
                      warehouse.warehouse_name,
                      rack.rack_name,
                      bay.bay_name,
                      room.room_name
                  FROM
                    picking_detail
                  LEFT JOIN picking ON picking_detail.transaction_id = picking.id
                  LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                  LEFT JOIN inventory ON picking_detail.inventory_id = inventory.id
                  LEFT JOIN rack ON picking_detail.rack_id = rack.id
                  LEFT JOIN bay ON picking_detail.bay_id = bay.id
                  LEFT JOIN room ON picking_detail.room_id = room.id
                  WHERE picking_detail.status = 0 AND picking_detail.deleted_at IS NULL AND picking_detail.pick_qty > 0';
          $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {

            if($request->input('code_transaction') == 'GDIS/') {
                $itemdata->where('rs_sql.manual', 0 );
                // ->orWhere('rs_sql.manual', null);
            }
            if($request->input('code_transaction') == 'GDMN/') {
                $itemdata->where('rs_sql.manual', 1 );
                // ->orWhere('rs_sql.manual', null);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(rs_sql.rack_name, ifnull(rs_sql.inventory_code,''), ifnull(rs_sql.inventory_name,''), ifnull(rs_sql.picking_number,''),  ifnull(rs_sql.pallet_number,'')) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $choose = '<a  type="button" class="btn btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->id.', '.$itemdata->inventory_id.', '.$itemdata->warehouse_id.', '.$itemdata->room_id.', '.$itemdata->bay_id.', '.$itemdata->rack_id.', '.$itemdata->id_detail.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
          return ''.$choose.'';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->rawColumns(['action', 'check'])
        ->make(true);
    }

    public function pick(Request $request, $id_header)
    {

      $ids = $request->ids;
      // dd($ids);
      foreach($ids as $key => $id) {

          $id_foo = str_replace("'","",$id);


          $sql = 'SELECT
                      picking_detail.id,
                      inventory.inventory_code,
                      inventory.inventory_name,
                      picking_detail.quantity,
                      picking_detail.pallet_number,
                      inventory.unit_of_measure,
                      IFNULL(picking_detail.inventory_id,0) AS inventory_id,
                      picking.picking_number,
                      picking_detail.id AS id_detail,
                      picking_detail.itemdo,
                      picking_detail.item_line,
                      picking_detail.pick_qty,
                      IFNULL(picking_detail.warehouse_id,0) AS warehouse_id,
                      IFNULL(picking_detail.rack_id,0) AS rack_id,
                      IFNULL(picking_detail.bay_id,0) AS bay_id,
                      IFNULL(picking_detail.room_id,0) AS room_id,
                      IFNULL(picking_detail.batch,0) AS batch,
                      picking.doc_date,
                      picking.customer,
                      picking.marketing,
                      warehouse.warehouse_name,
                      rack.rack_name,
                      bay.bay_name,
                      room.room_name
                  FROM
                    picking_detail
                  LEFT JOIN picking ON picking_detail.transaction_id = picking.id
                  LEFT JOIN warehouse ON picking_detail.warehouse_id = warehouse.id
                  LEFT JOIN inventory ON picking_detail.inventory_id = inventory.id
                  LEFT JOIN rack ON picking_detail.rack_id = rack.id
                  LEFT JOIN bay ON picking_detail.bay_id = bay.id
                  LEFT JOIN room ON picking_detail.room_id = room.id';
          $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id_foo)->first();


            $post = PickingDetail::Find($id_foo);
          if ($post)
            $post->status = 9;
            $post->save();

            $statement = new DeliveryOrderDetail;
            $statement->transaction_id = $id_header;
            $statement->inventory_id = $itemdata->inventory_id;
            $statement->unit_of_measure_code = $itemdata->unit_of_measure;
            $statement->doc_date = $itemdata->doc_date;
            $statement->item_line = $itemdata->item_line;
            $statement->quantity = $itemdata->pick_qty;
            $statement->pick_qty = $itemdata->pick_qty;
            $statement->warehouse_id = $itemdata->warehouse_id;
            $statement->room_id = $itemdata->room_id;
            $statement->bay_id = $itemdata->bay_id;
            $statement->rack_id = $itemdata->rack_id;
            $statement->picking_number = $itemdata->picking_number;
            $statement->picking_detail_id = $itemdata->id;
            $statement->pallet_number = $itemdata->pallet_number;
            $statement->batch = $itemdata->batch;
            $statement->status = 0;
            $statement->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');
      // $code_transaction = 'GDIS/';

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO delivery_order (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(delivery_order.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        delivery_order WHERE delivery_order.code_transaction = '".$code_transaction."' AND DATE_FORMAT(delivery_order.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('delivery_order')->max('id');

      $delivery_order = DeliveryOrder::where('id', $lastInsertedID)->first();
      $delivery_order->status = 0;
      $delivery_order->created_by = $userid;
      $delivery_order->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'GOODS ISSUE';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $delivery_order->no_transaction,
                            'source' => 'WMS',
                            'delivery_order_id' => $lastInsertedID
                        ]);
      $user_log->save();

      return redirect('main/warehouse/delivery-order/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $delivery_order = DeliveryOrder::Find($id);
      $detail_max = DeliveryOrderDetail::where('transaction_id', $id)->first();
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');

      $delivery_order_detail = DeliveryOrderDetail::where('transaction_id', $id)->first();

      if(isset($delivery_order_detail))
      {
        $picking_det = Picking::where('picking_number', $delivery_order_detail->picking_number)->first();
      };

      return view ('main.warehouse.delivery_order.form', compact('delivery_order',  'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list','inventory_list', 'uom_list', 'detail_max', 'picking_det'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = DeliveryOrder::Find($id);
        $post->description = $request->input('description');
        $post->warehouse_id = $request->input('warehouse_id');
        $post->no_sj = $request->input('no_sj');
        $post->customer_name = $request->input('customer_name');
        $post->customer_address = $request->input('customer_address');
        $post->send_to = $request->input('send_to');
        $post->notes = $request->input('notes');
        $post->customer = $request->input('customer');
        $post->driver = $request->input('driver');

        $post->spp = $request->input('spp');
        $post->sales_order = $request->input('sales_order');
        $post->delivery = $request->input('delivery');
        $post->jam_kirim = $request->input('jam_kirim');
        $post->driver = $request->input('driver');
        $post->tlp_driver = $request->input('tlp_driver');
        $post->no_pol = $request->input('no_pol');
        $post->bag_gudang = $request->input('bag_gudang');
        $post->save_manual = 1;

        $post->updated_by = Auth::id();
        $post->save();

        $userid= Auth::id();
        $user_log = new UserLog;
        $user_log->user_id = $userid;
        $user_log->scope = 'GOODS ISSUE';
        $user_log->data = json_encode([
                              'action' => 'update',
                              'transaction_number' => $post->no_transaction,
                              'description' => $request->input('description'),
                              'source' => 'WMS',
                              'goods_receive_id' => $id
                          ]);
        $user_log->save();


        return redirect()->action('Main\Warehouse\DeliveryOrderController@index');
    }

    public function cancel(Request $request, $id)
    {
      $statement = DeliveryOrder::where('id', $id)->first();
      $statement->status = 1;
      $statement->save();

      $do_detail = DeliveryOrderDetail::where('transaction_id', $id)->get();

      foreach($do_detail as $do_details)
      {
        $post = PickingDetail::where('id', $do_details->picking_detail_id)->first();
        $post->status = 1;
        $post->save();
      }
    }

    public function delete($id)
    {
      $post =  DeliveryOrder::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = DeliveryOrder::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
                delivery_order_detail.id,
                delivery_order_detail.transaction_id,
                RIGHT(inventory.inventory_code, 7) AS inventory_code,
                inventory.inventory_name AS inventory_name,
                delivery_order_detail.unit_of_measure_code AS inventory_unit_name,
                delivery_order_detail.inventory_id,
                RIGHT(delivery_order_detail.item_line, 2) AS item_line,
                delivery_order_detail.quantity,
                delivery_order_detail.pallet_number,
                delivery_order_detail.picking_number,
                delivery_order_detail.pick_qty,
                delivery_order_detail.batch,
                delivery_order_detail.notes_item,
                delivery_order.nodo,
                warehouse.warehouse_name,
                room.room_name,
                bay.bay_name,
                rack.rack_name,
                delivery_order_detail.`status`
              FROM
                delivery_order INNER JOIN delivery_order_detail ON delivery_order_detail.transaction_id = delivery_order.id
              LEFT JOIN inventory ON delivery_order_detail.inventory_id = inventory.id
              LEFT OUTER JOIN warehouse ON delivery_order_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN room ON delivery_order_detail.room_id = room.id
              LEFT OUTER JOIN bay ON delivery_order_detail.bay_id = bay.id
              LEFT OUTER JOIN rack ON delivery_order_detail.rack_id = rack.id
              WHERE
              delivery_order_detail.deleted_at IS NULL AND delivery_order_detail.quantity > 0 AND delivery_order_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        if(strlen($itemdata->nodo) > 0)
        {
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" disabled> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" disabled><i class="icon-trash"></i></a>';
        }else{
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-primary btn-float btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
        }
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }

  public function post_detail(Request $request, $id)
  {
    $statement = new DeliveryOrderDetail;
    $statement->transaction_id = $id;
    $statement->inventory_id = $request->inventory_id;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->item_line = $request->item_line;
    $statement->quantity = $request->quantity;
    $statement->pick_qty = $request->pick_qty;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->picking_number = $request->picking_number;
    $statement->picking_detail_id = $request->picking_detail_id;
    $statement->pallet_number = $request->pallet_number;
    $statement->batch = $request->batch;
    $statement->status = 0;
    $statement->save();

    $open_pick = PickingDetail::where('id', $request->picking_detail_id)->first();
    $open_pick->status = 9;
    $open_pick->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = DeliveryOrderDetail::where('id', $id)->first();
    $header = DeliveryOrder::where('id', $statement->transaction_id)->first();
    if($header->code_transaction == 'GDMN/')
    {
      $statement->unit_of_measure_code = $request->unit_of_measure_code;
    };
    // $statement->inventory_id = $request->inventory_id;
    // $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->pick_qty = $request->pick_qty;
    $statement->quantity = $request->quantity;
    $statement->notes_item = $request->notes_item;
    // $statement->picking_number = $request->picking_number;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = DeliveryOrderDetail::where('id', $id)->first();

    $open_pick = PickingDetail::where('id', $statement->picking_detail_id)->first();
    if(isset($open_pick))
    {
      $open_pick->status = 0;
      $open_pick->save();
    };

    $statement->delete();
  }

  public function post_response_header(Request $request, $id)
  {
    $date_transaction_array = explode("-",$request->date_transaction); // split the array
    $var_day_transaction = $date_transaction_array[0]; //day seqment
    $var_month_transaction = $date_transaction_array[1]; //month segment
    $var_year_transaction = $date_transaction_array[2]; //year segment
    $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

    $statement = DeliveryOrder::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->date_transaction = $date_transaction_format;
    $statement->save();

  }

  public function post_response(Request $request, $id)
  {
    $date_transaction_array = explode("-",$request->date_transaction); // split the array
    $var_day_transaction = $date_transaction_array[0]; //day seqment
    $var_month_transaction = $date_transaction_array[1]; //month segment
    $var_year_transaction = $date_transaction_array[2]; //year segment
    $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

    $statement = DeliveryOrder::where('id', $id)->first();
    $statement->nodo = $request->nodo;
    $statement->description = $request->description;
    $statement->date_transaction = $date_transaction_format;
    $statement->save();

    $userid = Auth::id();

    $user_log = new UserLog;
    $user_log->user_id = $userid;
    $user_log->scope = 'GOODS ISSUE';
    $user_log->data = json_encode([
                          'action' => 'update',
                          'transaction_number' => $statement->no_transaction,
                          'description' => $request->description,
                          'source' => 'WMS',
                          'goods_receive_id' => $id
                      ]);
    $user_log->save();
  }

  public function data_detail(Request $request, $id)
  {
    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();
    $end_of_line = DeliveryOrderDetail::where('transaction_id', $id)->max('item_line');

    // dd($end_of_line);

    $sql = 'SELECT
              delivery_order.no_transaction AS giwms,
              delivery_order_detail.picking_number AS nodo,
              delivery_order_detail.item_line AS itemdo,
              DATE_FORMAT(delivery_order.date_transaction, "%Y%m%d") AS post_date,
              inventory.inventory_code AS matrial,
              SUM(delivery_order_detail.quantity) AS qty,
              sum(delivery_order_detail.pick_qty) AS qtypicking
              -- ,
              -- delivery_order_detail.id AS id_detail
            FROM
              delivery_order
            INNER JOIN delivery_order_detail ON delivery_order.id = delivery_order_detail.transaction_id
            INNER JOIN inventory ON delivery_order_detail.inventory_id = inventory.id
            WHERE
            delivery_order_detail.deleted_at IS NULL AND delivery_order_detail.transaction_id = '.$id.'
            GROUP BY
            delivery_order.no_transaction,
            DATE_FORMAT(delivery_order.date_transaction, "%Y%m%d"),
            delivery_order_detail.picking_number,
            delivery_order_detail.item_line,
            inventory.inventory_code';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                    transaction_id,
                    count(item_line) AS item_line
                  FROM
                    (
                      SELECT
                        delivery_order_detail.transaction_id,
                        delivery_order_detail.item_line
                      FROM
                        delivery_order_detail
                      WHERE
                        delivery_order_detail.deleted_at IS NULL
                      AND delivery_order_detail.transaction_id = '.$id.'
                      GROUP BY
                        delivery_order_detail.transaction_id,
                        delivery_order_detail.item_line
                    ) AS derivdtbl
                  GROUP BY
                    transaction_id';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      sleep(3);
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['giwms'] = $datas->giwms;
        $json[$h_key]['nodo'] = $datas->nodo;
        $json[$h_key]['itemdo'] = $datas->itemdo;
        $json[$h_key]['matrial'] = $datas->matrial;
        $json[$h_key]['qty'] = $datas->qty;
        $json[$h_key]['qtypicking'] = $datas->qtypicking;
        $json[$h_key]['post_date'] = $datas->post_date;
        $json[$h_key]['total_items'] = $data_line->item_line;
        if($datas->itemdo == $end_of_line)
        {
          $json[$h_key]['bendera'] = 'X';
        }else{
          $json[$h_key]['bendera'] = '';
        };
      }
    }
    return response()->json($json);
  }

 // =============================================== REPORT ======================================================
 public function report()
 {
   return view ('main.warehouse.delivery_order.report');
 }

 public function data_report(Request $request)
 {

   $sql = 'SELECT
             delivery_order.no_transaction,
             delivery_order.date_transaction,
             delivery_order.status,
             purchase_order.code_vendor,
             inventory.inventory_code,
             inventory.inventory_name,
             delivery_order_detail.unit_of_measure_code,
             delivery_order_detail.quantity,
             delivery_order_detail.plant,
             delivery_order_detail.batch
           FROM
             delivery_order
           LEFT OUTER JOIN delivery_order_detail ON delivery_order.id = delivery_order_detail.transaction_id
           LEFT OUTER JOIN inventory ON delivery_order_detail.inventory_id = inventory.id';
     $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

     return Datatables::of($itemdata)

     ->filter(function ($itemdata) use ($request) {

         $date_from_array = explode("-",$request->input('date_from')); // split the array
         $var_day_from = $date_from_array[0]; //day seqment
         $var_month_from = $date_from_array[1]; //month segment
         $var_year_from = $date_from_array[2]; //year segment
         $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

         $date_to_array = explode("-",$request->input('date_to')); // split the array
         $var_day_to = $date_to_array[0]; //day seqment
         $var_month_to = $date_to_array[1]; //month segment
         $var_year_to = $date_to_array[2]; //year segment
         $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

         if($request->input('date_from') != '' || $request->input('date_to') != '')
         {
           $itemdata->whereBetween('rs_sql.date_transaction', [$date_from_format, $date_to_format]);
           // $itemdata->where('rs_sql.status', 0);
         }else{
           $itemdata->where('rs_sql.status', 0);
         };
     })
     ->make(true);
 }

 public function report_print($id, $id1)
 {
   $date_from_array = explode("-",$id); // split the array
   $var_day_from = $date_from_array[0]; //day seqment
   $var_month_from = $date_from_array[1]; //month segment
   $var_year_from = $date_from_array[2]; //year segment
   $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

   $date_to_array = explode("-",$id1); // split the array
   $var_day_to = $date_to_array[0]; //day seqment
   $var_month_to = $date_to_array[1]; //month segment
   $var_year_to = $date_to_array[2]; //year segment
   $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

   $itemdata = DB::table('goods_receive')
         ->leftjoin('warehouse', 'goods_receive.warehouse_id', '=', 'warehouse.id')
         ->whereBetween('goods_receive.date_transaction', [$date_from_format, $date_to_format])
         ->select('goods_receive.*', 'warehouse.warehouse_name')->get();

   return view ('main.warehouse.delivery_order.report_print', compact('itemdata'));
 }

 public function slip($id)
 {
   $preference = Preference::first();
  //  $delivery_order = DeliveryOrder::where('id', $id)->first();

   $sql_header = 'SELECT
              delivery_order.id,
              delivery_order.code_transaction,
              delivery_order.no_transaction,
              DATE_FORMAT(delivery_order.date_transaction, "%d-%m-%Y") as date_transaction,
              delivery_order.warehouse_id,
              warehouse.warehouse_name,
              delivery_order.description,
              delivery_order.nodo,
              delivery_order.no_sj,
              delivery_order.no_npwp,
              delivery_order.date_pkp,
              delivery_order.customer_name,
              delivery_order.send_from,
              delivery_order.send_to,
              delivery_order.customer_address,
              delivery_order.notes,
              delivery_order.customer,
              delivery_order.spp,
              delivery_order.sales_order,
              delivery_order.delivery,
              delivery_order.jam_kirim,
              delivery_order.driver,
              delivery_order.tlp_driver,
              delivery_order.no_pol,
              delivery_order.bag_gudang
            FROM
              delivery_order
            LEFT JOIN warehouse ON delivery_order.warehouse_id = warehouse.id
            WHERE delivery_order.id = '.$id.'';
    $delivery_order = DB::table(DB::raw("($sql_header) as rs_sql_header"))->first();

   $sql = 'SELECT
              delivery_order.no_transaction,
              DATE_FORMAT(delivery_order.date_transaction, "%d-%m-%Y") as date_transaction,
              delivery_order.status,
              inventory.inventory_code,
              inventory.inventory_name,
              delivery_order_detail.unit_of_measure_code,
              delivery_order_detail.quantity,
              delivery_order_detail.transaction_id,
              delivery_order_detail.notes_item,
              delivery_order_detail.description
            FROM
              delivery_order
            LEFT OUTER JOIN delivery_order_detail ON delivery_order.id = delivery_order_detail.transaction_id
            LEFT OUTER JOIN inventory ON delivery_order_detail.inventory_id = inventory.id';
    $delivery_order_detail = DB::table(DB::raw("($sql) as rs_sql"))->where('transaction_id', $id)->get();

   return view ('main.warehouse.delivery_order.slip', compact('preference', 'delivery_order', 'delivery_order_detail'));
 }

 public function sap_cancelgido(Request $request)
 {
       $curl = curl_init();
       $data = array(
               'nodo' => $request->nodo);

       curl_setopt_array($curl, array(
                                 CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgido",
                                 CURLOPT_RETURNTRANSFER => true,
                                 CURLOPT_POSTFIELDS => http_build_query($data),
                                 CURLOPT_ENCODING => "",
                                 CURLOPT_MAXREDIRS => 10,
                                 CURLOPT_TIMEOUT => 35,
                                 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                 CURLOPT_CUSTOMREQUEST => "POST",
                               ));

       $response = curl_exec($curl);
       $err = curl_error($curl);

       curl_close($curl);

       if ($err) {
       echo "cURL Error #:" . $err;
       } else {
         // echo $response;
         $json = json_decode($response);
         return response()->json($json);
       }
    }

    public function sap_exegido(Request $request)
    {
      $curl = curl_init();
      $data = array(
              'giwms' => $request->giwms);

      curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exegido",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($data),
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                              ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
      echo "cURL Error #:" . $err;
      } else {
        // echo $response;
        $json = json_decode($response);
        return response()->json($json);
      }
   }

   public function sap_gido(Request $request)
   {
     $curl = curl_init();
     $data = array(
           'giwms' => $request->giwms,
           'nodo' => $request->nodo,
           'itemdo' => $request->itemdo,
           'matrial' => $request->matrial,
           'qty' => $request->qty,
           'qtypicking' => $request->qtypicking,
           'notes' => $request->notes,
           'total_items' => $request->total_items,
           'post_date' => $request->post_date
           );

     curl_setopt_array($curl, array(
                               CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-gido",
                               CURLOPT_RETURNTRANSFER => true,
                               CURLOPT_POSTFIELDS => http_build_query($data),
                               CURLOPT_ENCODING => "",
                               CURLOPT_MAXREDIRS => 10,
                               CURLOPT_TIMEOUT => 35,
                               CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                               CURLOPT_CUSTOMREQUEST => "POST",
                             ));

     $response = curl_exec($curl);
     $err = curl_error($curl);

     curl_close($curl);

     if ($err) {
     echo "cURL Error #:" . $err;
     } else {
       // echo $response;
       $json = json_decode($response);
       return response()->json($json);
     }
  }


  public function sap_gido_new(Request $request, $id)
  {

    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();
    $end_of_line = DeliveryOrderDetail::where('transaction_id', $id)->max('item_line');

    // dd($end_of_line);

    $sql = 'SELECT
              delivery_order.no_transaction AS giwms,
              delivery_order.description AS description,
              delivery_order_detail.picking_number AS nodo,
              delivery_order_detail.item_line AS itemdo,
              DATE_FORMAT(delivery_order.date_transaction, "%Y%m%d") AS post_date,
              inventory.inventory_code AS matrial,
              SUM(delivery_order_detail.quantity) AS qty,
              sum(delivery_order_detail.pick_qty) AS qtypicking
              -- ,
              -- delivery_order_detail.id AS id_detail
            FROM
              delivery_order
            INNER JOIN delivery_order_detail ON delivery_order.id = delivery_order_detail.transaction_id
            INNER JOIN inventory ON delivery_order_detail.inventory_id = inventory.id
            WHERE
            delivery_order_detail.deleted_at IS NULL AND delivery_order_detail.transaction_id = '.$id.'
            GROUP BY
            delivery_order.no_transaction,
            delivery_order.description,
            DATE_FORMAT(delivery_order.date_transaction, "%Y%m%d"),
            delivery_order_detail.picking_number,
            delivery_order_detail.item_line,
            inventory.inventory_code';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                    transaction_id,
                    count(item_line) AS item_line
                  FROM
                    (
                      SELECT
                        delivery_order_detail.transaction_id,
                        delivery_order_detail.item_line
                      FROM
                        delivery_order_detail
                      WHERE
                        delivery_order_detail.deleted_at IS NULL
                      AND delivery_order_detail.transaction_id = '.$id.'
                      GROUP BY
                        delivery_order_detail.transaction_id,
                        delivery_order_detail.item_line
                    ) AS derivdtbl
                  GROUP BY
                    transaction_id';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['GI_WMS'] = $datas->giwms;
        $json[$h_key]['VBELN'] = $datas->nodo;
        $json[$h_key]['POSNR_VL'] = $datas->itemdo;
        $json[$h_key]['MATNR'] = $datas->matrial;
        $json[$h_key]['LFIMG'] = $datas->qty;
        $json[$h_key]['PIKMG'] = $datas->qtypicking;
        $json[$h_key]['NOTES'] = $datas->description;
        $json[$h_key]['LINES'] = strval($data_line->item_line);
        $json[$h_key]['POST_DATE'] = $datas->post_date;
      }
    };
 
    $data_json = array_values(
              $json
            );
    
    $test_data = array(
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "10",
                        "MATNR" => "000000000001100090",
                        "LFIMG" => "10",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ],
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "20",
                        "MATNR" => "000000000001100080",
                        "LFIMG" => "50",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ],
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "30",
                        "MATNR" => "000000000001100131",
                        "LFIMG" => "50",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ]
                );


    $curl = curl_init();
   
      // dd(json_encode($data_json));

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-gido",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 3000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data_json),
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // print_r(json_decode($response));
        $json = json_decode($response);
        return response()->json($json);
      }
  }

}
