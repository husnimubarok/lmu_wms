<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockTransfer;
use App\Model\StockTransferDetail;
use App\Model\Warehouse;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class ReportStockBranchController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_branch_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      $stock_branch_transfers = StockTransfer::all();
      return view ('main.warehouse.report_stock_branch.index', compact('stock_branch_transfers'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('stock_branch')
            ->leftjoin('warehouse', 'stock_branch.warehouse_id', '=', 'warehouse.id')
            ->leftjoin('room', 'stock_branch.room_id', '=', 'room.id')
            ->leftjoin('rack', 'stock_branch.rack_id', '=', 'rack.id')
            ->leftjoin('bay', 'stock_branch.bay_id', '=', 'bay.id')
            ->leftjoin('inventory', 'stock_branch.inventory_id', '=', 'inventory.id')
            ->where('stock_branch.quantity', '>', '0')
            ->select('stock_branch.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('stock_branch.status', $id);
            }else{
                $itemdata->where('stock_branch.status', 0);
            }
            if($keyword = $request->input('keyword')) {

              $itemdata->whereRaw("CONCAT(stock_branch.pallet_number, inventory.inventory_name, inventory.inventory_code, stock_branch.id) like ?", ["%{$keyword}%"]);

            }
        })
        ->make(true);
    }

     // ====================================================STOCK MOVEMENT===============================================
     public function index_movement()
     {
       return view ('main.warehouse.report_stock_branch.index_movement');
     }

     public function data_movement(Request $request)
     {

        //  $itemdata = DB::table('stock_branch')
        //      ->leftjoin('warehouse', 'stock_branch.warehouse_id', '=', 'warehouse.id')
        //      ->leftjoin('room', 'stock_branch.room_id', '=', 'room.id')
        //      ->leftjoin('rack', 'stock_branch.rack_id', '=', 'rack.id')
        //      ->leftjoin('bay', 'stock_branch.bay_id', '=', 'bay.id')
        //      ->leftjoin('inventory', 'stock_branch.inventory_id', '=', 'inventory.id')
        //      ->select('stock_branch.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        //      dd($itemdata);
          $sql = 'SELECT
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name,
                    DRVDTBL.unit,
                    DRVDTBL.batch,
                    SUM(goods_receive) AS goods_receive,
                    SUM(picking) AS picking,
                    SUM(gr_return) AS gr_return,
                    SUM(stock_branch_movement) AS stock_movement,
                    SUM(stock_branch) AS stock,
                      0 AS nol
                  FROM
                    (
                      SELECT
                        stock_branch_log.warehouse_id,
                        warehouse.warehouse_name,
                        stock_branch_log.room_id,
                        room.room_name,
                        stock_branch_log.bay_id,
                        bay.bay_name,
                        stock_branch_log.rack_id,
                        rack.rack_name,
                        inventory.inventory_code,
                        inventory.inventory_name,
                        stock_branch_log.unit,
                        stock_branch_log.pallet_number,
                        stock_branch_log.batch,
                        CASE
                      WHEN stock_branch_log.type_transaction = "GOODS RECEIVE" THEN
                        stock_branch_log.quantity
                      ELSE
                        0
                      END AS goods_receive,
                      CASE
                    WHEN stock_branch_log.type_transaction = "GOODS ISSUE" THEN
                      stock_branch_log.quantity
                    ELSE
                      0
                    END AS picking,
                    CASE
                  WHEN stock_branch_log.type_transaction = "GR RETURN" THEN
                    stock_branch_log.quantity
                  ELSE
                    0
                  END AS gr_return,
                  CASE
                  WHEN stock_branch_log.type_transaction = "STOCK MOVEMENT" THEN
                    stock_branch_log.quantity
                  ELSE
                    0
                  END AS stock_branch_movement,
                  stock_branch_log.quantity AS stock_branch
                  FROM
                    stock_branch_log
                  LEFT OUTER JOIN inventory ON stock_branch_log.inventory_id = inventory.id
                  LEFT OUTER JOIN rack ON stock_branch_log.rack_id = rack.id
                  LEFT OUTER JOIN warehouse ON stock_branch_log.warehouse_id = warehouse.id
                  LEFT OUTER JOIN room ON stock_branch_log.room_id = room.id
                  LEFT OUTER JOIN bay ON stock_branch_log.bay_id = bay.id
                    ) AS DRVDTBL
                  GROUP BY
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.batch,
                    DRVDTBL.unit,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name';
          $itemdata = DB::table(DB::raw("(" . $sql . ") as rs_sql"));

         return Datatables::of($itemdata)

         ->filter(function ($itemdata) use ($request) {
             if($keyword = $request->input('keyword')) {
               $itemdata->whereRaw("CONCAT(pallet_number, 'warehouse_name') like ?", ["%{$keyword}%"]);
             }
         })

         ->addColumn('action', function ($itemdata) {
           return '0';
         })
        ->rawColumns(['action', 'check'])
         ->make(true);
     }


     // ====================================================STOCK LEDGER===============================================
     public function index_ledger()
     {
       return view ('main.warehouse.report_stock_branch.index_ledger');
     }

     public function data_ledger(Request $request)
     {

         $itemdata = DB::table('stock_branch_log')
             ->leftjoin('warehouse', 'stock_branch_log.warehouse_id', '=', 'warehouse.id')
             ->leftjoin('room', 'stock_branch_log.room_id', '=', 'room.id')
             ->leftjoin('rack', 'stock_branch_log.rack_id', '=', 'rack.id')
             ->leftjoin('bay', 'stock_branch_log.bay_id', '=', 'bay.id')
             ->leftjoin('inventory', 'stock_branch_log.inventory_id', '=', 'inventory.id')
             ->select('stock_branch_log.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

         return Datatables::of($itemdata)

         ->filter(function ($itemdata) use ($request) {

            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($request->input('date_from') != '' || $request->input('date_to') != '')
            {
              $itemdata->whereBetween('stock_branch_log.date_transaction', [$date_from_format, $date_to_format]);
              // $itemdata->where('goods_receive.status', 0);
            }else{
              $itemdata->where('stock_branch_log.status', 0);
            };
        })

        ->addColumn('action', function ($itemdata) {
          return '0';
        })
        ->rawColumns(['action', 'check'])
         ->make(true);
     }

}
