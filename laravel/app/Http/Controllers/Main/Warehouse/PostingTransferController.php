<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PostingTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\Model\PostingTransfer;
use App\Model\PostingTransferDetail;
use App\Model\SalesReturnDetail;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\Reason;
use App\User;

use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class PostingTransferController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'posting_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $posting_transfers = PostingTransfer::all();
      return view ('main.warehouse.posting_transfer.index', compact('posting_transfers'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('posting_transfer')
            ->select('posting_transfer.*');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('posting_transfer.status', $id);
            }else{
                $itemdata->where('posting_transfer.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(posting_transfer.no_transaction, 'posting_transfer.description') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('posting_transfer', ['update'])) {
            $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="posting-transfer/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('posting_transfer', ['delete'])) {
            $archieve_disabled = '<a  type="button" disabled="disabled" class="btn btn-warning btn-float btn-xs" title="Archive"> <i class="icon-archive"></i></a>';
            $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $archieve_disabled = '';
            $reactive = '';
          };
        //   if($itemdata->status ==1){
        //     return ''.$reactive.'';
        //   }else{
        //     return ''.$edit.' '.$archieve.'';
        //   }
        // })

        if($itemdata->status ==1){
          return ''.$reactive.'';
        }elseif(strlen($itemdata->status) > 0){
            return ''.$edit.' '.$archieve_disabled.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO posting_transfer (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(posting_transfer.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        posting_transfer WHERE posting_transfer.code_transaction = '".$code_transaction."' AND DATE_FORMAT(posting_transfer.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('posting_transfer')->max('id');

      $posting_transfer = PostingTransfer::where('id', $lastInsertedID)->first();
      $posting_transfer->status = 0;
      $posting_transfer->created_by = $userid;
      $posting_transfer->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'GR RETURN';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $posting_transfer->no_transaction,
                            'source' => 'WMS',
                            'posting_transfer_id' => $lastInsertedID
                        ]);
      $user_log->save();


      return redirect('main/warehouse/posting-transfer/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $posting_transfer = PostingTransfer::Find($id);
      $inventory_list = Inventory::orderBy('inventory_name', 'asc')->get();
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $reason_list = Reason::all()->pluck('description', 'return_code');

      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      $store_loc_list = DB::connection('mysql')->select('SELECT
                                                    store_location_code AS store_location_code,
                                                    store_location_code AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY LEFT(store_location_code,4)');

      $plant_list = DB::connection('mysql')->select('SELECT
                                                    plant AS store_location_code,
                                                    plant AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY plant');

      return view ('main.warehouse.posting_transfer.form', compact('posting_transfer', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list','inventory_list', 'uom_list', 'reason_list', 'inventory_list', 'store_loc_list', 'plant_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = PostingTransfer::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        $post->updated_by = Auth::id();
        $post->save();

        $user_id = Auth::id();
        $user_log = new UserLog;
        $user_log->user_id = $userid;
        $user_log->scope = 'POSTING TRANSFER';
        $user_log->data = json_encode([
                              'action' => 'update',
                              'transaction_number' => $post->no_transaction,
                              'description' => $request->input('description'),
                              'source' => 'WMS',
                              'goods_receive_id' => $id
                          ]);
        $user_log->save();

        return redirect()->action('Main\Warehouse\PostingTransferController@index');
    }

    public function delete($id)
    {
      $post =  PostingTransfer::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = PostingTransfer::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
                posting_transfer_detail.id,
                posting_transfer_detail.transaction_id,
                inventory.inventory_name AS inventory_name,
                RIGHT(inventory.inventory_code, 7) AS inventory_code,
                posting_transfer_detail.unit_of_measure_code AS inventory_unit_name,
                posting_transfer_detail.inventory_id,
                posting_transfer_detail.quantity,
                posting_transfer_detail.quantity_transfer,
                warehouse.warehouse_name,
                IFNULL(posting_transfer_detail.warehouse_id,0) AS warehouse_id,
                IFNULL(posting_transfer_detail.room_id,0) AS room_id,
                IFNULL(posting_transfer_detail.bay_id,0) AS bay_id,
                IFNULL(posting_transfer_detail.rack_id,0) AS rack_id,
                IFNULL(posting_transfer_detail.batch,0) AS batch,
                rack.rack_name,
                bay.bay_name,
                room.room_name,
                posting_transfer_detail.pallet_number,
                posting_transfer_detail.store_loc,
                posting_transfer_detail.store_loc_transfer,
                posting_transfer_detail.plant,
                posting_transfer_detail.plant_transfer,
                inventory_tf.id AS inventory_id_tf,
                RIGHT(inventory_tf.inventory_code, 7) AS inventory_code_tf,
                inventory_tf.inventory_name AS inventory_name_tf,
                posting_transfer_detail.`status`
              FROM
              posting_transfer
              INNER JOIN posting_transfer_detail ON posting_transfer.id = posting_transfer_detail.transaction_id
              LEFT JOIN inventory ON posting_transfer_detail.inventory_id = inventory.id
              LEFT OUTER JOIN warehouse ON posting_transfer_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN rack ON posting_transfer_detail.rack_id = rack.id
              LEFT OUTER JOIN bay ON posting_transfer_detail.bay_id = bay.id
              LEFT OUTER JOIN room ON posting_transfer_detail.room_id = room.id
              LEFT JOIN inventory as inventory_tf ON posting_transfer_detail.inventory_id_transfer = inventory_tf.id
              WHERE
              posting_transfer_detail.deleted_at IS NULL AND posting_transfer_detail.quantity > 0 AND posting_transfer_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        // if(strlen($itemdata->status) > 0)
        // {
        //   return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" disabled> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" disabled><i class="icon-trash"></i></a>';
        // }else{
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs legitRipple" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."', '".$itemdata->inventory_id_tf."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs legitRipple" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
        // }
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }

  public function post_response(Request $request, $id)
  {
    $statement = PostingTransfer::where('id', $id)->first();
    $statement->nodoc = $request->nodoc;
    $statement->year = $request->year;
    $statement->save();
  }

  public function save_header(Request $request, $id)
  {

    $statement = PostingTransfer::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->save();
  }


  public function post_detail(Request $request, $id)
  {
    $statement = new PostingTransferDetail;
    $statement->transaction_id = $id;
    $statement->inventory_id = $request->inventory_id;
    $statement->inventory_id_transfer = $request->inventory_id_transfer;
    $statement->quantity_transfer = $request->quantity_transfer;
    $statement->inventory_code = $request->inventory_code;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->quantity = $request->quantity;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->batch = $request->batch;
    $statement->pallet_number = $request->pallet_number;
    $statement->plant = $request->plant;
    $statement->plant_transfer = $request->plant_transfer;
    $statement->store_loc = $request->store_loc;
    $statement->store_loc_transfer = $request->store_loc_transfer;
    $statement->status = 0;
    $statement->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = PostingTransferDetail::where('id', $id)->first();
    // $statement->inventory_id = $request->inventory_id;
    $statement->inventory_id_transfer = $request->inventory_id_transfer;
    $statement->quantity_transfer = $request->quantity_transfer;
    // $statement->inventory_code = $request->inventory_code;
    // $statement->unit_of_measure_code = $request->unit_of_measure_code;
    // $statement->quantity = $request->quantity;
    // $statement->batch = $request->batch;
    // $statement->pallet_number = $request->pallet_number;
    $statement->plant = $request->plant;
    $statement->plant_transfer = $request->plant_transfer;
    $statement->store_loc = $request->store_loc;
    $statement->store_loc_transfer = $request->store_loc_transfer;
    $statement->status = 0;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = PostingTransferDetail::where('id', $id)->first();

    $statement->delete();
  }

  public function data_modal(Request $request)
  {

      $itemdata = DB::table('stock')
          ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id')
          ->leftjoin('room', 'stock.room_id', '=', 'room.id')
          ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')
          ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')
          ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')
          ->where('stock.quantity', '>', '0')
          ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {
          if($id = $request->input('filter_status')) {
              $itemdata->where('stock.status', $id);
          }else{
              $itemdata->where('stock.status', 0);
          }
          if($keyword = $request->input('keyword')) {

            $itemdata->whereRaw("CONCAT(stock.pallet_number, inventory.inventory_name, inventory.inventory_code, stock.id) like ?", ["%{$keyword}%"]);

          }
      })

      ->addColumn('action', function ($itemdata) {
        $choose = '<a  type="button" class="btn btn-float btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->warehouse_id.', '.$itemdata->bay_id.', '.$itemdata->room_id.', '.$itemdata->rack_id.', '.$itemdata->inventory_id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
        return ''.$choose.'';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->rawColumns(['action', 'check'])
      ->make(true);
  }

  public function data_detail(Request $request, $id)
  {
    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();
    $end_of_line = PostingTransferDetail::where('transaction_id', $id)->max('item_line');


    $sql = 'SELECT
              posting_transfer.no_transaction,
              inventory.inventory_code AS material_from,
              inventory_to.inventory_code AS material_to,
              SUM(posting_transfer_detail.quantity_transfer) AS qty,
              posting_transfer.description AS note,
              posting_transfer_detail.plant,
              posting_transfer_detail.store_loc,
              posting_transfer_detail.unit_of_measure_code,
              warehouse.warehouse_name,
              posting_transfer_detail.item_line,
              DATE_FORMAT(posting_transfer.created_at, "%Y%m%d") AS created_at,
              DATE_FORMAT(posting_transfer.date_transaction, "%Y%m%d") AS posting_date,
              IFNULL(
                posting_transfer_detail.warehouse_id,
                0
              ) AS warehouse_id,
              IFNULL(posting_transfer_detail.room_id, 0) AS room_id,
              IFNULL(posting_transfer_detail.bay_id, 0) AS bay_id,
              IFNULL(posting_transfer_detail.batch, 0) AS batch,
              bay.bay_name,
              room.room_name
            FROM
              posting_transfer
            INNER JOIN posting_transfer_detail ON posting_transfer.id = posting_transfer_detail.transaction_id
            INNER JOIN inventory ON RIGHT(posting_transfer_detail.inventory_code,7) = RIGHT(inventory.inventory_code,7)
            INNER JOIN inventory as inventory_to ON posting_transfer_detail.inventory_id_transfer = inventory_to.id
            LEFT OUTER JOIN warehouse ON posting_transfer_detail.warehouse_id = warehouse.id
            LEFT OUTER JOIN rack ON posting_transfer_detail.rack_id = rack.id
            LEFT OUTER JOIN bay ON posting_transfer_detail.bay_id = bay.id
            LEFT OUTER JOIN room ON posting_transfer_detail.room_id = room.id
            WHERE
              posting_transfer_detail.deleted_at IS NULL
            AND posting_transfer_detail.transaction_id = '.$id.'
            GROUP BY
              posting_transfer.no_transaction,
              inventory.inventory_code,
              inventory_to.inventory_code,
              posting_transfer.description,
              posting_transfer_detail.plant,
              posting_transfer_detail.store_loc,
              posting_transfer_detail.unit_of_measure_code,
              warehouse.warehouse_name,
              posting_transfer_detail.item_line,
              DATE_FORMAT(posting_transfer.created_at, "%Y%m%d"),
              DATE_FORMAT(posting_transfer.date_transaction, "%Y%m%d"),
              IFNULL(
                posting_transfer_detail.warehouse_id,
                0
              ),
              IFNULL(posting_transfer_detail.room_id, 0),
              IFNULL(posting_transfer_detail.bay_id, 0),
              IFNULL(posting_transfer_detail.batch, 0),
              bay.bay_name,
              room.room_name
            ORDER BY
              posting_transfer_detail.item_line ASC';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                  	count(id) AS item_line
                  FROM
                  	(
                  		SELECT
                  			posting_transfer_detail.id,
                  			posting_transfer_detail.item_line
                  		FROM
                  			posting_transfer_detail
                  		WHERE
                  			posting_transfer_detail.deleted_at IS NULL
                  		AND posting_transfer_detail.transaction_id = '.$id.'
                  		GROUP BY
                  			posting_transfer_detail.id,
                  			posting_transfer_detail.item_line
                  	) AS derivdtbl';
    $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      sleep(3);
      $i = 1;
      foreach ($data as $h_key => $datas) {

        $item_line = $i++;
        $prefix = '000';
        $prefix_end = '0';

        // dd('00' + $item_line);

        $json[$h_key]['no_transaction'] = $datas->no_transaction;
        $json[$h_key]['item_line'] = $prefix.$item_line.$prefix_end;
        $json[$h_key]['material_from'] = $datas->material_from;
        $json[$h_key]['matrial_to'] = $datas->material_to;
        $json[$h_key]['plant_from'] = $datas->plant;
        $json[$h_key]['plant_to'] = $datas->plant;
        $json[$h_key]['storage_from'] = $datas->store_loc;
        $json[$h_key]['storage_to'] = $datas->store_loc;
        $json[$h_key]['quantity'] = $datas->qty;
        $json[$h_key]['unit'] = $datas->unit_of_measure_code;
        $json[$h_key]['user_name'] = $user->username;
        $json[$h_key]['text_header'] = $datas->note;
        $json[$h_key]['text_detail'] = $datas->note;
        $json[$h_key]['doc_date'] = $datas->created_at;
        $json[$h_key]['post_date'] = $datas->posting_date;
        $json[$h_key]['unit_of_measure_code'] = $datas->unit_of_measure_code;
        $json[$h_key]['total_items'] = $data_line->item_line;
        if($datas->item_line == $end_of_line)
        {
          $json[$h_key]['bendara'] = 'X';
        }else{
          $json[$h_key]['bendara'] = '';
        };
      }
    }
    return response()->json($json);
  }

  public function get_empty_nodo()
  {
    // $data = GRreturn::where('donum', '-')->orWhere('donum', null)->orWhere('donum', '')->where('code_transaction', 'GRRT')->get();
    $sql = 'SELECT
            posting_transfer.no_transaction,
        FROM
            posting_transfer
        INNER JOIN posting_transfer_detail ON posting_transfer.id = posting_transfer_detail.transaction_id
        WHERE
            (posting_transfer.donum IS NULL
        OR posting_transfer.donum = ""
        OR posting_transfer.donum = "-") AND posting_transfer.code_transaction = "GRRT"
        GROUP BY
            posting_transfer.no_transaction';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    // dd($data);

    // dd($data);
    if(count($data)>0)
    {
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['no_transaction'] = $datas->no_transaction;
      }
    }else {
      $json[0]['no_transaction'] = '';
    }
    return response()->json($json);

  }

  public function get_data_sap(Request $request)
  {
      // $response = file_get_contents('webapp.lmu.co.id:3000/m/v1/wms/devtestambilreturndo');
      $statement = GRreturn::where('no_transaction', $request->no_transaction)->first();
      $statement->trdat = $request->trdat;
      $statement->sonum = $request->sonum;
      $statement->donum = $request->donum;
      $statement->save();

  }

  public function cancel(Request $request, $id)
    {
      $statement = PostingTransfer::where('id', $id)->first();
      $statement->status = 1;
      $statement->save();

      $do_detail = PostingTransferDetail::where('transaction_id', $id)->get();

      foreach($do_detail as $do_details)
      {
        $post = SalesReturnDetail::where('id', $do_details->sales_return_detail_id)->first();
        $post->status = 1;
        $post->save();
      }
    }


    public function sap_exetrfposting(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'notrx' => $request->notrx);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exetrfposting",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                		CURLOPT_TIMEOUT => 35,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
      		}
    }



    public function sap_trfposting(Request $request)
    {
        $curl = curl_init();
        $data = array(
                'notrx' => $request->notrx,
                'item' => $request->item,
                'material_from' => $request->material_from,
                'matrial_to' => $request->matrial_to,
                'plant_from' => $request->plant_from,
                'plant_to' => $request->plant_to,
                'storage_from' => $request->storage_from,
                'storage_to' => $request->storage_to,
                'quantity' => $request->quantity,
                'unit' => $request->unit,
                'user_name' => $request->user_name,
                'text_header' => $request->text_header,
                'text_detail' => $request->text_detail,
                'doc_date' => $request->doc_date,
                'post_date' => $request->post_date,
                'total_items' => $request->total_items);

    		curl_setopt_array($curl, array(
                              		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-trfposting",
                              		CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                              		CURLOPT_ENCODING => "",
                              		CURLOPT_MAXREDIRS => 10,
                              		CURLOPT_TIMEOUT => 35,
                              		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              		CURLOPT_CUSTOMREQUEST => "POST",
                            		));

    		$response = curl_exec($curl);
    		$err = curl_error($curl);

    		curl_close($curl);

    		if ($err) {
    		echo "cURL Error #:" . $err;
    		} else {
      		// echo $response;
          $json = json_decode($response);
          return response()->json($json);
    		}
    }
}
