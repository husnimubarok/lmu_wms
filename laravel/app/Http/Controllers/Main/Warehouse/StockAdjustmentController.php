<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockAdjustmentRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockAdjustment;
use App\Model\StockAdjustmentDetail;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\User;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class StockAdjustmentController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_adjustment_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $stock_adjustments = StockAdjustment::all();
      return view ('main.warehouse.stock_adjustment.index', compact('stock_adjustments'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('stock_adjustment')
            ->select('stock_adjustment.*');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('stock_adjustment.status', $id);
            }else{
                $itemdata->where('stock_adjustment.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(stock_adjustment.no_transaction, 'stock_adjustment.description') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('stock_adjustment', ['update'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="stock-adjustment/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('stock_adjustment', ['delete'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO stock_adjustment (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_adjustment.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        stock_adjustment WHERE stock_adjustment.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_adjustment.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('stock_adjustment')->max('id');

      $stock_adjustment = StockAdjustment::where('id', $lastInsertedID)->first();
      $stock_adjustment->status = 0;
      $stock_adjustment->created_by = $userid;
      $stock_adjustment->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'STOCK ADJUSTMENT';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $stock_adjustment->no_transaction,
                            'source' => 'WMS',
                            'stock_adjustment_id' => $lastInsertedID
                        ]);
      $user_log->save();


      return redirect('main/warehouse/stock-adjustment/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $stock_adjustment = StockAdjustment::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      $store_loc_list = DB::connection('mysql')->select('SELECT
                                                    store_location_code AS store_location_code,
                                                    store_location_code AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY LEFT(store_location_code,4)');

      $plant_list = DB::connection('mysql')->select('SELECT
                                                    plant AS store_location_code,
                                                    plant AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY plant');

      return view ('main.warehouse.stock_adjustment.form', compact('stock_adjustment', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list', 'inventory_list', 'uom_list', 'store_loc_list', 'plant_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = StockAdjustment::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        $post->warehouse_id = $request->input('warehouse_id');
        $post->room_id = $request->input('room_id');
        $post->bay_id = $request->input('bay_id');
        $post->rack_id = $request->input('rack_id');
        $post->pallet_number = $request->input('pallet_number');
        // $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\StockAdjustmentController@index');
    }

    public function delete($id)
    {
      $post =  StockAdjustment::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = StockAdjustment::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
                stock_adjustment_detail.id,
                stock_adjustment.no_transaction AS notransaction,
                RIGHT(stock_adjustment_detail.item_line, 2) AS item_number,
                stock_adjustment_detail.plant,
                stock_adjustment_detail.quantity,
                stock_adjustment_detail.quantity_adjustment,
                stock_adjustment_detail.store_loc AS storage_loc,
                stock_adjustment_detail.quantity AS po_qty,
                stock_adjustment_detail.unit_of_measure_code AS po_unit,
                RIGHT(stock_adjustment_detail.batch, 8) AS batch,
                stock_adjustment_detail.store_loc,
                stock_adjustment_detail.item_line,
                stock_adjustment_detail.inventory_id,
                stock_adjustment_detail.id AS id_detail,
                RIGHT(inventory.inventory_code, 7) AS material_number,
                inventory.inventory_code,
                inventory.inventory_name,
                stock_adjustment.description AS doc_header,
                warehouse.warehouse_name,
                room.room_name,
                bay.bay_name,
                rack.rack_name,
                stock_adjustment_detail.pallet_number
              FROM
                stock_adjustment_detail
              LEFT OUTER JOIN stock_adjustment ON stock_adjustment.id = stock_adjustment_detail.transaction_id
              LEFT OUTER JOIN inventory ON stock_adjustment_detail.inventory_id = inventory.id
              LEFT OUTER JOIN warehouse ON stock_adjustment_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN room ON stock_adjustment_detail.room_id = room.id
              LEFT OUTER JOIN bay ON stock_adjustment_detail.bay_id = bay.id
              LEFT OUTER JOIN rack ON stock_adjustment_detail.rack_id = rack.id
              WHERE
              stock_adjustment_detail.deleted_at IS NULL AND stock_adjustment_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function data_modal(Request $request)
  {

      $itemdata = DB::table('stock')
          ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id')
          ->leftjoin('room', 'stock.room_id', '=', 'room.id')
          ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')
          ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')
          ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')
          ->where('stock.quantity', '>', '0')
          ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {
          if($id = $request->input('filter_status')) {
              $itemdata->where('stock.status', $id);
          }else{
              $itemdata->where('stock.status', 0);
          }
          if($keyword = $request->input('keyword')) {

            $itemdata->whereRaw("CONCAT(stock.pallet_number, inventory.inventory_name, inventory.inventory_code, stock.id) like ?", ["%{$keyword}%"]);

          }
      })

      ->addColumn('action', function ($itemdata) {
        $choose = '<a  type="button" class="btn btn-float btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->warehouse_id.', '.$itemdata->bay_id.', '.$itemdata->room_id.', '.$itemdata->rack_id.', '.$itemdata->inventory_id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
        return ''.$choose.'';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->rawColumns(['action', 'check'])
      ->make(true);
  }

  public function save_header(Request $request, $id)
  {

    $statement = StockAdjustment::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->save();
  }

  public function post_detail(Request $request, $id)
  {
    $statement = new StockAdjustmentDetail;
    $statement->transaction_id = $id;
    $statement->inventory_id = $request->inventory_id;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->quantity = $request->quantity;
    $statement->quantity_adjustment = $request->quantity_adjustment;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->batch = $request->batch;
    $statement->pallet_number = $request->pallet_number;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->status = 0;
    $statement->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = StockAdjustmentDetail::where('id', $id)->first();
    $statement->quantity_adjustment = $request->quantity_adjustment;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->status = 0;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = StockAdjustmentDetail::where('id', $id)->first();
    $statement->delete();
  }


  public function data_detail(Request $request, $id)
  {
    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();
    $end_of_line = StockAdjustmentDetail::where('transaction_id', $id)->max('item_line');


    $sql = 'SELECT
              stock_adjustment.no_transaction,
              inventory.inventory_code AS inventory_code,
              SUM(stock_adjustment_detail.quantity_adjustment) AS qty,
              stock_adjustment.description AS note,
              stock_adjustment_detail.plant,
              stock_adjustment_detail.store_loc,
              stock_adjustment_detail.unit_of_measure_code,
              warehouse.warehouse_name,
              stock_adjustment_detail.item_line,
              DATE_FORMAT(stock_adjustment.created_at, "%Y%m%d") AS created_at,
              DATE_FORMAT(stock_adjustment.date_transaction, "%Y%m%d") AS posting_date,
              IFNULL(
                stock_adjustment_detail.warehouse_id,
                0
              ) AS warehouse_id,
              IFNULL(stock_adjustment_detail.room_id, 0) AS room_id,
              IFNULL(stock_adjustment_detail.bay_id, 0) AS bay_id,
              IFNULL(stock_adjustment_detail.batch, 0) AS batch,
              bay.bay_name,
              room.room_name
            FROM
              stock_adjustment
            INNER JOIN stock_adjustment_detail ON stock_adjustment.id = stock_adjustment_detail.transaction_id
            INNER JOIN inventory ON stock_adjustment_detail.inventory_id = inventory.id
            LEFT OUTER JOIN warehouse ON stock_adjustment_detail.warehouse_id = warehouse.id
            LEFT OUTER JOIN rack ON stock_adjustment_detail.rack_id = rack.id
            LEFT OUTER JOIN bay ON stock_adjustment_detail.bay_id = bay.id
            LEFT OUTER JOIN room ON stock_adjustment_detail.room_id = room.id
            WHERE
              stock_adjustment_detail.deleted_at IS NULL
            AND stock_adjustment_detail.transaction_id = '.$id.'
            GROUP BY
              stock_adjustment.no_transaction,
              inventory.inventory_code,
              stock_adjustment.description,
              stock_adjustment_detail.plant,
              stock_adjustment_detail.store_loc,
              stock_adjustment_detail.unit_of_measure_code,
              warehouse.warehouse_name,
              stock_adjustment_detail.item_line,
              DATE_FORMAT(stock_adjustment.created_at, "%Y%m%d"),
              DATE_FORMAT(stock_adjustment.date_transaction, "%Y%m%d"),
              IFNULL(
                stock_adjustment_detail.warehouse_id,
                0
              ),
              IFNULL(stock_adjustment_detail.room_id, 0),
              IFNULL(stock_adjustment_detail.bay_id, 0),
              IFNULL(stock_adjustment_detail.batch, 0),
              bay.bay_name,
              room.room_name
            ORDER BY
              stock_adjustment_detail.item_line ASC';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                    count(inventory_id) AS item_line
                  FROM
                    (
                      SELECT
                        stock_adjustment_detail.transaction_id,
                        stock_adjustment_detail.inventory_id
                      FROM
                        stock_adjustment_detail
                      WHERE
                        stock_adjustment_detail.deleted_at IS NULL
                      AND stock_adjustment_detail.transaction_id = '.$id.'
                      GROUP BY
                        stock_adjustment_detail.transaction_id,
                        stock_adjustment_detail.inventory_id,
                        IFNULL(
                          stock_adjustment_detail.batch,
                          0
                        ),
                        IFNULL(
                          stock_adjustment_detail.room_id,
                          0
                        ),
                        IFNULL(
                          stock_adjustment_detail.bay_id,
                          0
                        ),
                        stock_adjustment_detail.plant,
                        stock_adjustment_detail.store_loc,
                        stock_adjustment_detail.unit_of_measure_code,
                        stock_adjustment_detail.item_line,
                        IFNULL(
                          stock_adjustment_detail.warehouse_id,
                          0
                        ),
                        IFNULL(
                          stock_adjustment_detail.room_id,
                          0
                        ),
                        IFNULL(
                          stock_adjustment_detail.bay_id,
                          0
                        ),
                        IFNULL(
                          stock_adjustment_detail.batch,
                          0
                        )
                    ) AS derivdtbl';
    $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      sleep(3);
      $i = 1;
      foreach ($data as $h_key => $datas) {

        $item_line = $i++;
        $prefix = '000';
        $prefix_end = '0';

        // dd('00' + $item_line);

        $json[$h_key]['no_transaction'] = $datas->no_transaction;
        $json[$h_key]['item_line'] = $prefix.$item_line.$prefix_end;
        $json[$h_key]['matrial'] = $datas->inventory_code;
        $json[$h_key]['plant'] = $datas->plant;
        $json[$h_key]['storage'] = $datas->store_loc;
        $json[$h_key]['quantity_adjustment'] = $datas->qty;
        $json[$h_key]['unit'] = $datas->unit_of_measure_code;
        $json[$h_key]['batch'] = $datas->batch;
        $json[$h_key]['user_name'] = $user->username;
        $json[$h_key]['text_header'] = $datas->note;
        $json[$h_key]['doc_date'] = $datas->created_at;
        $json[$h_key]['post_date'] = $datas->posting_date;
        $json[$h_key]['unit_of_measure_code'] = $datas->unit_of_measure_code;
        $json[$h_key]['total_items'] = $data_line->item_line;
        // if($datas->item_line == $end_of_line)
        // {
        //   $json[$h_key]['bendara'] = 'X';
        // }else{
        //   $json[$h_key]['bendara'] = '';
        // };
      }
    }
    return response()->json($json);
  }


    public function post_response(Request $request, $id)
    {

      $statement = StockAdjustment::where('id', $id)->first();
      $statement->nodoc = $request->nodoc;
      $statement->year = $request->year;
      $statement->description = $request->description;
      $statement->save();

      $userid = Auth::id();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'STOCK ADJUETMENT';
      $user_log->data = json_encode([
                            'action' => 'update',
                            'transaction_number' => $statement->no_transaction,
                            'description' => $request->description,
                            'source' => 'WMS',
                            'goods_receive_id' => $id
                        ]);
      $user_log->save();
    }

    public function cancel(Request $request, $id)
    {
      $statement = StockAdjustment::where('id', $id)->first();
      $statement->status = 1;
      $statement->save();

    }


    public function sap_exeadjustminus(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'notrx' => $request->notrx);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exeadjustminus",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                		CURLOPT_TIMEOUT => 120,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
      		}
    }


    public function sap_adjustminus(Request $request)
    {
        $curl = curl_init();
        $data = array(
                'notrx' => $request->notrx,
                'item' => $request->item,
                'matrial' => $request->matrial,
                'plant' => $request->plant,
                'storloc' => $request->storloc,
                'qty' => $request->qty,
                'unit' => $request->unit,
                'docdate' => $request->docdate,
                'postdate' => $request->postdate,
                'batch' => $request->batch,
                'textheader' => $request->textheader,
                'user' => $request->user,
                'lines' => $request->lines);

    		curl_setopt_array($curl, array(
                              		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-adjustminus",
                              		CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                              		CURLOPT_ENCODING => "",
                              		CURLOPT_MAXREDIRS => 10,
                              		CURLOPT_TIMEOUT => 120,
                              		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              		CURLOPT_CUSTOMREQUEST => "POST",
                            		));

    		$response = curl_exec($curl);
    		$err = curl_error($curl);

    		curl_close($curl);

    		if ($err) {
    		echo "cURL Error #:" . $err;
    		} else {
      		// echo $response;
          $json = json_decode($response);
          return response()->json($json);
    		}
    }


    public function sap_exeadjustplus(Request $request)
    {
        $curl = curl_init();
        $data = array(
                'notrx' => $request->notrx);

        curl_setopt_array($curl, array(
                                  CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exeadjustplus",
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                                  CURLOPT_ENCODING => "",
                                  CURLOPT_MAXREDIRS => 10,
                                  CURLOPT_TIMEOUT => 120,
                                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                  CURLOPT_CUSTOMREQUEST => "POST",
                                ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
          // echo $response;
          $json = json_decode($response);
          return response()->json($json);
        }
    }


    public function sap_adjustplus(Request $request)
    {
        $curl = curl_init();
        $data = array(
              'notrx' => $request->notrx,
              'item' => $request->item,
              'matrial' => $request->matrial,
              'plant' => $request->plant,
              'storloc' => $request->storloc,
              'qty' => $request->qty,
              'unit' => $request->unit,
              'docdate' => $request->docdate,
              'postdate' => $request->postdate,
              'batch' => $request->batch,
              'textheader' => $request->textheader,
              'user' => $request->user,
              'lines' => $request->lines);

        curl_setopt_array($curl, array(
                                  CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-adjustplus",
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                                  CURLOPT_ENCODING => "",
                                  CURLOPT_MAXREDIRS => 10,
                                  CURLOPT_TIMEOUT => 120,
                                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                  CURLOPT_CUSTOMREQUEST => "POST",
                                ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
          // echo $response;
          $json = json_decode($response);
          return response()->json($json);
        }
    }

}
