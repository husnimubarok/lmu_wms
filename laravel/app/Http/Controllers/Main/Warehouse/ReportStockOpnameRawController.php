<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockOpnameRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\Model\StockOpname;
use App\Model\StockOpnameDetail;
use App\Model\Warehouse;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class ReportStockOpnameRawController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      $stock_transfers = StockOpname::all();
      return view ('main.warehouse.report_stock_opname_raw.index', compact('stock_transfers'));
    }

    public function data(Request $request)
    {

        $sql = 'SELECT
                	warehouse.warehouse_code,
                	warehouse.warehouse_name,
                	room.room_code,
                	room.room_name,
                	bay.bay_code,
                	bay.bay_name,
                	rack.rack_code,
                	rack.rack_name,
                	inventory.inventory_code,
                	inventory.inventory_name,
                	stock_opname_mobile.pallet_number,
                	stock_opname_mobile.batch,
	                stock_opname_mobile.unit_of_measure_code as unit,
                	stock_opname_mobile.quantity,
                	stock_opname_mobile.created_by,
                	stock_opname_mobile.created_at,
                	stock_opname_mobile.`status`
                FROM
                	stock_opname_mobile
                LEFT OUTER JOIN warehouse ON stock_opname_mobile.warehouse_id = warehouse.id
                LEFT OUTER JOIN room ON stock_opname_mobile.room_id = room.id
                LEFT OUTER JOIN rack ON stock_opname_mobile.rack_id = rack.id
                LEFT OUTER JOIN bay ON stock_opname_mobile.bay_id = bay.id
                LEFT OUTER JOIN inventory ON stock_opname_mobile.inventory_id = inventory.id';

        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('rs_sql.status', $id);
            }else{
                $itemdata->where('rs_sql.status', 0);
            }
            if($keyword = $request->input('keyword')) {

              $itemdata->whereRaw("CONCAT(rs_sql.pallet_number, rs_sql.inventory_name, rs_sql.inventory_code) like ?", ["%{$keyword}%"]);

            }
        })
        ->make(true);
    }

}
