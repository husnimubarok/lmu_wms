<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockOpnameRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockOpname; 
use App\Model\StockOpnameDetail; 
use App\Model\Warehouse; 
use App\Model\Room; 
use App\Model\Rack; 
use App\Model\Pallet; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Stock;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class TestAPIController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'stock_opname_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $stock_opnames = StockOpname::all();
      return view ('main.warehouse.test_api.index', compact('stock_opnames'));
    }

   
}
