<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\GoodsReceiveRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\User;
use App\Model\GoodsReceive;
use App\Model\GoodsReceiveDetail;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\InventoryReceive;
use App\Model\InventoryReceiveDetail;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class GoodsReceiveController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'goods_receive_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index_open()
    {
      return view ('main.warehouse.goods_receive.index_open');
    }

    public function data_open(Request $request)
    {

        //$itemdata = InventoryReceive::all();
      $sql = 'SELECT
              inventory_receive.id,
              purchase_order.po_number,
              purchase_order.doc_date
              FROM
              inventory_receive
              INNER JOIN purchase_order ON purchase_order.id = inventory_receive.purchase_order_id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();
        return Datatables::of($itemdata)

        ->addColumn('action', function ($itemdata) {
          $edit = '<a href="open/edit/'.$itemdata->id.'" type="button" class="btn btn-xs bg-teal-400 btn-labeled"><b><i class="icon-folder"></i></b> Open</a>';
          return ''.$edit.'';
        })

        ->rawColumns(['action'])
        ->make(true);
    }

    public function data_modal(Request $request)
    {
        $sql = 'SELECT
                  inventory.inventory_code,
                  inventory.inventory_name,
                  inventory_receive_detail.id,
                  inventory_receive_detail.uom,
                  inventory_receive_detail.plant,
                  inventory_receive_detail.item_line,
                  inventory_receive_detail.supplier_code,
                  inventory_receive_detail.store_location,
                  inventory_receive_detail.quantity,
                  inventory_receive_detail.inventory_id,
                  inventory_receive_detail.inventory_id AS inventory_id_val,
                  inventory_receive_detail.po_number,
                  inventory_receive_detail.doc_date,
                  inventory_receive_detail.revision,
                  warehouse.warehouse_name,
                  IFNULL(inventory_receive.warehouse_id,0) AS warehouse_id,
                  IFNULL(inventory_receive.room_id,0) AS room_id,
                  IFNULL(inventory_receive.bay_id,0) AS bay_id,
                  IFNULL(inventory_receive.rack_id,0) AS rack_id,
                  rack.rack_name,
                  bay.bay_name,
                  room.room_name,
                  inventory_receive.manual,
                  inventory_receive.pallet_number
                FROM
                  inventory_receive_detail
                LEFT OUTER JOIN inventory_receive ON inventory_receive_detail.transaction_id = inventory_receive.id
                LEFT OUTER JOIN warehouse ON inventory_receive.warehouse_id = warehouse.id
                LEFT OUTER JOIN inventory ON inventory_receive_detail.inventory_id = inventory.id
                LEFT OUTER JOIN rack ON inventory_receive.rack_id = rack.id
                LEFT OUTER JOIN bay ON inventory_receive.bay_id = bay.id
                LEFT OUTER JOIN room ON inventory_receive.room_id = room.id
                WHERE inventory_receive_detail.status = 0 AND inventory_receive_detail.quantity > 0 AND inventory_receive.putaway_status = 1';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        // dd($itemdata);


        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {

            if($request->input('code_transaction') == 'GRNC') {
                $itemdata->where('rs_sql.manual', 0 );
                // ->orWhere('rs_sql.manual', null);
            }
            if($keyword = $request->input('keyword_modal')) {
              // dd($keyword);
              $itemdata->whereRaw("CONCAT(ifnull(rs_sql.plant,''), ifnull(rs_sql.item_line,''), ifnull(rs_sql.supplier_code,''), ifnull(rs_sql.inventory_code,''), ifnull(rs_sql.inventory_name,''), ifnull(rs_sql.pallet_number,''), ifnull(rs_sql.po_number,'')) like ?", ["%{$keyword}%"]);
            }
        })


        ->addColumn('action', function ($itemdata) {
          $choose = '<a  type="button" class="btn btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->warehouse_id.', '.$itemdata->bay_id.', '.$itemdata->room_id.', '.$itemdata->rack_id.', '.$itemdata->inventory_id.', '.$itemdata->id.'); return false;" title="Edit"> <i class="icon-touch"></i></a>';
          return ''.$choose.'';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->rawColumns(['action', 'check'])
        ->make(true);
    }

    public function edit_open(Request $request, $id)
    {

       $sql_inventory_receive_detail = 'SELECT
                                          inventory_receive_detail.id,
                                          inventory.inventory_code,
                                          inventory.inventory_name,
                                          inventory_receive_detail.quantity,
                                          inventory_receive_detail.pallet_number,
                                          inventory_receive_detail.inventory_id,
                                          inventory_receive_detail.po_number,
                                          inventory_receive_detail.doc_date,
                                          warehouse.warehouse_name,
                                          rack.rack_name,
                                          bay.bay_name,
                                          room.room_name
                                        FROM
                                          inventory_receive_detail
                                        LEFT JOIN inventory_receive ON inventory_receive_detail.transaction_id = inventory_receive.id
                                        LEFT JOIN warehouse ON inventory_receive.warehouse_id = warehouse.id
                                        LEFT JOIN inventory ON inventory_receive_detail.inventory_id = inventory.id
                                        LEFT JOIN rack ON inventory_receive.rack_id = rack.id
                                        LEFT JOIN bay ON inventory_receive.bay_id = bay.id
                                        LEFT JOIN room ON inventory_receive.room_id = room.id
                                        WHERE inventory_receive_detail.quantity > 0';
      $inventory_receive_detail = DB::table(DB::raw("($sql_inventory_receive_detail) as rs_sql"))->get();

      return view ('main.warehouse.goods_receive.form_open', compact('inventory_receive', 'inventory_receive_detail'));
    }

    public function pick(Request $request, $id_header)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {

          $id_foo = str_replace("'","",$id);

          $sql = 'SELECT
                  inventory.inventory_code,
                  inventory.inventory_name,
                  inventory_receive_detail.id,
                  inventory_receive_detail.uom,
                  inventory_receive_detail.plant,
                  inventory_receive_detail.item_line,
                  inventory_receive_detail.supplier_code,
                  inventory_receive_detail.store_location,
                  inventory_receive_detail.quantity,
                  inventory_receive_detail.inventory_id,
                  inventory_receive_detail.po_number,
                  inventory_receive_detail.doc_date,
                  inventory_receive_detail.revision,
                  warehouse.warehouse_name,
                  IFNULL(inventory_receive.warehouse_id,0) AS warehouse_id,
                  IFNULL(inventory_receive.room_id,0) AS room_id,
                  IFNULL(inventory_receive.bay_id,0) AS bay_id,
                  IFNULL(inventory_receive.rack_id,0) AS rack_id,
                  rack.rack_name,
                  bay.bay_name,
                  room.room_name,
                  inventory_receive.pallet_number
                FROM
                  inventory_receive_detail
                LEFT OUTER JOIN inventory_receive ON inventory_receive_detail.transaction_id = inventory_receive.id
                LEFT OUTER JOIN warehouse ON inventory_receive.warehouse_id = warehouse.id
                LEFT OUTER JOIN inventory ON inventory_receive_detail.inventory_id = inventory.id
                LEFT OUTER JOIN rack ON inventory_receive.rack_id = rack.id
                LEFT OUTER JOIN bay ON inventory_receive.bay_id = bay.id
                LEFT OUTER JOIN room ON inventory_receive.room_id = room.id';
          $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id_foo)->first();

          // dd($itemdata);

          $post = InventoryReceiveDetail::Find($id_foo);
          if ($post)
            $post->status = 9;
            $post->save();

            $date_from_array = explode("/",$request->date_transaction); // split the array
            $var_month_from = $date_from_array[0]; //month segment
            $var_day_from = $date_from_array[1]; //day seqment
            $var_year_from = $date_from_array[2]; //year segment
            $date_transaction_format = $var_day_from.$var_month_from.$var_year_from; // join them together

            $statement = new GoodsReceiveDetail;
            $statement->transaction_id = $id_header;
            $statement->po_number = $itemdata->po_number;
            $statement->inventory_id = $itemdata->inventory_id;
            $statement->unit_of_measure_code = $itemdata->uom;
            $statement->quantity = $itemdata->quantity;
            $statement->batch = "00".$date_transaction_format;
            $statement->plant = $itemdata->plant;
            $statement->store_loc = $itemdata->store_location;
            $statement->item_line = $itemdata->item_line;
            $statement->doc_date = $itemdata->doc_date;
            $statement->supplier_id = $itemdata->supplier_code;
            $statement->warehouse_id = $itemdata->warehouse_id;
            $statement->room_id = $itemdata->room_id;
            $statement->bay_id = $itemdata->bay_id;
            $statement->rack_id = $itemdata->rack_id;
            $statement->pallet_number = $itemdata->pallet_number;
            $statement->revision = $itemdata->revision;
            $statement->inventory_receive_detail_id = $itemdata->id;
            $statement->status = 0;
            $statement->save();
      };

      

      return response()->json([
          'status' => TRUE
      ]);
    }

    public function store_open(Request $request)
    {
        $userid= Auth::id();
        // $codetrans = $request->input('code_trans');
        $codetrans = 'GRNC';

        DB::insert("INSERT INTO goods_receive (code_transaction, no_transaction, date_transaction, created_by, created_at)
          SELECT
          '".$codetrans."', IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%y%m%d'),RIGHT((RIGHT(MAX(RIGHT(goods_receive.no_transaction,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%y%m%d'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW())
          FROM
          goods_receive");

        $lastInsertedID = DB::table('goods_receive')->max('id');

        foreach($request->input('good_receive_detail') as $detail_key => $detail_value)
        {
          $inventory_receive_detail = InventoryReceiveDetail::where('id', $detail_value['id'])->first();

          $good_receive_detail = new GoodsReceiveDetail;
          $good_receive_detail->transaction_id = $lastInsertedID;
          $good_receive_detail->inventory_id =  $detail_value['inventory_id'];
          $good_receive_detail->quantity =  $inventory_receive_detail->quantity;
          $good_receive_detail->save();
        }

      return redirect('main/warehouse/goods-receive/edit/'.$lastInsertedID.'');
    }

    public function index()
    {
      $goods_receives = GoodsReceive::all();
      return view ('main.warehouse.goods_receive.index', compact('goods_receives'));
    }

    public function data(Request $request)
    {

        // $itemdata = DB::table('goods_receive')
        //     ->leftjoin('warehouse', 'goods_receive.warehouse_id', '=', 'warehouse.id')
        //     ->select('goods_receive.*', 'warehouse.warehouse_name');

        // $sql = 'SELECT
        //         	`goods_receive`.*, `warehouse`.`warehouse_name`, container.container_no, users.username
        //         FROM
        //         	`goods_receive`
        //         LEFT JOIN `warehouse` ON `goods_receive`.`warehouse_id` = `warehouse`.`id`
        //         LEFT JOIN (SELECT
        //         	goods_receive_detail.transaction_id,
        //         	purchase_order.container_no
        //         FROM
        //         	goods_receive_detail
        //         INNER JOIN purchase_order ON goods_receive_detail.po_number = purchase_order.po_number
        //         GROUP BY
        //         	purchase_order.container_no,
        //         	goods_receive_detail.transaction_id) AS container ON goods_receive.id = container.transaction_id
        //         INNER JOIN users ON goods_receive.created_by = users.id
        //         WHERE
        //         	`goods_receive`.`status` < 2';

        $sql = 'SELECT
                  `goods_receive`.*, `warehouse`.`warehouse_name`, purchase_order.container_no, users.username
                FROM
                  `goods_receive`
                LEFT JOIN `warehouse` ON `goods_receive`.`warehouse_id` = `warehouse`.`id`
                LEFT JOIN  purchase_order ON goods_receive.nopo = purchase_order.po_number
                INNER JOIN users ON goods_receive.created_by = users.id
                WHERE
                  `goods_receive`.`status` < 2';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($id = $request->input('filter_status')) {
                $itemdata->where('rs_sql.status', $id);
            }else{
                $itemdata->where('rs_sql.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(IFNULL(rs_sql.no_transaction,''), IFNULL(rs_sql.nopo,''), IFNULL(rs_sql.nodoc,''), IFNULL(rs_sql.container_no,''), ifnull(rs_sql.warehouse_name,'')) like ?", ["%{$keyword}%"]);
            }
              $itemdata->whereBetween('rs_sql.date_transaction', [$date_from_format, $date_to_format]);

        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('goods_receive', ['update'])) {
              $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="goods-receive/edit/'.$itemdata->id.'" title="Display"> <i class="icon-pencil7"></i> </a> <a  type="button" class="btn btn-warning btn-float btn-xs" href="#" onClick="getDataSAPNew('.$itemdata->id.')" title="Get PO Revision"> <i class="icon-toggle"></i> </a>';
              // $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="goods-receive/edit/'.$itemdata->id.'" title="Display"> <i class="icon-pencil7"></i> </a> ';

          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('goods_receive', ['delete'])) {
            if($itemdata->code_transaction == 'GRMN' && $itemdata->created_by != Auth::id())
            {
              $archieve = '';
              $archieve_disabled = '';
              $reactive = '';
            }else{
              $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
              $archieve_disabled = '<a  type="button" disabled="disabled" class="btn btn-warning btn-float btn-xs" title="Archive"> <i class="icon-archive"></i></a>';
              $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
            };
          }else{
            $archieve = '';
            $archieve_disabled = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
            }elseif(strlen($itemdata->nodoc) > 0){
              return ''.$edit.' '.$archieve_disabled.'';
            }else{
              return ''.$edit.' '.$archieve.'';
            }
          })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0 && $itemdata->nodoc == null) {
            return '<span class="label label-success">Active</span>';
          }else if ($itemdata->status == 0 && $itemdata->nodoc != null){
            return '<span class="label label-primary">Posted</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function data_header(Request $request, $id)
    {
      $user_id = Auth::id();
      $user = User::where('id', $user_id)->first();

      $sql = 'SELECT
                sum(counter) AS counter,
                transaction_id
              FROM
                (
                  SELECT
                    goods_receive_detail.po_number,
                    goods_receive_detail.transaction_id,
                    1 AS counter
                  FROM
                    goods_receive_detail
                  WHERE goods_receive_detail.transaction_id = '.$id.'
                  GROUP BY
                    goods_receive_detail.po_number,
                    goods_receive_detail.transaction_id
                ) AS drvdtbl
              GROUP BY
                transaction_id';
      $data = DB::table(DB::raw("($sql) as rs_sql"))->first();

      // if(count($data)>0)
      // {
      //   foreach ($data as $h_key => $datas) {
          $json['transaction_id'] = $data->transaction_id;
          $json['counter'] = $data->counter;
      //   }
      // }

      return response()->json($json);
    }

    public function data_detail(Request $request, $id)
    {
      $user_id = Auth::id();
      $user = User::where('id', $user_id)->first();
      $end_of_line = GoodsReceiveDetail::where('transaction_id', $id)->whereNull('deleted_at')->max('item_line');

      // dd($end_of_line);

      $sql = 'SELECT
                goods_receive.id,
                goods_receive.no_transaction AS notransaction,
                goods_receive_detail.po_number AS purchasing_number,
                goods_receive_detail.item_line AS item_number,
                goods_receive_detail.plant,
                goods_receive_detail.store_loc AS storage_loc,
                SUM(goods_receive_detail.quantity) AS po_qty,
                goods_receive_detail.unit_of_measure_code AS po_unit,
                DATE_FORMAT(
                  goods_receive_detail.doc_date,
                  "%Y%m%d"
                ) AS po_date,
                DATE_FORMAT(goods_receive.date_transaction, "%Y%m%d") AS posting_date,
                DATE_FORMAT(goods_receive.date_transaction,
                  "%d.%m.%Y"
                ) AS date_transaction_sap,
                goods_receive_detail.supplier_id,
                goods_receive_detail.item_line,
                goods_receive_detail.inventory_id,
                goods_receive_detail.batch,
                supplier.supplier_code AS vendor_number,
                inventory.inventory_code AS material_number,
                goods_receive.description AS doc_header
              FROM
                goods_receive_detail
              INNER JOIN goods_receive ON goods_receive.id = goods_receive_detail.transaction_id
              INNER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
              LEFT JOIN supplier ON goods_receive_detail.supplier_id = supplier.supplier_code
              WHERE
              goods_receive_detail.deleted_at IS NULL AND goods_receive_detail.transaction_id = '.$id.'
              GROUP BY
              goods_receive.id,
              goods_receive.no_transaction,
              goods_receive_detail.po_number,
              goods_receive_detail.item_line,
              goods_receive_detail.plant,
              goods_receive_detail.store_loc,
              goods_receive_detail.unit_of_measure_code,
              DATE_FORMAT(
                goods_receive_detail.doc_date,
                "%d.%m.%Y"
              ),
              goods_receive_detail.supplier_id,
              goods_receive_detail.inventory_id,
              goods_receive_detail.batch,
              supplier.supplier_code,
              inventory.inventory_code,
              goods_receive.description
              ORDER BY goods_receive_detail.item_line ASC';
      $data = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('item_line', 'ASC')->get();

      $sql_line = 'SELECT
                      transaction_id,
                      count(item_line) AS item_line,
                      count(store_loc) AS store_loc
                    FROM
                      (
                        SELECT
                          goods_receive_detail.transaction_id,
                          goods_receive_detail.item_line,
                          goods_receive_detail.store_loc
                        FROM
                          goods_receive_detail
                        WHERE
                          goods_receive_detail.deleted_at IS NULL
                        AND goods_receive_detail.transaction_id = '.$id.'
                        GROUP BY
                          goods_receive_detail.transaction_id,
                          goods_receive_detail.item_line,
                          goods_receive_detail.store_loc
                      ) AS derivdtbl
                    GROUP BY
                      transaction_id';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

      // dd($data_line->item_line);

      if(count($data)>0)
      {
        sleep(3);
        foreach ($data as $h_key => $datas) {

          $item_text = GoodsReceiveDetail::where('po_number', $datas->purchasing_number)->where('description', '!=', '')->where('description', '!=', null)->first();

          if(isset($item_text)){
            $text_item_desc = $item_text->description;
          }else{
            $text_item_desc = '-';
          };

          $json[$h_key]['notransaction'] = $datas->notransaction;
          $json[$h_key]['date_transaction_sap'] = $datas->date_transaction_sap;
          $json[$h_key]['purchasing_number'] = $datas->purchasing_number;
          $json[$h_key]['item_number'] = $datas->item_number;
          $json[$h_key]['plant'] = $datas->plant;
          $json[$h_key]['storage_loc'] = $datas->storage_loc;
          $json[$h_key]['po_qty'] = $datas->po_qty;
          $json[$h_key]['po_unit'] = $datas->po_unit;
          $json[$h_key]['po_date'] = $datas->po_date;
          $json[$h_key]['posting_date'] = $datas->posting_date;
          $json[$h_key]['batch'] = $datas->batch;
          $json[$h_key]['vendor_number'] = $datas->vendor_number;
          $json[$h_key]['material_number'] = $datas->material_number;
          $json[$h_key]['doc_header'] = $datas->doc_header;
          $json[$h_key]['inventory_id'] = $datas->inventory_id;
          $json[$h_key]['grrcpt'] = $user->username;
          $json[$h_key]['item_text'] = $text_item_desc;
          $json[$h_key]['total_items'] = $data_line->item_line;
          if($datas->item_line == $end_of_line)
          {
            $json[$h_key]['flag'] = 'X';
          }else{
            $json[$h_key]['flag'] = '';
          };
        }
      }
      return response()->json($json);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');
      // $code_transaction = 'GRNC';

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO goods_receive (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(goods_receive.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        goods_receive WHERE goods_receive.code_transaction = '".$code_transaction."' AND DATE_FORMAT(goods_receive.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('goods_receive')->max('id');

      $goods_receive = GoodsReceive::where('id', $lastInsertedID)->first();
      $goods_receive->status = 0;
      $goods_receive->created_by = $userid;
      $goods_receive->save();

      // foreach($request->input('inventory_receive_detail') as $detail_key => $detail_value)
      // {

      //   $sql_inventory_receive_detail = 'SELECT
      //                                     inventory_receive_detail.id,
      //                                     inventory_receive.warehouse_id,
      //                                     inventory_receive.room_id,
      //                                     inventory_receive.bay_id,
      //                                     inventory_receive.rack_id,
      //                                     inventory_receive.pallet_number,
      //                                     inventory_receive_detail.inventory_id,
      //                                     inventory_receive_detail.quantity,
      //                                     inventory_receive_detail.po_number,
      //                                     inventory_receive_detail.doc_date
      //                                   FROM
      //                                     inventory_receive_detail
      //                                   LEFT JOIN inventory_receive ON inventory_receive_detail.transaction_id = inventory_receive.id
      //                                   LEFT JOIN warehouse ON inventory_receive.warehouse_id = warehouse.id
      //                                   LEFT JOIN inventory ON inventory_receive_detail.inventory_id = inventory.id
      //                                   LEFT JOIN rack ON inventory_receive.rack_id = rack.id
      //                                   LEFT JOIN bay ON inventory_receive.bay_id = bay.id
      //                                   LEFT JOIN room ON inventory_receive.room_id = room.id
      //                                   WHERE inventory_receive_detail.id = '.$detail_key.'';
      //   $inventory_receive_detail = DB::table(DB::raw("($sql_inventory_receive_detail) as rs_sql"))->first();

      //   $post_detail = New GoodsReceiveDetail;
      //   $post_detail->transaction_id = $lastInsertedID;
      //   $post_detail->inventory_id = $inventory_receive_detail->inventory_id;
      //   $post_detail->quantity = $inventory_receive_detail->quantity;
      //   $post_detail->warehouse_id = $inventory_receive_detail->warehouse_id;
      //   $post_detail->room_id = $inventory_receive_detail->room_id;
      //   $post_detail->bay_id = $inventory_receive_detail->bay_id;
      //   $post_detail->rack_id = $inventory_receive_detail->rack_id;
      //   $post_detail->pallet_number = $inventory_receive_detail->pallet_number;
      //   $post_detail->save();
      // }

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'GOODS RECEIVE';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $goods_receive->no_transaction,
                            'source' => 'WMS',
                            'description' => 'Create Goods Receive',
                            'goods_receive_id' => $lastInsertedID
                        ]);
      $user_log->save();

      return redirect('main/warehouse/goods-receive/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $goods_receive = GoodsReceive::Find($id);
      // dd($goods_receive);
      $sql_goods_receive_po = 'SELECT
                                	`goods_receive`.*, `warehouse`.`warehouse_name`, container.container_no
                                FROM
                                	`goods_receive`
                                LEFT JOIN `warehouse` ON `goods_receive`.`warehouse_id` = `warehouse`.`id`
                                LEFT JOIN (SELECT
                                	goods_receive_detail.transaction_id,
                                	purchase_order.container_no
                                FROM
                                	goods_receive_detail
                                INNER JOIN purchase_order ON goods_receive_detail.po_number = purchase_order.po_number
                                GROUP BY
                                	purchase_order.container_no,
                                	goods_receive_detail.transaction_id) AS container ON goods_receive.id = container.transaction_id
                                WHERE goods_receive.id = '.$id.'';
      $goods_receive_po = DB::table(DB::raw("($sql_goods_receive_po) as rs_sql"))->first();

      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $revision = GoodsReceiveDetail::where('revision', '1')->where('transaction_id', $id)->count();

      $sql_line = 'SELECT
                    	COUNT(goods_receive_detail.id) AS id
                    FROM
                    	goods_receive_detail
                    INNER JOIN stock ON goods_receive_detail.pallet_number = stock.pallet_number
                    WHERE
                    	goods_receive_detail.transaction_id = '.$id.' AND goods_receive_detail.quantity > stock.quantity  AND goods_receive_detail.pallet_number = stock.pallet_number';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

      $sql_doc_year = 'SELECT
                        date_FORMAT(
                          inventory_receive_detail.created_at,
                          "%Y"
                        ) AS doc_year
                      FROM
                        inventory_receive_detail
                      INNER JOIN goods_receive_detail ON inventory_receive_detail.id = goods_receive_detail.inventory_receive_detail_id
                      GROUP BY
                        date_FORMAT(
                          inventory_receive_detail.created_at,
                          "%Y"
                        )';
      $doc_year = DB::table(DB::raw("($sql_doc_year) as rs_sql_doc_year"))->first();

      // dd($revision);
      // $revision = GoodsReceive::where('revision', '1')->first();


      return view ('main.warehouse.goods_receive.form', compact('goods_receive', 'goods_receive_po', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list','inventory_list', 'uom_list', 'revision', 'data_line', 'doc_year'));
    }

    public function update($id, Request $request)
    {

        // $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        // $var_day_transaction = $date_transaction_array[0]; //day seqment
        // $var_month_transaction = $date_transaction_array[1]; //month segment
        // $var_year_transaction = $date_transaction_array[2]; //year segment
        // $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $date_from_array = explode("/",$request->input('date_transaction')); // split the array
        $var_month_from = $date_from_array[0]; //month segment
        $var_day_from = $date_from_array[1]; //day seqment
        $var_year_from = $date_from_array[2]; //year segment
        $date_transaction_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

        $post = GoodsReceive::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        // $post->warehouse_id = $request->input('warehouse_id');
        // $post->bay_id = $request->input('bay_id');
        // $post->room_id = $request->input('room_id');
        // $post->rack_id = $request->input('rack_id');
        // $post->pallet_number = $request->input('pallet_number');
        $post->updated_by = Auth::id();
        $post->save();

        $userid = Auth::id();

        $user_log = new UserLog;
        $user_log->user_id = $userid;
        $user_log->scope = 'GOODS RECEIVE';
        $user_log->data = json_encode([
                              'action' => 'update',
                              'transaction_number' => $post->no_transaction,
                              'description' => $request->input('description'),
                              'source' => 'WMS',
                              'goods_receive_id' => $id
                          ]);
        $user_log->save();

        return redirect()->action('Main\Warehouse\GoodsReceiveController@index');
    }

    public function delete($id)
    {
      $post =  GoodsReceive::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = GoodsReceive::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }

  public function cancel(Request $request, $id)
  {
    $sql_line = 'SELECT
                  	COUNT(goods_receive_detail.id) AS id
                  FROM
                  	goods_receive_detail
                  INNER JOIN stock ON goods_receive_detail.pallet_number = stock.pallet_number
                  WHERE
                  	goods_receive_detail.transaction_id = '.$id.' AND goods_receive_detail.quantity > stock.quantity AND goods_receive_detail.pallet_number = stock.pallet_number';
    $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if($data_line->id < 1)
    {
      $statement = GoodsReceive::where('id', $id)->first();
      $statement->status = 1;
      $statement->save();

      $gr_detail = GoodsReceiveDetail::where('transaction_id', $id)->get();

      foreach($gr_detail as $gr_details)
      {
        $post = InventoryReceiveDetail::where('id', $gr_details->inventory_receive_detail_id)->first();
        $post->status = 0;
        $post->save();
      }
    };

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'GOODS RECEIVE';
      $user_log->data = json_encode([
                            'action' => 'cancel',
                            'transaction_number' => $statement->no_transaction,
                            'source' => 'WMS',
                            'description' => 'Cancel Goods Receive',
                            'goods_receive_id' => $id
                        ]);
      $user_log->save();
  }

  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
                goods_receive.id,
                goods_receive.nodoc,
                goods_receive.code_transaction,
                goods_receive.created_by,
                goods_receive.no_transaction AS notransaction,
                goods_receive_detail.po_number AS purchasing_number,
                RIGHT (
                  goods_receive_detail.item_line,
                  2
                ) AS item_number,
                goods_receive_detail.plant,
                goods_receive_detail.quantity,
                goods_receive_detail.store_loc AS storage_loc,
                goods_receive_detail.quantity AS po_qty,
                goods_receive_detail.unit_of_measure_code AS po_unit,
                DATE_FORMAT(
                  goods_receive_detail.doc_date,
                  "%d.%m.%Y"
                ) AS po_date,
                DATE_FORMAT(
                  goods_receive_detail.doc_date,
                  "%d.%m.%Y"
                ) AS doc_date,
                RIGHT (
                  goods_receive_detail.supplier_id,
                  7
                ) AS supplier_id,
                RIGHT (goods_receive_detail.batch, 8) AS batch,
                goods_receive_detail.store_loc,
                goods_receive_detail.po_number,
                goods_receive_detail.item_line,
                goods_receive_detail.inventory_id,
                goods_receive_detail.id AS id_detail,
                supplier.supplier_code AS vendor_number,
                RIGHT (inventory.inventory_code, 7) AS material_number,
                supplier.supplier_name,
                inventory.inventory_code,
                inventory.inventory_name,
                goods_receive.description AS doc_header,
                warehouse.warehouse_name,
                room.room_name,
                bay.bay_name,
                rack.rack_name,
                goods_receive_detail.pallet_number,
                goods_receive_detail.description AS item_text,
                goods_receive_detail.revision,
                DATE_FORMAT(inventory_receive_detail.created_at,
                  "%d.%m.%Y"
                ) AS receive_date,
                DATE_FORMAT(goods_receive.date_transaction,
                  "%d.%m.%Y"
                ) AS date_transaction_sap
              FROM
                goods_receive_detail
              LEFT OUTER JOIN goods_receive ON goods_receive.id = goods_receive_detail.transaction_id
              LEFT OUTER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
              LEFT OUTER JOIN supplier ON goods_receive_detail.supplier_id = supplier.supplier_code
              LEFT OUTER JOIN warehouse ON goods_receive_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN room ON goods_receive_detail.room_id = room.id
              LEFT OUTER JOIN bay ON goods_receive_detail.bay_id = bay.id
              LEFT OUTER JOIN rack ON goods_receive_detail.rack_id = rack.id
              LEFT JOIN inventory_receive_detail ON goods_receive_detail.inventory_receive_detail_id = inventory_receive_detail.id
              WHERE
              goods_receive_detail.deleted_at IS NULL AND goods_receive_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        if(strlen($itemdata->nodoc) > 0)
        {
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" disabled> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" disabled><i class="icon-trash"></i></a>';
        }else{
          if($itemdata->code_transaction == 'GRMN' && $itemdata->created_by != Auth::id())
          {
            return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" disabled> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" disabled><i class="icon-trash"></i></a>';
          }else{
            return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id_detail."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id_detail."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
          };
        };
      })

      // ->addColumn('revision', function ($itemdata) {
      //     if ($itemdata->revision == 0) {
      //       return '<span class="label label-success">No</span>';
      //     }else{
      //     return '<span class="label label-danger">Yes</span>';
      //   };
      // })
      // ->rawColumns(['action'])
      ->make(true);
      } else {
        exit("No data available");
      }
  }

  public function post_response_date(Request $request, $id)
  {
    $userid = Auth::id();


    $date_from_array = explode("/",$request->date_transaction); // split the array
    $var_month_from = $date_from_array[0]; //month segment
    $var_day_from = $date_from_array[1]; //day seqment
    $var_year_from = $date_from_array[2]; //year segment
    $date_transaction_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

    $statement = GoodsReceive::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->date_transaction = $date_transaction_format;
    $statement->save();

  }

  public function post_response(Request $request, $id)
  {
    $userid = Auth::id();


    $date_from_array = explode("/",$request->date_transaction); // split the array
    $var_month_from = $date_from_array[0]; //month segment
    $var_day_from = $date_from_array[1]; //day seqment
    $var_year_from = $date_from_array[2]; //year segment
    $date_transaction_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together



    $statement = GoodsReceive::where('id', $id)->first();
    $statement->notransaction = $request->notransaction;
    $statement->nodoc = $request->nodoc;
    $statement->tahundoc = $request->tahundoc;
    $statement->nopo = $request->nopo;
    $statement->description = $request->description;
    $statement->date_transaction = $date_transaction_format;
    $statement->save();

    $user_log = new UserLog;
    $user_log->user_id = $userid;
    $user_log->scope = 'GOODS RECEIVE';
    $user_log->data = json_encode([
                          'action' => 'update',
                          'transaction_number' => $request->no_transaction,
                          'description' => $request->description,
                          'source' => 'WMS',
                          'goods_receive_id' => $id
                      ]);
    $user_log->save();
  }

  public function post_response_detail(Request $request, $id)
  {
    $statement = GoodsReceiveDetail::where('po_number', $request->nopo)->get();

    foreach($statement as $statements)
    {
      $post = GoodsReceiveDetail::where('id', $statement->id)->first();
      $post->revision = 1;
      $post->save();
    }

  }

  public function post_detail(Request $request, $id)
  {
    $statement = new GoodsReceiveDetail;
    $statement->transaction_id = $id;
    $statement->po_number = $request->po_number;
    $statement->inventory_id = $request->inventory_id;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->quantity = $request->quantity;
    $statement->batch = $request->batch;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->item_line = $request->item_line;
    $statement->doc_date = $request->doc_date;
    $statement->supplier_id = $request->supplier_id;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->pallet_number = $request->pallet_number;
    $statement->description = $request->item_text;
    $statement->inventory_receive_detail_id = $request->inventory_receive_detail_id;
    $statement->status = 0;
    $statement->save();

    $receive = InventoryReceiveDetail::where('id', $request->inventory_receive_detail_id)->first();
    $receive->status = 9;
    $receive->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = GoodsReceiveDetail::where('id', $id)->first();
    $statement->inventory_id = $request->inventory_id;
    $statement->description = $request->item_text;

    $header = GoodsReceive::where('id', $statement->transaction_id)->first();
    if($header->code_transaction == 'GRMN')
    {
      $statement->unit_of_measure_code = $request->unit_of_measure_code;
    };

    $statement->quantity = $request->quantity;
    // $statement->plant = $request->plant;
    // $statement->store_loc = $request->store_loc;
    // $statement->item_line = $request->item_line;
    // $statement->doc_date = $request->doc_date;
    // $statement->unit_of_measure_code = $request->unit_of_measure_code;
    // $statement->supplier_id = $request->supplier_id;
    // $statement->warehouse_id = $request->warehouse_id;
    // $statement->room_id = $request->room_id;
    // $statement->bay_id = $request->bay_id;
    // $statement->rack_id = $request->rack_id;
    // $statement->pallet_number = $request->pallet_number;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = GoodsReceiveDetail::where('id', $id)->first();

    $open_pick = InventoryReceiveDetail::where('id', $statement->inventory_receive_detail_id)->first();
    $open_pick->status = 0;
    $open_pick->save();

    $statement->delete();
  }

  public function get_po_sap($id)
  {
    // $now = Carbon::now();
    // $date_now = $now->format('Y-m-d');
    // $data = GRreturn::where('donum', '-')->orWhere('donum', null)->orWhere('donum', '')->where('code_transaction', 'GRRT')->get();
    $sql = 'SELECT
              goods_receive_detail.inventory_id,
              inventory.inventory_code,
              goods_receive_detail.quantity,
              goods_receive_detail.revision,
              goods_receive_detail.item_line,
              goods_receive_detail.po_number as nopo
            FROM
              goods_receive
            INNER JOIN goods_receive_detail ON goods_receive.id = goods_receive_detail.transaction_id
            INNER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
            WHERE goods_receive.code_transaction = "GRNC" AND goods_receive.id = '.$id.' ';
            // -- WHERE
            //   -- goods_receive_detail.revision = 1';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    if(count($data)>0)
    {
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['inventory_code'] = $datas->inventory_code;
        $json[$h_key]['quantity'] = $datas->quantity;
        $json[$h_key]['nopo'] = $datas->nopo;
        $json[$h_key]['item_line'] = $datas->item_line;
      }
    }else {
      $json[0]['inventory_code'] = '';
      $json[0]['quantity'] = '';
      $json[0]['nopo'] = '';
      $json[0]['item_line'] = '';
    }
    return response()->json($json);

  }

  public function post_po_revision(Request $request)
  {
    // try {
        $inventory = Inventory::where('inventory_code', $request->inventory_code)->first();

        // dd($request->inventory_code);

        $inventory_receive = InventoryReceiveDetail::where('po_number', $request->nopo)->where('item_line', $request->item_line)->get();
        if(isset($inventory_receive) && isset($inventory))
        {
          if(isset($inventory))
          {
            foreach($inventory_receive as $inventory_receives)
            {
              $post_inventory_receive = InventoryReceiveDetail::where('id', $inventory_receives->id)->first();
              // $post_inventory_receive->quantity = $request->quantity;
              $post_inventory_receive->inventory_id = $inventory->id;
              $post_inventory_receive->revision = 9;
              $post_inventory_receive->save();

              DB::update("UPDATE inventory_receive_detail SET `revision` = 9 WHERE po_number = ".$inventory_receives->po_number."");

            };
          };
        };

        $statement = GoodsReceiveDetail::where('po_number', $request->nopo)->where('item_line', $request->item_line)->get();
        if(isset($statement))
        {
          // if(isset($inventory))
          // {
            // $statement->quantity = $request->quantity;
            foreach($statement as $statements)
            {
              $post = GoodsReceiveDetail::where('id', $statements->id)->first();
              $post->inventory_id = $inventory->id;
              $post->unit_of_measure_code = $inventory->unit_of_measure;
              $post->revision = 0;
              $post->save();
            };
          // };
        }else{
          if(isset($inventory))
          {
            $statement = new GoodsReceiveDetail;
            $post_new->quantity = $request->quantity;
            $post_new->inventory_id = $inventory->id;
            $post_new->po_number = $nopo;
            $post_new->unit_of_measure_code = $inventory->unit_of_measure;
            $post_new->batch = $request->batch;
            $post_new->plant = $plant;
            $post_new->store_loc = $store_loc;
            $post_new->item_line = $item_line;
            $post_new->doc_date = $doc_date;
            $post_new->supplier_id = $supplier;
            // $post_new->warehouse_id = $warehouse_id;
            // $post_new->room_id = $room_id;
            // $post_new->bay_id = $bay_id;
            // $post_new->rack_id = $rack_id;
            // $post_new->pallet_number = $pallet_number;
            $post_new->status = 0;
            $post_new->revision = 0;
            $post_new->save();
          };

        };
        if($request->deletation == 'L')
        {
          $post_deletation = GoodsReceiveDetail::where('nopo', $nopo)->where('item_line', $item_line)->first();
          if(isset($post_deletation))
          {
            $post_deletation->delete();
          }
        };

        if($request->deletation == 'L')
        {
          $post_deletation = InventoryReceiveDetail::where('po_number', $nopo)->where('item_line', $item_line)->where('item_line', $item_line)->first();
          $post_deletation->delete();
        };
    //   return response()->json([
    //     'code' => 1,
    //     'msg' => 'Success',
    //     'data' => response()->json($request->all())
    //   ]);

    // } catch (\Exception $e) {
    //   $json['code'] = 0;
    //   $json['msg'] = "Failed";
    // }
  }

  // =============================================== REPORT ======================================================
  public function report()
  {
    return view ('main.warehouse.goods_receive.report');
  }

  public function report_outstanding()
  {
    return view ('main.warehouse.goods_receive.report_outstanding');
  }

  public function data_report(Request $request)
  {

    $sql = 'SELECT
              goods_receive.no_transaction,
              goods_receive.date_transaction,
              goods_receive.status,
              goods_receive_detail.po_number,
              purchase_order.code_vendor,
              inventory.inventory_code,
              inventory.inventory_name,
              goods_receive_detail.unit_of_measure_code,
              goods_receive_detail.quantity,
              goods_receive_detail.plant,
              goods_receive_detail.batch,
              container.container_no
            FROM
              goods_receive
            LEFT OUTER JOIN goods_receive_detail ON goods_receive.id = goods_receive_detail.transaction_id
            LEFT OUTER JOIN purchase_order ON goods_receive_detail.po_number = purchase_order.po_number
            LEFT JOIN container ON purchase_order.container_id = container.id
            LEFT OUTER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {

          $date_from_array = explode("-",$request->input('date_from')); // split the array
          $var_day_from = $date_from_array[0]; //day seqment
          $var_month_from = $date_from_array[1]; //month segment
          $var_year_from = $date_from_array[2]; //year segment
          $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

          $date_to_array = explode("-",$request->input('date_to')); // split the array
          $var_day_to = $date_to_array[0]; //day seqment
          $var_month_to = $date_to_array[1]; //month segment
          $var_year_to = $date_to_array[2]; //year segment
          $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

          if($request->input('date_from') != '' || $request->input('date_to') != '')
          {
            $itemdata->whereBetween('rs_sql.date_transaction', [$date_from_format, $date_to_format]);
            // $itemdata->where('rs_sql.status', 0);
          }else{
            $itemdata->where('rs_sql.status', 0);
          };
      })
      ->make(true);
  }


  public function report_outstanding_summary()
  {
    return view ('main.warehouse.goods_receive.report_outstanding_summary');
  }

  public function data_report_outstanding_summary(Request $request)
  {

    $sql = 'SELECT
              inventory_receive_detail.po_number,
              purchase_order.code_vendor,
              inventory.inventory_code,
              inventory.inventory_name,
              inventory_receive_detail.uom AS unit_of_measure_code,
              SUM(ifnull(inventory_receive_detail.quantity,0)) AS quantity,
              inventory_receive_detail.item_line,
              inventory_receive_detail.revision,
              supplier.supplier_name
            FROM
              inventory_receive
            LEFT OUTER JOIN inventory_receive_detail ON inventory_receive.id = inventory_receive_detail.transaction_id
            LEFT OUTER JOIN purchase_order ON inventory_receive_detail.po_number = purchase_order.po_number
            LEFT JOIN supplier ON purchase_order.code_vendor = supplier.supplier_code
            LEFT OUTER JOIN container ON purchase_order.container_id = container.id
            LEFT OUTER JOIN inventory ON inventory_receive_detail.inventory_id = inventory.id
            WHERE
            -- inventory_receive_detail.revision = 1 AND
            inventory_receive.putaway_status > 0
            GROUP BY
              inventory_receive_detail.revision,
              inventory_receive_detail.po_number,
              purchase_order.code_vendor,
              inventory.inventory_code,
              inventory.inventory_name,
              inventory_receive_detail.uom,
              inventory_receive_detail.item_line ASC,
              supplier.supplier_name';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)

      // dd()
      ->filter(function ($itemdata) use ($request) {
          if($request->input('filter_status')  == 9) {
              $itemdata->where('rs_sql.revision', 9);
          }else{
              $itemdata->where('rs_sql.revision', 1);
          }

          if($keyword = $request->input('keyword')) {
            $itemdata->whereRaw("CONCAT(IFNULL(rs_sql.po_number,''), IFNULL(rs_sql.inventory_code,''), IFNULL(rs_sql.inventory_name,''), IFNULL(rs_sql.code_vendor,''), ifnull(rs_sql.supplier_name,'')) like ?", ["%{$keyword}%"]);
          }
      })
      // ->addColumn('mstatus', function ($itemdata) {
      //   if ($itemdata->status == 0) {
      //     return '<span class="label label-success">Yet GR</span>';
      //   }else{
      //    return '<span class="label label-danger">Has Been GR</span>';
      //  };
      // })

      // ->filter(function ($itemdata) use ($request) {

          // $date_from_array = explode("-",$request->input('date_from')); // split the array
          // $var_day_from = $date_from_array[0]; //day seqment
          // $var_month_from = $date_from_array[1]; //month segment
          // $var_year_from = $date_from_array[2]; //year segment
          // $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

          // $date_to_array = explode("-",$request->input('date_to')); // split the array
          // $var_day_to = $date_to_array[0]; //day seqment
          // $var_month_to = $date_to_array[1]; //month segment
          // $var_year_to = $date_to_array[2]; //year segment
          // $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

          // if($request->input('date_from') != '' || $request->input('date_to') != '')
          // {
          //   $itemdata->whereBetween('rs_sql.date_transaction', [$date_from_format, $date_to_format]);
          //   // $itemdata->where('rs_sql.status', 0);
          // }else{
            // $itemdata->where('rs_sql.status', 0);
          // };
      //     if($keyword = $request->input('keyword')) {
      //         $itemdata->whereRaw("CONCAT(IFNULL(rs_sql.po_number,''), IFNULL(rs_sql.inventory_code,''), IFNULL(rs_sql.inventory_name,''), IFNULL(rs_sql.code_vendor,''), ifnull(rs_sql.supplier_name,'')) like ?", ["%{$keyword}%"]);
      //       }
      // })
      ->rawColumns(['mstatus'])
      ->make(true);
  }

  public function data_report_outstanding(Request $request)
  {

    $sql = 'SELECT
                goods_receive.id,
                goods_receive.nodoc,
                goods_receive.no_transaction AS no_transaction,
                goods_receive.date_transaction AS date_transaction,
                goods_receive_detail.po_number AS purchasing_number,
                goods_receive_detail.item_line AS item_number,
                goods_receive_detail.plant,
                goods_receive_detail.quantity,
                goods_receive_detail.store_loc AS storage_loc,
                goods_receive_detail.quantity AS po_qty,
                goods_receive_detail.unit_of_measure_code AS po_unit,
                DATE_FORMAT(goods_receive_detail.doc_date,"%d.%m.%Y") AS po_date,
                DATE_FORMAT(goods_receive_detail.doc_date,"%d.%m.%Y") AS doc_date,
                RIGHT(goods_receive_detail.supplier_id,7) AS supplier_id,
                -- goods_receive_detail.batch,
                RIGHT(goods_receive_detail.batch, 8) AS batch,
                goods_receive_detail.store_loc,
                goods_receive_detail.po_number,
                goods_receive_detail.item_line,
                goods_receive_detail.inventory_id,
                goods_receive_detail.id AS id_detail,
                supplier.supplier_code as vendor_number,
                inventory.inventory_code AS material_number,
                inventory.inventory_code,
                inventory.inventory_name,
                goods_receive.description AS doc_header,
                warehouse.warehouse_name,
                room.room_name,
                bay.bay_name,
                rack.rack_name,
                goods_receive_detail.pallet_number
              FROM
                goods_receive_detail
              LEFT OUTER JOIN goods_receive ON goods_receive.id = goods_receive_detail.transaction_id
              LEFT OUTER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
              LEFT OUTER JOIN supplier ON goods_receive_detail.supplier_id = supplier.id
              LEFT OUTER JOIN warehouse ON goods_receive_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN room ON goods_receive_detail.room_id = room.id
              LEFT OUTER JOIN bay ON goods_receive_detail.bay_id = bay.id
              LEFT OUTER JOIN rack ON goods_receive_detail.rack_id = rack.id
              WHERE
              goods_receive_detail.deleted_at IS NULL AND goods_receive_detail.revision =1';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {

          $date_from_array = explode("-",$request->input('date_from')); // split the array
          $var_day_from = $date_from_array[0]; //day seqment
          $var_month_from = $date_from_array[1]; //month segment
          $var_year_from = $date_from_array[2]; //year segment
          $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

          $date_to_array = explode("-",$request->input('date_to')); // split the array
          $var_day_to = $date_to_array[0]; //day seqment
          $var_month_to = $date_to_array[1]; //month segment
          $var_year_to = $date_to_array[2]; //year segment
          $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

          if($request->input('date_from') != '' || $request->input('date_to') != '')
          {
            $itemdata->whereBetween('rs_sql.date_transaction', [$date_from_format, $date_to_format]);
            // $itemdata->where('rs_sql.status', 0);
          }else{
            $itemdata->where('rs_sql.status', 0);
          };
      })
      ->make(true);
  }

  public function report_print($id, $id1)
  {
    $date_from_array = explode("-",$id); // split the array
    $var_day_from = $date_from_array[0]; //day seqment
    $var_month_from = $date_from_array[1]; //month segment
    $var_year_from = $date_from_array[2]; //year segment
    $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

    $date_to_array = explode("-",$id1); // split the array
    $var_day_to = $date_to_array[0]; //day seqment
    $var_month_to = $date_to_array[1]; //month segment
    $var_year_to = $date_to_array[2]; //year segment
    $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

    $itemdata = DB::table('goods_receive')
          ->leftjoin('warehouse', 'goods_receive.warehouse_id', '=', 'warehouse.id')
          ->whereBetween('goods_receive.date_transaction', [$date_from_format, $date_to_format])
          ->select('goods_receive.*', 'warehouse.warehouse_name')->get();

    return view ('main.warehouse.goods_receive.report_print', compact('itemdata'));
  }

  public function sap_gr(Request $request)
  {

        $curl = curl_init();
        $data = array(
                'notransaction' => $request->notransaction,
                'purchasing_number' => $request->purchasing_number,
                'item_number' => $request->item_number,
                'material_number' => $request->material_number,
                'plant' => $request->plant,
                'storage_loc' => $request->storage_loc,
                'po_qty' => $request->po_qty,
                'po_unit' => $request->po_unit,
                'po_date' => $request->po_date,
                'posting_date' => $request->posting_date,
                'vendor_number' => $request->vendor_number,
                'batch' => $request->batch,
                'item_text' => $request->item_text,
                'doc_header' => $request->doc_header,
                'grrcpt' => $request->grrcpt,
                'total_items' => $request->total_items);

    		curl_setopt_array($curl, array(
                              		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-gr",
                              		CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                              		CURLOPT_ENCODING => "",
                              		CURLOPT_MAXREDIRS => 10,
                              		// CURLOPT_TIMEOUT => 35,
                              		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              		CURLOPT_CUSTOMREQUEST => "POST",
    		                        ));

    		$response = curl_exec($curl);
    		$err = curl_error($curl);

    		curl_close($curl);

    		if ($err) {
    		echo "cURL Error #:" . $err;
    		} else {
      		// echo $response;
          $json = json_decode($response);
          return response()->json($json);
    		}
  }

  public function sap_exegr(Request $request)
  {
        $curl = curl_init();
        $data = array(
                'notransaction' => $request->notransaction,
                'nodoc' => $request->nodoc,
                'tahundoc' => $request->tahundoc,
                'nopo' => $request->nopo,
                'description' => $request->description);

    		curl_setopt_array($curl, array(
                              		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exegr",
                              		CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_POSTFIELDS => http_build_query($data),
                              		CURLOPT_ENCODING => "",
                              		CURLOPT_MAXREDIRS => 10,
                              		// CURLOPT_TIMEOUT => 35,
                              		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              		CURLOPT_CUSTOMREQUEST => "POST",
                            		));

    		$response = curl_exec($curl);
    		$err = curl_error($curl);

    		curl_close($curl);

    		if ($err) {
    		echo "cURL Error #:" . $err;
    		} else {
      		// echo $response;
          $json = json_decode($response);
          return response()->json($json);
    		}
    }

    public function sap_cancelgr(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'notransaction' => $request->notransaction,
                  'nodoc' => $request->nodoc,
                  'tahundoc' => $request->tahundoc,
                  'nopo' => $request->nopo);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgr",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 35,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
          }
          
    }



  public function sap_gr_new(Request $request, $id)
  {

    $user_id = Auth::id();
      $user = User::where('id', $user_id)->first();
      $end_of_line = GoodsReceiveDetail::where('transaction_id', $id)->whereNull('deleted_at')->max('item_line');

      // dd($end_of_line);

      $sql = 'SELECT
                goods_receive.id,
                goods_receive.no_transaction AS notransaction,
                goods_receive_detail.po_number AS purchasing_number,
                goods_receive_detail.item_line AS item_number,
                goods_receive_detail.plant,
                goods_receive_detail.store_loc AS storage_loc,
                SUM(goods_receive_detail.quantity) AS po_qty,
                goods_receive_detail.unit_of_measure_code AS po_unit,
                DATE_FORMAT(
                  goods_receive_detail.doc_date,
                  "%Y%m%d"
                ) AS po_date,
                DATE_FORMAT(goods_receive.date_transaction, "%Y%m%d") AS posting_date,
                DATE_FORMAT(goods_receive.date_transaction,
                  "%d.%m.%Y"
                ) AS date_transaction_sap,
                goods_receive_detail.supplier_id,
                goods_receive_detail.item_line,
                goods_receive_detail.inventory_id,
                goods_receive_detail.batch,
                supplier.supplier_code AS vendor_number,
                inventory.inventory_code AS material_number,
                goods_receive.description AS doc_header
              FROM
                goods_receive_detail
              INNER JOIN goods_receive ON goods_receive.id = goods_receive_detail.transaction_id
              INNER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
              LEFT JOIN supplier ON goods_receive_detail.supplier_id = supplier.supplier_code
              WHERE
              goods_receive_detail.deleted_at IS NULL AND goods_receive_detail.transaction_id = '.$id.'
              GROUP BY
              goods_receive.id,
              goods_receive.no_transaction,
              goods_receive_detail.po_number,
              goods_receive_detail.item_line,
              goods_receive_detail.plant,
              goods_receive_detail.store_loc,
              goods_receive_detail.unit_of_measure_code,
              DATE_FORMAT(
                goods_receive_detail.doc_date,
                "%d.%m.%Y"
              ),
              goods_receive_detail.supplier_id,
              goods_receive_detail.inventory_id,
              goods_receive_detail.batch,
              supplier.supplier_code,
              inventory.inventory_code,
              goods_receive.description
              ORDER BY goods_receive_detail.item_line ASC';
      $data = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('item_line', 'ASC')->get();

      $sql_line = 'SELECT
                      transaction_id,
                      count(item_line) AS item_line,
                      count(store_loc) AS store_loc
                    FROM
                      (
                        SELECT
                          goods_receive_detail.transaction_id,
                          goods_receive_detail.item_line,
                          goods_receive_detail.store_loc
                        FROM
                          goods_receive_detail
                        WHERE
                          goods_receive_detail.deleted_at IS NULL
                        AND goods_receive_detail.transaction_id = '.$id.'
                        GROUP BY
                          goods_receive_detail.transaction_id,
                          goods_receive_detail.item_line,
                          goods_receive_detail.store_loc
                      ) AS derivdtbl
                    GROUP BY
                      transaction_id';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

      // dd($data_line->item_line);

      if(count($data)>0)
      {
        sleep(3);
        foreach ($data as $h_key => $datas) {

          $item_text = GoodsReceiveDetail::where('po_number', $datas->purchasing_number)->where('description', '!=', '')->where('description', '!=', null)->first();

          if(isset($item_text)){
            $text_item_desc = $item_text->description;
          }else{
            $text_item_desc = '-';
          };

          $json[$h_key]['notransaction'] = $datas->notransaction;
          $json[$h_key]['date_transaction_sap'] = $datas->date_transaction_sap;
          $json[$h_key]['purchasing_number'] = $datas->purchasing_number;
          $json[$h_key]['item_number'] = $datas->item_number;
          $json[$h_key]['plant'] = $datas->plant;
          $json[$h_key]['storage_loc'] = $datas->storage_loc;
          $json[$h_key]['po_qty'] = $datas->po_qty;
          $json[$h_key]['po_unit'] = $datas->po_unit;
          $json[$h_key]['po_date'] = $datas->po_date;
          $json[$h_key]['posting_date'] = $datas->posting_date;
          $json[$h_key]['batch'] = $datas->batch;
          $json[$h_key]['vendor_number'] = $datas->vendor_number;
          $json[$h_key]['material_number'] = $datas->material_number;
          $json[$h_key]['doc_header'] = $datas->doc_header;
          $json[$h_key]['inventory_id'] = $datas->inventory_id;
          $json[$h_key]['grrcpt'] = $user->username;
          $json[$h_key]['item_text'] = $text_item_desc;
          $json[$h_key]['total_items'] = $data_line->item_line;
          // if($datas->item_line == $end_of_line)
          // {
          //   $json[$h_key]['flag'] = 'X';
          // }else{
          //   $json[$h_key]['flag'] = '';
          // };
        }
      }
 
    $data_json = array_values(
              $json
            );
    
    $test_data = array(
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "10",
                        "MATNR" => "000000000001100090",
                        "LFIMG" => "10",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ],
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "20",
                        "MATNR" => "000000000001100080",
                        "LFIMG" => "50",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ],
                    [
                        "GI_WMS" => "GDIS/II/20/00000",
                        "VBELN" => "3000000097",
                        "POSNR_VL" => "30",
                        "MATNR" => "000000000001100131",
                        "LFIMG" => "50",
                        "PIKMG" => "1000000",
                        "NOTES" => "TEST TEXT",
                        "LINES" => "3",
                        "POST_DATE" => "20200220"
                    ]
                );


      $curl = curl_init();
   
      // dd(json_encode($data_json));

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wmsdev/sap-gido",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 3000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data_json),
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // print_r(json_decode($response));
        $json = json_decode($response);
        return response()->json($json);
      }
  }

  public function post_data_sap_new(Request $request, $id)
  {
    $sql = 'SELECT
              goods_receive_detail.inventory_id,
              inventory.inventory_code,
              goods_receive_detail.quantity,
              goods_receive_detail.revision,
              goods_receive_detail.item_line,
              goods_receive_detail.po_number as nopo
            FROM
              goods_receive
            INNER JOIN goods_receive_detail ON goods_receive.id = goods_receive_detail.transaction_id
            INNER JOIN inventory ON goods_receive_detail.inventory_id = inventory.id
            WHERE goods_receive.code_transaction = "GRNC" AND goods_receive.id = '.$id.' ';
            // -- WHERE
            //   -- goods_receive_detail.revision = 1';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    if(count($data)>0)
    {
      foreach ($data as $h_key => $datas) {
        // $json[$h_key]['inventory_code'] = $datas->inventory_code;
        // $json[$h_key]['quantity'] = $datas->quantity;
        // $json[$h_key]['nopo'] = $datas->nopo;
        // $json[$h_key]['item_line'] = $datas->item_line;

        // dd("https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/po-revisi/" . $datas->nopo . "/" . $datas->item_line);

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/po-revisi/" . $datas->nopo . "/"  . $datas->item_line,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 3000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          // CURLOPT_POSTFIELDS => json_encode($data_json),
          CURLOPT_HTTPHEADER => array(
              // Set here requred headers
              "accept: */*",
              "accept-language: en-US,en;q=0.8",
              "content-type: application/json",
          ),
        ));

        $response = curl_exec($curl);
        $po_decode = json_decode($response, true);
        $err = curl_error($curl);

        // dd($po_decode["podetail"][0]["MATNR"]);


        // ----------------------------------------- POST PO SAP -------------------------------------------
        if(isset($po_decode["podetail"][0]["MATNR"]))
        {

            $inventory = Inventory::where('inventory_code', $po_decode["podetail"][0]["MATNR"])->first();

            // dd($request->inventory_code);

            $inventory_receive = InventoryReceiveDetail::where('po_number', $datas->nopo)->where('item_line', $datas->item_line)->get();
            if(isset($inventory_receive) && isset($inventory))
            {
              if(isset($inventory))
              {
                foreach($inventory_receive as $inventory_receives)
                {
                  $post_inventory_receive = InventoryReceiveDetail::where('id', $inventory_receives->id)->first();
                  // $post_inventory_receive->quantity = $request->quantity;
                  $post_inventory_receive->inventory_id = $inventory->id;
                  $post_inventory_receive->revision = 9;
                  $post_inventory_receive->save();

                  DB::update("UPDATE inventory_receive_detail SET `revision` = 9 WHERE po_number = ".$inventory_receives->po_number."");

                };
              };
            };

            $statement = GoodsReceiveDetail::where('po_number', $datas->nopo)->where('item_line', $datas->item_line)->get();
            if(isset($statement))
            {
              // if(isset($inventory))
              // {
                // $statement->quantity = $request->quantity;
                foreach($statement as $statements)
                {
                  $post = GoodsReceiveDetail::where('id', $statements->id)->first();
                  $post->inventory_id = $inventory->id;
                  $post->unit_of_measure_code = $inventory->unit_of_measure;
                  $post->revision = 0;
                  $post->save();
                };
              // };
            }else{
              if(isset($inventory))
              {
                $statement = new GoodsReceiveDetail;
                $post_new->quantity = $po_decode["podetail"][0]["MATNR"];
                $post_new->inventory_id = $po_decode["podetail"][0]["MENGE"];
                $post_new->po_number = $datas->nopo;
                $post_new->unit_of_measure_code = $po_decode["podetail"][0]["MEINS"];
                // $post_new->batch = $po_decode["podetail"][0]["MATNR"];
                $post_new->plant = $po_decode["podetail"][0]["WERKS"];
                $post_new->store_loc = $po_decode["podetail"][0]["LGORT"];
                $post_new->item_line = $datas->item_line;
                $post_new->doc_date = $po_decode["podetail"][0]["BUDAT"];
                // $post_new->supplier_id = $po_decode["podetail"][0]["LIFNR"];
                // $post_new->warehouse_id = $warehouse_id;
                // $post_new->room_id = $room_id;
                // $post_new->bay_id = $bay_id;
                // $post_new->rack_id = $rack_id;
                // $post_new->pallet_number = $pallet_number;
                $post_new->status = 0;
                $post_new->revision = 0;
                $post_new->save();
              };

            };
            if($request->deletation == 'L')
            {
              $post_deletation = GoodsReceiveDetail::where('nopo', $nopo)->where('item_line', $item_line)->first();
              if(isset($post_deletation))
              {
                $post_deletation->delete();
              }
            };

            if($request->deletation == 'L')
            {
              $post_deletation = InventoryReceiveDetail::where('po_number', $nopo)->where('item_line', $item_line)->where('item_line', $item_line)->first();
              $post_deletation->delete();
            };
        };
      };

        // ----------------------------------------- POST PO SAP -------------------------------------------

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          // print_r(json_decode($response));
          $json = json_decode($response);
          return response()->json($json);
        }

        
      }
  }

}
