<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\GRReturnRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\Model\GRReturn;
use App\Model\GRReturnDetail;
use App\Model\SalesReturnDetail;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\Reason;
use App\User;
use Carbon\Carbon;

use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class GRReturnController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'gr_return_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $gr_returns = GRReturn::all();
      return view ('main.warehouse.gr_return.index', compact('gr_returns'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('gr_return')
            ->leftjoin('warehouse', 'gr_return.warehouse_id', '=', 'warehouse.id')
            ->leftjoin('room', 'gr_return.room_id', '=', 'room.id')
            ->leftjoin('rack', 'gr_return.rack_id', '=', 'rack.id')
            ->leftjoin('bay', 'gr_return.bay_id', '=', 'bay.id')
            ->select('gr_return.*', 'warehouse.warehouse_name', 'room.room_name', 'rack.rack_name', 'bay.bay_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {

            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($id = $request->input('filter_status')) {
                $itemdata->where('gr_return.status', $id);
            }else{
                $itemdata->where('gr_return.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(gr_return.no_transaction, gr_return.sonum, gr_return.donum, gr_return.grnum,'gr_return.warehouse_id') like ?", ["%{$keyword}%"]);
            }
             $itemdata->whereBetween('gr_return.date_transaction', [$date_from_format, $date_to_format]);
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('gr_return', ['update'])) {
            $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="gr-return/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('gr_return', ['delete'])) {
            $archieve_disabled = '<a  type="button" disabled="disabled" class="btn btn-warning btn-float btn-xs" title="Archive"> <i class="icon-archive"></i></a>';
            $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $archieve_disabled = '';
            $reactive = '';
          };
        //   if($itemdata->status ==1){
        //     return ''.$reactive.'';
        //   }else{
        //     return ''.$edit.' '.$archieve.'';
        //   }
        // })

        if($itemdata->status ==1){
          return ''.$reactive.'';
          }elseif(strlen($itemdata->grnum) > 0){
            return ''.$edit.' '.$archieve_disabled.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

         ->addColumn('do_no', function($itemdata) {
            $return_det = GRReturnDetail::where('transaction_id', $itemdata->id)->first();

            // foreach($picking_det as $picking_dets)
            // {
            //   $picking_data = $picking_dets->picking_number;
            // };

            // $test = $picking_data;
            // print_r($test);
            if(isset($return_det))
            {
              return $return_det->do_number;
            }else{
              return '-';
            };
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function pick(Request $request, $id_header)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {

          $id_foo = str_replace("'","",$id);

          $sql = 'SELECT
                    sales_return_detail.id,
                    sales_return_detail.do_number,
                    inventory.id AS inventory_id,
                    inventory.inventory_code,
                    inventory.inventory_name,
                    sales_return_detail.quantity,
                    sales_return_detail.plant,
                    sales_return.spm,
                    sales_return_detail.store_loc,
                    sales_return_detail.item_line,
                    sales_return_detail.batch,
                    sales_return_detail.id AS id_detail,
                    warehouse.warehouse_name,
                    sales_return_detail.uom,
                    IFNULL(sales_return.warehouse_id,0) AS warehouse_id,
                    IFNULL(sales_return.room_id,0) AS room_id,
                    IFNULL(sales_return.bay_id,0) AS bay_id,
                    IFNULL(sales_return.rack_id,0) AS rack_id,
                    rack.rack_name,
                    bay.bay_name,
                    room.room_name,
                    sales_return.pallet_number
                  FROM
                    sales_return
                  INNER JOIN sales_return_detail ON sales_return.id = sales_return_detail.transaction_id
                  LEFT OUTER JOIN warehouse ON sales_return.warehouse_id = warehouse.id
                  LEFT OUTER JOIN inventory ON sales_return_detail.inventory_code = inventory.inventory_code
                  LEFT OUTER JOIN rack ON sales_return.rack_id = rack.id
                  LEFT OUTER JOIN bay ON sales_return.bay_id = bay.id
                  LEFT OUTER JOIN room ON sales_return.room_id = room.id';
          $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id_foo)->first();

          // dd($itemdata);

          $sql_batch = 'SELECT
                          delivery_order_detail.id as do_id,
                          sales_return_detail.id,
                          delivery_order_detail.batch
                        FROM
                          sales_return_detail
                        INNER JOIN delivery_order ON sales_return_detail.do_number = delivery_order.nodo
                        INNER JOIN delivery_order_detail ON delivery_order.id = delivery_order_detail.transaction_id
                        WHERE sales_return_detail.id = '.$id_foo.'';
          $data_batch = DB::table(DB::raw("($sql_batch) as rs_sql_batch"))->first();

          $post = SalesReturnDetail::Find($id_foo);
          if ($post)
            $post->status = 9;
            $post->save();

            $batch = Carbon::now()->format("dmY");

            $statement = new GRReturnDetail;
            $statement->transaction_id = $id_header;

            if(isset($data_batch))
            {
              $statement->batch = $data_batch->batch;
            };

            $statement->inventory_id = $itemdata->inventory_id;
            $statement->inventory_code = $itemdata->inventory_code;
            $statement->unit_of_measure_code = $itemdata->uom;
            $statement->do_number = $itemdata->do_number;
            $statement->item_line = $itemdata->item_line;
            $statement->quantity = $itemdata->quantity;
            $statement->plant = $itemdata->plant;
            $statement->store_loc = $itemdata->store_loc;
            $statement->warehouse_id = $itemdata->warehouse_id;
            $statement->room_id = $itemdata->room_id;
            $statement->bay_id = $itemdata->bay_id;
            $statement->rack_id = $itemdata->rack_id;
            $statement->pallet_number = $itemdata->pallet_number;
            $statement->batch = '00'.$batch;
            $statement->sales_return_detail_id = $itemdata->id;
            $statement->status = 0;
            $statement->save();

      }

      return response()->json([
          'status' => TRUE
      ]);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO gr_return (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(gr_return.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        gr_return WHERE gr_return.code_transaction = '".$code_transaction."' AND DATE_FORMAT(gr_return.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('gr_return')->max('id');

      $gr_return = GRReturn::where('id', $lastInsertedID)->first();
      $gr_return->status = 0;
      $gr_return->created_by = $userid;
      $gr_return->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'GR RETURN';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $gr_return->no_transaction,
                            'source' => 'WMS',
                            'gr_return_id' => $lastInsertedID
                        ]);
      $user_log->save();


      return redirect('main/warehouse/gr-return/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $gr_return = GRReturn::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $reason_list = Reason::all()->pluck('description', 'return_code');

      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      return view ('main.warehouse.gr_return.form', compact('gr_return', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list','inventory_list', 'uom_list', 'reason_list'));
    }

    public function update($id, Request $request)
    {
        $user_id = Auth::id();

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = GRReturn::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        $post->warehouse_id = $request->input('warehouse_id');
        $post->room_id = $request->input('room_id');
        $post->bay_id = $request->input('bay_id');
        $post->rack_id = $request->input('rack_id');
        $post->pallet_number = $request->input('pallet_number');
        $post->return_reason_code = $request->input('return_reason_code');
        // $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        $user_log = new UserLog;
        $user_log->user_id = $userid;
        $user_log->scope = 'GR RETURN';
        $user_log->data = json_encode([
                              'action' => 'update',
                              'transaction_number' => $post->no_transaction,
                              'description' => $request->input('description'),
                              'source' => 'WMS',
                              'goods_receive_id' => $id
                          ]);
        $user_log->save();

        return redirect()->action('Main\Warehouse\GRReturnController@index');
    }

    public function delete($id)
    {
      $post =  GRReturn::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = GRReturn::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
                gr_return_detail.id,
                gr_return.grnum,
                gr_return_detail.transaction_id,
                inventory.inventory_name AS inventory_name,
                RIGHT(inventory.inventory_code, 7) AS inventory_code,
                gr_return_detail.unit_of_measure_code AS inventory_unit_name,
                RIGHT(gr_return_detail.item_line, 2) AS item_line,
                gr_return_detail.do_number,
                gr_return_detail.inventory_id,
                gr_return_detail.quantity,
                gr_return_detail.plant,
                gr_return_detail.store_loc,
                warehouse.warehouse_name,
                IFNULL(gr_return_detail.warehouse_id,0) AS warehouse_id,
                IFNULL(gr_return_detail.room_id,0) AS room_id,
                IFNULL(gr_return_detail.bay_id,0) AS bay_id,
                IFNULL(gr_return_detail.rack_id,0) AS rack_id,
                IFNULL(gr_return_detail.batch,0) AS batch,
                rack.rack_name,
                bay.bay_name,
                room.room_name,
                gr_return_detail.pallet_number,
                gr_return_detail.`status`
              FROM
              gr_return
              INNER JOIN gr_return_detail ON gr_return.id = gr_return_detail.transaction_id
              LEFT JOIN inventory ON gr_return_detail.inventory_id = inventory.id
              LEFT OUTER JOIN warehouse ON gr_return_detail.warehouse_id = warehouse.id
              LEFT OUTER JOIN rack ON gr_return_detail.rack_id = rack.id
              LEFT OUTER JOIN bay ON gr_return_detail.bay_id = bay.id
              LEFT OUTER JOIN room ON gr_return_detail.room_id = room.id
              WHERE
              gr_return_detail.deleted_at IS NULL AND gr_return_detail.quantity > 0 AND gr_return_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        if(strlen($itemdata->grnum) > 0)
        {
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" disabled> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" disabled><i class="icon-trash"></i></a>';
        }else{
          return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs legitRipple" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs legitRipple" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
        }
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }

  public function post_response(Request $request, $id)
  {
    $user_id = Auth::id();

    $statement = GRReturn::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->pallet_number = $request->pallet_number;
    $statement->return_reason_code = $request->return_reason_code;
    $statement->trdat = $request->trdat;
    $statement->sonum = $request->sonum;
    $statement->donum = $request->donum;
    $statement->grnum = $request->grnum;
    $statement->mjahr = $request->mjahr;
    $statement->save();

    $user_log = new UserLog;
    $user_log->user_id = $userid;
    $user_log->scope = 'GR Return';
    $user_log->data = json_encode([
                          'action' => 'update',
                          'transaction_number' => $statement->no_transaction,
                          'description' => $request->description,
                          'source' => 'WMS',
                          'goods_receive_id' => $id
                      ]);
    $user_log->save();
  }


  public function post_detail(Request $request, $id)
  {

    $statement = new GRReturnDetail;
    $statement->transaction_id = $id;
    $statement->inventory_id = $request->inventory_id;
    $statement->inventory_code = $request->inventory_code;
    $statement->unit_of_measure_code = $request->unit_of_measure_code;
    $statement->do_number = $request->do_number;
    $statement->item_line = $request->item_line;
    $statement->quantity = $request->quantity;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->warehouse_id = $request->warehouse_id;
    $statement->room_id = $request->room_id;
    $statement->bay_id = $request->bay_id;
    $statement->rack_id = $request->rack_id;
    $statement->batch = $request->batch;
    $statement->pallet_number = $request->pallet_number;
    $statement->sales_return_detail_id = $request->sales_return_detail_id;
    $statement->status = 0;
    $statement->save();

    $receive = SalesReturnDetail::where('id', $request->sales_return_detail_id)->first();
    $receive->status = 9;
    $receive->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = GRReturnDetail::where('id', $id)->first();
    // $statement->inventory_id = $request->inventory_id;
    // $statement->unit_of_measure_code = $request->unit_of_measure_code;
    // $statement->do_number = $request->do_number;
    // $statement->item_line = $request->item_line;
    $statement->quantity = $request->quantity;
    $statement->plant = $request->plant;
    $statement->store_loc = $request->store_loc;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = GRReturnDetail::where('id', $id)->first();

    $open_pick = SalesReturnDetail::where('id', $statement->sales_return_detail_id)->first();
    $open_pick->status = 0;
    $open_pick->save();

    $statement->delete();
  }

  public function data_modal(Request $request)
  {

      $sql = 'SELECT
                sales_return_detail.id,
                sales_return_detail.do_number,
                inventory.id AS inventory_id,
                inventory.inventory_code,
                inventory.inventory_name,
                sales_return_detail.quantity,
                sales_return_detail.plant,
                sales_return.spm,
                sales_return_detail.store_loc,
                sales_return_detail.item_line,
                sales_return_detail.batch,
                sales_return_detail.id AS id_detail,
                warehouse.warehouse_name,
                IFNULL(sales_return.warehouse_id,0) AS warehouse_id,
                IFNULL(sales_return.room_id,0) AS room_id,
                IFNULL(sales_return.bay_id,0) AS bay_id,
                IFNULL(sales_return.rack_id,0) AS rack_id,
                rack.rack_name,
                bay.bay_name,
                room.room_name,
                sales_return.pallet_number
              FROM
                sales_return
              INNER JOIN sales_return_detail ON sales_return.id = sales_return_detail.transaction_id
              LEFT OUTER JOIN warehouse ON sales_return.warehouse_id = warehouse.id
              LEFT OUTER JOIN inventory ON sales_return_detail.inventory_code = inventory.inventory_code
              LEFT OUTER JOIN rack ON sales_return.rack_id = rack.id
              LEFT OUTER JOIN bay ON sales_return.bay_id = bay.id
              LEFT OUTER JOIN room ON sales_return.room_id = room.id
              WHERE sales_return_detail.status = 0 AND sales_return_detail.quantity > 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {
        if($request->input('code_transaction') == 'GPOD') {
            $itemdata->where('rs_sql.spm', 1);
        }else{
            $itemdata->where('rs_sql.spm', 0);
        }
        if($keyword = $request->input('keyword')) {
          $itemdata->whereRaw("CONCAT(IFNULL(do_number,''), IFNULL(inventory_code,''), IFNULL(rack_name,''), IFNULL(inventory_name,''), IFNULL(pallet_number,''), IFNULL(warehouse_name,''), IFNULL(room_name,'')) like ?", ["%{$keyword}%"]);
        }
    })

      ->addColumn('action', function ($itemdata) {
        $choose = '<a  type="button" class="btn btn-float btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->warehouse_id.', '.$itemdata->bay_id.', '.$itemdata->room_id.', '.$itemdata->rack_id.', '.$itemdata->inventory_id.', '.$itemdata->id_detail.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
        return ''.$choose.'';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->rawColumns(['action', 'check'])
      ->make(true);
  }

  public function data_detail(Request $request, $id)
  {
    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();
    $end_of_line = GRReturnDetail::where('transaction_id', $id)->max('item_line');

    // dd($end_of_line);

    $sql = 'SELECT
              gr_return.no_transaction AS grreturnwms,
              inventory.inventory_code AS matrial,
              gr_return_detail.do_number,
              gr_return_detail.item_line,
              SUM(gr_return_detail.quantity) AS qty,
              gr_return_detail.plant AS plant,
              gr_return_detail.store_loc AS storloc,
              gr_return.description AS note,
              warehouse.warehouse_name,
              IFNULL(
                gr_return_detail.warehouse_id,
                0
              ) AS warehouse_id,
              IFNULL(gr_return_detail.room_id, 0) AS room_id,
              IFNULL(gr_return_detail.bay_id, 0) AS bay_id,
              IFNULL(gr_return_detail.batch, 0) AS batch,
              bay.bay_name,
              room.room_name
            FROM
              gr_return
            INNER JOIN gr_return_detail ON gr_return.id = gr_return_detail.transaction_id
            INNER JOIN inventory ON gr_return_detail.inventory_code = inventory.inventory_code
            LEFT OUTER JOIN warehouse ON gr_return_detail.warehouse_id = warehouse.id
            LEFT OUTER JOIN rack ON gr_return_detail.rack_id = rack.id
            LEFT OUTER JOIN bay ON gr_return_detail.bay_id = bay.id
            LEFT OUTER JOIN room ON gr_return_detail.room_id = room.id
            WHERE
              gr_return_detail.deleted_at IS NULL
            AND gr_return_detail.transaction_id = '.$id.'
            GROUP BY
              gr_return.no_transaction,
              inventory.inventory_code,
              gr_return_detail.do_number,
              gr_return_detail.item_line,
              gr_return_detail.plant,
              gr_return_detail.store_loc,
              gr_return.description,
              warehouse.warehouse_name,
              IFNULL(
                gr_return_detail.warehouse_id,
                0
              ),
              IFNULL(gr_return_detail.room_id, 0),
              IFNULL(gr_return_detail.bay_id, 0),
              IFNULL(gr_return_detail.batch, 0),
              bay.bay_name,
              room.room_name
            ORDER BY
              gr_return_detail.item_line ASC';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                    transaction_id,
                    count(item_line) AS item_line
                  FROM
                    (
                      SELECT
                        gr_return_detail.transaction_id,
                        gr_return_detail.item_line
                      FROM
                        gr_return_detail
                      WHERE
                        gr_return_detail.deleted_at IS NULL
                      AND gr_return_detail.transaction_id = '.$id.'
                      GROUP BY
                        gr_return_detail.transaction_id,
                        gr_return_detail.item_line
                    ) AS derivdtbl
                  GROUP BY
                    transaction_id';
    $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      sleep(3);
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['grreturnwms'] = $datas->grreturnwms;
        $json[$h_key]['matrial'] = $datas->matrial;
        $json[$h_key]['qty'] = $datas->qty;
        $json[$h_key]['plant'] = $datas->plant;
        $json[$h_key]['storloc'] = $datas->storloc;
        $json[$h_key]['note'] = $datas->note;
        $json[$h_key]['do_number'] = $datas->do_number;
        $json[$h_key]['item_line'] = $datas->item_line;
        $json[$h_key]['createdname'] = $user->username;
        $json[$h_key]['total_items'] = $data_line->item_line;
        if($datas->item_line == $end_of_line)
        {
          $json[$h_key]['bendara'] = 'X';
        }else{
          $json[$h_key]['bendara'] = '';
        };
      }
    }
    return response()->json($json);
  }

  public function get_empty_nodo()
  {
    // $data = GRreturn::where('donum', '-')->orWhere('donum', null)->orWhere('donum', '')->where('code_transaction', 'GRRT')->get();
    $sql = 'SELECT
            gr_return.no_transaction,
            gr_return_detail.do_number
        FROM
            gr_return
        INNER JOIN gr_return_detail ON gr_return.id = gr_return_detail.transaction_id
        WHERE
            (gr_return.donum IS NULL
        OR gr_return.donum = ""
        OR gr_return.donum = "-") AND gr_return.code_transaction = "GRRT"
        GROUP BY
            gr_return.no_transaction,
            gr_return_detail.do_number';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    // dd($data);

    // dd($data);
    if(count($data)>0)
    {
      foreach ($data as $h_key => $datas) {
        $json[$h_key]['do_number'] = $datas->do_number;
        $json[$h_key]['no_transaction'] = $datas->no_transaction;
      }
    }else {
      $json[0]['do_number'] = '';
      $json[0]['no_transaction'] = '';
    }
    return response()->json($json);

  }

  public function get_data_sap(Request $request)
  {
      // $response = file_get_contents('webapp.lmu.co.id:3000/m/v1/wms/devtestambilreturndo');
      $statement = GRreturn::where('no_transaction', $request->no_transaction)->first();
      $statement->trdat = $request->trdat;
      $statement->sonum = $request->sonum;
      $statement->donum = $request->donum;
      $statement->grnum = $request->grnum;
      $statement->save();

  }

  public function cancel(Request $request, $id)
    {
      $statement = GRReturn::where('id', $id)->first();
      $statement->status = 1;
      $statement->save();

      $do_detail = GRReturnDetail::where('transaction_id', $id)->get();

      foreach($do_detail as $do_details)
      {
        $post = SalesReturnDetail::where('id', $do_details->sales_return_detail_id)->first();
        $post->status = 1;
        $post->save();
      }
    }


    public function sap_exe_tambah_item(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'grreturnwms' => $request->grreturnwms);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exe-tambah-item",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                		CURLOPT_TIMEOUT => 35,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
      		}
    }

    public function sap_tambahitem(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'grreturnwms' => $request->grreturnwms,
                  'matrial' => $request->matrial,
                  'qty' => $request->qty,
                  'plant' => $request->plant,
                  'storloc' => $request->storloc,
                  'note' => $request->note,
                  'createdname' => $request->createdname,
                  'itemnya' => $request->itemnya,
                  'total_items' => $request->total_items);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-tambahitem",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                		CURLOPT_TIMEOUT => 35,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
      		}
    }


    public function sap_returndo(Request $request)
    {
          $curl = curl_init();
          $data = array(
                  'grreturnwms' => $request->grreturnwms,
                  'nodo' => $request->nodo,
                  'itemdo' => $request->itemdo,
                  'matrial' => $request->matrial,
                  'qty' => $request->qty,
                  'orderreason' => $request->orderreason,
                  'total_items' => $request->total_items);

      		curl_setopt_array($curl, array(
                                		CURLOPT_URL => "https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-returndo",
                                		CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POSTFIELDS => http_build_query($data),
                                		CURLOPT_ENCODING => "",
                                		CURLOPT_MAXREDIRS => 10,
                                		CURLOPT_TIMEOUT => 35,
                                		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                		CURLOPT_CUSTOMREQUEST => "POST",
                              		));

      		$response = curl_exec($curl);
      		$err = curl_error($curl);

      		curl_close($curl);

      		if ($err) {
      		echo "cURL Error #:" . $err;
      		} else {
        		// echo $response;
            $json = json_decode($response);
            return response()->json($json);
      		}
    }
}
