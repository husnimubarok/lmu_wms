<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockTransfer; 
use App\Model\StockTransferDetail; 
use App\Model\Warehouse; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class StockTransferController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $stock_transfers = StockTransfer::all();
      return view ('main.warehouse.stock_transfer.index', compact('stock_transfers'));
    }

    public function data(Request $request)
    {    

        $itemdata = DB::table('stock_transfer')
            ->leftjoin('warehouse', 'stock_transfer.warehouse_id', '=', 'warehouse.id') 
            ->select('stock_transfer.*', 'warehouse.warehouse_name');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('stock_transfer.status', $id);
            }else{
                $itemdata->where('stock_transfer.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(stock_transfer.no_transaction, 'stock_transfer.warehouse_id') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="stock-transfer/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');  

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO stock_transfer (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_transfer.no_transaction, 3)),3))+1001,3)), CONCAT('".$code_transaction."','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','001')), DATE(NOW()), 0, '".$userid."', DATE(NOW()) 
        FROM
        stock_transfer WHERE stock_transfer.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_transfer.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('stock_transfer')->max('id'); 

      $stock_transfer = StockTransfer::where('id', $lastInsertedID)->first();  
      $stock_transfer->status = 0;     
      $stock_transfer->created_by = $userid;
      $stock_transfer->save();

      $user_log = new UserLog;  
      $user_log->user_id = $userid;     
      $user_log->scope = 'MATERIAL USED';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'stock_transfer_id' => $lastInsertedID 
                        ]);
      $user_log->save();
 

      return redirect('main/warehouse/stock-transfer/edit/'.$lastInsertedID.''); 
    }

    public function edit($id)
    {
      $stock_transfer = StockTransfer::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      return view ('main.warehouse.stock_transfer.form', compact('stock_transfer', 'warehouse_list','inventory_list', 'uom_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = StockTransfer::Find($id); 
        $post->date_transaction = $date_transaction_format; 
        $post->description = $request->input('description');  
        $post->warehouse_id = $request->input('warehouse_id');  
        // $post->status = $request->input('status'); 
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\StockTransferController@index'); 
    }

    public function delete($id)
    {
      $post =  StockTransfer::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = StockTransfer::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 
    }


  // detail
  public function get_detail(Request $request, $id)
  {  

   if($request->ajax()){ 

      $sql = 'SELECT
                stock_transfer_detail.id,
                stock_transfer_detail.transaction_id,
                inventory.inventory_name AS inventory_name,
                stock_transfer_detail.unit_of_measure_code AS inventory_unit_name,
                stock_transfer_detail.inventory_id, 
                stock_transfer_detail.quantity, 
                stock_transfer_detail.`status` 
              FROM
                stock_transfer_detail
              LEFT JOIN inventory ON stock_transfer_detail.inventory_id = inventory.id
              WHERE
              stock_transfer_detail.deleted_at IS NULL AND stock_transfer_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function post_detail(Request $request, $id)
  {   
    $statement = new StockTransferDetail;  
    $statement->transaction_id = $id;  
    $statement->inventory_id = $request->inventory_id; 
    $statement->unit_of_measure_code = $request->unit_of_measure_code;  
    $statement->quantity = $request->quantity; 
    $statement->status = 0;
    $statement->save(); 
  }

  public function put_detail(Request $request, $id)
  {   
    $statement = StockTransferDetail::where('id', $id)->first();  
    $statement->inventory_id = $request->inventory_id; 
    $statement->unit_of_measure_code = $request->unit_of_measure_code;  
    $statement->quantity = $request->quantity; 
    $statement->save(); 
  }

  public function delete_detail(Request $request, $id)
  {   
    $statement = StockTransferDetail::where('id', $id)->first();   
    $statement->delete(); 
  }
}
