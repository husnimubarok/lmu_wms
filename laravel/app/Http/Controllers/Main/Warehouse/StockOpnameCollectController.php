<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockOpnameCollectRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\Model\StockOpnameCollect;
use App\Model\StockOpnameCollectDetail;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Warehouse;
use App\Model\Room;
use App\Model\Bay;
use App\Model\Rack;
use App\Model\Pallet;
use App\Model\GLAccount;
use App\Model\CostCenter;
use App\User;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use RoleManager;

class StockOpnameCollectController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_opname_collect_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $stock_opname_collects = StockOpnameCollect::all();
      return view ('main.warehouse.stock_opname_collect.index', compact('stock_opname_collects'));
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('stock_opname_collect')
            ->select('stock_opname_collect.*');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('stock_opname_collect.status', $id);
            }else{
                $itemdata->where('stock_opname_collect.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(stock_opname_collect.no_transaction, 'stock_opname_collect.description') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          if (RoleManager::actionStart('stock_opname_collect', ['delete'])) {
            $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="stock-opname-collect/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          }else{
            $edit = '';
          };

          if (RoleManager::actionStart('stock_opname_collect', ['update'])) {
            $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
            $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';
          }else{
            $archieve = '';
            $reactive = '';
          };

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check'])
        ->make(true);
    }

    public function store(Request $request)
    {
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO stock_opname_collect (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(stock_opname_collect.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW())
        FROM
        stock_opname_collect WHERE stock_opname_collect.code_transaction = '".$code_transaction."' AND DATE_FORMAT(stock_opname_collect.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('stock_opname_collect')->max('id');

      $stock_opname_collect = StockOpnameCollect::where('id', $lastInsertedID)->first();
      $stock_opname_collect->status = 0;
      $stock_opname_collect->created_by = $userid;
      $stock_opname_collect->save();

      $user_log = new UserLog;
      $user_log->user_id = $userid;
      $user_log->scope = 'STOCK ADJUSTMENT';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $stock_opname_collect->no_transaction,
                            'source' => 'WMS',
                            'stock_opname_collect_id' => $lastInsertedID
                        ]);
      $user_log->save();

      return redirect('main/warehouse/stock-opname-collect/edit/'.$lastInsertedID.'');
    }

    public function edit($id)
    {
      $stock_opname_collect = StockOpnameCollect::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');
      $inventory_list = Inventory::all()->pluck('inventory_name', 'id');
      // $gl_account_list = GLAccount::all();
      $gl_account_list = DB::connection('mysql')->select('SELECT
                                                              account_code,
                                                              concat(
                                                                account_code,
                                                                " - ",
                                                                account_name
                                                              ) AS account_name
                                                          FROM
                                                              gl_account');
      $cost_center_list = CostCenter::all();
      // $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      $store_loc_list = DB::connection('mysql')->select('SELECT
                                                    store_location_code AS store_location_code,
                                                    store_location_code AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY LEFT(store_location_code,4)');

      $plant_list = DB::connection('mysql')->select('SELECT
                                                    plant AS store_location_code,
                                                    plant AS store_location_name
                                                  FROM
                                                    store_location
                                                  WHERE deleted_at IS NULL
                                                  GROUP BY plant');

      return view ('main.warehouse.stock_opname_collect.form', compact('stock_opname_collect', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list', 'inventory_list', 'uom_list', 'gl_account_list', 'cost_center_list', 'store_loc_list', 'plant_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = StockOpnameCollect::Find($id);
        $post->date_transaction = $date_transaction_format;
        $post->description = $request->input('description');
        // $post->status = $request->input('status');
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\StockOpnameCollectController@index');
    }

    public function delete($id)
    {
      $post =  StockOpnameCollect::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save();

      return response()->json($post);
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;
      foreach($ids as $key => $id) {
          $post = StockOpnameCollect::Find($id[1]);
          if ($post)
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save();
      }

      return response()->json([
          'status' => TRUE
      ]);
    }


  // detail
  public function get_detail(Request $request, $id)
  {

   if($request->ajax()){

      $sql = 'SELECT
              	stock_opname_collect_detail.id,
              	stock_opname_collect_detail.stock_opname_id,
              	stock_opname.no_transaction,
              	stock_opname.date_transaction,
              	stock_opname.description,
              	stock_opname.code_transaction
              FROM
              	stock_opname_collect_detail
              INNER JOIN stock_opname ON stock_opname_collect_detail.stock_opname_id = stock_opname.id
              WHERE
              	stock_opname_collect_detail.deleted_at IS NULL AND stock_opname_collect_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get();

      return Datatables::of($itemdata)

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Open Detail" class="btn btn-float btn-success btn-xs" onclick="openModalItem('."'".$itemdata->stock_opname_id."'".')"><i class="icon-menu6"></i></a> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->no_transaction."'".')"><i class="icon-trash"></i></a> ';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function data_modal(Request $request)
  {

      // $itemdata = DB::table('stock_opname')
      // ->select('stock_opname.*');
      $sql = 'SELECT
                *
              FROM
              stock_opname
              -- INNER JOIN () AS detail
              WHERE
                stock_opname.deleted_at IS NULL
              AND stock_opname.id NOT IN
                (
                  SELECT stock_opname_id FROM stock_opname_collect_detail WHERE stock_opname_collect_detail.deleted_at IS NULL
                )';

        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

      return Datatables::of($itemdata)


      ->addColumn('action', function ($itemdata) {
        $choose = '<a  type="button" class="btn btn-float btn-info btn-float btn-xs" href="#" onclick="saveDetail('.$itemdata->id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
        return ''.$choose.'';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->rawColumns(['action', 'check'])
      ->make(true);
  }


    // detail
    public function data_modal_item(Request $request)
    {

      if($request->ajax()){

        $sql = 'SELECT
                  stock_opname.id,
                  stock_opname.no_transaction AS notransaction,
                  RIGHT(stock_opname_detail.item_line, 2) AS item_number,
                  stock_opname_detail.plant,
                  stock_opname_detail.quantity,
                  stock_opname_detail.quantity_actual,
                  stock_opname_detail.quantity_adjustment,
                  stock_opname_detail.store_loc AS storage_loc,
                  stock_opname_detail.quantity AS po_qty,
                  stock_opname_detail.unit_of_measure_code AS po_unit,
                  RIGHT(stock_opname_detail.batch, 8) AS batch,
                  stock_opname_detail.store_loc,
                  stock_opname_detail.item_line,
                  stock_opname_detail.inventory_id,
                  stock_opname_detail.status,
                  stock_opname_detail.id AS id_detail,
                  RIGHT(inventory.inventory_code, 7) AS material_number,
                  inventory.inventory_code,
                  inventory.inventory_name,
                  stock_opname.description AS doc_header,
                  warehouse.warehouse_name,
                  room.room_name,
                  bay.bay_name,
                  rack.rack_name,
                  stock_opname_detail.pallet_number
                FROM
                  stock_opname_detail
                LEFT OUTER JOIN stock_opname ON stock_opname.id = stock_opname_detail.transaction_id
                LEFT OUTER JOIN inventory ON stock_opname_detail.inventory_id = inventory.id
                LEFT OUTER JOIN warehouse ON stock_opname_detail.warehouse_id = warehouse.id
                LEFT OUTER JOIN room ON stock_opname_detail.room_id = room.id
                LEFT OUTER JOIN bay ON stock_opname_detail.bay_id = bay.id
                LEFT OUTER JOIN rack ON stock_opname_detail.rack_id = rack.id
                WHERE
                stock_opname_detail.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"));

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
          if($id = $request->input('stock_opname_id')) {
              $itemdata->where('rs_sql.id', $id);
          }
          if($keyword = $request->input('keyword')) {
            $itemdata->whereRaw("CONCAT(rs_sql.no_transaction, 'rs_sql.pallet_number') like ?", ["%{$keyword}%"]);
          }
        })

        ->addColumn('action', function ($itemdata) {
          if($itemdata->status > 0){
            return '<a  href="javascript:void(0)" title="Restore" class="btn btn-float btn-warning btn-xs" onclick="restore('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-reload-alt"></i></a>';
          }else{
            return '<a  href="javascript:void(0)" title="Approve" class="btn btn-float btn-success btn-xs" onclick="approve('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-check"></i></a> <a  href="javascript:void(0)" title="Not Approve" class="btn btn-float btn-danger btn-xs" onclick="not_approve('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-close2"></i></a>';
          };
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0 || $itemdata->status == null) {
            return '<span class="label label-warning">Outstanding</span>';
          }else if ($itemdata->status == 1){
            return '<span class="label label-success">Approve</span>';
          }else if ($itemdata->status == 9){
            return '<span class="label label-danger">Not Approve</span>';
          };
        })

        ->rawColumns(['action', 'mstatus'])

        ->make(true);
        } else {
          exit("No data available");
        }
    }

  public function post_response(Request $request, $id)
  {
    $statement = StockOpnameCollect::where('id', $id)->first();
    $statement->nodoc = $request->nodoc;
    $statement->year = $request->year;
    $statement->save();
  }

  public function save_header(Request $request, $id)
  {

    $statement = StockOpnameCollect::where('id', $id)->first();
    $statement->description = $request->description;
    $statement->save();
  }

  public function post_detail(Request $request, $id)
  {
    $statement = new StockOpnameCollectDetail;
    $statement->transaction_id = $id;
    $statement->stock_opname_id = $request->stock_opname_id;
    $statement->status = 0;
    $statement->save();
  }

  public function put_detail(Request $request, $id)
  {
    $statement = StockOpnameCollectDetail::where('id', $id)->first();
    $statement->quantity_decreasing = $request->quantity_decreasing;
    $statement->account_code = $request->account_code;
    $statement->cost_center_code = $request->cost_center_code;
    $statement->status = 0;
    $statement->save();
  }

  public function delete_detail(Request $request, $id)
  {
    $statement = StockOpnameCollectDetail::where('id', $id)->first();
    $statement->delete();
  }


  public function cancel(Request $request, $id)
  {
    $statement = StockOpnameCollect::where('id', $id)->first();
    $statement->status = 1;
    $statement->save();
  }


  public function sap_so_new(Request $request, $id)
  {

    $user_id = Auth::id();
    $user = User::where('id', $user_id)->first();

    $sql = 'SELECT stock_opname_detail.id,
              stock_opname_collect.no_transaction AS notransaction,
              RIGHT(stock_opname_detail.item_line, 2) AS item_number,
              DATE_FORMAT(stock_opname.date_transaction, "%Y%m%d") AS date_transaction,
              stock_opname_detail.plant,
              stock_opname_detail.quantity,
              stock_opname_detail.quantity_actual,
              stock_opname_detail.quantity_adjustment,
              stock_opname_detail.store_loc AS storage_loc,
              stock_opname_detail.quantity AS po_qty,
              stock_opname_detail.unit_of_measure_code AS po_unit,
              RIGHT(stock_opname_detail.batch, 8) AS batch,
              stock_opname_detail.store_loc,
              stock_opname_detail.item_line,
              stock_opname_detail.inventory_id,
              stock_opname_detail.`status`,
              stock_opname_detail.id AS id_detail,
              RIGHT(inventory.inventory_code, 7) AS material_number,
              inventory.inventory_code,
              inventory.inventory_name,
              stock_opname.description AS doc_header,
              stock_opname_detail.pallet_number
            FROM stock_opname_detail LEFT OUTER JOIN stock_opname ON stock_opname.id = stock_opname_detail.transaction_id
            LEFT OUTER JOIN inventory ON stock_opname_detail.inventory_id = inventory.id
            INNER JOIN stock_opname_collect_detail ON stock_opname_detail.transaction_id = stock_opname_collect_detail.stock_opname_id
            INNER JOIN stock_opname_collect ON stock_opname_collect_detail.transaction_id = stock_opname_collect.id
            WHERE stock_opname_detail.deleted_at IS NULL AND stock_opname_collect_detail.transaction_id = '.$id.' AND stock_opname_collect_detail.deleted_at IS NULL AND stock_opname_detail.deleted_at IS NULL';
    $data = DB::table(DB::raw("($sql) as rs_sql"))->get();

    $sql_line = 'SELECT
                    transaction_id,
                    count(item_line) AS item_line
                  FROM
                    (
                      SELECT
                        stock_opname_detail.transaction_id,
                        stock_opname_detail.id AS item_line
                      FROM
                        stock_opname_detail
                      INNER JOIN stock_opname_collect_detail ON stock_opname_detail.transaction_id = stock_opname_collect_detail.stock_opname_id
                      WHERE
                        stock_opname_detail.deleted_at IS NULL
                      AND stock_opname_collect_detail.transaction_id = '.$id.'
                      GROUP BY
                        stock_opname_detail.transaction_id,
                        stock_opname_detail.id
                    ) AS derivdtbl
                  GROUP BY
                    transaction_id';
      $data_line = DB::table(DB::raw("($sql_line) as rs_sql_line"))->first();

    if(count($data)>0)
    {
      $i = 1;
      foreach ($data as $h_key => $datas) {

        $item_line = $i++;
        $prefix = '00';
        // $prefix_end = '0';

        $json[$h_key]['DOCWMS'] = $datas->notransaction;
        // $json[$h_key]['ITEM'] = strval($data_line->item_line);
        $json[$h_key]['ITEM'] = $prefix.$item_line;
        $json[$h_key]['ZLDAT'] = $datas->date_transaction;
        $json[$h_key]['BLDAT'] = $datas->date_transaction;
        $json[$h_key]['PLANT'] = strval($datas->plant);
        $json[$h_key]['LGORT'] = strval($datas->storage_loc);
        $json[$h_key]['MATNR'] = strval($datas->material_number);
        $json[$h_key]['CHARG'] = strval($datas->batch);
        $json[$h_key]['ERFMG'] = strval($datas->quantity_actual);
        if($datas->quantity_actual == 0){
          $json[$h_key]['XNULL'] = 'X';
        }else{
          $json[$h_key]['XNULL'] = '';
        };
        $json[$h_key]['LINES'] = strval($data_line->item_line);

      }
    };

    $data_json = array_values(
              $json
            );


    $curl = curl_init();

      // dd(json_encode($data_json));

      $curl = curl_init();

      curl_setopt_array($curl, array(
        // CURLOPT_URL => "https://7yyu6hhocc.execute-api.ap-southeast-1.amazonaws.com/wmsdev/sap-stockopname",
        CURLOPT_URL => "http://webapp.lmu.co.id:3002/m/v1/wmsdev/stockopname",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 3000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        // CURLOPT_POSTFIELDS => json_encode($data_json),
        CURLOPT_POSTFIELDS => '[
                                {
                                    "DOCWMS": "SOCL/II/20/00002",
                                    "ITEM": "001",
                                    "ZLDAT": "20200317",
                                    "BLDAT": "20200317",
                                    "PLANT": "1001",
                                    "LGORT": "0008",
                                    "MATNR": "000000000001100080",
                                    "CHARG": "20180317",
                                    "ERFMG": "1223",
                                    "XNULL": "",
                                    "LINES": "2"
                                },
                                {
                                    "DOCWMS": "SOCL/II/20/00002",
                                    "ITEM": "002",
                                    "ZLDAT": "20200317",
                                    "BLDAT": "20200317",
                                    "PLANT": "1001",
                                    "LGORT": "0008",
                                    "MATNR": "000000000001100131",
                                    "CHARG": "20180317",
                                    "ERFMG": "1335",
                                    "XNULL": "",
                                    "LINES": "2"
                                }
                            ]',
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // print_r(json_decode($response));
        $json = json_decode($response);
        return response()->json($json);
      }
  }
}
