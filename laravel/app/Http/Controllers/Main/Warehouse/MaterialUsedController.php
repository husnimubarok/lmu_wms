<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialUsedRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\MaterialUsed; 
use App\Model\MaterialUsedDetail; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\Warehouse; 
use App\Model\Room; 
use App\Model\Bay; 
use App\Model\Rack; 
use App\Model\Pallet; 

use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class MaterialUsedController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'material_used_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $material_useds = MaterialUsed::all();
      return view ('main.warehouse.material_used.index', compact('material_useds'));
    }

    public function data(Request $request)
    {    

        $itemdata = DB::table('material_used')
            ->leftjoin('warehouse', 'material_used.warehouse_id', '=', 'warehouse.id') 
            ->leftjoin('room', 'material_used.room_id', '=', 'room.id') 
            ->leftjoin('rack', 'material_used.rack_id', '=', 'rack.id')  
            ->leftjoin('bay', 'material_used.bay_id', '=', 'bay.id')  
            ->select('material_used.*', 'warehouse.warehouse_name', 'room.room_name', 'rack.rack_name', 'bay.bay_name');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('material_used.status', $id);
            }else{
                $itemdata->where('material_used.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(material_used.no_transaction, 'material_used.warehouse_id') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a type="button" class="btn btn-info btn-float btn-xs" href="material-used/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a>';
          $archieve = '<a type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->no_transaction."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          } 
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function store(Request $request)
    { 
      $userid= Auth::id();
      $code_transaction = $request->input('code_transaction');  

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO material_used (code_transaction, no_transaction, date_transaction, status, created_by, created_at)
        SELECT '".$code_transaction."', IFNULL(CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(material_used.no_transaction, 5)),5))+100001,5)), CONCAT('".$code_transaction."','/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','00001')), DATE(NOW()), 0, '".$userid."', DATE(NOW()) 
        FROM
        material_used WHERE material_used.code_transaction = '".$code_transaction."' AND DATE_FORMAT(material_used.date_transaction, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('material_used')->max('id'); 

      $material_used = MaterialUsed::where('id', $lastInsertedID)->first();  
      $material_used->status = 0;     
      $material_used->created_by = $userid;
      $material_used->save();

      $user_log = new UserLog;  
      $user_log->user_id = $userid;     
      $user_log->scope = 'MATERIAL USED';
      $user_log->data = json_encode([
                            'action' => 'create',
                            'transaction_number' => $material_used->no_transaction,
                            'source' => 'WMS',
                            'material_used_id' => $lastInsertedID 
                        ]);
      $user_log->save();

      return redirect('main/warehouse/material-used/edit/'.$lastInsertedID.''); 
    }

    public function edit($id)
    {
      $material_used = MaterialUsed::Find($id);
      $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id');
      $room_list = Room::all()->pluck('room_name', 'id');
      $bay_list = Bay::all()->pluck('bay_name', 'id');
      $rack_list = Rack::all()->pluck('rack_name', 'id');
      $pallet_list = Pallet::all()->pluck('pallet_number', 'pallet_number');

      $inventory_list = Inventory::all();
      $uom_list = UnitOfMeasure::all();

      return view ('main.warehouse.material_used.form', compact('material_used', 'warehouse_list', 'room_list', 'bay_list', 'rack_list', 'pallet_list','inventory_list', 'uom_list'));
    }

    public function update($id, Request $request)
    {

        $date_transaction_array = explode("-",$request->input('date_transaction')); // split the array
        $var_day_transaction = $date_transaction_array[0]; //day seqment
        $var_month_transaction = $date_transaction_array[1]; //month segment
        $var_year_transaction = $date_transaction_array[2]; //year segment
        $date_transaction_format = "$var_year_transaction-$var_month_transaction-$var_day_transaction"; // join them together

        $post = MaterialUsed::Find($id); 
        $post->date_transaction = $date_transaction_format; 
        $post->description = $request->input('description');  
        $post->warehouse_id = $request->input('warehouse_id');  
        $post->room_id = $request->input('room_id');  
        $post->bay_id = $request->input('bay_id');  
        $post->rack_id = $request->input('rack_id');  
        $post->pallet_number = $request->input('pallet_number');  
        // $post->status = $request->input('status'); 
        $post->updated_by = Auth::id();
        $post->save();

        return redirect()->action('Main\Warehouse\MaterialUsedController@index'); 
    }

    public function delete($id)
    {
      $post =  MaterialUsed::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = MaterialUsed::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 
    }


  // detail
  public function get_detail(Request $request, $id)
  {  

   if($request->ajax()){ 

      $sql = 'SELECT
                material_used_detail.id,
                material_used_detail.transaction_id,
                inventory.inventory_name AS inventory_name,
                material_used_detail.unit_of_measure_code AS inventory_unit_name,
                material_used_detail.inventory_id, 
                material_used_detail.quantity, 
                material_used_detail.batch, 
                material_used_detail.`status` 
              FROM
                material_used_detail
              LEFT JOIN inventory ON material_used_detail.inventory_id = inventory.id
              WHERE
              material_used_detail.deleted_at IS NULL AND material_used_detail.transaction_id = '.$id.'';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Edit" class="btn btn-float btn-primary btn-xs" onclick="editDetail(this, '."'".$itemdata->id."', '".$itemdata->inventory_id."'".')"> <i class="icon-pencil7"></i> <a  href="javascript:void(0)" title="Delete" class="btn btn-float btn-danger btn-xs" onclick="delete_detail('."'".$itemdata->id."', '".$itemdata->inventory_name."'".')"><i class="icon-trash"></i></a>';
      })

      ->make(true);
      } else {
        exit("No data available");
      }
  }


  public function post_detail(Request $request, $id)
  {   
    $statement = new MaterialUsedDetail;  
    $statement->transaction_id = $id;  
    $statement->inventory_id = $request->inventory_id; 
    $statement->unit_of_measure_code = $request->unit_of_measure_code;  
    $statement->batch = $request->batch; 
    $statement->quantity = $request->quantity; 
    $statement->status = 0;
    $statement->save(); 
  }

  public function put_detail(Request $request, $id)
  {   
    $statement = MaterialUsedDetail::where('id', $id)->first();  
    $statement->inventory_id = $request->inventory_id; 
    $statement->unit_of_measure_code = $request->unit_of_measure_code;  
    $statement->batch = $request->batch; 
    $statement->quantity = $request->quantity; 
    $statement->save(); 
  }

  public function delete_detail(Request $request, $id)
  {   
    $statement = MaterialUsedDetail::where('id', $id)->first();   
    $statement->delete(); 
  }
}
