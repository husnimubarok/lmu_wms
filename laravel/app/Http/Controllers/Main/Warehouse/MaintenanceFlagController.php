<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;

use App\Model\InventoryReceive;
use App\Model\StockMovement;
use App\Model\StockMovementDetail;
use App\Model\Warehouse;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use App\Model\StockFlag;
use App\Model\Stock;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class MaintenanceFlagController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      return view ('main.warehouse.maintenance_flag.index');
    }

    public function data(Request $request)
    {

        $itemdata = DB::table('stock_flag')
            ->leftjoin('warehouse', 'stock_flag.warehouse_id', '=', 'warehouse.id')
            ->leftjoin('room', 'stock_flag.room_id', '=', 'room.id')
            ->leftjoin('rack', 'stock_flag.rack_id', '=', 'rack.id')
            ->leftjoin('bay', 'stock_flag.bay_id', '=', 'bay.id')
            ->leftjoin('inventory', 'stock_flag.inventory_id', '=', 'inventory.id')
            ->where('stock_flag.quantity', '>', '0')
            ->select('stock_flag.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        return Datatables::of($itemdata)

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('stock_flag.status', $id);
            }else{
                $itemdata->where('stock_flag.status', 0);
            }
            if($keyword = $request->input('keyword')) {
              $itemdata->whereRaw("CONCAT(ifnull(stock_flag.pallet_number, ''), ifnull(stock_flag.transaction_number,''), ifnull(warehouse.warehouse_name,''), ifnull(stock_flag.source,''), ifnull(inventory.inventory_name,''), ifnull(inventory.inventory_code,''), ifnull(room.room_name,''), ifnull(rack.rack_name,'')) like ?", ["%{$keyword}%"]);

            }
        })

        ->addColumn('action', function ($itemdata) {
          if($itemdata->source == 'receive' || $itemdata->source == 'movement')
          {
            $choose = '<a  type="button" class="btn btn-danger btn-float btn-xs" href="#" onclick="DeleteTr(this)"; return false;" title="Delete"> <i class="icon-trash"></i></a>';
          }else{
            $choose = '<a  type="button" class="btn btn-danger btn-float btn-xs" href="#" onclick="DeleteSt(this)" return false;" title="Delete"> <i class="icon-trash"></i></a>';
          };
          return ''.$choose.'';
        })

        ->rawColumns(['action'])
        ->make(true);
    }

    public function add_data(Request $request)
    {
      $stock = Stock::where('pallet_number', $request->pallet_number)->where('warehouse_id', $request->warehouse_id)->first();

      // dd($stock);

      $stock_flag = StockFlag::where('pallet_number', $stock->pallet_number)->first();

      if(isset($stock))
      {
        if(!isset($stock_flag))
        {
          $post_stock = new StockFlag;
          $post_stock->source = 'stock';
          $post_stock->transaction_number = "-";
          $post_stock->pallet_number = $stock->pallet_number;
          $post_stock->warehouse_id = $stock->warehouse_id;
          $post_stock->inventory_id = $stock->inventory_id;
          $post_stock->rack_id = $stock->rack_id;
          $post_stock->room_id = $stock->room_id;
          $post_stock->bay_id = $stock->bay_id;
          $post_stock->batch = $stock->batch;
          $post_stock->unit = $stock->unit;
          $post_stock->quantity = $stock->quantity;
          $post_stock->save();
        };
      }

    }
    public function get_transaction_data()
    {

      $delete_receive = StockFlag::where('source', 'receive')->delete();
      $delete_movement = StockFlag::where('source', 'movement')->delete();

      $sql_receive = "SELECT
                        goods_receive_detail.inventory_id,
                        goods_receive_detail.unit_of_measure_code,
                        goods_receive_detail.pallet_number,
                        ifnull(
                          `goods_receive_detail`.`warehouse_id`,
                          0
                        ) AS warehouse_id,
                        ifnull(
                          `goods_receive_detail`.`bay_id`,
                          0
                        ) AS bay_id,
                        ifnull(
                          `goods_receive_detail`.`room_id`,
                          0
                        ) AS room_id,
                        ifnull(
                          `goods_receive_detail`.`rack_id`,
                          0
                        ) AS rack_id,
                        goods_receive_detail.quantity,
                        goods_receive_detail.batch,
                        goods_receive.no_transaction,
                        goods_receive.date_transaction,
                        'GOODS RECEIVE' AS type_transaction,
                        goods_receive_detail.inventory_receive_detail_id,
                        inventory_receive_detail.transaction_id,
                        inventory_receive.branch
                      FROM
                        goods_receive
                      INNER JOIN goods_receive_detail ON goods_receive.id = goods_receive_detail.transaction_id
                      INNER JOIN inventory_receive_detail ON goods_receive_detail.inventory_receive_detail_id = inventory_receive_detail.id
                      INNER JOIN inventory_receive ON inventory_receive_detail.transaction_id = inventory_receive.id
                      WHERE
                        (
                          (`goods_receive`.`status` = 0)
                          AND (
                            `goods_receive`.`updated_at` IS NOT NULL
                          )
                          AND isnull(
                            `goods_receive_detail`.`deleted_at`
                          )
                        )";
      $flag_receive = DB::table(DB::raw("($sql_receive) as rs_sql_receive"))->get();

      foreach($flag_receive as $flag_receives)
      {
        if($flag_receives->branch == 1)
        {
          $post_receive = new StockFlag;
          $post_receive->source = 'receive';
          $post_receive->transaction_number = $flag_receives->no_transaction;
          $post_receive->pallet_number = $flag_receives->pallet_number;
          $post_receive->warehouse_id = $flag_receives->warehouse_id;
          $post_receive->inventory_id = $flag_receives->inventory_id;
          $post_receive->rack_id = $flag_receives->rack_id;
          $post_receive->room_id = $flag_receives->room_id;
          $post_receive->bay_id = $flag_receives->bay_id;
          $post_receive->batch = $flag_receives->batch;
          $post_receive->unit = $flag_receives->unit_of_measure_code;
          $post_receive->quantity = $flag_receives->quantity;
          $post_receive->save();
        }
      }


      $sql_movement = "SELECT
                          stock_movement_detail.inventory_id,
                          stock_movement_detail.unit_of_measure_code,
                          stock_movement_detail.pallet_number_to AS pallet_number,
                          ifnull(
                            `stock_movement_detail`.`warehouse_id_to`,
                            0
                          ) AS warehouse_id,
                          ifnull(
                            `stock_movement_detail`.`bay_id_to`,
                            0
                          ) AS bay_id,
                          ifnull(
                            `stock_movement_detail`.`room_id_to`,
                            0
                          ) AS room_id,
                          ifnull(
                            `stock_movement_detail`.`rack_id_to`,
                            0
                          ) AS rack_id,
                          stock_movement_detail.quantity,
                          stock_movement_detail.batch,
                          stock_movement.no_transaction,
                          stock_movement.transaction_date AS date_transaction,
                          'STOCK MOVEMENT' AS type_transaction
                        FROM
                          stock_movement
                        INNER JOIN stock_movement_detail ON stock_movement.id = stock_movement_detail.transaction_id
                        WHERE
                          stock_movement_detail.flag = 1";
      $flag_movement = DB::table(DB::raw("($sql_movement) as rs_sql_movement"))->get();

      foreach($flag_movement as $flag_movements)
      {
        $post_movement = new StockFlag;
        $post_movement->source = 'movement';
        $post_movement->transaction_number = $flag_movements->no_transaction;
        $post_movement->pallet_number = $flag_movements->pallet_number;
        $post_movement->inventory_id = $flag_movements->inventory_id;
        $post_movement->warehouse_id = $flag_movements->warehouse_id;
        $post_movement->rack_id = $flag_movements->rack_id;
        $post_movement->room_id = $flag_movements->room_id;
        $post_movement->bay_id = $flag_movements->bay_id;
        $post_movement->batch = $flag_movements->batch;
        $post_movement->unit = $flag_movements->unit_of_measure_code;
        $post_movement->quantity = $flag_movements->quantity;
        $post_movement->save();
      }

    }

  public function data_modal(Request $request)
  {

      $itemdata = DB::table('stock')
          ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id')
          ->leftjoin('room', 'stock.room_id', '=', 'room.id')
          ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')
          ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')
          ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')
          ->where('stock.quantity', '>', '0')
          ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

      return Datatables::of($itemdata)

      ->filter(function ($itemdata) use ($request) {
          if($id = $request->input('filter_status')) {
              $itemdata->where('stock.status', $id);
          }else{
              $itemdata->where('stock.status', 0);
          }
          if($keyword = $request->input('keyword')) {

            $itemdata->whereRaw("CONCAT(ifnull(stock.pallet_number, ''), ifnull(warehouse.warehouse_name,''), ifnull(inventory.inventory_name,''), ifnull(inventory.inventory_code,''), ifnull(room.room_name,''), ifnull(rack.rack_name,'')) like ?", ["%{$keyword}%"]);

          }
      })

      ->addColumn('action', function ($itemdata) {
        $pallet_number = $itemdata->pallet_number;
        // dd($pallet_number);
        $choose = '<a  type="button" class="btn btn-float btn-info btn-float btn-xs" href="#" onclick="addValue(this, '.$itemdata->warehouse_id.'); return false;" title="Edit"> <i class="icon-touch"></i> </a>';
        return ''.$choose.'';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->rawColumns(['action', 'check'])
      ->make(true);
  }

  public function delete($id)
  {
    $post =  StockFlag::where('pallet_number', $id)->first();
    $post->delete();

    return response()->json($post);
  }


  public function delete_tr(Request $request)
  {

    $post =  StockFlag::where('transaction_number', $request->transaction_number)->first();

    if($post->source == 'receive')
    {
      $receive = InventoryReceive::where('pallet_number', $post->pallet_number)->first();
      $receive->branch = 0;
      $receive->save();
    };

    if($post->source == 'movement')
    {
      $movement = StockMovementDetail::where('pallet_number_to', $post->pallet_number)->first();
      $movement->flag = 0;
      $movement->save();
    };

    $post->delete();

    return response()->json($post);
  }

}
