<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockTransfer;
use App\Model\StockTransferDetail;
use App\Model\Warehouse;
use App\Model\Inventory;
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class ReportStockContainerController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      $stock_transfers = StockTransfer::all();
      return view ('main.warehouse.report_stock_container.index', compact('stock_transfers'));
    }

    public function data(Request $request)
    {

        // $itemdata = DB::table('stock')
        //     ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id')
        //     ->leftjoin('room', 'stock.room_id', '=', 'room.id')
        //     ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')
        //     ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')
        //     ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')
        //     ->where('stock.quantity', '>', '0')
        //     ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        $sql_header = 'SELECT
                        	`stock`.*, `warehouse`.`warehouse_name`,
                        	`room`.`room_name`,
                        	`bay`.`bay_name`,
                        	`rack`.`rack_name`,
                        	`inventory`.`inventory_code`,
                        	`inventory`.`inventory_name`,
                          `warehouse`.store_location,
                        	movement_flag.pallet_flag,
                          movement_flag.desc_flag
                        FROM
                        	`stock`
                        LEFT JOIN `warehouse` ON `stock`.`warehouse_id` = `warehouse`.`id`
                        LEFT JOIN `room` ON `stock`.`room_id` = `room`.`id`
                        LEFT JOIN `rack` ON `stock`.`rack_id` = `rack`.`id`
                        LEFT JOIN `bay` ON `stock`.`bay_id` = `bay`.`id`
                        LEFT JOIN `inventory` ON `stock`.`inventory_id` = `inventory`.`id`
                        LEFT JOIN (
                        	SELECT
                        		stock_movement_detail.pallet_number_to AS pallet_flag,
                            "Movement" AS desc_flag
                        	FROM
                        		stock_movement_detail
                        	WHERE
                        		stock_movement_detail.flag = 1
                        	GROUP BY
                        		stock_movement_detail.pallet_number_to
                          UNION ALL
                          SELECT
                          	goods_receive_detail.pallet_number AS pallet_flag,
                          	"Branch" AS desc_flag
                          FROM
                          	goods_receive_detail
                          INNER JOIN inventory_receive ON goods_receive_detail.pallet_number = inventory_receive.pallet_number
                          WHERE
                          	inventory_receive.branch = 1
                          GROUP BY
                          	goods_receive_detail.pallet_number
                          UNION ALL
                          SELECT
                            stock_flag.pallet_number AS pallet_flag,
                            "Flag Manual" AS desc_flag
                          FROM
                            stock_flag
                          WHERE
                            stock_flag.source = "stock"
                          GROUP BY
                            stock_flag.pallet_number
                        ) AS movement_flag ON stock.pallet_number = movement_flag.pallet_flag
                        LEFT JOIN (
                          SELECT
                          	purchase_order.container_no,
                          	goods_receive_detail.pallet_number,
                          	goods_receive_detail.inventory_id
                          FROM
                          	goods_receive_detail
                          INNER JOIN purchase_order ON goods_receive_detail.po_number = purchase_order.po_number
                          GROUP BY
                          	purchase_order.container_no,
                          	goods_receive_detail.pallet_number,
                          	goods_receive_detail.inventory_id
                        ) AS container_no ON stock.pallet_number = container_no.pallet_number AND stock.inventory_id = container_no.inventory_id
                        WHERE
                        SUM(stock) <> 0
                        AND `stock`.`status` = 0
                        ORDER BY
                        	`bay_name` ASC';
        $itemdata = DB::table(DB::raw("(" . $sql_header . ") as rs_sql"));

        return Datatables::of($itemdata)

        ->addColumn('mstatus', function ($itemdata) {
          if(isset($itemdata->pallet_flag)) {
            if($itemdata->desc_flag == 'Movement'){
                return '<span class="label label-success">'.$itemdata->desc_flag.'</span>';
            }elseif($itemdata->desc_flag == 'Branch'){
                return '<span class="label label-danger">'.$itemdata->desc_flag.'</span>';
            }elseif($itemdata->desc_flag == 'Flag Manual'){
                return '<span class="label label-warning">'.$itemdata->desc_flag.'</span>';
            }elseif($itemdata->store_location == 'V003'){
                return '<span class="label label-primary">SBY</span>';
            }
          }
        })

        ->filter(function ($itemdata) use ($request) {
            if($id = $request->input('filter_status')) {
                $itemdata->where('rs_sql.status', $id);
            }else{
                $itemdata->where('rs_sql.status', 0);
            }
            if($keyword = $request->input('keyword')) {

              $itemdata->whereRaw("CONCAT(rs_sql.pallet_number, rs_sql.inventory_name, rs_sql.inventory_code, rs_sql.id) like ?", ["%{$keyword}%"]);

            }
        })

        ->rawColumns(['mstatus'])
        ->make(true);
    }

     // ====================================================STOCK MOVEMENT===============================================
     public function index_movement()
     {
       return view ('main.warehouse.report_stock_container.index_movement');
     }

     public function data_movement(Request $request)
     {

        //  $itemdata = DB::table('stock')
        //      ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id')
        //      ->leftjoin('room', 'stock.room_id', '=', 'room.id')
        //      ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')
        //      ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')
        //      ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')
        //      ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        //      dd($itemdata);
          $sql = 'SELECT
                    DRVDTBL.inventory_id,
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name,
                    DRVDTBL.unit,
                    DRVDTBL.batch,
                    container_no.container_no,
                    DRVDTBL.pallet_flag,
                    DRVDTBL.desc_flag,   
                    ifnull(stock_picking.quantity,0) AS stock_booking,
                    SUM(posting_transfer) AS posting_transfer,
                    SUM(stock_adjustment) AS stock_adjustment,
                    SUM(stock_susut) AS stock_susut,
                    SUM(goods_receive) AS goods_receive,
                    SUM(picking) AS picking,
                    SUM(gr_return) AS gr_return,
                    SUM(stock_movement) AS stock_movement,
                    SUM(stock_opname) AS stock_opname,
                    SUM(stock) AS stock,
                    SUM(stock) - ifnull(stock_picking.quantity,0) AS stock_remain,
                      0 AS nol
                  FROM
                    (
                      SELECT
                        stock_log.inventory_id,
                        stock_log.warehouse_id,
                        warehouse.warehouse_name,
                        stock_log.room_id,
                        room.room_name,
                        stock_log.bay_id,
                        bay.bay_name,
                        stock_log.rack_id,
                        rack.rack_name,
                        inventory.inventory_code,
                        inventory.inventory_name,
                        stock_log.unit,
                        stock_log.pallet_number,
                        stock_log.batch,
                        CASE
                      WHEN stock_log.type_transaction = "POSTING TRANSFER" THEN
                        stock_log.quantity
                      ELSE
                        0
                      END AS posting_transfer,
                        CASE
                      WHEN stock_log.type_transaction = "STOCK ADJUSMENT" THEN
                        stock_log.quantity
                      ELSE
                        0
                      END AS stock_adjustment,
                        CASE
                      WHEN stock_log.type_transaction = "STOCK SUSUT" THEN
                        stock_log.quantity
                      ELSE
                        0
                      END AS stock_susut,
                        CASE
                      WHEN stock_log.type_transaction = "GOODS RECEIVE" THEN
                        stock_log.quantity
                      ELSE
                        0
                      END AS goods_receive,
                      CASE
                    WHEN stock_log.type_transaction = "GOODS ISSUE" THEN
                      stock_log.quantity
                    ELSE
                      0
                    END AS picking,
                    CASE
                  WHEN stock_log.type_transaction = "GR RETURN" THEN
                    stock_log.quantity
                  ELSE
                    0
                  END AS gr_return,
                  CASE
                  WHEN stock_log.type_transaction = "STOCK MOVEMENT" THEN
                    stock_log.quantity
                  ELSE
                    0
                  END AS stock_movement,
                  CASE
                  WHEN stock_log.type_transaction = "STOCK OPNAME" THEN
                    stock_log.quantity
                  ELSE
                    0
                  END AS stock_opname,
                  stock_log.quantity AS stock,
                  movement_flag.pallet_flag,
                  movement_flag.desc_flag
                  FROM
                    stock_log
                  LEFT OUTER JOIN inventory ON stock_log.inventory_id = inventory.id
                  LEFT OUTER JOIN rack ON stock_log.rack_id = rack.id
                  LEFT OUTER JOIN warehouse ON stock_log.warehouse_id = warehouse.id
                  LEFT OUTER JOIN room ON stock_log.room_id = room.id
                  LEFT OUTER JOIN bay ON stock_log.bay_id = bay.id
                  LEFT JOIN (
                    SELECT
                      stock_movement_detail.pallet_number_to AS pallet_flag,
                      "Movement" AS desc_flag
                    FROM
                      stock_movement_detail
                    WHERE
                      stock_movement_detail.flag = 1
                    GROUP BY
                      stock_movement_detail.pallet_number_to
                    UNION ALL
                    SELECT
                      goods_receive_detail.pallet_number AS pallet_flag,
                      "Branch" AS desc_flag
                    FROM
                      goods_receive_detail
                    INNER JOIN inventory_receive ON goods_receive_detail.pallet_number = inventory_receive.pallet_number
                    WHERE
                      inventory_receive.branch = 1
                    GROUP BY
                      goods_receive_detail.pallet_number
                  ) AS movement_flag ON stock_log.pallet_number = movement_flag.pallet_flag
                    ) AS DRVDTBL
                  LEFT JOIN (SELECT
                                picking_detail.pallet_number,
                              	picking_detail.inventory_id,
                              	sum(picking_detail.pick_qty) AS quantity
                              FROM
                              	picking_detail
                              WHERE
                              	picking_detail.deleted_at IS NULL AND picking_detail.status = 0
                              GROUP BY
                              	picking_detail.pallet_number,
                                picking_detail.inventory_id) AS stock_picking ON DRVDTBL.pallet_number = stock_picking.pallet_number AND DRVDTBL.inventory_id = stock_picking.inventory_id
                  LEFT JOIN (SELECT
                                purchase_order.container_no,
                                goods_receive_detail.pallet_number,
                                goods_receive_detail.inventory_id
                              FROM
                                goods_receive_detail
                              INNER JOIN purchase_order ON goods_receive_detail.po_number = purchase_order.po_number
                              GROUP BY
                                purchase_order.container_no,
                                goods_receive_detail.pallet_number,
                                goods_receive_detail.inventory_id
                            ) AS container_no ON DRVDTBL.pallet_number = container_no.pallet_number AND DRVDTBL.inventory_id = container_no.inventory_id
                  GROUP BY
                    DRVDTBL.inventory_id,
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.batch,
                    DRVDTBL.unit,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name,
                    DRVDTBL.pallet_flag,
                    DRVDTBL.desc_flag,    
                    ifnull(stock_picking.quantity,0),
                    container_no.container_no
                    HAVING
                    SUM(stock) <> 0';
          $itemdata = DB::table(DB::raw("(" . $sql . ") as rs_sql"));

         return Datatables::of($itemdata)

         ->filter(function ($itemdata) use ($request) {
             if($keyword = $request->input('keyword')) {
               $itemdata->whereRaw("CONCAT(pallet_number, 'warehouse_name') like ?", ["%{$keyword}%"]);
             }
         })

         ->addColumn('mstatus', function ($itemdata) {
           if(isset($itemdata->pallet_flag)) {
             if($itemdata->desc_flag == 'Movement'){
                 return '<span class="label label-success">'.$itemdata->desc_flag.'</span>';
             }elseif($itemdata->desc_flag == 'Branch'){
                 return '<span class="label label-danger">'.$itemdata->desc_flag.'</span>';
             }elseif($itemdata->store_location == 'V003'){
                 return '<span class="label label-primary">SBY</span>';
             }
           }
         })

         ->filter(function ($itemdata) use ($request) {
             if($keyword = $request->input('keyword')) {

               $itemdata->whereRaw("CONCAT(rs_sql.pallet_number, rs_sql.inventory_name, rs_sql.inventory_code, rs_sql.batch) like ?", ["%{$keyword}%"]);

             }
         })

         ->addColumn('action', function ($itemdata) {
          return '0';
        })
        ->rawColumns(['action', 'check', 'mstatus'])
         ->make(true);
     }


     // ====================================================STOCK LEDGER===============================================
     public function index_ledger()
     {
       return view ('main.warehouse.report_stock.index_ledger');
     }

     public function data_ledger(Request $request)
     {

         $itemdata = DB::table('stock_log')
             ->leftjoin('warehouse', 'stock_log.warehouse_id', '=', 'warehouse.id')
             ->leftjoin('room', 'stock_log.room_id', '=', 'room.id')
             ->leftjoin('rack', 'stock_log.rack_id', '=', 'rack.id')
             ->leftjoin('bay', 'stock_log.bay_id', '=', 'bay.id')
             ->leftjoin('inventory', 'stock_log.inventory_id', '=', 'inventory.id')
             ->select('stock_log.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

         return Datatables::of($itemdata)

         ->filter(function ($itemdata) use ($request) {

            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($request->input('date_from') != '' || $request->input('date_to') != '')
            {
              $itemdata->whereBetween('stock_log.date_transaction', [$date_from_format, $date_to_format]);
              // $itemdata->where('goods_receive.status', 0);
            }else{
              $itemdata->where('stock_log.status', 0);
            };
        })

        ->addColumn('action', function ($itemdata) {
          return '0';
        })
        ->rawColumns(['action', 'check'])
         ->make(true);
     }

}
