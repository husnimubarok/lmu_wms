<?php

namespace App\Http\Controllers\Main\Warehouse;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Popup;
use App\Model\StockExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;
use App\User;
use Carbon\Carbon;
use Auth;

class StockBackupController extends Controller
{
    public function index()
    {
      $user_id = Auth::id();
      $user = User::where('id', $user_id)->first();

      // output
      $output = [];

      $data_output = \File::files(public_path('stock'));

      foreach($data_output as $path_output)
      {
          $output[] = pathinfo($path_output);
      }

      // dd($output);
      // dd($data_output[0]->path);

    	return view ('main.warehouse.stock_backup.index', compact('output'));
    }


    public function delete_stock($id)
    {

      // Storage::delete(public_path('stock/'));
      Storage::disk('stock')->delete($id);

      return back();

      // dd($output['filename']);
      // dd($data_output[0]->path); 
    }

    public function export_stock()
    {
      $prefix = Carbon::now()->format("Y-m-d h:i:s");
      (new FastExcel(StockExport::all()))->export(public_path('stock/'.$prefix.'-stock.csv'), function ($user) {
        return [
              'Warehouse Name' => $user->warehouse_name,
              'Room' => $user->room_name,
              'Bay' => $user->bay_name,
              'Rack' => $user->rack_name,
              'Pallet No' => $user->pallet_number,
              'Batch' => $user->batch,
              'Inventory Code' => $user->inventory_code,
              'Inventory Name' => $user->inventory_name,
              'Unit' => $user->unit,
              'Stock' => $user->quantity,
          ];
      });

      return back();


    }

}
