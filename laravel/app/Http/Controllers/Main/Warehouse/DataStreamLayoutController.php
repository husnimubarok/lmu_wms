<?php

namespace App\Http\Controllers\Main\Warehouse;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StockTransferRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\StockTransfer; 
use App\Model\StockTransferDetail; 
use App\Model\Warehouse; 
use App\Model\Inventory; 
use App\Model\UserLog;
use App\Model\UnitOfMeasure;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class DataStreamLayoutController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'stock_transfer_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ====================================================STOCK ENDING===============================================
    public function index()
    {
      $sql = 'SELECT
                warehouse.id,
                COUNT(stock.rack_id) AS quantity_rack,
                warehouse.warehouse_code,
                warehouse.warehouse_name,
                warehouse.rack_capacity
              FROM
                stock
              RIGHT JOIN warehouse ON stock.warehouse_id = warehouse.id
              GROUP BY
                warehouse.warehouse_code,
                warehouse.warehouse_name,
                warehouse.id,
                warehouse.rack_capacity';
      $warehouse = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get(); 

      return view ('main.warehouse.data_stream_layout.index', compact('warehouse'));
    }

    public function detail()
    {
      $stock_transfers = StockTransfer::all();
      return view ('main.warehouse.data_stream_layout_detail.index', compact('stock_transfers'));
    }

    public function data(Request $request)
    {    

        // $itemdata = DB::table('stock')
        //     ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id') 
        //     ->leftjoin('room', 'stock.room_id', '=', 'room.id') 
        //     ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')  
        //     ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')  
        //     ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')  
        //     ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        $sql = 'SELECT
                    warehouse.id,
                    warehouse.warehouse_code,
                    warehouse.warehouse_name,
                    warehouse.rack_capacity,
                    rack.id AS id_rack,
                    room.room_code,
                    room.room_name,
                    rack.rack_code,
                    rack.rack_name,
                    stock.batch,
                    SUM(stock.quantity) AS quantity
                  FROM
                    warehouse
                  LEFT JOIN room ON warehouse.id = room.warehouse_id
                  LEFT JOIN rack ON room.id = rack.room_id
                  LEFT JOIN stock ON rack.id = stock.rack_id
                  GROUP BY
                    warehouse.id,
                    warehouse.warehouse_code,
                    warehouse.warehouse_name,
                    warehouse.rack_capacity,
                    room.room_code,
                    room.room_name,
                    rack.rack_code,
                    rack.rack_name,
                    stock.batch';
        $itemdata = DB::table(DB::raw("(" . $sql . ") as rs_sql"));

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_rack')) { 
                $itemdata->where('rs_sql.id', $id);
            }else{
                $itemdata->where('rs_sql.id', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(rs_sql.room_code, 'rs_sql.rack_code') like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="javascript:void(0)" title="Edit" onclick="open_modal_detail('."'".$itemdata->id_rack."'".')"> Item </a>';
            return ''.$edit.'';
        })
        ->make(true);
    }


    public function data_detail(Request $request)
    {    

        $itemdata = DB::table('stock')
            ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id') 
            ->leftjoin('room', 'stock.room_id', '=', 'room.id') 
            ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')  
            ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')  
            ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')  
            ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_rack_detail')) { 
                $itemdata->where('rack.id', $id);
            }else{
                $itemdata->where('rack.id', 0);
            }
            if($keyword = $request->input('keyword')) { 
              $itemdata->whereRaw("CONCAT(room.room_code, 'room.room_code') like ?", ["%{$keyword}%"]);
            }
        })
 
        ->make(true);
    }

     // ====================================================STOCK MOVEMENT===============================================
     public function index_movement()
     {
       return view ('main.warehouse.report_stock.index_movement');
     }
 
     public function data_movement(Request $request)
     {    
 
        //  $itemdata = DB::table('stock')
        //      ->leftjoin('warehouse', 'stock.warehouse_id', '=', 'warehouse.id') 
        //      ->leftjoin('room', 'stock.room_id', '=', 'room.id') 
        //      ->leftjoin('rack', 'stock.rack_id', '=', 'rack.id')  
        //      ->leftjoin('bay', 'stock.bay_id', '=', 'bay.id')  
        //      ->leftjoin('inventory', 'stock.inventory_id', '=', 'inventory.id')  
        //      ->select('stock.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

        //      dd($itemdata);
          $sql = 'SELECT
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name,
                    DRVDTBL.unit,
                    DRVDTBL.batch,
                    SUM(goods_receive) AS goods_receive,
                    SUM(picking) AS picking,
                    SUM(gr_return) AS gr_return,
                    SUM(stock_movement) AS stock_movement,
                    SUM(stock) AS stock,
                      0 AS nol
                  FROM
                    (
                      SELECT
                        stock_log.warehouse_id,
                        warehouse.warehouse_name,
                        stock_log.room_id,
                        room.room_name,
                        stock_log.bay_id,
                        bay.bay_name,
                        stock_log.rack_id,
                        rack.rack_name,
                        inventory.inventory_code,
                        inventory.inventory_name,
                        stock_log.unit,
                        stock_log.pallet_number,
                        stock_log.batch,
                        CASE
                      WHEN stock_log.type_transaction = "GOODS RECEIVE" THEN
                        stock_log.quantity
                      ELSE
                        0
                      END AS goods_receive,
                      CASE
                    WHEN stock_log.type_transaction = "GOODS ISSUE" THEN
                      stock_log.quantity
                    ELSE
                      0
                    END AS picking,
                    CASE
                  WHEN stock_log.type_transaction = "GR RETURN" THEN
                    stock_log.quantity
                  ELSE
                    0
                  END AS gr_return,
                  CASE
                  WHEN stock_log.type_transaction = "STOCK MOVEMENT" THEN
                    stock_log.quantity
                  ELSE
                    0
                  END AS stock_movement,
                  stock_log.quantity AS stock
                  FROM
                    stock_log
                  LEFT OUTER JOIN inventory ON stock_log.inventory_id = inventory.id
                  LEFT OUTER JOIN rack ON stock_log.rack_id = rack.id
                  LEFT OUTER JOIN warehouse ON stock_log.warehouse_id = warehouse.id
                  LEFT OUTER JOIN room ON stock_log.room_id = room.id
                  LEFT OUTER JOIN bay ON stock_log.bay_id = bay.id
                    ) AS DRVDTBL
                  GROUP BY
                    DRVDTBL.warehouse_id,
                    DRVDTBL.warehouse_name,
                    DRVDTBL.room_id,
                    DRVDTBL.room_name,
                    DRVDTBL.bay_id,
                    DRVDTBL.bay_name,
                    DRVDTBL.rack_id,
                    DRVDTBL.rack_name,
                    DRVDTBL.pallet_number,
                    DRVDTBL.batch,
                    DRVDTBL.unit,
                    DRVDTBL.inventory_code,
                    DRVDTBL.inventory_name';
          $itemdata = DB::table(DB::raw("(" . $sql . ") as rs_sql")); 
      
         return Datatables::of($itemdata) 
 
         ->filter(function ($itemdata) use ($request) {  
             if($keyword = $request->input('keyword')) { 
               $itemdata->whereRaw("CONCAT(pallet_number, 'warehouse_name') like ?", ["%{$keyword}%"]);
             }
         })

         ->addColumn('action', function ($itemdata) {
          return '0';
        })
        ->rawColumns(['action', 'check']) 
         ->make(true);
     }


     // ====================================================STOCK LEDGER===============================================
     public function index_ledger()
     {
       return view ('main.warehouse.report_stock.index_ledger');
     }
 
     public function data_ledger(Request $request)
     {    
 
        //  $itemdata = DB::table('stock_log')
        //      ->leftjoin('warehouse', 'stock_log.warehouse_id', '=', 'warehouse.id') 
        //      ->leftjoin('room', 'stock_log.room_id', '=', 'room.id') 
        //      ->leftjoin('rack', 'stock_log.rack_id', '=', 'rack.id')  
        //      ->leftjoin('bay', 'stock_log.bay_id', '=', 'bay.id')  
        //      ->leftjoin('inventory', 'stock_log.inventory_id', '=', 'inventory.id')  
        //      ->select('stock_log.*', 'warehouse.warehouse_name', 'room.room_name', 'bay.bay_name', 'rack.rack_name', 'inventory.inventory_code', 'inventory.inventory_name');

          $sql = 'SELECT
                    warehouse.id,
                    warehouse.warehouse_code,
                    warehouse.warehouse_name,
                    warehouse.rack_capacity,
                    room.room_code,
                    room.room_name,
                    rack.rack_code,
                    rack.rack_name,
                    stock.batch,
                    SUM(stock.quantity) AS quantity
                  FROM
                    warehouse
                  LEFT JOIN room ON warehouse.id = room.warehouse_id
                  LEFT JOIN rack ON room.id = rack.room_id
                  LEFT JOIN stock ON rack.id = stock.rack_id
                  GROUP BY
                    warehouse.id,
                    warehouse.warehouse_code,
                    warehouse.warehouse_name,
                    warehouse.rack_capacity,
                    room.room_code,
                    room.room_name,
                    rack.rack_code,
                    rack.rack_name,
                    stock.batch';
          $itemdata = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get(); 
 
         return Datatables::of($itemdata) 

         ->filter(function ($itemdata) use ($request) { 

            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($request->input('date_from') != '' || $request->input('date_to') != '')
            {
              $itemdata->whereBetween('stock_log.date_transaction', [$date_from_format, $date_to_format]);
              // $itemdata->where('goods_receive.status', 0);
            }else{
              $itemdata->where('stock_log.status', 0);
            };
        })

        ->addColumn('action', function ($itemdata) {
          return '0';
        })
        ->rawColumns(['action', 'check']) 
         ->make(true);
     }
 
}
