<?php

namespace App\Http\Controllers\Main;

use Auth;
use Datatables;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\GeneralJournalRequest;
use App\Http\Controllers\Controller;
use App\Model\GeneralJournal;  
use App\Model\GeneralJournalDetail;   
use App\Model\Account;   
use App\Model\User;  
use App\Model\UserLog;   
use Validator;
use View; 



class GeneralJournalController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'no_trans' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 
    return view ('main.general_journal.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      $sql = 'SELECT
                general_journal.id,
                general_journal.no_trans,
                instruction.no_trans AS instruction_no,
                DATE_FORMAT(general_journal.date_trans, "%d-%m-%Y") AS date_trans,
                customer.customer_code,
                customer.customer_name,
                general_journal.warehouse_id,
                general_journal.general_journal_type_id,
                general_journal.branch_id,
                general_journal.instruction_status,
                branch.branch_name,
                salesman.salesman_name,
                general_journal.reference_no,
                DATE_FORMAT(general_journal.date_plan, "%d-%m-%Y") AS date_plan,
                general_journal.image,
                general_journal.`status`,
                general_journal_type.general_journal_type_name,
                warehouse.warehouse_name 
              FROM
                general_journal
                LEFT JOIN customer ON general_journal.customer_id = customer.id
                LEFT JOIN branch ON general_journal.branch_id = branch.id
                LEFT JOIN salesman ON general_journal.salesman_id = salesman.id
                LEFT JOIN instruction ON general_journal.instruction_id = instruction.id
                LEFT JOIN general_journal_type ON general_journal.general_journal_type_id = general_journal_type.id
                LEFT JOIN warehouse ON general_journal.warehouse_id = warehouse.id
                INNER JOIN user_branch ON general_journal.branch_id = user_branch.branch_id 
              WHERE user_branch.user_id = '.Auth::id().'';
      $general_journaldata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($general_journaldata) 

      ->addColumn('action', function ($general_journaldata) {
        if ($general_journaldata->status == 0) {
        return '<a href="delivery-order/edit/'.$general_journaldata->id.'" title="Edit" class="btn btn-primary btn-xs"> <i class="ti-pencil-alt"></i> Edit</a>';
        }else{
         return '<a href="delivery-order/edit/'.$general_journaldata->id.'" title="Edit" class="btn btn-primary btn-xs"> <i class="ti-pencil-alt"></i> View Detail</a>';
       };
      })
 

      ->addColumn('image', function ($general_journaldata) {
          if ($general_journaldata->image == null) {
            return '';
          }else{
           return '<a href="../uploads/general_journal/'.$general_journaldata->image.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
       })

      ->addColumn('mstatus', function ($general_journaldata) {
        if ($general_journaldata->status == 0) {
          return '<span class="label label-danger"> Process </span>';
        }else{
         return '<span class="label label-success"> Finish </span>';
       };
     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store(Request $request)
  {  
      $userid= Auth::id();
      $code_trans = $request->input('do_type_id'); 
      $branch_code = $request->input('branch_code'); 

      $branch_id = Branch::where('branch_code', $request->input('branch_code'))->first();
      // $warehouse_code = $request->input('warehouse_code'); 

      $array_month = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $month = $array_month[date('n')];

      DB::insert("INSERT INTO general_journal (code_trans, branch_code, branch_id, no_trans, date_trans, created_by, created_at)
        SELECT '".$code_trans."', '".$branch_code."', '".$branch_id->id."', 
        IFNULL(CONCAT('".$code_trans."','/".$branch_code."/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(RIGHT(general_journal.no_trans, 3)),3))+1001,3)), CONCAT('".$code_trans."','/".$branch_code."/', '".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
        FROM
        general_journal WHERE general_journal.code_trans = '".$code_trans."' AND general_journal.branch_code = '".$branch_code."' AND DATE_FORMAT(general_journal.date_trans, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      // number format is: DO TYPE / BRANCH CODE / WAREHOUSE CODE

      // DB::insert("INSERT INTO general_journal (code_trans, branch_code, warehouse_code, no_trans, date_trans, created_by, created_at)
      //   SELECT '".$code_trans."', '".$branch_code."', '".$warehouse_code."',
      //   IFNULL(CONCAT('".$code_trans."','/".$branch_code."/','".$warehouse_code."', '/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/',RIGHT((RIGHT(MAX(SUBSTRING(general_journal.no_trans, -7, 3)),3))+100001,3), '/BNW'), CONCAT('".$code_trans."','/SBY/','".$month."', '/', DATE_FORMAT(NOW(), '%y'),'/','001/BNW')), DATE(NOW()), '".$userid."', DATE(NOW()) 
      //   FROM
      //   general_journal WHERE general_journal.code_trans = '".$code_trans."' AND general_journal.branch_code = '".$branch_code."' AND DATE_FORMAT(general_journal.date_trans, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')");

      $lastInsertedID = DB::table('general_journal')->max('id'); 

      $general_journal_user_by = new GeneralJournalUser;  
      $general_journal_user_by->status = 0;     
      $general_journal_user_by->trans_id = $lastInsertedID;     
      $general_journal_user_by->user_type = 'created';     
      $general_journal_user_by->updated_by = $userid;
      $general_journal_user_by->user_id = $userid;
      $general_journal_user_by->save();

      $userLog = UserLog::create([
            'user_id' => Auth::id(),
            'scope' => 'DELIVERY ORDER',
            'data' => json_encode([
                'action' => 'create',
                'general_journal_id' => $lastInsertedID 
            ])
        ]);

      return redirect('editor/delivery-order/edit/'.$lastInsertedID.''); 
  }

  public function edit($id)
  {
    $general_journal = GeneralJournal::find($id); 
    $general_journal_type_list = DoType::all()->pluck('do_type_name', 'do_type_name'); 
    $customer_list = Customer::where('status', '1')->orderBy('customer_name', 'ASC')->get()->pluck('customer_name', 'id'); 
    $branch_list = Branch::whereNotNull('branch_name')->get()->pluck('branch_name', 'id'); 
    $warehouse_list = Warehouse::all()->pluck('warehouse_name', 'id'); 
    $city_list = City::all()->pluck('city_name', 'id'); 
    $user_list = User::orderBy('first_name', 'ASC')->get()->pluck('first_name', 'id'); 

    $customer_list = Customer::where('status', 1)->orderBy('customer_name', 'ASC')->get()->pluck('customer_name', 'id'); 
    $salesman_list = Salesman::orderBy('salesman_name', 'ASC')->get()->pluck('salesman_name', 'id'); 


    $instruction_no = Instruction::where('id', $general_journal->instruction_id)->first(); 
    // $item = Item::all(); 
    $item = Item::select(
            DB::raw("CONCAT(seq_no,'. ',item_name, ' (', item_unit, ')') AS item_name"),'id')
            ->orderBy('seq_no', 'ASC')
            ->get();

    $item_unit = ItemUnit::all(); 


    return view ('editor.general_journal.form', compact('general_journal', 'general_journal_type_list', 'city_list', 'customer_list', 'branch_list', 'warehouse_list', 'city_list', 'item', 'item_unit', 'customer_list', 'salesman_list', 'user_list', 'instruction_no'));
  }

  public function update($id, Request $request)
  {

    $date_array = explode("-",$request->input('date_trans')); // split the array
    $var_day = $date_array[0]; //day seqment
    $var_month = $date_array[1]; //month segment
    $var_year = $date_array[2]; //year segment
    $new_date_format = "$var_year-$var_month-$var_day"; // join them together
     
    $general_journal = GeneralJournal::Find($id);  
    $general_journal->date_trans = $new_date_format;  
    $general_journal->customer_id = $request->input('customer_id');  
    $general_journal->salesman_id = $request->input('salesman_id');   
    // $general_journal->branch_id = $request->input('branch_id');   
    $general_journal->instruction_id = $request->input('instruction_id');   
    $general_journal->reference_no = $request->input('reference_no');   
    $general_journal->date_plan = $request->input('date_plan');     
    $general_journal->ship_name = $request->input('ship_name');     
    $general_journal->instruction_status = $request->input('instruction_status');     
    $general_journal->updated_by = Auth::id();
    $general_journal->save();


    $instruction = Instruction::where('id', $request->input('instruction_id'))->first();  
    if(isset($instruction)){
      $instruction->instruction_status = $request->input('instruction_status'); 
      $instruction->save();  
    };
 
    $general_journal_user_cancel = GeneralJournalUser::where('trans_id', $id)->where('user_type', 'processed')->get();  
    foreach($general_journal_user_cancel AS $general_journal_user_cancel)
    {
      $general_journal_user_cancel->status =  1;     
      $general_journal_user_cancel->user_type =  'canceled';     
      $general_journal_user_cancel->updated_by = Auth::id();
      $general_journal_user_cancel->save();
    };

    $general_journal_user_for = new GeneralJournalUser(); 
    $general_journal_user_for->user_id = $request->input('user_id');  
    $general_journal_user_for->trans_id = $id;  
    $general_journal_user_for->status =  0;     
    $general_journal_user_for->user_type =  'processed';     
    $general_journal_user_for->created_by = Auth::id();
    $general_journal_user_for->save();

    $general_journal_user_by = GeneralJournalUser::where('trans_id', $id)->where('user_id', Auth::id())->get();  
    if(isset($general_journal_user_by)){
      foreach ($general_journal_user_by as $general_journal_user_by) {
        $general_journal_user_by->status = 1;     
        $general_journal_user_by->updated_by = Auth::id();
        $general_journal_user_by->save();
      }
    };

    if($request->image)
    {
    $general_journal = GeneralJournal::FindOrFail($general_journal->id);
    $original_directory = "uploads/general_journal/";

    if(!File::exists($original_directory))
      {
        File::makeDirectory($original_directory, $mode = 0777, true, true);
      }

      //$file_extension = $request->image->getClientOriginalExtension();
      $general_journal->image = Carbon::now()->format("d-m-Y-h-i-s").str_replace(" ", "", $request->image->getClientOriginalName());
      $request->image->move($original_directory, $general_journal->image);

      // $thumbnail_directory = $original_directory."thumbnail/";
      // if(!File::exists($thumbnail_directory))
      //   {
      //    File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
      //  }
      //  $thumbnail = Image::make($original_directory.$general_journal->image);
      //  $thumbnail->fit(10,10)->save($thumbnail_directory.$general_journal->image);

       $general_journal->save(); 
     }

     $userLog = UserLog::create([
          'user_id' => Auth::id(),
          'scope' => 'DELIVERY ORDER',
          'data' => json_encode([
              'action' => 'update',
              'general_journal_id' => $id 
          ])
      ]);

     return redirect('editor/delivery-order'); 

    // return redirect()->action('Editor\GeneralJournalController@index'); 
  }

  public function delete($id)
  {
    $post =  GeneralJournal::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }


  // detail
  public function get_detail($id)
  {  
   // $item_name = "%" . $_GET['item_name'] . "%";
   // $item_unit = "%" . $_GET['item_unit'] . "%"; 

   // $statement = GeneralJournalDetail::get();

    $sql = 'SELECT
              general_journal_detail.id,
              general_journal_detail.trans_id,
              general_journal_detail.item_id AS item_name,
              general_journal_detail.item_unit AS item_unit_name,
              general_journal_detail.quantity,
              general_journal_detail.image,
              general_journal_detail.`status` 
            FROM
              general_journal_detail
            LEFT JOIN item ON general_journal_detail.item_id = item.id
            WHERE
            general_journal_detail.deleted_at IS NULL AND general_journal_detail.trans_id = '.$id.'';
    $statement = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    if(isset($statement))
    {
      for($i = 0; $i < $statement->count(); $i++)
        {
          $json[$i] = 
          [
          'id' => $statement[$i]->id, 
          'Item Name' => $statement[$i]->item_name, 
          // 'Unit' => $statement[$i]->item_unit_name, 
          'Quantity' => $statement[$i]->quantity,  
          ];
        } 
    }else{
      $json =
      [
        'message' => 'Data is not avaliable!',
      ]; 
    }
   return response()->json($json);
  }


  public function post_detail(Request $request, $id)
  {   
    $statement = new GeneralJournalDetail;  
    $statement->trans_id = $id; 
    $statement->item_id = Input::get('item.Item Name'); 
    // $statement->item_unit = Input::get('item.Unit'); 
    $statement->quantity = Input::get('item.Quantity'); 
    $statement->save(); 
  }

  public function put_detail(Request $request)
  {   
    $statement = GeneralJournalDetail::where('id', Input::get('item.id'))->first();  
    $statement->item_id = Input::get('item.Item Name'); 
    // $statement->item_unit = Input::get('item.Unit'); 
    $statement->quantity = Input::get('item.Quantity'); 
    $statement->save(); 
  }

  public function delete_detail(Request $request)
  {   
    $statement = GeneralJournalDetail::where('id', Input::get('item.id'))->first();   
    $statement->delete(); 
  }

}
