<?php

namespace App\Http\Controllers\Main\Preference;
use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PreferenceRequest;
use App\Http\Controllers\Controller;
use File;
use Carbon\Carbon;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;
use Alert;
use App\Http\Requests;
use Artisan;
use Log;
use Storage;

use App\Model\Preference;
use App\Model\Country;
use App\Model\City;
use App\Model\District;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class PreferenceController extends Controller
{

   protected $signature = 'db:backup';

   protected $description = 'Backup the database';

   protected $process;


  /**
    * @var array
    */
  protected $rules =
  [
    'supplier_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $preference = Preference::first();

      return view ('main.preference.preference.index', compact('preference'));
    }

    public function backup_database()
    {
      $file_name = Carbon::now()->format("Y-m-d h:i:s");

      $file = config('laravel-backup.backup.name') . '/' . $file_name;
      $disk = Storage::disk(config('laravel-backup.backup.destination.disks')[0]);
      if ($disk->exists($file)) {
          $fs = Storage::disk(config('laravel-backup.backup.destination.disks')[0])->getDriver();
          $stream = $fs->readStream($file);
          return \Response::stream(function () use ($stream) {
              fpassthru($stream);
          }, 200, [
              "Content-Type" => $fs->getMimetype($file),
              "Content-Length" => $fs->getSize($file),
              "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
          ]);
      } else {
          abort(404, "The backup file doesn't exist.");
      }
    }

    public function update($id, Request $request)
    {
        // $date_from_array = explode("-",$request->input('pkp_date')); // split the array
        // $var_day_from = $date_from_array[0]; //day seqment
        // $var_month_from = $date_from_array[1]; //month segment
        // $var_year_from = $date_from_array[2]; //year segment
        // $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

        $user = Preference::find($id);
        $user->company_name = $request->input('company_name');
        $user->address = $request->input('address');
        $user->email = $request->input('email');
        $user->fax = $request->input('fax');
        $user->web = $request->input('web');
        $user->telp = $request->input('telp');
        $user->pkp_no = $request->input('pkp_no');
        $user->pkp_date = $request->input('pkp_date');
        $user->branch_code = $request->input('branch_code');
        $user->file_path = $request->input('file_path');
        $user->save();

        if($request->logo)
        {
          $preference = Preference::FindOrFail($user->id);

          $original_directory = "uploads/preference/";

          if(!File::exists($original_directory))
            {
              File::makeDirectory($original_directory, $mode = 0777, true, true);
            }
            $preference->logo = Carbon::now()->format("d-m-Y h-i-s").$request->logo->getClientOriginalName();
            $request->logo->move($original_directory, $preference->logo);
            $preference->save();
        }

        return redirect()->action('Main\Preference\PreferenceController@index');
    }
}
