<?php

namespace App\Http\Controllers\Main;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Model\SalesReturn; 
use App\Model\SalesReturnDetail; 
use App\Model\PutawayReceive; 
use App\Model\warehouse;
use App\Model\Rack;
use App\Model\Bay;
use Maatwebsite\Excel\Concerns\FromCollection;
  
class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Bay::all();
    }
}