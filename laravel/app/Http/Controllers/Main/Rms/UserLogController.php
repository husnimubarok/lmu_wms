<?php

namespace App\Http\Controllers\Main\Rms;

use Auth;
use TestCase;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Users;
use App\Model\UserLog;
use App\Model\ViewUserLog;
use App\Model\Role;
use Validator;
use Response;
use DateTime;
use App\Post;
use View;
use Carbon\Carbon;



class UserLogController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'user_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

      return view ('main.rms.user_log.index');
    }

    public function data(Request $request)
    {
        // $itemdata = UserLog::all();
        // $userLog = UserLog::orderBy('id', 'DESC')->get();
        // $userLog = DB::table('view')
        //     ->join('user_log', 'users.id', '=', 'user_log.user_id')
        //     ->orderBy('user_log.id', 'DESC')
        //     ->select('user_log.*', 'users.username');
            // ->get();

        $userLog = DB::table('view_user_log')
            ->orderBy('view_user_log.id', 'DESC')
            ->select('view_user_log.*');

        return Datatables::of($userLog)

        ->filter(function ($userLog) use ($request) {
            $date_from_array = explode("-",$request->input('date_from')); // split the array
            $var_day_from = $date_from_array[0]; //day seqment
            $var_month_from = $date_from_array[1]; //month segment
            $var_year_from = $date_from_array[2]; //year segment
            $date_from_format = "$var_year_from-$var_month_from-$var_day_from"; // join them together

            $date_to_array = explode("-",$request->input('date_to')); // split the array
            $var_day_to = $date_to_array[0]+1; //day seqment
            $var_month_to = $date_to_array[1]; //month segment
            $var_year_to = $date_to_array[2]; //year segment
            $date_to_format = "$var_year_to-$var_month_to-$var_day_to"; // join them together

            if($keyword = $request->input('keyword')) {
              $userLog->whereRaw("CONCAT(view_user_log.data, view_user_log.username, ifnull(view_user_log.description,'')) like ?", ["%{$keyword}%"]);
            }
            $userLog->whereBetween('view_user_log.created_at', [$date_from_format, $date_to_format]);
        })

        ->addColumn('username', function ($userLog) {
          return ''.$userLog->username.'';
        })

        ->addColumn('transaction_number', function ($userLog) {
          $data = json_decode($userLog->data);
            if(isset($data->transaction_number))
            {
              return ''.$data->transaction_number.'';
            }else{
              return '-';
            };
        })

        ->addColumn('source', function ($userLog) {
          $data = json_decode($userLog->data);
            if(isset($data->source))
            {
              return ''.$data->source.'';
            }else{
              return '-';
            };
        })

        ->addColumn('action', function ($userLog) {
          $data = json_decode($userLog->data);
            if(isset($data->action))
            {
              return ''.$data->action.'';
            }else{
              return '-';
            };
        })

        ->addColumn('description', function ($userLog) {
          $data = json_decode($userLog->data);
            if(isset($data->description))
            {
              return ''.$data->description.'';
            }else{
              return '-';
            };
        })


        ->rawColumns(['transaction_number', 'action', 'source'])
        ->make(true);

        // dd($itemdata);

    }
}
