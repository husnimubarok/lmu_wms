<?php

namespace App\Http\Controllers\Main\Rms;

use Auth;
use TestCase;
use Datatables; 
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use \App\Notifications\TelegramNotification;


use App\Model\Users; 
use App\Model\Role; 
use Validator;
use Response;
use DateTime;
use App\Post;
use View;


class UserController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'user_name' => 'required|min:2|max:128'
  ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    { 
      return view ('main.rms.user.index');
    }

    public function data(Request $request)
    {    

        $itemdata = DB::table('users') 
            ->leftjoin('roles', 'users.role_id', '=', 'roles.id') 
            ->select('users.*', 'roles.name AS role_name');

        return Datatables::of($itemdata) 

        ->filter(function ($itemdata) use ($request) { 
            if($id = $request->input('filter_status')) { 
                $itemdata->where('users.status', $id);
            }else{
                $itemdata->where('users.status', 0);
            }
            if($keyword = $request->input('keyword')) { 
                $itemdata->whereRaw("CONCAT(ifnull(users.email,''), roles.name, users.username) like ?", ["%{$keyword}%"]);
            }
        })

        ->addColumn('action', function ($itemdata) {
          $edit = '<a  type="button" class="btn btn-info btn-float btn-xs" href="user/edit/'.$itemdata->id.'" title="Edit"> <i class="icon-pencil7"></i> </a> <a  type="button" class="btn btn-info btn-float btn-xs" href="user/edit-password/'.$itemdata->id.'" title="Change Password"> <i class="icon-key"></i> </a>';
          $archieve = '<a  type="button" class="btn btn-warning btn-float btn-xs"  href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->username."', '".$itemdata->status."'".')"> <i class="icon-archive"></i></a>';
          $reactive = '<a  type="button" class="btn btn-success btn-float btn-xs" href="javascript:void(0)" title="Archive" onclick="change_status('."'".$itemdata->id."', '".$itemdata->username."', '".$itemdata->status."'".')"> <i class="icon-reset"></i></a>';

          if($itemdata->status ==1){
            return ''.$reactive.'';
          }else{
            return ''.$edit.' '.$archieve.'';
          }
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('check_status', function ($itemdata) {
          return '<input type="checkbox" class="styled">';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success">Active</span>';
          }else{
           return '<span class="label label-danger">Inactive</span>';
         };

       })

        ->rawColumns(['action', 'check_status', 'mstatus', 'check']) 
        ->make(true);
    }

    public function create()
    {   
      $role_list = Role::all()->pluck('name', 'id'); 
      return view ('main.rms.user.form', compact('role_list'));
    }

    public function store(Request $request)
    {  
        $post = new Users(); 
        $post->username = $request->input('username');  
        $post->role_id = $request->input('role_id'); 
        $post->type_name = $request->input('type_name'); 
        $post->status = 0; 
        $post->save();

        return redirect()->action('Main\Rms\UserController@index'); 
    }

    public function edit($id)
    {
      $user = Users::Find($id);
      $role_list = Role::all()->pluck('name', 'id'); 

      return view ('main.rms.user.form', compact('user', 'role_list'));
    }

    public function update($id, Request $request)
    { 
        $post = Users::Find($id); 
        $post->username = $request->input('username');
        $post->role_id = $request->input('role_id');  
        $post->type_name = $request->input('type_name'); 
        $post->status = $request->input('status'); 
        $post->save();

        return redirect()->action('Main\Rms\UserController@index'); 
    }

    public function edit_password($id)
    {
      $user = Users::Find($id);

      return view ('main.rms.user.form_password', compact('user'));
    }

    public function update_password($id, Request $request)
    {
        $post = Users::Find($id);  
        $post->password = bcrypt($request->input('password'));
        $post->save();

        return redirect()->action('Main\Rms\UserController@index'); 
    }

    public function delete($id)
    {
      $post =  User::Find($id);
      if($post->status == 1){
        $post->status = 0;
      }else{
        $post->status = 1;
      };
      $post->save(); 

      return response()->json($post); 
    }

    public function bulk_change_status(Request $request)
    {

      $ids = $request->ids;   
      foreach($ids as $key => $id) {
          $post = User::Find($id[1]);
          if ($post) 
            if($post->status == 1){
              $post->status = 0;
            }else{
              $post->status = 1;
            };
            $post->save(); 
      }

      return response()->json([
          'status' => TRUE
      ]); 

  }
}
