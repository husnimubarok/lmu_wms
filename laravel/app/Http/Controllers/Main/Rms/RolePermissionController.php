<?php

namespace App\Http\Controllers\Main\Rms;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Model\Permission;
use App\Model\RolePermission;
use App\Model\UserLog;
use App\Model\User;
use RoleManager;
use Datatables;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        return view ('main.rms.roles_permission.index');
    }

    public function data(Request $request) {
        if ($request->ajax()) {
            $roles = Role::orderBy('name', 'ASC')
                        ->get();

            $result = collect([]);
            $res_val;;
            foreach ($roles as $role) {
                $res_val = [
                    'status' => $role->status,
                    'id'   => $role->id,
                    'name' => $role->name,
                    'description' => $role->description,
                    'countEmp' => count($role->users()->whereNull('status')->whereNull('deleted_at')->orderBy('id', 'DESC')->get())
                ];

                if (is_array($res_val)) {
                    $result->push($res_val);
                }
            }

            $itemdata = $result;

            return Datatables::of($itemdata)
            ->addIndexColumn()
            ->addColumn('name', function($itemdata) {
                return !empty($itemdata['name']) ? $itemdata['name'] : "-";
            })
            ->addColumn('description', function($itemdata) {
                return !empty($itemdata['description']) ? $itemdata['description'] : "-";
            })
            ->addColumn('countEmp', function($itemdata) {
                return !empty($itemdata['countEmp']) ? $itemdata['countEmp'] : "0";
            })
            ->addColumn('status', function($itemdata) {
                $status = "ACTIVE";
                if (!empty($itemdata['status'])) {
                    if ($itemdata['status'] === 'ARCHIVED') {
                        $status = 'ARCHIVED';
                    }
                }

                return $status;
            })
            ->addColumn('action', function($itemdata) {
                $view = "";
                $edit = "";
                $archive = "";

                $view = '';

                if (empty($itemdata['status'])) {
                    $edit = '<a href="role-permission/' . $itemdata['id'] . '/edit" class="btn btn-primary btn-xs"><i class="icon-pencil7" title="Edit"></i></a>';
                }

                if (empty($itemdata['status'])) {
                    $archive = '<a href="role-permission/' . $itemdata['id'] . '/archive" class="btn btn-danger btn-xs"><i class="icon-archive" title="Archive"></i></a>';
                }

                return "$view $edit $archive";
            })
            ->make(true);
        } else {
            exit("No data available");
        }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allPermissions = Permission::orderBy('modulename', 'asc')->get();
        foreach ($allPermissions as $permission) {
            $permission->add = false;
            $permission->read = false;
            $permission->update = false;
            $permission->delete = false;
            $permission->archive = false;
            $permission->duplicate = false;
            $permission->approve = false;
            $permission->download = false;
            $permission->reject = false;
        }

        return view ('main.rms.roles_permission.form', [
            'allPermissions' => $allPermissions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {

            $role = Role::create([
                    'company_id' => Auth::user()->company_id,
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'created_by' => Auth::user()->id
                ]);

           
            $allPermissions = Permission::all();
            $action = [];
            foreach ($allPermissions as $index => $permission) {
                $save = false;

                if (
                    $request->input($permission->keyname .'_create') === '1' ||
                    $request->input($permission->keyname .'_create') === '0'
                ) {
                    $save = true;
                    array_push($action, 'create');
                }

                if (
                    $request->input($permission->keyname .'_read') === '1' ||
                    $request->input($permission->keyname .'_read') === '0'
                ) {
                    $save = true;
                    array_push($action, 'read');
                }

                if (
                    $request->input($permission->keyname .'_update') === '1' ||
                    $request->input($permission->keyname .'_update') === '0'
                ) {
                    $save = true;
                    array_push($action, 'update');
                }

                if (
                    $request->input($permission->keyname .'_delete') === '1' ||
                     $request->input($permission->keyname .'_delete') === '0'
                ) {
                    $save = true;
                    array_push($action, 'delete');
                }
 

                if ($save) {
                    if (count($action) > 1)
                        $actionResult = implode('|', $action);
                    else
                        $actionResult = $action[0];

                    $rolePermission = $role->rolePermissions()->where('permission_id', $permission->id)->first();
                    if ($rolePermission) {
                        $rolePermission->action = $actionResult;
                        $rolePermission->updated_by = Auth::user()->id;
                        $rolePermission->save();
                    } else {
                        $rolePermission = new RolePermission;
                        $rolePermission->role_id = $role->id;
                        $rolePermission->permission_id = $permission->id;
                        $rolePermission->action = $actionResult;
                        $rolePermission->created_by = Auth::user()->id;
                        $rolePermission->save();
                    }

                    $save = false;
                } else {
                    $rolePermission = $role->rolePermissions()->where('permission_id', $permission->id)->first();
                    if ($rolePermission) {
                        $rolePermission->delete();
                    }
                }

                $action = [];
                $actionResult = null;
            }


        //     $alert = 'alert-success';
        //     $message = 'Role was successful added!';
        // } catch (\Exception $e) {
        //     $alert = 'alert-danger';
        //     $message = 'Role was failed added!';
        // }

        // $request->session()->flash($alert, $message);
        return redirect()->route("main.role-permission.index");
    }

    /**
     * Show detail of item
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $role = Role::where('company_id', Auth::user()->company_id)
                        ->where('id', $id)
                        ->first();
        $users = $role->users()->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
        $rolePermissions = $role->rolePermissions()->orderBy('id', 'asc')->get();

        return view ('roles_permissions.view', [
            'role' => $role,
            'users' => $users,
            'rolePermissions' => $rolePermissions
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id', $id)
                        ->first();
        $users = $role->users()->whereNull('status')->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
        $rolePermissions = $role->rolePermissions;

        $allPermissions = Permission::orderBy('modulename', 'asc')->get();


        foreach ($allPermissions as $permission) {
            $permission->add = false;
            $permission->read = false;
            $permission->update = false;
            $permission->delete = false;
            $permission->archive = false;
            $permission->duplicate = false;
            $permission->approve = false;
            $permission->download = false;
            $permission->reject = false;

            foreach ($rolePermissions as $rp) {
                // dd($permission->id);
                // before
                // if ($permission->id === $rp->permission_id) {
                // after
                if ($permission->id == $rp->permission_id) {
                    $action = explode('|', $rp->action);

                    if (in_array('create', $action)) {
                        $permission->add = true;
                    }
                    if (in_array('read', $action)) {
                        $permission->read = true;
                    }
                    if (in_array('update', $action)) {
                        $permission->update = true;
                    }
                    if (in_array('delete', $action)) {
                        $permission->delete = true;
                    }
                    if (in_array('archive', $action)) {
                        $permission->archive = true;
                    }
                    if (in_array('duplicate', $action)) {
                        $permission->duplicate = true;
                    }
                    if (in_array('approve', $action)) {
                        $permission->approve = true;
                    }
                    if (in_array('reject', $action)) {
                        $permission->reject = true;
                    }
                    if (in_array('download', $action)) {
                        $permission->download = true;
                    }
                }
            }
        }

        return view ('main.rms.roles_permission.form', [
            'role'  => $role,
            'users' => $users,
            'allPermissions' => $allPermissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // try {
            $role = Role::where('id', $id)
                        ->first();
            $role->name = $request->input('name');
            $role->description = $request->input('description');
            $role->updated_by  = Auth::user()->id;  
            $role->save();

            $allPermissions = Permission::all();

            // dd($allPermissions);
            // dd($request->input('supplier_read'));

            $action = [];
            foreach ($allPermissions as $index => $permission) {
                $save = false;

                if (
                    $request->input($permission->keyname .'_create') === '1' ||
                    $request->input($permission->keyname .'_create') === '0'
                ) {
                    $save = true;
                    array_push($action, 'create');
                }

                if (
                    $request->input($permission->keyname .'_read') === '1' ||
                    $request->input($permission->keyname .'_read') === '0'
                ) {
                    $save = true;
                    array_push($action, 'read');
                }

                if (
                    $request->input($permission->keyname .'_update') === '1' ||
                    $request->input($permission->keyname .'_update') === '0'
                ) {
                    $save = true;
                    array_push($action, 'update');
                }

                if (
                    $request->input($permission->keyname .'_delete') === '1' ||
                     $request->input($permission->keyname .'_delete') === '0'
                ) {
                    $save = true;
                    array_push($action, 'delete');
                }
 

                if ($save) {
                    if (count($action) > 1)
                        $actionResult = implode('|', $action);
                    else
                        $actionResult = $action[0];

                    $rolePermission = $role->rolePermissions()->where('permission_id', $permission->id)->first();
                    if ($rolePermission) {
                        $rolePermission->action = $actionResult;
                        $rolePermission->updated_by = Auth::user()->id;
                        $rolePermission->save();
                    } else {
                        $rolePermission = new RolePermission;
                        $rolePermission->role_id = $role->id;
                        $rolePermission->permission_id = $permission->id;
                        $rolePermission->action = $actionResult;
                        $rolePermission->created_by = Auth::user()->id;
                        $rolePermission->save();
                    }

                    $save = false;
                } else {
                    $rolePermission = $role->rolePermissions()->where('permission_id', $permission->id)->first();
                    if ($rolePermission) {
                        $rolePermission->delete();
                    }
                }

                $action = [];
                $actionResult = null;
            }

            

        //     $alert = 'alert-success';
        //     $message = 'Role was successful updated!';
        // } catch (\Exception $e) {
        //     $alert = 'alert-danger';
        //     $message = 'Role was failed updated!';
        // }

        // $request->session()->flash($alert, $message);
        return redirect()->route("main.role-permission.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request, $id)
    {
        try {
            $users = User::where('role_id', $id)->whereNull('status')->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
            if (count($users) === 0) {
                $role = Role::whereNull('status')->find($id);

                $role->updated_by = Auth::user()->id;
                $role->status = 'ARCHIVED';
                $role->save();

                $userLog = UserLog::create([
                        'company_id' => Auth::user()->company_id,
                        'user_id' => Auth::user()->id,
                        'scope' => 'ROLE-PERMISSION',
                        'data' => json_encode([
                            'action' => 'archive',
                            'role_id' => $role->id
                        ])
                    ]);

                $alert = 'alert-success';
                $message = 'Role Permission was successful archived!';
            } else {
                $alert = 'alert-danger';
                $message = 'There are users in this role permission!';    
            }
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Role Permission was failed archived!';
        }

        $request->session()->flash($alert, $message);
        return redirect()->route("role-permission.index");
    }
}
