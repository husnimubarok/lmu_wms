<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\Model\Branch;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
  	{
  		  $this->middleware(function ($request, $next) {
  			$userid = auth()->id();

  			$sql_branch_filter = 'SELECT
                        * FROM users
                      WHERE `user`.id = '.$userid.'';
	    	$this->branch_global = DB::table(DB::raw("($sql_branch_filter) as rs_sql"))->first();
	    	View::share([ 'branch_global' => $this->branch_global ]);
	    return $next($request);
      });

        View::share([ 'notif_limit' => $this->notif_limit ]);

  	}
}
