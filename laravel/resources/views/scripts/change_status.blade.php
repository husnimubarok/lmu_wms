<script>
  function bulk_change_status() {
    var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    if (list_id.length > 0) {
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to archive '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'ARCHIVE',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                            },
                            url: "@yield("url_variable")/bulk-change-status",
                            type: "POST", 
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = { 
                                        "positionClass": "toast-bottom-right", 
                                        "timeOut": 1000, 
                                    };
                                    toastr.success('Success archive!', 'Success Alert', options);
                                    reload_table();
                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'archive failed!',
                                    });
                                    reload_table();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'archive failed!',
                                });
                                reload_table();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
  }
  $('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script> 



