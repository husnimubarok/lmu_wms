<ul class="nav nav-tabs nav-tabs-highlight">
  <input type="hidden" name="filter_status" id="filter_status">
  <li class="active" onclick="active_status();"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-folder-check position-left"></i> Active</a></li>
  <li onclick="inactive_status();"><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-folder-minus2 position-left"></i> Inactive</a></li>
  </li>
  <li class="pull-right">
    <a tabindex="0" href="#" target="_blank"><i class="icon-question4"></i> <span>Help</span></a>
  </li>
  <li class="pull-right">
    <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
  </li>
  <li class="pull-right">
      <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a> 
  </li>
  <li class="pull-right">
    <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print</span></a>
  </li>
</ul>
