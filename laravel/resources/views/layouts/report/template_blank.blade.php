<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>LMU WMS -  @yield('title')</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/core.css" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/components.css" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/colors.css" rel="stylesheet" type="text/css">
  <link href="{{url('css') }}/customstyle.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- toastr notifications -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  {{-- confirm --}}
  <link rel="stylesheet" href="{{url('plugins') }}/jQueryConfirm/jquery-confirm.min.css"> 

  @yield('scripts')


  <!-- Core JS files -->
  <script type="text/javascript" src="{{url('js') }}/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/plugins/loaders/blockui.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/plugins/ui/nicescroll.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/plugins/ui/drilldown.js"></script>
  <script type="text/javascript" src="{{url('js') }}/core/libraries/jquery_ui/widgets.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/pages/jqueryui_forms.js"></script>
  <script type="text/javascript" src="{{url('js') }}/plugins/forms/inputs/touchspin.min.js"></script>
  <script type="text/javascript" src="{{url('js') }}/pages/form_input_groups.js"></script>

  <!-- Theme JS files -->
	<!-- <script type="text/javascript" src="{{url('/js') }}/plugins/visualization/echarts/echarts.js"></script>

  <script type="text/javascript" src="{{url('/js') }}/core/app.js"></script>
  <script type="text/javascript" src="{{url('/js') }}/charts/echarts/pies_donuts.js"></script>

  <script type="text/javascript" src="{{url('/js') }}/plugins/ui/ripple.min.js"></script> -->

</head>
<body>

<!-- <div class="wrapper">  -->
<!-- Content Wrapper. Contains page content -->
@yield('content')
<!-- /.content-wrapper -->
<!-- ./wrapper -->
@yield('modal')

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{url('plugins') }}/jQueryConfirm/jquery-confirm.min.js"></script>

<!-- Theme JS files -->
<script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/extensions/scroller.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/extensions/col_reorder.min.js"></script>

<script type="text/javascript" src="{{url('js') }}/pages/datatables_extension_buttons_html5.js"></script>

<script type="text/javascript" src="{{url('js') }}/core/app.js"></script>
{{-- <script type="text/javascript" src="{{url('js') }}/pages/datatables_extension_reorder.js"></script>  --}}

{{-- <script type="text/javascript" src="{{url('js') }}/plugins/forms/selects/select2.min.js"></script> --}}
<script type="text/javascript" src="{{url('js') }}/pages/datatables_sorting.js"></script>
{{-- <script type="text/javascript" src="{{url('js') }}/pages/datatables_extension_scroller.js"></script> --}}
<script type="text/javascript" src="{{url('js') }}/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/pages/form_select2.js"></script>


<!-- Theme JS files -->
<script type="text/javascript" src="{{url('js') }}/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/plugins/pickers/daterangepicker.js"></script>

<script type="text/javascript" src="{{url('js') }}/plugins/ui/ripple.min.js"></script>
{{-- <script type="text/javascript" src="{{url('js') }}/plugins/tables/datatables/datatables.min.js"></script> --}}

<script type="text/javascript" src="{{url('js') }}/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="{{url('js') }}/pages/layout_navbar_secondary_fixed.js"></script>
<script type="text/javascript" src="{{url('js') }}/pages/dashboard.js"></script>

{{-- add --}}
{{-- <script src="{{url('plugins') }}/datatables/dataTables.bootstrap.min.js"></script> --}}
{{-- <script src="{{url('plugins') }}/datatables/dataTables.tableTools.min.js"></script> --}}
{{-- <script src="{{url('plugins') }}/datatables/dataTables.colVis.min.js"></script>  --}}
{{-- <script src="{{url('plugins') }}/select2/select2.full.min.js"></script>  --}}
{{-- <script src="https://cdn.datatables.net/fixedcolumns/3.2.3/js/dataTables.fixedColumns.min.js"></script> --}}


