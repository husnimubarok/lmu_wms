<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
    $main_menu = Request::segment(3);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group"> 
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-close2 text-indigo-400"></i> <span>Close</span></a> 
      <!-- <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>  -->
      <!-- <a href="{{ URL::route('main.gr-return.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-close2 text-indigo-400"></i> <span>Close</span></a> -->
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header