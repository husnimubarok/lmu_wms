<ul class="nav nav-tabs nav-tabs-highlight">
  <input type="hidden" name="filter_status" id="filter_status">  
  <li class="active"><a href="{{ URL::route('main.goods-receive.open') }}"><i class="icon-folder position-left"></i> Open</a></li>
  <li><a href="{{ URL::route('main.goods-receive.index') }}"><i class="icon-folder-check position-left"></i> Active</a></li>
  <li onclick="inactive_status();"><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-folder-minus2 position-left"></i> Inactive</a></li>
  <li class="pull-right" onclick="inactive_status();"> 
  </li>
</ul>  