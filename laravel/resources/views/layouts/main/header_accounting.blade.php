{{-- Main navbar --}}
  <div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{url('/')}}/main"><img src="{{url('images') }}/logo_metaepsi_white.png" alt=""></a>

      <ul class="nav navbar-nav pull-right visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-git-compare"></i>
            <span class="visible-xs-inline-block position-right">Git updates</span>
            <span class="badge bg-warning-400">9</span>
          </a>
          
          <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-heading">
              Git updates
              <ul class="icons-list">
                <li><a href="#"><i class="icon-sync"></i></a></li>
              </ul>
            </div>

            <ul class="media-list dropdown-content-body width-350">
              <li class="media">
                <div class="media-left">
                  <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                </div>

                <div class="media-body">
                  Drop the IE <a href="#">specific hacks</a> for temporal inputs
                  <div class="media-annotation">4 minutes ago</div>
                </div>
              </li>

              <li class="media">
                <div class="media-left">
                  <a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
                </div>
                
                <div class="media-body">
                  Add full font overrides for popovers and tooltips
                  <div class="media-annotation">36 minutes ago</div>
                </div>
              </li>

              <li class="media">
                <div class="media-left">
                  <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                </div>
                
                <div class="media-body">
                  <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                  <div class="media-annotation">2 hours ago</div>
                </div>
              </li>

              <li class="media">
                <div class="media-left">
                  <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                </div>
                
                <div class="media-body">
                  <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                  <div class="media-annotation">Dec 18, 18:36</div>
                </div>
              </li>

              <li class="media">
                <div class="media-left">
                  <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                </div>
                
                <div class="media-body">
                  Have Carousel ignore keyboard events
                  <div class="media-annotation">Dec 12, 05:46</div>
                </div>
              </li>
            </ul>

            <div class="dropdown-content-footer">
              <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
            </div>
          </div>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-people"></i>
            <span class="visible-xs-inline-block position-right">Users</span>
          </a>
          
          <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-heading">
              Users online
              <ul class="icons-list">
                <li><a href="#"><i class="icon-gear"></i></a></li>
              </ul>
            </div>

            <ul class="media-list dropdown-content-body width-300">
              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading text-semibold">Jordana Ansley</a>
                  <span class="display-block text-muted text-size-small">Lead web developer</span>
                </div>
                <div class="media-right media-middle"><span class="status-mark border-success"></span></div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading text-semibold">Will Brason</a>
                  <span class="display-block text-muted text-size-small">Marketing manager</span>
                </div>
                <div class="media-right media-middle"><span class="status-mark border-danger"></span></div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading text-semibold">Hanna Walden</a>
                  <span class="display-block text-muted text-size-small">Project manager</span>
                </div>
                <div class="media-right media-middle"><span class="status-mark border-success"></span></div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading text-semibold">Dori Laperriere</a>
                  <span class="display-block text-muted text-size-small">Business developer</span>
                </div>
                <div class="media-right media-middle"><span class="status-mark border-warning-300"></span></div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading text-semibold">Vanessa Aurelius</a>
                  <span class="display-block text-muted text-size-small">UX expert</span>
                </div>
                <div class="media-right media-middle"><span class="status-mark border-grey-400"></span></div>
              </li>
            </ul>

            <div class="dropdown-content-footer">
              <a href="#" data-popup="tooltip" title="All users"><i class="icon-menu display-block"></i></a>
            </div>
          </div>
        </li>
      </ul>

      <p class="navbar-text"><span class="label bg-success-400">Online</span></p>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown language-switch">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{url('images') }}/flags/gb.png" class="position-left" alt="">
            English
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">
            <li><a class="deutsch"><img src="{{url('images') }}/flags/de.png" alt=""> Deutsch</a></li>
            <li><a class="ukrainian"><img src="{{url('images') }}/flags/ua.png" alt=""> Українська</a></li>
            <li><a class="english"><img src="{{url('images') }}/flags/gb.png" alt=""> English</a></li>
            <li><a class="espana"><img src="{{url('images') }}/flags/es.png" alt=""> España</a></li>
            <li><a class="russian"><img src="{{url('images') }}/flags/ru.png" alt=""> Русский</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-bubbles4"></i>
            <span class="visible-xs-inline-block position-right">Messages</span>
            <span class="badge bg-warning-400">2</span>
          </a>
          
          <div class="dropdown-menu dropdown-content width-350">
            <div class="dropdown-content-heading">
              Messages
              <ul class="icons-list">
                <li><a href="#"><i class="icon-compose"></i></a></li>
              </ul>
            </div>

            <ul class="media-list dropdown-content-body">
              <li class="media">
                <div class="media-left">
                  <img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt="">
                  <span class="badge bg-danger-400 media-badge">5</span>
                </div>

                <div class="media-body">
                  <a href="#" class="media-heading">
                    <span class="text-semibold">James Alexander</span>
                    <span class="media-annotation pull-right">04:58</span>
                  </a>

                  <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                </div>
              </li>

              <li class="media">
                <div class="media-left">
                  <img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt="">
                  <span class="badge bg-danger-400 media-badge">4</span>
                </div>

                <div class="media-body">
                  <a href="#" class="media-heading">
                    <span class="text-semibold">Margo Baker</span>
                    <span class="media-annotation pull-right">12:16</span>
                  </a>

                  <span class="text-muted">That was something he was unable to do because...</span>
                </div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading">
                    <span class="text-semibold">Jeremy Victorino</span>
                    <span class="media-annotation pull-right">22:48</span>
                  </a>

                  <span class="text-muted">But that would be extremely strained and suspicious...</span>
                </div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading">
                    <span class="text-semibold">Beatrix Diaz</span>
                    <span class="media-annotation pull-right">Tue</span>
                  </a>

                  <span class="text-muted">What a strenuous career it is that I've chosen...</span>
                </div>
              </li>

              <li class="media">
                <div class="media-left"><img src="{{url('images') }}/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                <div class="media-body">
                  <a href="#" class="media-heading">
                    <span class="text-semibold">Richard Vango</span>
                    <span class="media-annotation pull-right">Mon</span>
                  </a>
                  
                  <span class="text-muted">Other travelling salesmen live a life of luxury...</span>
                </div>
              </li>
            </ul>

            <div class="dropdown-content-footer">
              <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
            </div>
          </div>
        </li>

        <li class="dropdown dropdown-user">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{url('images') }}/placeholder.jpg" alt="">
            <span>Victoria</span>
            <i class="caret"></i>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
            <li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
            <li>
              <a href="#" onclick="logout.submit()"><i class="icon-switch2"></i>  Logout</a>
            </li>
            <form name="logout" id="logout-form" action="{{ url('/logout') }}" method="POST">
                {{ csrf_field() }} 
            </form>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->


  <!-- Second navbar -->
  <div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
      <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
      <ul class="nav navbar-nav navbar-nav-material"> 

         
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog position-left"></i> Setting <span class="caret"></span></a>
          <ul class="dropdown-menu width-200">
             {{-- <li class="dropdown-header">Transaction</li> --}}
             <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-calculator3"></i> Accounting Setting</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.period.index') }}">Accounting Period</a></li>
                <li><a href="{{ URL::route('main.monthly-rate.index') }}">Monthly Rate</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Conversion Balance</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Currency Converter</a></li>
                <li><a href="{{ URL::route('main.deferral-distribution-type.index') }}">Deferral Distribution Type</a></li>
                <li><a href="{{ URL::route('main.deferral-profile-maintenance.index') }}">Deferral Profile Maintenance</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Journal Segmen</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Linked Account</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Linked Account Alias</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Tax Code</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Tax Converter</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Tax Number</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Term of Payment</a></li>
              </ul>
            </li> 

            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-truck"></i> Asset Setting</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.period.index') }}">Maintanance Period</a></li>
                <li><a href="{{ URL::route('main.monthly-rate.index') }}">Maintenance Category</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Maintenance Type</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Category</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Custom Field Setting</a></li> 
              </ul>
            </li>

            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-grid"></i> System Setting</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.period.index') }}">Dashboard Management</a></li>
                <li><a href="{{ URL::route('main.monthly-rate.index') }}">System Preference</a></li>  
              </ul>
            </li> 
          </ul>
        </li>
           

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-city position-left"></i> Fixed Asset <span class="caret"></span></a>
          <ul class="dropdown-menu width-250"> 
            <li><a href="{{ URL::route('main.depreciation-type.index') }}"><i class="icon-cabinet"></i> Assets</a></li>
            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-basket"></i> Asset Transaction</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}">Asset Revaluation</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Transfer</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Capitalization</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Depreciation</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Disposal</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Write Off</a></li>
              </ul>
            </li>
            <li><a href="{{ URL::route('main.customer_type.index') }}"><i class="icon-briefcase3"></i> Void Asset Transaction</a></li>
            <li><a href="{{ URL::route('main.customer_type.index') }}"><i class="icon-calendar2"></i> Maintenance Order</a></li>
            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-list"></i> Asset Report</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}">Asset History Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset List Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Depreciation Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Maintenance Scheduler Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Maintenance Order Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Status History Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Location History Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Asset Maintenance History Report</a></li>
              </ul>
            </li>
          </ul>
        </li> 

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-archive position-left"></i> Ledger <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">  
            <li><a href="{{ URL::route('main.account.index') }}"><i class="icon-tree7"></i> Chart of Account</a></li>
            <li><a href="{{ URL::route('main.general-journal.index') }}"><i class="icon-list-numbered"></i> Journal Request</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-compose"></i> Journal Entry</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-folder-upload"></i> Journal Entry Upload</a></li> 
            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-calendar"></i> Budget</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}">Budget Period</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Budget Plan</a></li> 
              </ul>
            </li>
            <li><a href="starters/layout_boxed.html"><i class="icon-credit-card2"></i> Revenue and Expense Deferral</a></li> 
            <li><a href="starters/layout_boxed.html"><i class="icon-calculator4"></i> Transaction Journal</a></li> 
            <li><a href="starters/layout_boxed.html"><i class="icon-wallet"></i> Revalue AAP</a></li> 
            <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-list"></i> Consolidation</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}">Account Match</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Consolidation COA</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Worksheet</a></li> 
              </ul>
            </li>
            <li><a href="starters/layout_boxed.html"><i class="icon-chart"></i> Financial Analysis</a></li>  
            <li><a href="starters/layout_boxed.html"><i class="icon-magic-wand2"></i> Costing</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-magic-wand"></i> Posting</a></li> 
             <li class="dropdown-submenu"> 
              <a href="#"><i class="icon-list"></i> Ladger Report</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}">Budget Comparasion Report</a></li>
                <li><a href="{{ URL::route('main.customer_type.index') }}">Budget Plan Report</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Budget VS Actual Report</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">COA Report</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">General Ledger Report</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Journal Report</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Income Stataement</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Trial Balance</a></li> 
                <li><a href="{{ URL::route('main.customer_type.index') }}">Balance Sheet</a></li> 
              </ul>
            </li>
            

          </ul>
        </li>

{{--         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-wallet position-left"></i> Accounting <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">  
            <li><a href="starters/layout_boxed.html"><i class="icon-magic-wand2"></i> Costing</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-magic-wand"></i> Posting</a></li> 
          </ul>
        </li> --}}

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-book3 position-left"></i> Financial Statement <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">  
            <li><a href="starters/layout_boxed.html"><i class="icon-file-spreadsheet2"></i> Unbalance Journal</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-file-text3"></i> Balance Sheet</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-file-presentation"></i> Trial Balance</a></li>
            <li><a href="starters/layout_boxed.html"><i class="icon-paste3"></i> Income Statement</a></li> 
          </ul>
        </li>



      <ul class="nav navbar-nav navbar-nav-material navbar-right"> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-cog3"></i>
            <span class="visible-xs-inline-block position-right">Share</span>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
            <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
            <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- /second navbar -->

 
  


