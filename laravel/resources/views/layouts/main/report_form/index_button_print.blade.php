<!-- Page header -->
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title" id="title">
      <h4>
        <i class="icon-arrow-right6 position-left"></i>
        <span class="text-semibold">Home</span> - @yield('title')
        <small class="display-block">@yield('title'), @yield('caption')</small>
      </h4>
    </div>
    @php
      $main = Request::segment(2);
    @endphp 
    
    <style>
    @media print {
        #printButton {
          display: none;
        }
        #historyButton {
          display: none;
        }
        #title {
          display: none;
        }
        #titlePrint {
          display: inline;
        }
      }
    </style>
    <div class="heading-elements">
      <div class="heading-btn-group"> 
        <a href="#" id="historyButton" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a> 
        <a href="#" id="printButton" onclick="window.print();" class="btn btn-link btn-float has-text text-size-small"><i class="icon-printer text-indigo-400"></i> <span>Print</span></a>
      </div>
    </div>
  </div>
</div>
<!-- /page header