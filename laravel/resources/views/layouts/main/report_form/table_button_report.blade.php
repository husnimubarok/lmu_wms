<ul class="nav nav-tabs nav-tabs-highlight">
  <li>
    <div class="input-group" style="margin-left:10px !important">
      {{ Form::text('date_from', date('d-m-Y', strtotime(now())), ['class' => 'form-control datepicker', 'placeholder' => 'Date From', 'id' => 'date_from', 'onChange' => 'filter_data();']) }}
    </div>
  </li> 
  <li>
    <div class="input-group" style="margin-left:10px !important">
      {{ Form::text('date_to', date('d-m-Y', strtotime(now())), ['class' => 'form-control datepicker', 'placeholder' => 'Date To', 'id' => 'date_to', 'onChange' => 'filter_data();']) }}
    </div>
  </li>
  <li>
  &nbsp;
  </li>
  <li> 
    <button class="btn btn-success btn-xs pull-left" type="button" onclick="filter_data();" data-dismiss="modal"><i class="icon-filter3"></i> Filter</button>
  </li>
  <li>
  &nbsp;
  </li>
  <li>   
  <button onclick="print();" class="btn btn-primary btn-xs pull-left" type="button" data-dismiss="modal"><i class="icon-printer2"></i> Print Filter</button>
  </li>
  <!-- <li class="pull-right">
    <a tabindex="0" href="#" target="_blank"><i class="icon-question4"></i> <span>Help</span></a>
  </li>  -->
  <li class="pull-right">
    <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
  </li>
  <li class="pull-right">
      <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a> 
  </li>
  <li class="pull-right">
    <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print Table</span></a>
  </li>
</ul>  