{{-- Main navbar --}}
  <div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{ secure_asset('images/logo_metaepsi_white.png') }}" alt=""></a>

      <ul class="nav navbar-nav pull-right visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">


      <ul class="nav navbar-nav navbar-right">

        <li class="dropdown dropdown-user">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ secure_asset('images/placeholder.jpg') }}" alt="">
            <span>{{Auth::user()->username}}</span>
            <i class="caret"></i>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            <!-- <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>  -->
            <li>
              <a href="#" onclick="logout.submit()"><i class="icon-switch2"></i>  Logout</a>
            </li>
            <form name="logout" id="logout-form" action="{{ secure_asset('/logout') }}" method="POST">
                {{ csrf_field() }}
            </form>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->


  <!-- Second navbar -->
  <div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
      <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
      <ul class="nav navbar-nav navbar-nav-material">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog position-left"></i> Master <span class="caret"></span></a>
          <ul class="dropdown-menu width-200">
             {{-- <li class="dropdown-header">Transaction</li> --}}
             <li class="dropdown-submenu">
              <a href="#"><i class="icon-calculator3"></i> Purchasing</a>
              <ul class="dropdown-menu width-200">
                @actionStart('supplier', ['read'])
                <li><a href="{{ URL::route('main.vendor.index') }}">Vendor</a></li>
                @actionEnd
                @actionStart('container', ['read'])
                <li><a href="{{ URL::route('main.container.index') }}">Container</a></li>
                @actionEnd
                @actionStart('country', ['read'])
                <li><a href="{{ URL::route('main.country.index') }}">Country</a></li>
                @actionEnd
                @actionStart('city', ['read'])
                <li><a href="{{ URL::route('main.city.index') }}">City</a></li>
                @actionEnd
                @actionStart('district', ['read'])
                <li><a href="{{ URL::route('main.district.index') }}">District</a></li>
                @actionEnd
              </ul>
            </li>

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-home5"></i> Warehouse</a>
              <ul class="dropdown-menu width-200">
                @actionStart('warehouse', ['read'])
                <li><a href="{{ URL::route('main.warehouse.index') }}">Warehouse</a></li>
                @actionEnd
                @actionStart('ship_from', ['read'])
                <li><a href="{{ URL::route('main.ship-from.index') }}">Ship From</a></li>
                @actionEnd
                @actionStart('room', ['read'])
                <li><a href="{{ URL::route('main.store-location.index') }}">Store Location</a></li>
                @actionEnd
                @actionStart('room', ['read'])
                <li><a href="{{ URL::route('main.room.index') }}">Room</a></li>
                @actionEnd
                @actionStart('bay', ['read'])
                <li><a href="{{ URL::route('main.bay.index') }}">Bay</a></li>
                @actionEnd
                @actionStart('rack', ['read'])
                <li><a href="{{ URL::route('main.rack.index') }}">Rack</a></li>
                @actionEnd
                @actionStart('pallet', ['read'])
                <li><a href="{{ URL::route('main.pallet.index') }}">Pallet</a></li>
                @actionEnd
                @actionStart('temprature', ['read'])
                <li><a href="{{ URL::route('main.temprature.index') }}">Temprature</a></li>
                @actionEnd
              </ul>
            </li>

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-drawer"></i> Inventory</a>
              <ul class="dropdown-menu width-200">
                @actionStart('inventory', ['read'])
                <li><a href="{{ URL::route('main.inventory.index') }}">Inventory</a></li>
                @actionEnd
                @actionStart('inventory_group', ['read'])
                <li><a href="{{ URL::route('main.inventory-group.index') }}">Inventory Group</a></li>
                @actionEnd
                @actionStart('uom', ['read'])
                <li><a href="{{ URL::route('main.unit-of-measure.index') }}">Unit of Measure</a></li>
                @actionEnd
                @actionStart('inventory_type', ['read'])
                <li><a href="{{ URL::route('main.inventory-type.index') }}">Type</a></li>
                @actionEnd
                @actionStart('inventory_size', ['read'])
                <li><a href="{{ URL::route('main.inventory-size.index') }}">Size</a></li>
                @actionEnd
                @actionStart('inventory_weight', ['read'])
                <li><a href="{{ URL::route('main.inventory-weight.index') }}">Weight</a></li>
                @actionEnd
              </ul>
            </li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-city position-left"></i> Warehouse <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">
            @actionStart('goods_receive', ['read'])
            <!-- <li><a href="{{ URL::route('main.goods-receive.index') }}"><i class="icon-folder-download2"></i> Goods Receive</a></li>  -->
            @actionEnd
            <li class="dropdown-submenu">
              <a href="#"><i class="icon-folder-download2"></i> Goods Receive</a>
              <ul class="dropdown-menu width-200">
                @actionStart('goods_receive', ['read'])
                <li><a href="{{ URL::route('main.goods-receive.index') }}">Goods Receive</a></li>
                @actionEnd
                <!-- @actionStart('goods_receive', ['read'])
                <li><a href="{{ URL::route('main.goods-receive.index') }}">Goods Receive Manual</a></li>
                @actionEnd -->
                <!-- @actionStart('goods_receive', ['read'])
                <li><a href="{{ URL::route('main.gr-revision.index') }}"> GR Revision</a></li>
                @actionEnd -->
                <!-- @actionStart('inventory_group', ['read'])
                <li><a href="{{ URL::route('main.goods-receive-report-outstanding.index') }}">GR Outstanding</a></li>
                @actionEnd -->
                @actionStart('gr_outstanding', ['read'])
                <li><a href="{{ URL::route('main.goods-receive-report-outstanding-summary.index') }}">GR Outstanding</a></li>
                @actionEnd
              </ul>
            </li>
            @actionStart('po_revision', ['read'])
            <!-- <li><a href="{{ URL::route('main.po-revision.index') }}"><i class="icon-pencil4"></i> PO Revision</a></li>  -->
            @actionEnd
            @actionStart('po_revision', ['read'])
            <!-- <li><a href="{{ URL::route('main.gr-return.index') }}"><i class="icon-reset"></i> GR Return</a></li> -->
            <li class="dropdown-submenu">
              <a href="#"><i class="icon-reset"></i> GR Return</a>
              <ul class="dropdown-menu width-200">
                @actionStart('gr_return', ['read'])
                <li><a href="{{ URL::route('main.gr-return.index') }}">GR Return</a></li>
                @actionEnd
                @actionStart('gr_return_manual', ['read'])
                <li><a href="{{ URL::route('main.gr-return-manual.index') }}">GR Return Manual</a></li>
                @actionEnd
              </ul>
            </li>
            @actionEnd
            @actionStart('goods_issue', ['read'])
            <li><a href="{{ URL::route('main.delivery-order.index') }}"><i class="icon-truck"></i> Goods Issue</a></li>
            @actionEnd
            <!-- @actionStart('material_used', ['read'])
            <li><a href="{{ URL::route('main.material-used.index') }}"><i class="icon-exit2"></i> Material Used</a></li>
            @actionEnd -->

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-folder-search"></i> Stock Opname</a>
              <ul class="dropdown-menu width-200">
                @actionStart('stock_opname', ['read'])
                <li><a href="{{ URL::route('main.stock-opname.index') }}"> Stock Opname</a></li>
                @actionEnd

                @actionStart('stock_opname_collect', ['read'])
                <li><a href="{{ URL::route('main.stock-opname-collect.index') }}"> Stock Opname Collect</a></li>
                @actionEnd

              </ul>
            </li>
            <li class="dropdown-submenu">
              <a href="#"><i class="icon-folder-open"></i> Stock Adjustment</a>
              <ul class="dropdown-menu width-200">
                @actionStart('stock_adjustment_plus_minus', ['read'])
                <li><a href="{{ URL::route('main.stock-adjustment.index') }}"> Adjustment +-</a></li>
                @actionEnd
                @actionStart('stock_adjustment_susut', ['read'])
                <li><a href="{{ URL::route('main.stock-decreasing.index') }}"> Adjustment Susut</a></li>
                @actionEnd
                @actionStart('posting_transfer', ['read'])
                <li><a href="{{ URL::route('main.posting-transfer.index') }}"> Posting Transfer</a></li>
                @actionEnd
              </ul>
            </li>

            <!-- <hr> -->
            <!-- <li><a href="{{ URL::route('main.test-api.index') }}"><i class="icon-folder-download2"></i> Test API</a></li>  -->
            <!-- @actionStart('stock_transfer', ['read'])
            <li><a href="{{ URL::route('main.stock-transfer.index') }}"><i class="icon-diff"></i> Stock Transfer</a></li>
            @actionEnd -->
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-archive position-left"></i> Stock <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">
            <!-- <li><a href="{{ URL::route('main.report-stock.index') }}"> <i class="icon-stack-check"></i> Stock Ending</a></li>
            <li><a href="{{ URL::route('main.report-stock-movement.index') }}"> <i class="icon-stack-up"></i> Stock Movement</a></li>
            <li><a href="{{ URL::route('main.report-stock-ledger.index') }}"> <i class="icon-stack-text"></i> Stock Ledger</a></li> -->
            <li class="dropdown-submenu">
              <a href="#"><i class="icon-stack-text"></i> Stock Detail</a>
              <ul class="dropdown-menu width-200">
                @actionStart('stock_ending', ['read'])
                <li><a href="{{ URL::route('main.report-stock.index') }}"> Stock Ending</a></li>
                <li><a href="{{ URL::route('main.report-stock-my-fruit.index') }}"> Stock Ending My Fruit</a></li>
                @actionEnd
                @actionStart('stock_movement', ['read'])
                <li><a href="{{ URL::route('main.report-stock-movement.index') }}"> Stock Movement</a></li>
                @actionEnd 
              </ul>
            </li>

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-stack-text"></i> Stock Detail + Container</a>
              <ul class="dropdown-menu width-200">
                @actionStart('stock_movement_container', ['read'])
                <li><a href="{{ URL::route('main.report-stock-movement-container.index') }}"> Stock Movement</a></li>
                @actionEnd
              </ul>
            </li>

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-stack-check"></i> Stock Summary</a>
              <ul class="dropdown-menu width-200">
                @actionStart('stock_ending_summary', ['read'])
                <li><a href="{{ URL::route('main.report-stock-summary.index') }}">  Stock Ending Pallet Summary</a></li>
                @actionEnd
                @actionStart('stock_ending_warehouse_summary', ['read'])
                <li><a href="{{ URL::route('main.report-stock-warehouse-summary.index') }}">  Stock Ending Warehouse Summary</a></li>
                @actionEnd
                @actionStart('stock_movement_summary', ['read'])
                <li><a href="{{ URL::route('main.report-stock-movement-summary.index') }}">  Stock Movement Summary</a></li>
                @actionEnd
              </ul>
            </li>
            @actionStart('backup_stock', ['read'])
            <li><a href="{{ URL::route('main.stock-backup.index') }}"><i class="icon-file-eye position-left"></i> Stock Daily Backup</a></li>
            @actionEnd

            <!-- <li class="dropdown-submenu">
              <a href="#"><i class="icon-stack-up"></i> Stock Branch</a>
              <ul class="dropdown-menu width-200">
                  <li><a href="{{ URL::route('main.report-stock-branch.index') }}"> Stock Ending</a></li>
                  <li><a href="{{ URL::route('main.report-stock-branch-movement.index') }}"> Stock Movement</a></li>
                  <li><a href="{{ URL::route('main.report-stock-branch-ledger.index') }}"> Stock Ledger</a></li>
              </ul>
            </li>

            <li class="dropdown-submenu">
              <a href="#"><i class="icon-stack-text"></i> Stock Booking</a>
              <ul class="dropdown-menu width-200">
                  <li><a href="{{ URL::route('main.report-stock-picking.index') }}"> Stock Ending</a></li>
                  <li><a href="{{ URL::route('main.report-stock-picking-movement.index') }}"> Stock Movement</a></li>
                  <li><a href="{{ URL::route('main.report-stock-picking-ledger.index') }}"> Stock Ledger</a></li>
              </ul>
            </li> -->
            <!-- <li class="dropdown-submenu">
              <a href="#"><i class="icon-calculator3"></i> Transaction History</a>
              <ul class="dropdown-menu width-200">
                  @actionStart('goods_receive', ['read'])
                  <li><a href="{{ URL::route('main.goods-receive-report.index') }}"> Goods Receive</a></li>
                  @actionEnd
                  @actionStart('po_revision', ['read'])
                  <li><a href="{{ URL::route('main.po-revision-report.index') }}"> PO Revision</a></li>
                  @actionEnd
                  @actionStart('po_revision', ['read'])
                  <li><a href="{{ URL::route('main.gr-return.index') }}"> GR Return</a></li>
                  @actionEnd
                  @actionStart('po_revision', ['read'])
                  <li><a href="{{ URL::route('main.delivery-order-report.index') }}"> Goods Issue</a></li>
                  @actionEnd
                  @actionStart('material_used', ['read'])
                  <li><a href="{{ URL::route('main.material-used.index') }}"> Material Used</a></li>
                  @actionEnd
                  @actionStart('stock_adjustment', ['read'])
                  <li><a href="{{ URL::route('main.stock-adjustment.index') }}"> Stock Adjustment</a></li>
                  @actionEnd
              </ul>
            </li>  -->

            <!-- <li class="dropdown-submenu">
              <a href="#"><i class="icon-calendar"></i> Stock</a>
              <ul class="dropdown-menu width-200">
                <li><a href="{{ URL::route('main.account.index') }}"> Stock By Warehouse</a></li>
                <li><a href="{{ URL::route('main.general-journal.index') }}"> Stock by Rack</a></li>
                <li><a href="{{ URL::route('main.general-journal.index') }}"> Stock by Bay</a></li>
                <li><a href="{{ URL::route('main.general-journal.index') }}"> Stock by Pallet</a></li>
                <li><a href="{{ URL::route('main.general-journal.index') }}"> Stock by Item</a></li>
              </ul>
            </li>  -->
          </ul>
        </li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-notebook position-left"></i> Report <span class="caret"></span></a>
          <ul class="dropdown-menu width-250">
            @actionStart('stosk_ledger', ['read'])
            <li><a href="{{ URL::route('main.report-stock-ledger.index') }}"> <i class="icon-stack-text"></i> Stock Ledger</a></li>
            <li><a href="{{ URL::route('main.report-stock-ledger-picking.index') }}"> <i class="icon-stack-text"></i> Stock Ledger Picking</a></li>
            @actionEnd
            @actionStart('stock_opname_monitoring', ['read'])
            <li><a href="{{ URL::route('main.report-stock-opname-raw.index') }}"><i class="icon-file-eye position-left"></i> Stock Opname Raw Monitoring</a></li>
            @actionEnd 
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-users4"></i> RMS
            <span class="visible-xs-inline-block position-right">Share</span>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
              @actionStart('company', ['read'])
              <li><a href="{{ URL::route('main.company.index') }}"><i class="icon-home"></i>Company</a></li>
              @actionEnd
              @actionStart('user', ['read'])
              <li><a href="{{ URL::route('main.user.index') }}"><i class="icon-user"></i>User</a></li>
              @actionEnd
              @actionStart('role', ['read'])
              <li><a href="{{ URL::route('main.role.index') }}"><i class="icon-user-lock"></i> Role</a></li>
              @actionEnd
              @actionStart('role_permission', ['read'])
              <li><a href="{{ URL::route('main.role-permission.index') }}"><i class="icon-accessibility"></i> Privilege</a></li>
              @actionEnd
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-cog3"></i> Setting
            <span class="visible-xs-inline-block position-right">Share</span>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            @actionStart('flag_maintenance', ['read'])
            <li><a href="{{ URL::route('main.maintenance-flag.index') }}"><i class="icon-checkbox-checked position-left"></i>Flag Maintenance</a></li>
            @actionEnd
            @actionStart('preference', ['read'])
            <li><a href="{{ URL::route('main.preference.index') }}"><i class="icon-grid52 position-left"></i> Preference </a> </li>
            @actionEnd
          </ul>
        </li>
      <li>
        @actionStart('user_log', ['read'])
        <a href="{{ URL::route('main.user-log.index') }}"><i class="icon-collaboration position-left"></i> User Log </a>
        @actionEnd
      </li>

      <li>
        @actionStart('monitoring', ['read'])
        <a href="{{ URL::route('main.data-stream-layout.index') }}" target="_blank"><i class="icon-display4 position-left"></i> Monitoring </a>
        @actionEnd
      </li>
<!--
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-display4 position-left"></i> Data Stream <span class="caret"></span></a>
        <ul class="dropdown-menu width-250">
          <li><a target="_blank" href="{{ URL::route('main.data-stream-layout.index') }}"><i class="icon-display4"></i> New Layout</a></li>
          <li><a target="_blank" href="http://wms.lmu.co.id:8002/#/"><i class="icon-display4"></i> Data Stream Stock</a></li>
        </ul>
      </li> -->
    </div>
  </div>
  <!-- /second navbar -->
