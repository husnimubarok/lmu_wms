<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> -->
  <title>LMU WMS -  @yield('title')</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="{{ secure_asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/core.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/components.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/colors.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/customstyle.css') }}" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- toastr notifications -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  {{-- confirm --}}
  <link rel="stylesheet" href="{{ secure_asset('plugins/jQueryConfirm/jquery-confirm.min.css') }}">


  @yield('scripts')


  <!-- Core JS files -->
  <script type="text/javascript" src="{{ secure_asset('js/plugins/loaders/pace.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/core/libraries/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/core/libraries/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/ui/nicescroll.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/ui/drilldown.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/core/libraries/jquery_ui/widgets.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/pages/jqueryui_forms.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/forms/inputs/touchspin.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/pages/form_input_groups.js') }}"></script>

</head>
<body>

<!-- <div class="wrapper">  -->
@include('layouts.main.header')
<!-- Content Wrapper. Contains page content -->
@yield('content')
<!-- /.content-wrapper -->
@include('layouts.main.footer')
<!-- ./wrapper -->
@yield('modal')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{ secure_asset('plugins/jQueryConfirm/jquery-confirm.min.js') }}"></script>

<!-- Theme JS files -->
<script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/extensions/scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/extensions/col_reorder.min.js') }}"></script>

<script type="text/javascript" src="{{ secure_asset('js/core/app.js') }}"></script>
{{-- <script type="text/javascript" src="{{ secure_asset('js/pages/datatables_extension_reorder.js') }}"></script>  --}}
{{-- <script type="text/javascript" src="{{ secure_asset('js/plugins/forms/selects/select2.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ secure_asset('js/pages/datatables_sorting.js') }}"></script>
{{-- <script type="text/javascript" src="{{ secure_asset('js/pages/datatables_extension_scroller.js') }}"></script> --}}
<script type="text/javascript" src="{{ secure_asset('js/plugins/ui/ripple.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/pages/form_select2.js') }}"></script>

<!-- Theme JS files -->
<script type="text/javascript" src="{{ secure_asset('js/plugins/visualization/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/ui/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/plugins/pickers/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ secure_asset('js/plugins/ui/ripple.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ secure_asset('js/plugins/tables/datatables/datatables.min.js') }}"></script> --}}

<script type="text/javascript" src="{{ secure_asset('js/plugins/ui/ripple.min.js') }}"></script>
<script type="text/javascript" src="{{ secure_asset('js/pages/layout_navbar_secondary_fixed.js') }}"></script>
<!-- <script type="text/javascript" src="{{ secure_asset('js/pages/dashboard.js') }}"></script> -->

{{-- add --}}
{{-- <script src="{{ secure_asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script> --}}
{{-- <script src="{{ secure_asset('plugins/datatables/dataTables.tableTools.min.js') }}"></script> --}}
{{-- <script src="{{ secure_asset('plugins/datatables/dataTables.colVis.min.js') }}"></script>  --}}
{{-- <script src="{{ secure_asset('plugins/select2/select2.full.min.js') }}"></script>  --}}
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>  -->
