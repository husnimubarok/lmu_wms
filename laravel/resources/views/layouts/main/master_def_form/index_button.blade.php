<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
    $url = str_replace("-", "_", Request::segment(3));
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
      @actionStart($url, ['delete'])
      <a href="#" onclick="bulk_change_status();" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-minus2 text-indigo-400"></i> <span>archive</span></a>
      @actionEnd
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>
      <a href="#" onClick="reload_table()" class="btn btn-link btn-float has-text text-size-small"><i class=" icon-database-refresh text-indigo-400"></i> <span>Refresh</span></a>
      @actionStart($url, ['create'])
      <a href="@yield("url_variable")/create" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-plus text-indigo-400"></i> <span>Create</span></a>
      @actionEnd
      <!-- <a href="create" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-plus text-indigo-400"></i> <span>Create</span></a> -->
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header
