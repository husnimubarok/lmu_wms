<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>LMU WMS - Login</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/core.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/components.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ secure_asset('css/colors.css') }}" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <script type="text/javascript" src="{{ secure_asset('js/plugins/loaders/pace.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/core/libraries/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/core/libraries/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/ui/nicescroll.min.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/plugins/ui/drilldown.js') }}"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="{{ secure_asset('js/plugins/forms/styling/uniform.min.js') }}"></script>

  <script type="text/javascript" src="{{ secure_asset('js/core/app.js') }}"></script>
  <script type="text/javascript" src="{{ secure_asset('js/pages/login.js') }}"></script>

  <script type="text/javascript" src="{{ secure_asset('js/plugins/ui/ripple.min.js') }}"></script>

</head>

  @yield('content')

<!-- Footer -->
<div class="footer text-muted text-center">
   <a href="#">This product is licensed for PT. LARIS MANIS UTAMA</a><br> 
</div>
<!-- /footer -->
</body>
</html>
