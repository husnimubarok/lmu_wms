<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
  //  ajax: {
  //       url: "{{ URL::route('main.gr-return-manual.data') }}",
  //       data: function (d) {
  //           d.filter_status = $('input[name=filter_status]').val();
  //           d.keyword = $('input[name=keyword]').val();
  //       }
  //   },
    ajax: {
         url: "{{ URL::route('main.gr-return-manual.data') }}",
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val();
            d.keyword = $('input[name=keyword]').val();
        }
   },
   processing: true,
   deferRender: true,
   colReorder: true,
   scrollX: true,
   order: [[ 2, "desc" ]],
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
        $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
              ? '.buttons-excel'
              : null;
          if (btnClass) $buttons.find(btnClass).click();
        })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
     },
   columns: [
   { data: 'check', name: 'check', orderable: false, searchable: false },
   { data: 'action', name: 'action'},
   { data: 'no_transaction', name: 'no_transaction' },
   { data: 'date_transaction', name: 'date_transaction', render: function(d){return moment(d).format("DD-MM-YYYY");} },
  //  { data: 'warehouse_name', name: 'warehouse_name' },
  //  { data: 'room_name', name: 'room_name' },
  //  { data: 'bay_name', name: 'bay_name' },
  //  { data: 'rack_name', name: 'rack_name' },
   { data: 'trdat', name: 'trdat' },
   { data: 'sonum', name: 'sonum' },
   { data: 'donum', name: 'donum' },
   { data: 'grnum', name: 'grnum' },
   { data: 'mjahr', name: 'mjahr' },
   { data: 'description', name: 'description' },
   { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });


  function add()
  {

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

   function active_status()
  {
    $("#filter_status").val('0');
    reload_table();
  }

  function inactive_status()
  {
    $("#filter_status").val('1');
    reload_table();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function add()
  {
    $("#btnSave").attr("onclick","save()");
    $("#btnSaveAdd").attr("onclick","saveadd()");

    $('.errorCustomerTypeName').addClass('hidden');

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

  function change_status(id, no_transaction, status)
  {
      var no_transaction = no_transaction.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + no_transaction + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '@yield("url_variable")/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }

  // function getDataSAP(id)
  // {
  //   waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
  //   //Ajax Load data from ajax
  //   $.ajax({
  //     url: 'gr-return-manual/get-data-sap',
  //     type: "POST",
  //     data: {
  //       '_token': $('input[name=_token]').val(),
  //     },
  //     success: function(data) {
  //       waitingDialog.hide();
  //       reload_table();
  //     },
  //   })
  // };

  function getDataSAP(){
    waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
    axios.get('gr-return-manual/get-empty-nodo')
      .then(response => {
        this.data = response.data;
        console.log(data);
        // if (typeof data !== 'undefined') {
        this.data.forEach((item) => {
           console.log(data);
            axios({
            method: 'post',
            url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-ambilreturndo',
            // headers: {
                          // 'Content-Type': 'application/json',
                      // },
            data: {
                    '_token': $('input[name=_token]').val(),
                    'grreturnwms': item.no_transaction,
                    'nodo': item.do_number,
                  },
            crossdomain: true,
            })
            .then(function (response) {
                waitingDialog.hide();
                console.log(response);

                var trdat = response.data.T_SALRET[0].TRDAT;
                var sonum = response.data.T_SALRET[0].SONUM;
                var donum = response.data.T_SALRET[0].DONUM;
                var grnum = response.data.T_SALRET[0].GRNUM;

                axios({
                  method: 'post',
                  url: 'gr-return-manual/get-data-sap',
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'no_transaction': item.no_transaction,
                          'trdat': trdat,
                          'sonum': sonum,
                          'donum': donum,
                          'grnum': grnum,
                        },
                  })
            })
            .catch(function (response) {
                //handle error
                console.log(response);
            });
          });
        // }
      })
    }


  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);

  $('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>
