@extends('layouts.main.template')
@section('title', 'GR Return')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'gr-return-manual')
@section('val_variable', 'gr_return_manual')
@section('content')
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($gr_return_manual, array('route' => ['main.gr-return-manual.update', $gr_return_manual->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">

              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'GR Return WMS', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'GR Return WMS', 'id' => 'no_transaction', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group" id="div_sonum">
                      {{ Form::label('seq_no', 'SO Num SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('sonum', old('sonum'), ['class' => 'form-control', 'placeholder' => 'SO Num SAP', 'id' => 'sonum', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                    <div class="form-group" id="div_donum">
                      {{ Form::label('seq_no', 'DO Num SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('donum', old('donum'), ['class' => 'form-control', 'placeholder' => 'DO Num SAP', 'id' => 'donum', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                    <div class="form-group" id="div_grnum">
                      {{ Form::label('seq_no', 'GR Num SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('grnum', old('grnum'), ['class' => 'form-control', 'placeholder' => 'GR Num SAP', 'id' => 'grnum', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group" id="div_description">
                      {{ Form::label('seq_no', 'Header Text *', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                      {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Header Text *', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold" id="div_add">
                      ADDITIONAL INFORMATION
                    </legend>
                     <!-- <div class="form-group">
                      {{ Form::label('seq_no', 'Warehouse', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('warehouse_id', $warehouse_list, old('warehouse_id'), array('class' => 'select-search', 'placeholder' => 'Select Warehouse', 'id' => 'warehouse_id')) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Room', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('room_id', $room_list, old('room_id'), array('class' => 'select-search', 'placeholder' => 'Select Room', 'id' => 'room_id')) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('bay_id', 'Bay', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('bay_id', $room_list, old('bay_id'), array('class' => 'select-search', 'placeholder' => 'Select Bay', 'id' => 'bay_id')) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Rack', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('rack_id', $rack_list, old('rack_id'), array('class' => 'select-search', 'placeholder' => 'Select Rack', 'id' => 'rack_id')) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Pallet', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'placeholder' => 'Pallet Number', 'id' => 'pallet_number']) }}
                      </div>
                    </div>  -->

                    <div class="form-group" id="div_trdat">
                      {{ Form::label('seq_no', 'Document Date', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('trdat', old('trdat'), ['class' => 'form-control', 'placeholder' => 'Document Date', 'id' => 'trdat', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group" id="div_mjahr">
                      {{ Form::label('seq_no', 'Doc Year', ['class' => 'col-lg-3 control-label']) }}

                      <div class="col-lg-9">
                        {{ Form::text('mjahr', old('mjahr'), ['class' => 'form-control', 'placeholder' => 'Doc Year', 'id' => 'mjahr', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Posting Date', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($gr_return_manual))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($gr_return_manual->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Posting Date', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Posting Date', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="div_return_reason_code">
                      {{ Form::label('seq_no', 'Order Reason', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('return_reason_code', $reason_list, old('return_reason_code'), array('class' => 'select-search', 'placeholder' => 'Select Reason', 'id' => 'return_reason_code')) }}
                      </div>
                    </div>

                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>
                        <th>Action</th>
                        <th>Item</th>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Warehouse Name</th>
                        <th>Room</th>
                        <th>Bay</th>
                        <th>Rack</th>
                        <th>Pallet Number</th>
                        <th>UOM</th>
                        <th>Plant</th>
                        <th>Store Loc</th>
                        <th>DO Number</th>
                        <th>Batch</th>
                      </tr>
                    </thead>
                    @if(strlen($gr_return_manual->grnum)==0)
                    <thead>
                      <tr>
                      <td>
                        <a href="#" class="btn btn-primary btn-xs btn-float legitRipple" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                        <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a>
                        <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                      </td>
                      <td>
                        {{ Form::text('item_line', old('item_line'), ['class' => 'form-control', 'id' => 'item_line', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('inventory_code', old('inventory_code'), ['class' => 'form-control', 'id' => 'inventory_code', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'id' => 'inventory_name', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::number('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity']) }}
                      </td>
                      <td>
                        {{ Form::text('warehouse_name', old('warehouse_name'), ['class' => 'form-control', 'id' => 'warehouse_name', 'readonly']) }}
                        {{ Form::hidden('warehouse_id', old('warehouse_id'), ['class' => 'form-control', 'id' => 'warehouse_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('room_name', old('room_name'), ['class' => 'form-control', 'id' => 'room_name', 'readonly']) }}
                        {{ Form::hidden('room_id', old('room_id'), ['class' => 'form-control', 'id' => 'room_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('bay_name', old('bay_name'), ['class' => 'form-control', 'id' => 'bay_name', 'readonly']) }}
                        {{ Form::hidden('bay_id', old('bay_id'), ['class' => 'form-control', 'id' => 'bay_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('rack_name', old('rack_name'), ['class' => 'form-control', 'id' => 'rack_name', 'readonly']) }}
                        {{ Form::hidden('rack_id', old('rack_id'), ['class' => 'form-control', 'id' => 'rack_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number', 'readonly']) }}
                      </td>
                      <td>
                        <!-- {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code', 'readonly']) }} -->
                        <select class="select" id="unit_of_measure_code" name="unit_of_measure_code">
                          @foreach($uom_list as $uom_lists)
                          <option value="{{$uom_lists->unit_of_measure_code}}">{{$uom_lists->unit_of_measure_code}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select-search" id="plant_transfer" name="plant_transfer">
                          @foreach($plant_list as $plant_lists)
                          <option value="{{$plant_lists->store_location_code}}">{{$plant_lists->store_location_name}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select-search" id="store_loc_transfer" name="store_loc_transfer">
                          @foreach($store_loc_list as $store_loc_lists)
                          <option value="{{$store_loc_lists->store_location_code}}">{{$store_loc_lists->store_location_name}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <div class="input-group">
                          {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }}
                          {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }}
                          {{ Form::hidden('sales_return_detail_id', old('sales_return_detail_id'), ['class' => 'form-control', 'id' => 'sales_return_detail_id']) }}
                          {{ Form::text('do_number', old('do_number'), ['class' => 'form-control', 'id' => 'do_number']) }}
                          <!-- <span class="input-group-btn">
                            <a href="#" class="btn btn-primary btn-xs btn-float legitRipple" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                          </span> -->
                         </div>
                      </td>
                      <td>
                        {{ Form::text('batch', old('batch'), ['class' => 'form-control', 'id' => 'batch']) }}
                      </td>

                      </tr>
                    </thead>
                    @endif
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              <!-- <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button> -->
              @if($gr_return_manual->code_transaction == 'GPOD')
              <a href="#"  class="btn btn-danger" onclick="cancelDOPOD();"> Cancel POD <i class="icon-cancel-circle2 position-right"></i></a>
              @if(strlen($gr_return_manual->grnum)==0)
              <a href="#"  class="btn btn-primary" onclick="postSAP();"> SAVE POD <i class="icon-arrow-right14 position-right"></i></a>
              @endif
              @else
              <a href="#"  class="btn btn-danger" onclick="cancelDO();"> CANCEL WITH DO <i class="icon-cancel-circle2 position-right"></i></a>
              @if(strlen($gr_return_manual->grnum)==0)
              <a href="#"  class="btn btn-primary" onclick="postSAPDO();"> SAVE WITH DO <i class="icon-arrow-right14 position-right"></i></a>

              @endif
              @endif
            </div>
          </div>
          <!-- <a href="#"  class="btn btn-primary" onclick="syncSAP();"> SYNC <i class="icon-arrow-right14 position-right"></i></a> -->

        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:80% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Inventory</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$gr_return_manual->code_transaction}}">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Item</th>
                <th>Material</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Warehouse Name</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet Number</th>
                <th>Plant</th>
                <th>Store Loc</th>
                <th>DO Number</th>
                <th>Batch</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
        <button onclick="pick();"  class="btn btn-primary legitRipple" type="button"><i class="icon-touch"></i> Pick</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    var code_trans = '{{$gr_return_manual->code_transaction}}';

    if(code_trans=='GPOD')
    {
      $('#div_return_reason_code').hide();
      $('#div_trdat').hide();
      $('#div_sonum').hide();
      $('#div_donum').hide();
      $('#div_grnum').hide();
      $('#div_mjahr').hide();
      $('#div_add').hide();
    }else{
      $('#div_description').hide();
    };

    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
    ajax: {
      url: "{{ url('main/warehouse/gr-return-manual/get-detail') }}/{{$gr_return_manual->id}}",
      type : "POST",
      headers : {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
     },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'item_line', name: 'item_line' },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'inventory_unit_name', name: 'inventory_unit_name' },
     { data: 'plant', name: 'plant' },
     { data: 'store_loc', name: 'store_loc' },
     { data: 'do_number', name: 'do_number' },
     { data: 'batch', name: 'batch' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });

  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ url('main/warehouse/gr-return-manual/data-modal') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.code_transaction = $('input[name=code_transaction]').val();
              d.keyword = $('input[name=keyword]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
    //  { data: 'action', name: 'action'},
     { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'item_line', name: 'item_line' },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'plant', name: 'plant' },
     { data: 'store_loc', name: 'store_loc' },
     { data: 'do_number', name: 'do_number' },
     { data: 'batch', name: 'batch' },

     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  function pick()
 {
  var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    if (list_id.length > 0) {
       var date_transaction = $('#date_transaction').val();
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to pick '+list_id.length+' data?',
            type: 'green',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'YES PICK',
                    btnClass: 'btn-green',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                                'date_transaction': date_transaction.replace("-","").replace("-",""),
                            },
                            url: "../../gr-return-manual/pick/{{$gr_return_manual->id}}",
                            type: "POST",
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = {
                                        "positionClass": "toast-bottom-right",
                                        "timeOut": 1000,
                                    };
                                    toastr.success('Success pick data!', 'Success Alert', options);
                                    reload_table();
                                    reload_table_inventory();
                                    $('#modal_form').modal('toggle');

                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'Pick data failed!',
                                    });
                                    reload_table();
                                    reload_table_inventory();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Pick data failed!',
                                });
                                reload_table();
                                reload_table_inventory();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorGR ReturnName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal

    reload_table_inventory();
    reload_table();
  }

  function addValue(str, warehouse_id, bay_id, room_id, rack_id, inventory_id, id_detail){
    clearDetail();
    var inventory_id = inventory_id;
    var do_number = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_code = $(str).closest('tr').find('td:eq(2)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(3)').text();
    var item_line = $(str).closest('tr').find('td:eq(4)').text();
    var quantity = $(str).closest('tr').find('td:eq(5)').text();
    var note = $(str).closest('tr').find('td:eq(1)').text();
    var plant = $(str).closest('tr').find('td:eq(6)').text();
    var store_loc = $(str).closest('tr').find('td:eq(7)').text();

    var warehouse_name = $(str).closest('tr').find('td:eq(8)').text();
    var room_name = $(str).closest('tr').find('td:eq(9)').text();
    var bay_name = $(str).closest('tr').find('td:eq(10)').text();
    var rack_name = $(str).closest('tr').find('td:eq(11)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(12)').text();
    var batch = $(str).closest('tr').find('td:eq(13)').text();

    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);

    $("#warehouse_id").val(warehouse_id);
    $("#room_id").val(room_id);
    $("#bay_id").val(bay_id);
    $("#rack_id").val(rack_id);
    $("#sales_return_detail_id").val(id_detail);

    $("#inventory_id").val(inventory_id);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#do_number").val(do_number);
    $("#item_line").val(item_line);
    $("#quantity").val(quantity);
    $("#plant").val(plant);
    $("#store_loc").val(store_loc);
    $("#batch").val(batch);
    setBtnAdd();
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var inventory_id = $('#inventory_id').val();
     var unit_of_measure_code =   $('#unit_of_measure_code').val();
     var quantity =   $('#quantity').val();

     if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../gr-return-manual/post-detail/{{$gr_return_manual->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'inventory_code': $('#inventory_code').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'item_line': $('#item_line').val(),
          'do_number': $('#do_number').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'batch': $('#batch').val(),
          'sales_return_detail_id': $('#sales_return_detail_id').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(inventory_id);

    var do_number = $(str).closest('tr').find('td:eq(13)').text();
    var inventory_code = $(str).closest('tr').find('td:eq(2)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(3)').text();
    var item_line = $(str).closest('tr').find('td:eq(1)').text();
    var quantity = $(str).closest('tr').find('td:eq(4)').text();
    var plant = $(str).closest('tr').find('td:eq(10)').text();
    var store_loc = $(str).closest('tr').find('td:eq(11)').text();
    var warehouse_name = $(str).closest('tr').find('td:eq(5)').text();
    var room_name = $(str).closest('tr').find('td:eq(6)').text();
    var bay_name = $(str).closest('tr').find('td:eq(7)').text();
    var rack_name = $(str).closest('tr').find('td:eq(8)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(9)').text();
    var batch = $(str).closest('tr').find('td:eq(14)').text();

    $("#id_detail").val(id_detail);
    $("#inventory_id").val(inventory_id);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#do_number").val(do_number);
    $("#item_line").val(item_line);
    $("#quantity").val(quantity);
    $("#plant").val(plant);
    $("#store_loc").val(store_loc);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#batch").val(batch);
    // $('#unit_of_measure_code').val(unit_of_measure_code).trigger('change.select2');

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =  $('#quantity').val();

    if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../gr-return-manual/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'item_line': $('#item_line').val(),
          'do_number': $('#do_number').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'batch': $('#batch').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_code').val(''),
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#do_number').val(''),
    $('#item_line').val(''),
    $('#plant').val(''),
    $('#store_loc').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#pallet_number').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#batch').val(''),
    $('#sales_return_detail_id').val('')
  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'DELETE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../gr-return-manual/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };


  function syncSAP(){
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exe-tambah-item',
          url: '../sap-exe-tambah-item',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
        data: {
                '_token': $('input[name=_token]').val(),
                'grreturnwms': "{{$gr_return_manual->no_transaction}}",
            },
        // crossdomain: true,
      })
      .then(function (response) {
          console.log('test promise');
          // var customer_code = response.data.IT_GRTPOD[0].KUNAG;
          // var customer_name = response.data.IT_GRTPOD[0].SOLT_NAME;
          axios({
            method: 'post',
            url: '../post-response/{{$gr_return_manual->id}}',
            data: {
                    '_token': $('input[name=_token]').val(),
                    'description': $('#description').val(),
                    'grnum': response.data.IT_GRTPOD[0].GRNUM,
                    'mjahr': response.data.IT_GRTPOD[0].MJAHR,
                    // 'customer_code': customer_code,
                    // 'customer_name': customer_name,
                  },
            })
          waitingDialog.hide();

          if(response.data.IT_GRTPOD[0].STATUS == 'Success')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../gr-return-manual');

          };

          if(response.data.IT_GRTPOD[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.IT_GRTPOD[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          waitingDialog.hide();
          // window.location.assign('../../goods-receive');
      })
    //   .catch(function (response) {
    //       //handle error
    //       // alert('Sync data SAP Error!');
    //       $.alert({
    //       type: 'red',
    //       icon: 'fa fa-red', // glyphicon glyphicon-heart
    //       title: 'Error',
    //       // content: response.msg,
    //       content: 'Synchronize data to SAP Error!',
    //       });
    // });
  };

  function postSAP(){
    // alert($('#description').val());
    waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
    axios.get('../data-detail/{{$gr_return_manual->id}}')
      .then(response => {
        this.data = response.data;
        this.data.forEach((item) => {

            // start loop
              var i = 0;
              function myLoop() {
              setTimeout(function () {
            // ----

            axios({
            method: 'post',
            // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-tambahitem',
            url: '../sap-tambahitem',
            // headers: {
                          // 'Content-Type': 'application/json',
                          // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                      // },
            data: {
                    'grreturnwms': item.grreturnwms,
                    'matrial': item.matrial,
                    'qty': item.qty.toString(),
                    'plant': item.plant,
                    'storloc': item.storloc,
                    'note': $('#description').val(),
                    'createdname': item.createdname,
                    'itemnya': item.item_line,
                    'total_items': item.total_items.toString(),
                  },
            // crossdomain: true,
            })
            .then(function (response) {
                console.log(item.grreturnwms);
                console.log(item.matrial);
                console.log(item.qty).toString(),
                console.log(item.plant);
                console.log(item.storloc);
                console.log($('#description').val());
                console.log(item.total_items.toString());
                console.log(item.createdname);
                // console.log(item.bendara);

                // alert(item.item_line);

                //handle success
                console.log(response);

                // if(item.bendara == 'X'){
                  console.log('end');

                  // console.log(response.data.T_SALRET[0].GRNUM);
                  // console.log(response.data.T_SALRET[0].MJAHR);

                  var grnum = response.data.IT_GRTPOD[0].GRNUM;
                  var mjahr = response.data.IT_GRTPOD[0].MJAHR;
                  // var customer_code = response.data.IT_GRTPOD[0].KUNAG;
                  // var customer_name = response.data.IT_GRTPOD[0].SOLT_NAME;


                  if(response.data.IT_GRTPOD[0].STATUS == 'NOT SUCCESS')
                  {
                    $.alert({
                      type: 'red',
                      icon: 'fa fa-red', // glyphicon glyphicon-heart
                      title: 'Error',
                      content: response.data.IT_GRTPOD[0].MESSAGE,
                    });
                  };

                  if(response.data.IT_GRTPOD[0].STATUS == 'Continue')
                  {
                    var options = {
                                      "positionClass": "toast-bottom-right",
                                      "timeOut": 5000,
                                  };
                    toastr.success('Post Line SAP Success', 'Success Alert', options);
                  };

                  if(response.data.IT_GRTPOD[0].STATUS == 'Success')
                  {
                      var options = {
                          "positionClass": "toast-bottom-right",
                          "timeOut": 5000,
                      };
                      toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                      console.log('end');
                      syncSAP();
                  };

                  // axios({
                  // method: 'post',
                  // url: '../post-response/{{$gr_return_manual->id}}',
                  // data: {
                  //         '_token': $('input[name=_token]').val(),
                  //         'description': $('#description').val(),
                  //         'grnum': response.data.IT_GRTPOD[0].GRNUM,
                  //         'mjahr': response.data.IT_GRTPOD[0].MJAHR,
                  //         'customer_code': customer_code,
                  //         'customer_name': customer_name,
                  //       },
                  // })

                  // waitingDialog.hide();
                  // alert('loading hide');
                  // $.alert({
                  //     type: 'green',
                  //     icon: 'fa fa-success', // glyphicon glyphicon-heart
                  //     title: 'Alert',
                  //     content: 'Post Data SAP Success!',
                  // });
                  // window.location.assign('../../gr-return-manual');
                // }
            })
            .catch(function (response) {
                //handle error
                console.log(response);
                $.alert({
                  type: 'red',
                  icon: 'fa fa-red', // glyphicon glyphicon-heart
                  title: 'Error',
                  // content: response.msg,
                  content: 'Post data SAP Error, check your connection or call your IT administrator!',
                });

                waitingDialog.hide();
            });
            // ----
                i++;
                // if (i < 4) {
                //     myLoop();
                // }
            }, 5000)
          }
               myLoop();
            // loop
          });
      })
    }


  // ===================================================================== POD/SPM =====================================


  function syncSAPDO(){
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exe-tambah-item',
          url: '../sap-exe-tambah-item',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
      data: {
              '_token': $('input[name=_token]').val(),
              'grreturnwms': "{{$gr_return_manual->no_transaction}}",
          },
      })
      .then(function (response) {
          console.log('test promise');
          console.log(response.data.T_DATA[0].VBELN);
          // var customer_code = response.data.IT_GRTPOD[0].KUNAG;
          // var customer_name = response.data.IT_GRTPOD[0].SOLT_NAME;
          axios({
            method: 'post',
            url: '../post-response/{{$gr_return_manual->id}}',
            data: {
                    '_token': $('input[name=_token]').val(),
                    'return_reason_code': $('#return_reason_code').val(),
                    'grnum': response.data.T_SECRET[0].GRRET,
                    'mjahr': response.data.T_SECRET[0].VBELN,
                    // 'customer_code': customer_code,
                    // 'customer_name': customer_name,
                  },
            crossdomain: true,
            })
          waitingDialog.hide();

          if(response.data.T_SECRET[0].STATUS == 'Success')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

          };

          if(response.data.GRRET[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.GRRET[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          waitingDialog.hide();
          window.location.assign('../../gr-return-manual');

          // window.location.assign('../../goods-receive');
      })
    //   .catch(function (response) {
    //       //handle error
    //       // alert('Sync data SAP Error!');
    //       $.alert({
    //       type: 'red',
    //       icon: 'fa fa-red', // glyphicon glyphicon-heart
    //       title: 'Error',
    //       // content: response.msg,
    //       content: 'Synchronize data to SAP Error!',
    //       });
    // });
  };

  function postSAPDO(){
    // alert($('#description').val());
    waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
    axios.get('../data-detail/{{$gr_return_manual->id}}')
      .then(response => {
        this.data = response.data;
        this.data.forEach((item) => {

            // // start loop
            //   var i = 0;
            //   function myLoop() {
            //   setTimeout(function () {
            // // ----

            axios({
            method: 'post',
            // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-returndo',
            url: '../sap-returndo',
            // headers: {
                          // 'Content-Type': 'application/json',
                          // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                      // },
            data: {
                    'grreturnwms': item.grreturnwms,
                    'nodo': item.do_number,
                    'itemdo': item.item_line,
                    'matrial': item.matrial,
                    'qty': item.qty.toString(),
                    'orderreason': $('#return_reason_code').val(),
                    'total_items': item.total_items.toString(),
                    // 'note': $('#description').val(),

                    // 'grreturnwms': item.grreturnwms,
                    // 'matrial': item.matrial,
                    // 'qty': item.qty,
                    // 'plant': item.plant,
                    // 'storloc': item.storloc,
                    // 'note': $('#description').val(),
                    // 'createdname': item.createdname,
                    // 'item': item.item_line,
                    // 'total_items': item.total_items.toString(),
                  },
                // crossdomain: true,
            })
            .then(function (response) {
                console.log(item.grreturnwms);
                console.log(item.matrial);
                console.log(item.qty);
                console.log(item.plant);
                console.log(item.storloc);
                console.log($('#return_reason_code').val());
                console.log('total item' + item.total_items);

                // alert(item.item_line);

                //handle success
                console.log(response);

                // if(item.bendara == 'X'){
                  // console.log('end');


                  if(response.data.T_SECRET[0].STATUS == 'NOT SUCCESS')
                  {
                    $.alert({
                      type: 'red',
                      icon: 'fa fa-red', // glyphicon glyphicon-heart
                      title: 'Error',
                      content: response.data.T_SECRET[0].MESSAGE,
                    });
                  };

                  if(response.data.T_SECRET[0].STATUS == 'Continue')
                  {
                    var options = {
                                      "positionClass": "toast-bottom-right",
                                      "timeOut": 5000,
                                  };
                    toastr.success('Post Line SAP Success', 'Success Alert', options);
                  };

                  if(response.data.T_SECRET[0].STATUS == 'Success')
                  {
                      var options = {
                          "positionClass": "toast-bottom-right",
                          "timeOut": 5000,
                      };
                      toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                      console.log('end');
                      // syncSAPDO();
                      waitingDialog.hide();

                      $.alert({
                        type: 'green',
                        icon: 'fa fa-green', // glyphicon glyphicon-heart
                        title: 'Success',
                        content: 'Synchronize data SAP Success!',
                      });
                  };
                  // console.log(response.data.T_SALRET[0].TRDAT);
                  // console.log(response.data.T_SALRET[0].SONUM);
                  // console.log(response.data.T_SALRET[0].DONUM);
                  // console.log(response.data.T_SALRET[0].GRNUM);

                  // var trdat = response.data.T_SALRET[0].TRDAT;
                  // var sonum = response.data.T_SALRET[0].SONUM;
                  // var donum = response.data.T_SALRET[0].DONUM;
                  // var grnum = response.data.T_SALRET[0].GRNUM;

                  // var customer_code = response.data.T_SALRET[0].KUNAG;
                  // var customer_name = response.data.T_SALRET[0].SOLT_NAME;

                  // axios({
                  // method: 'post',
                  // url: '../post-response/{{$gr_return_manual->id}}',
                  // data: {
                  //         '_token': $('input[name=_token]').val(),
                  //         'return_reason_code': $('#return_reason_code').val(),
                  //         'warehouse_id': $('#warehouse_id').val(),
                  //         'room_id': $('#room_id').val(),
                  //         'bay_id': $('#bay_id').val(),
                  //         'rack_id': $('#rack_id').val(),
                  //         'pallet_number': $('#pallet_number').val(),
                  //         // 'trdat': trdat,
                  //         // 'sonum': sonum,
                  //         // 'donum': donum,
                  //         // 'grnum': grnum,
                  //         'trdat': '-',
                  //         'sonum': '-',
                  //         'donum': '-',
                  //         'grnum': '-',
                  //         // 'customer_code': customer_code,
                  //         // 'customer_name': customer_name,
                  //       },
                  // })
                  // waitingDialog.hide();
                  // window.location.assign('../../gr-return-manual');
                // }
            })
            // .catch(function (response) {
            //     //handle error
            //     console.log(response);
            // });
          //   // ----
          //       i++;
          //       // if (i < 4) {
          //       //     myLoop();
          //       // }
          //   }, 2000)
          // }
          //      myLoop();
          //   // loop
          });
      })
    }

    function cancelDO(){
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to cancel?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: 'CANCEL',
          btnClass: 'btn-red',
          action: function () {
            waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
            axios({
              method: 'post',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgido',
                  url: '../sap-cancelgido',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
              data: {
                      '_token': $('input[name=_token]').val(),
                      'nodo': '{{$gr_return_manual->donum}}',
                  },
              crossdomain: true,
              })
              .then(function (response) {
                  axios({
                    method: 'post',
                    url: '../../gr-return-manual/cancel/{{$gr_return_manual->id}}',
                    data: {
                            '_token': $('input[name=_token]').val(),
                          },
                  })
                  //handle success
                  console.log(response);
                  waitingDialog.hide();
                  // window.location.assign('../../delivery-order');
              })
              .catch(function (response) {
                  //handle error
                  console.log(response);
                  $.alert({
                    type: 'red',
                    icon: 'fa fa-red', // glyphicon glyphicon-heart
                    title: 'Error',
                    // content: response.msg,
                    content: 'Post data SAP Error, check your connection or call your IT administrator!',
                  });

                  waitingDialog.hide();

              });
            }
        },
      }
   });
  }

  function cancelDOPOD(){
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to cancel {{$gr_return_manual->no_transaction}} number?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: 'YES',
          btnClass: 'btn-red',
          action: function () {
            waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
            axios({
              method: 'post',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgr',
                  url: '../sap-cancelgr',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'notransaction': "{{$gr_return_manual->no_transaction}}",
                          'nodoc': "{{$gr_return_manual->grnum}}",
                          'tahundoc': "2020",
                          'nopo': "0000000001",
                      },
                  crossdomain: true,
              })
              .then(function (response) {
                  axios({
                    method: 'post',
                    url: '../../gr-return-manual/cancel/{{$gr_return_manual->id}}',
                    data: {
                            '_token': $('input[name=_token]').val(),
                          },
                  })

                  //handle success
                  console.log(response);
                  waitingDialog.hide();
                  // window.location.assign('../../goods-receive');
              })
              .catch(function (response) {
                  //handle error
                  console.log(response);
                  $.alert({
                    type: 'red',
                    icon: 'fa fa-red', // glyphicon glyphicon-heart
                    title: 'Error',
                    // content: response.msg,
                    content: 'Post data SAP Error, check your connection or call your IT administrator!',
                  });

                  waitingDialog.hide();

              });
            }
        },
      }
   });
  }

  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);
</script>
@stop
