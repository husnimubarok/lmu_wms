@extends('layouts.main.template')
@section('title', 'Material Used')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'po-revision')   
@section('val_variable', 'po_revision')   
@section('content') 
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
          {!! Form::model($po_revision, array('route' => ['main.po-revision.update', $po_revision->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!} 
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
                 
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group"> 
                      {{ Form::label('seq_no', 'No Transaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'no_transaction', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Date Trancaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        <div class="input-group">
                          @if(isset($po_revision))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($po_revision->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div> 
 
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      ADDITIONAL INFORMATION
                    </legend> 
                     <div class="form-group"> 
                      {{ Form::label('seq_no', 'PO Number', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::select('purchase_order_id', $purchase_order_list, old('purchase_order_id'), array('class' => 'select-search', 'placeholder' => 'Select PO Number', 'id' => 'purchase_order_id', 'onchange' => 'saveDetail();')) }} 
                      </div>
                    </div>

                    <div class="form-group"> 
                      {{ Form::label('seq_no', 'PO Number New', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::text('po_number_new', old('po_number_new'), ['class' => 'form-control', 'placeholder' => 'PO Number New', 'id' => 'po_number_new']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12"> 
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>   
                        <th>Inventory Name</th> 
                        <!-- <th>Unit</th> -->
                        <th>Quantity</th> 
                        <th>Action</th> 
                      </tr>
                    </thead>
                    <thead>
                      <tr>
                      <td>
                        <div class="input-group">
                          {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }} 
                          {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }} 
                          {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'id' => 'inventory_name', 'readonly']) }} 
                          <span class="input-group-btn">
                            <a href="#" class="btn btn-primary btn-float btn-xs" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                          </span>
                         </div>
                      </td> 
                      <td>
                        {{ Form::text('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity', 'readonly']) }}
                      </td> 
                      <td>
                        <!-- <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a> -->
                        <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                      </td>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table> 
                </div>
              </div>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:70% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange"> 
         <h5 class="modal-title">Data Inventory</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>   
                <th>Inventory Code</th> 
                <th>Inventory Name</th>
                <th>UOM</th>
                <!-- <th>Inventory Group</th>  -->
                <th>Description</th> 
                <th>Action</th> 
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
      </div>
      <div class="modal-footer"> 
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button">Refresh</button> 
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script type="text/javascript">  
  var table;
  var status = 0;
  $(document).ready(function() {
    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({ 
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
     ajax: {
          url: "{{ url('main/warehouse/po-revision/get-detail') }}/{{$po_revision->id}}", 
          data: function (d) {
              d.filter_status = $('input[name=filter_status]').val(); 
          }
      },
     processing: true, 
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
       }, 
     columns: [  
     { data: 'inventory_name', name: 'inventory_name' },
    //  { data: 'inventory_unit_name', name: 'inventory_unit_name' }, 
     { data: 'quantity', name: 'quantity' }, 
     { data: 'action', name: 'action'}, 
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });

  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({ 
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
     ajax: {
          url: "{{ url('main/setting/inventory/data-modal') }}", 
          data: function (d) {
              d.filter_status = $('input[name=filter_status]').val(); 
          }
      },
     processing: true, 
     deferRender: true,
     colReorder: true,
     ordering: true,
     pageLength: 5,
     autowidth: false,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
       }, 
     columns: [  
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'unit_of_measure_name', name: 'unit_of_measure_name' },
    //  { data: 'inventory_group_name', name: 'inventory_group_name' },
     { data: 'description', name: 'description' },  
     { data: 'action', name: 'action'}, 
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax 
  }

  function openModal()
  {
  //  $("#btnSave").attr("onclick","save()");
  //  $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorMaterial UsedName').addClass('hidden');

   save_method = 'add'; 
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
  }  

  function addValue(str, id){
    clearDetail();
    var inventory_id = id;
    var inventory_name = $(str).closest('tr').find('td:eq(1)').text(); 
    var note = $(str).closest('tr').find('td:eq(1)').text(); 

    $("#inventory_id").val(inventory_id);
    $("#inventory_name").val(inventory_name); 
    // setBtnAdd();
    $('#modal_form').modal('toggle');
    
  }

  function setBtnAdd() {
    // $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    // $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var purchase_order_id = $('#purchase_order_id').val(); 
     var purchase_order_number = $("#purchase_order_id option:selected").text();
     
     $('#po_number_new').val(purchase_order_number); 
    //  alert(purchase_order_number);
     
     if(purchase_order_id == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';  

      //Ajax Load data from ajax
      $.ajax({
        url: '../../po-revision/post-detail/{{$po_revision->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(), 
          'purchase_order_id': $('#purchase_order_id').val() 
        },
        success: function(data) { 
          reload_table(); 
          clearDetail(); 
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id)
   {
    clearDetail();
    console.log(str); 
    console.log(id_detail); 
    console.log(inventory_id); 

    var inventory_name = $(str).closest('tr').find('td:eq(0)').text(); 
    var quantity = $(str).closest('tr').find('td:eq(1)').text();  
    // var quantity = $(str).closest('tr').find('td:eq(2)').text(); 

    $("#id_detail").val(id_detail);
    $("#inventory_id").val(inventory_id);
    $("#inventory_name").val(inventory_name); 
    $("#quantity").val(quantity);  
    
    setBtnUpdate();
  };


  function updateDetail(id)
  { 
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val(); 

    if(inventory_id == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';  

      //Ajax Load data from ajax
      $.ajax({
        url: '../../po-revision/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'id_detail': $('#id_detail').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(), 
          'quantity': $('#quantity').val(), 
        },
        success: function(data) { 
          reload_table(); 
          clearDetail(); 
          setBtnAdd(); 
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax 
  }

  function clearDetail()
  {
    
  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () { 
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../po-revision/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val() 
            },
            success: function(data)
            { 
              reload_table();
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };
</script>
@stop


