@extends('layouts.report.template')
@section('title', 'Report Stock Summary')
@section('caption', 'Report for stock ending summary!')
@section('url_variable', 'stock-transfer')
@section('val_variable', 'stock_transfer')
@section('content')
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th>Warehouse Name</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <!-- <th>Unit</th> -->
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- End Bootstrap modal -->
@include('main.warehouse.report_stock_summary.script_warehouse')
@include('scripts.change_status')
@stop
