@extends('layouts.main.template')
@section('title', 'Material Used')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'material_used')   
@section('val_variable', 'material_used')   
@section('content') 
@include('layouts.main.transaction_form.form_button')



<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
          {!! Form::model($material_used, array('route' => ['main.material-used.update', $material_used->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!} 
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
                 <div class="form-group pull-right">
                    <label for="real_name" class="col-sm-4 control-label">Status: &nbsp; &nbsp; </label>
                    <div class="col-sm-8 pull-right">
                      <select class="select" name="status" id="status">
                       <option value="0">Active</option>
                       <option value="1">Inactive</option>
                     </select>
                   </div>
                 </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"></legend>

                    <div class="form-group"> 
                      {{ Form::label('seq_no', 'No Transaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'no_transaction']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Date Trancaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        <div class="input-group">
                          @if(isset($material_used))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($material_used->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div> 
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                    </legend> 
                     <div class="form-group"> 
                      {{ Form::label('seq_no', 'Warehouse', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::select('warehouse_id', $warehouse_list, old('warehouse_id'), array('class' => 'select', 'placeholder' => 'Select Warehouse', 'id' => 'warehouse_id')) }} 
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

                <hr>
                <div class="col-md-12">
                  <div id="grid_table"></div>
                </div>
              </div>

              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- /core JS files -->
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> --}}
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
<script>
 
$('#grid_table').jsGrid({

 width: "100%",
 height: "400px",
 filtering: true, 
 inserting:true,
 editing: true, 
 sorting: true,
 paging: true,
 autoload: true,
 pageSize: 10,
 pageButtonCount: 5,
 deleteConfirm: "Do you really want to delete data?",
 controller: {
  loadData: function(filter){
   return $.ajax({
    type: "GET",
    url: "{{ URL::route('main.material-used.get-detail', $material_used->id) }}",
    data: filter
   });
  },
 insertItem: function(item){
   return $.ajax({
    type: "POST",
    url: "{{ URL::route('main.material-used.post-detail', $material_used->id) }}",
    data: {
        '_token': $('input[name=_token]').val(), 
        item
      },
   });
  }, 
  updateItem: function(item){
   return $.ajax({
    type: "PUT",
    url: "{{ URL::route('main.material-used.put-detail') }}",
    data: {
        '_token': $('input[name=_token]').val(), 
        item
      },
   });
  }, 
  deleteItem: function(item){
   return $.ajax({
    type: "DELETE",
    url: "{{ URL::route('main.material-used.delete-detail') }}",
    data: {
        '_token': $('input[name=_token]').val(), 
        item
      },
   });
  }, 
 },

 
 fields: [
  {
    name: "id",
    type: "hidden",
    css: 'hide'
  },  
  {
    name: "Inventory Name", 
    type: "select", 
    class: "form-control", 
    align: "left",
    items: [
     { Name: "Select Inventory", Id: 0 },
     @foreach ($inventory_list as $inventory_lists)
     { Name: "{{$inventory_lists->inventory_name}}", Id: {{$inventory_lists->id}} },
     @endforeach
    ], 
    valueField: "Id", 
    textField: "Name", 
    validate: "required"
  },  
  {
      name: "Unit", 
      type: "text", 
      align: "left",
      class: "form-control", 
      width: 50,  
  }, 
  {
    name: "Quantity", 
    type: "text", 
    align: "right",
    width: 50, 
  validate: function(value)
  {
   if(value > 0)
   {
    return true;
   }
  }
}, 
{
 type: "control"
}
 ]
});

</script>
@stop


