@extends('layouts.main.template')
@section('title', 'GR Revision')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'gr-revision')   
@section('val_variable', 'gr_revision')   
@section('content') 
@include('layouts.main.transaction_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.transaction_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr> 
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th> 
                <th>No Transaction</th> 
                <th>Date Transaction</th>
                <th>GR Number</th> 
                <th>GR Date</th> 
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:30% !important">
    <div class="modal-content">
      {!! Form::open(array('route' => 'main.gr-revision.store', 'files' => 'true', 'id' => 'form', 'class' => 'form-horizontal'))!!}
      {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Select Transaction</h5>
       </div>
       <div class="modal-body">
          <div style="display:none">
            <select class="select" name="code_transaction" id="code_transaction">
              <option value="GRRV">GR REVISION</option> 
            </select>
           </div>
      </div>
      <div class="modal-footer"> 
        @include('layouts.main.transaction_form.modal_button')
      </div>
    {!! Form::close() !!}
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@include('main.warehouse.gr_revision.script')
@include('scripts.change_status')
@stop


