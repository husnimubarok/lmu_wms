@extends('layouts.main.template')
@section('title', 'Stock Opname')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'stock_opname')
@section('val_variable', 'stock_opname')
@section('content')
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($stock_opname, array('route' => ['main.stock-opname.update', $stock_opname->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">

              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'No Transaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'no_transaction', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Date Trancaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($stock_opname))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($stock_opname->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>

                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      STOCK INFORMATION
                    </legend>
                     <!-- <div class="form-group">
                      {{ Form::label('seq_no', 'Room', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('room_id', $room_list, old('room_id'), array('class' => 'select-search', 'placeholder' => 'Select Room', 'id' => 'room_id')) }}
                      </div>
                    </div> -->
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

                <!-- Generate -->
                  <div class="col-md-12">
                    <div class="text-left">
                      <a href=# onclick="generate();" class="btn btn-sm btn-danger">Get Stock <i class="icon-magic-wand position-right"></i></a>
                      <a href=# onclick="reload_table(); return false;" class="btn btn-sm btn-success">Refresh <i class="icon-database-refresh position-right"></i></a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <hr>
                  </div>
                <div class="col-md-12">
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>
                         <th rowspan="2">Action</th>
                         <th rowspan="2">Status</th>
                         <th rowspan="2">Material</th>
                         <th rowspan="2">Description</th>
                         <th colspan="3" style="background-color:#dc5d5d"><center>Qty Opname</center></th>
                         <th rowspan="2">UOM</th>
                         <th rowspan="2">Batch</th>
                         <th rowspan="2">Warehouse Name</th>
                         <th rowspan="2">Room</th>
                         <th rowspan="2">Bay</th>
                         <th rowspan="2">Rack</th>
                         <th rowspan="2">Pallet Number</th>
                         <th rowspan="2">Plant</th>
                         <th rowspan="2">Store Loc</th>
                      </tr>
                      <tr>
                        <th>Quantity Stock</th>
                        <th>Quantity Actual</th>
                        <th>Quantity Adjustment</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              <button type="submit" class="btn btn-primary"> SAVE <i class="icon-arrow-right14 position-right"></i></a>
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Form</h5>
       </div>
       <div class="modal-body"> 
       <div class="form-group">
          <label class="control-label col-md-2">Material</label>
          <div class="col-md-9">
            <input name="id_detail" id="id_detail" class="form-control" type="hidden">
            <input name="inventory_id" id="inventory_id" class="form-control" type="hidden">
            <input type="text" class="form-control" name="inventory_name" id="inventory_name" style="background-color:#b1e7bf" placeholder="Tap ..."  onClick="openModalMaster();  return false;" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">Plant</label>
          <div class="col-md-9">
            <input name="plant" id="plant" class="form-control" type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">Store Loc</label>
          <div class="col-md-9">
            <input name="store_loc" id="store_loc" class="form-control" type="text">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2">Quantity</label>
          <div class="col-md-9">
            <input name="quantity_actual" id="quantity_actual" class="form-control" type="number">
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer"> 
      <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
      <input type="hidden" value="1" name="submit" />
      <button onclick="updateDetail()" class="btn btn-warning legitRipple" type="submit">Update</button> 
    </div>
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_item" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:38% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Master Inventory</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventoryMaster" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Code</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory_master();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
    ajax: {
      url: "{{ url('main/warehouse/stock-opname/get-detail') }}/{{$stock_opname->id}}",
      type : "POST",
      headers : {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
     },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'mstatus', name: 'mstatus'},
     { data: 'material_number', name: 'material_number' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'quantity_actual', name: 'quantity_actual' },
     { data: 'quantity_adjustment', name: 'quantity_adjustment' },
     { data: 'po_unit', name: 'po_unit' },
     { data: 'batch', name: 'batch' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'plant', name: 'plant' },
     { data: 'store_loc', name: 'store_loc' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });


  var table_inventory_master;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory_master = $('#dtTableInventoryMaster').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ URL::route('main.inventory.data-modal-transfer') }}",
          type : "GET",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' }
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  function reload_table_inventory_master()
  {
    table_inventory_master.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorStock OpnameName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
  }

  $("input[name='quantity']").TouchSpin({
      min: 0,
      max: 1,
  });


  function addValue(str, id){
    clearDetail();
    var inventory_id = id;
    var inventory_name = $(str).closest('tr').find('td:eq(1)').text();
    var note = $(str).closest('tr').find('td:eq(1)').text();

    $("#inventory_id").val(inventory_id);
    $("#inventory_name").val(inventory_name);
    setBtnAdd();
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var inventory_id = $('#inventory_id').val();
     var unit_of_measure_code =   $('#unit_of_measure_code').val();
     var quantity =   $('#quantity').val();

     if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-opname/post-detail/{{$stock_opname->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
        },
      })
    }
    return false;
  };


  function generate_old(id)
  {
      waitingDialog.show('Get Mobile Data...', {dialogSize: 'sm', progressType: 'warning'});

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-opname/generate/{{$stock_opname->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
        
          waitingDialog.hide();
          reload_table();
          clearDetail();
        },
      })
    return false;
  };

  function editDetail(str, id_detail, inventory_id)
   {
    $('.errorCustomerTypeName').addClass('hidden');
    $("#btnSave").attr("onclick","update("+id_detail+")");
    $("#btnSaveAdd").attr("onclick","updateadd("+id_detail+")");

    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
      url : '../../stock-opname/edit-detail/' + id_detail,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="store_loc"]').val(data.store_loc);
        $('[name="plant"]').val(data.plant);
        $('[name="inventory_id"]').val(data.inventory_id); 
        $('[name="inventory_name"]').val(data.inventory_name); 
        $('[name="quantity_actual"]').val(data.quantity_actual); 
        $('[name="id_detail"]').val(data.id_detail);  
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit @yield("title")'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  };


  function updateDetail()
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var plant =   $('#plant').val();
    var store_loc =   $('#store_loc').val();

    if(inventory_id == '' || plant == ''){
      alert("Please complete data!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-opname/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'store_loc': $('#store_loc').val(),
          'inventory_id': $('#inventory_id').val(),
          'quantity_actual': $('#quantity_actual').val(),
          'plant': $('#plant').val(),
        },
        success: function(data) {
          $('#modal_form').modal('hide');
          reload_table();
          reload_table_detail();
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function reload_table()
  {
      table.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#quantity').val('')
  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function approve(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-green';
        text_field = 'APPROVE';
        type_field = 'green';
      }else{
        btn = 'btn-green';
        text_field = 'APPROVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to approve ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname/approve-detail/' + id_detail,
            type: "POST",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully approve data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function not_approve(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'NOT APPROVE';
        type_field = 'red';
      }else{
        btn = 'btn-red';
        text_field = 'NOT APPROVE';
        type_field = 'red';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to not approve ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname/not-approve-detail/' + id_detail,
            type: "POST",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully not approve data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function restore(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-orange';
        text_field = 'RESTORE';
        type_field = 'orange';
      }else{
        btn = 'btn-orange';
        text_field = 'RESTORE';
        type_field = 'orange';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Make sure data not yet post to SAP, are you sure to restore ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname/restore-detail/' + id_detail,
            type: "POST",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully restore data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function generate()
  {

      if(status == 0){
        btn = 'btn-green';
        text_field = 'GET';
        type_field = 'green';
      }else{
        btn = 'btn-green';
        text_field = 'GET';
        type_field = 'green';
      }; 

      $.confirm({
        title: 'Confirm!',
        content: 'Make sure data not yet post to SAP, are you sure to get Stock Opname data from mobile?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname/generate/{{$stock_opname->id}}',
            type: "POST",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully get stock data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };


  function openModalMaster()
  {
    // console.log('tes');
    $('#modal_form_item').modal('show'); // show bootstrap modal

    reload_table_inventory_master();
    reload_table();
  }


  function addItem(str, id, inventory_name){
    var inventory_id_transfer = id;
    // var inventory_code = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(2)').text();

    // console.log(inventory_code);
    console.log(inventory_name);

    $("#inventory_id").val(inventory_id_transfer);
    // $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);

    // setBtnAdd();
    $('#modal_form_item').modal('toggle');

  }

 
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);
</script>
@stop
