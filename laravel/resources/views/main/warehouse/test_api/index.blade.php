@extends('layouts.main.template')
@section('title', 'Material Used')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'material-used')
@section('val_variable', 'material_used')
@section('content')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
        <form method="post" method="post" enctype="multipart/form-data">
        notransaction: <input type="text" name="notransaction" id="notransaction" value="1000000001"><br>
        purchasing_number: <input type="text" name="purchasing_number" id="purchasing_number" value="3102000049"><br>
        item_number: <input type="text" name="item_number" id="item_number" value="00020"><br>
        material_number: <input type="text" name="material_number" id="material_number" value="1100040"><br>
        plant: <input type="text" name="plant" id="plant" value="1001"><br>
        storage_loc: <input type="text" name="storage_loc" id="storage_loc" value="008A"><br>
        po_qty: <input type="text" name="po_qty" id="po_qty" value="50"><br>
        po_unit: <input type="text" name="po_unit" id="po_unit" value="KAR"><br>
        po_date: <input type="text" name="po_date" id="po_date" value="08.04.2019"><br>
        vendor_number: <input type="text" name="vendor_number" id="vendor_number" value="1300000"><br>
        batch: <input type="text" name="batch" id="batch" value="0029042019"><br>
        doc_header: <input type="text" name="doc_header" id="doc_header" value="Test GR"><br>
        grrcpt: <input type="text" name="grrcpt" id="grrcpt" value="IT TEAM"><br>
        flag: <input type="text" name="flag" id="flag" value="X"><br>
          <a href="#" onclick="save();" class="btn btn-success">Save</a>
        </form>

      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">

    function save(){
      axios({
      method: 'post',
      url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-grpod',
      // headers: {
      //               'Content-Type': 'application/json',
      //               'X-Authorization': '1mlvRQgAnKm1xVqDQU12CmPUjHxgojWnMg0ruG3tXqyTU2jNnPazC8hFZ2POCr3M',

      //           },

      data: {
              '_token': $('input[name=_token]').val(),
              'notransaction': $('#notransaction').val(),
              'purchasing_number': $('#purchasing_number').val(),
              'item_number': $('#item_number').val(),
              'material_number': $('#material_number').val(),
              'plant': $('#plant').val(),
              'storage_loc': $('#storage_loc').val(),
              'po_qty': $('#po_qty').val(),
              'po_unit': $('#po_unit').val(),
              'po_date': $('#po_date').val(),
              'vendor_number': $('#vendor_number').val(),
              'batch': $('#batch').val(),
              'doc_header': $('#doc_header').val(),
              'grrcpt': $('#grrcpt').val(),
              'flag': $('#flag').val(),
            },
            // headers: {
                    // 'Content-Type': 'Content-Type',
                    // 'X-Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',

                // },
            // crossdomain: true,
        })
      .then(function (response) {
          //handle success
          console.log(response);
      })
      .catch(function (response) {
          //handle error
          console.log(response);

      });
    }
</script>

@stop
