@extends('layouts.main.template')
@section('title', 'Goods Receive Outstanding')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'goods-receive')
@section('val_variable', 'goods_receive')
@section('content')

<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
        <a href="#" onclick="getDataSAP();" class="btn btn-link btn-float has-text text-size-small" style="color:red"><i class="icon-toggle" style="color:red"></i> <span>UPDATE PO SAP</span></a>
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header -->

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
            <ul class="nav nav-tabs nav-tabs-highlight">
            <input type="hidden" name="filter_status" id="filter_status">
            <li class="active" onclick="active_status();"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-folder-check position-left"></i> Active</a></li>
            <li onclick="inactive_status();"><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-folder-minus2 position-left"></i> Inactive</a></li>
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
            </li>
            <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a>
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print Table</span></a>
            </li>
          </ul>
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th>PO Number</th>
                <th>Material</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Item</th>
                <th>UoM</th>
                <th>Vendor Code</th>
                <th>Vendor Name</th>
                <!-- <th>Status</th> -->
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- End Bootstrap modal -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
@include('main.warehouse.goods_receive.script_report_outstanding_summary')
@include('scripts.change_status')
@stop
