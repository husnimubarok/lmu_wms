@extends('layouts.main.template')
@section('title', 'Goods Receive')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'goods_receive')   
@section('val_variable', 'goods_receive')   
@section('content') 
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
         {!! Form::open(array('route' => 'main.goods-receive.store', 'files' => 'true', 'id' => 'form_pick', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}

          <div class="panel panel-flat">
            @include('layouts.main.generate_form.table_open_button')
            <div class="panel-body">
                <div class="col-md-12"> 
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>    
                        <th>Warehouse</th>
                        <th>Room</th>
                        <th>Bay</th>
                        <th>Rack</th>
                        <th>Pallet</th>
                        <th>PO Number</th>
                        <!-- <th>PO Date</th> -->
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>PO Quantity</th>  
                        <th><input type="checkbox" name="selectall" id="selectall" value="" onchange="updateSum()"/></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($inventory_receive_detail as $inventory_receive_detail)
                      <tr> 
                      <th>
                        {{ Form::hidden('inventory_receive_detail['.$inventory_receive_detail->id.'][id]', old($inventory_receive_detail->id.'[id]', $inventory_receive_detail->id), ['class' => 'form-control', 'id' => 'id_'.$inventory_receive_detail->id, 'disabled' => 'true']) }}
                        {{$inventory_receive_detail->warehouse_name}}
                      </th> 
                      <th>
                        {{$inventory_receive_detail->room_name}}
                      </th> 
                      <th>
                        {{$inventory_receive_detail->bay_name}}
                      </th> 
                      <th>
                        {{$inventory_receive_detail->rack_name}}
                      </th> 
                      <th>
                        {{$inventory_receive_detail->pallet_number}}
                      </th> 
                      <th>
                        {{$inventory_receive_detail->po_number}}
                      </th> 
                      <!-- <th>
                        {{$inventory_receive_detail->doc_date}}
                      </th>  -->
                       <th>
                        {{ Form::hidden('good_receive_detail['.$inventory_receive_detail->id.'][inventory_id]', old($inventory_receive_detail->id.'[inventory_id]', $inventory_receive_detail->inventory_id), ['class' => 'form-control', 'id' => 'inventory_id_'.$inventory_receive_detail->id, 'disabled' => 'true']) }}  
                        {{ Form::hidden('good_receive_detail['.$inventory_receive_detail->id.'][id]', old($inventory_receive_detail->id.'[id]', $inventory_receive_detail->id), ['class' => 'form-control', 'id' => 'inventory_id_'.$inventory_receive_detail->id, 'disabled' => 'true']) }}  
                        {{$inventory_receive_detail->inventory_code}}
                      </th>
                       <th>
                        {{$inventory_receive_detail->inventory_name}}
                      </th> 
                       <th>
                        {{$inventory_receive_detail->quantity}}
                      </th>
                       <th>
                          {{ Form::checkbox('good_receive_detail['.$inventory_receive_detail->id.'][checkbox]', $inventory_receive_detail->id, null, ['id' => 'checkbox_'.$inventory_receive_detail->id, 'class' => 'checkbox', 'onchange' => 'updateSum();']) }} 

                           {{ Form::text('good_receive_detail['.$inventory_receive_detail->id.'][quantity]',  '1', ['class' => 'form-control', 'id' => 'quantity_'.$inventory_receive_detail->id, 'min' => '0', 'disabled' => 'true', 'style' => 'display: none']) }}  
                       </th> 
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th> 
                        <th></th> 
                        <th></th> 
                        <th></th> 
                        <th></th> 
                        <th></th> 
                        <th></th>
                        <th>Total Pick</th> 
                        <th ><div id="sum">0</div></th>
                      </tr>
                    </tfoot>
                  </table> 
                </div>

              </div> 
            <div class="panel-body">
                {{ Form::hidden('code_trans', 'GRNT') }} 
                <button class="btn btn-success pull-right pick" style="display: none" type="button" onclick="post_pick();"><i class="icon-file-check2"></i>  Pick</button>
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:70% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange"> 
         <h5 class="modal-title">Data Inventory</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>   
                <th>Inventory Code</th> 
                <th>Inventory Name</th>
                <th>UOM</th>
                <th>Inventory Group</th> 
                <th>Description</th> 
                <th>Action</th> 
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
      </div>
      <div class="modal-footer"> 
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button">Refresh</button> 
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script type="text/javascript">  
  //Checkbox pick
 $(document).ready(function(){ 
    $('#selectall').on('click',function(){ 
     if(this.checked){
      $('.checkbox').each(function(){
        this.checked = true;
        $(this).parents("tr:eq(0)").find(".form-control").prop("disabled",false); 
      });
    }else{
     $('.checkbox').each(function(){
      this.checked = false;
      $(this).parents("tr:eq(0)").find(".form-control").prop("disabled",true);
    });
   }
  });
});

  // Tick all checkbox
  $('#selectall').click (function () {  
    var checkedStatus = this.checked;
    $('table tbody').find('input[type="checkbox"]').each(function () {
      $(this).prop('checked', checkedStatus);        
    });
  });

  function updateSum() {  
    var sum = 0;
    $("tbody input:checked").each(function(){
      var pickVal = $(this).closest('tr').find('input[type="text"]').val();
      var varNum = parseFloat(pickVal.replace( ",", "."));
      if (!isNaN(varNum)) {
        sum += varNum;
      }        
    });    
    $("#sum").text(sum.toFixed(0).replace(".",","));
  }
  function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
  }

  $('.checkbox').change( function() {
    setPickButton();
    var isChecked = this.checked;
    if(isChecked) {
      $(this).parents("tr:eq(0)").find(".form-control").prop("disabled",false); 
    } else {
      $(this).parents("tr:eq(0)").find(".form-control").prop("disabled",true);
    }
  });

  function setPickButton()
  {
    var sum = $('#sum').html();
    if(sum == 0)
    {
      $('.pick').hide(100); 
    }else{
     $('.pick').show(100);
   }

 }

 function updateSum() {  
    var sum = 0;
    $("tbody input:checked").each(function(){
      var test = $(this).closest('tr').find('input[type="text"]').val();
      var testNum = parseFloat(test.replace( ",", "."));
      if (!isNaN(testNum)) {
        sum += testNum;
      }        
    });    
    $("#sum").text(sum.toFixed(0).replace(".",","));
    setPickButton();
  }
  function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
  }

  function post_pick(id)
  {
      var count = $("#sum").text();
      var count_val = count.bold(); 

      btn = 'btn-green';
      text_field = 'PICK';
      type_field = 'green';

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to pick ' + count_val + ' data from PO number ?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () { 
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $("#form_pick").submit();
         }
       },
     }
   });
  }
</script>
@stop


