@extends('layouts.main.template')
@section('title', 'Goods Receive')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'goods-receive')
@section('val_variable', 'goods_receive')
@section('content')
<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
      @actionStart('goods_receive', ['update'])
        <!-- <a href="#" onclick="getDataSAP();" class="btn btn-link btn-float has-text text-size-small" style="color:red"><i class="icon-toggle" style="color:red"></i> <span>UPDATE PO SAP</span></a> -->
      @actionEnd
      @actionStart('goods_receive', ['delete'])
        <a href="#" onclick="bulk_change_status();" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-minus2 text-indigo-400"></i> <span>archive</span></a>
      @actionEnd
        <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>
        <a href="#" onClick="reload_table()" class="btn btn-link btn-float has-text text-size-small"><i class=" icon-database-refresh text-indigo-400"></i> <span>Refresh</span></a>
      @actionStart('goods_receive', ['create'])
        <a href="#" class="btn btn-link btn-float has-text text-size-small" onClick="add()"><i class="icon-file-plus text-indigo-400"></i> <span>Create</span></a>
      @actionEnd
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header -->

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.generate_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th>
                <th>GR WMS</th>
                <th>Date Transaction</th>
               <!-- <th>Warehouse</th> -->
                <th>GR SAP</th>
                <th>Doc Year</th>
                <th>Container No</th>
                <th>PO No</th>
                <th>Header Text</th>
                <th>User</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:30% !important">
    <div class="modal-content">
      {!! Form::open(array('route' => 'main.goods-receive.store', 'files' => 'true', 'id' => 'form', 'class' => 'form-horizontal'))!!}
      {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Select Transaction</h5>
       </div>
       <div class="modal-body">
        <div>
            <select class="select" name="code_transaction" id="code_transaction">
              <option value="GRNC">GOODS RECEIVE</option>
              <option value="GRMN">GOODS RECEIVE MANUAL</option>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        @include('layouts.main.transaction_form.modal_button')
      </div>
    {!! Form::close() !!}
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
@include('main.warehouse.goods_receive.script')
@include('scripts.change_status')
@stop
