@extends('layouts.main.template')
@section('title', 'Goods Receive Outstanding')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'goods-receive')   
@section('val_variable', 'goods_receive')   
@section('content') 

<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
        <a href="#" onclick="getDataSAP();" class="btn btn-link btn-float has-text text-size-small" style="color:red"><i class="icon-toggle" style="color:red"></i> <span>UPDATE PO SAP</span></a>
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header -->

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button_report')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>  
                <th>Receive Number</th>
                <th>Date Receive</th>
                <th>Item</th>
                <th>Material</th> 
                <th>Description</th> 
                <th>Quantity</th>
                <th>UoM</th>
                <th>Batch</th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Plant</th>
                <th>Store Loc</th>
                <th>PO Number</th>
                <th>Supplier</th>
                <th>Doc Date</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

@include('main.warehouse.goods_receive.script_report_outstanding')
@include('scripts.change_status')
@stop


