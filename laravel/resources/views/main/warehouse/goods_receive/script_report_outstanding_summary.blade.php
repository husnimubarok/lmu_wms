<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
         url: "{{ URL::route('main.goods-receive.data-report-outstanding-summary') }}",
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.date_from = $('input[name=date_from]').val();
            d.filter_status = $('input[name=filter_status]').val();
            d.date_to = $('input[name=date_to]').val();
            d.keyword = $('input[name=keyword]').val();
        }
   },
   processing: true,
   deferRender: true,
   colReorder: true,
   scrollX: true,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
     },
   columns: [
    { data: 'po_number', name: 'po_number' },
    { data: 'inventory_code', name: 'inventory_code' },
    { data: 'inventory_name', name: 'inventory_name' },
    { data: 'quantity', name: 'quantity' },
    { data: 'item_line', name: 'item_line' },
    { data: 'unit_of_measure_code', name: 'unit_of_measure_code' },
    { data: 'code_vendor', name: 'code_vendor' },
    { data: 'supplier_name', name: 'supplier_name' },
    // { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });

  function filter_data()
  {
    var date_from = $('input[name=date_from]').val();
    var date_to = $('input[name=date_to]').val();
    if(date_from == '' || date_to == '')
    {
      $.alert({
        title: 'Warning!',
        type_field: 'red',
        content: 'Filter from is required!',
      });
    }else{
      reload_table();

    };
  }

  function active_status()
 {
   $("#filter_status").val('1');
   reload_table();
 }

 function inactive_status()
 {
   $("#filter_status").val('9');
   reload_table();
 }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function print()
  {
    var date_from = $('input[name=date_from]').val();
    var date_to = $('input[name=date_to]').val();
    window.location.assign('report/print/' + date_from + '/' + date_to);
  }


  function getDataSAP(){

    waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
    axios.get('../goods-receive/get-po-sap')
      .then(response => {
        this.data = response.data;
        // console.log(data);
        // if (typeof data !== 'undefined') {
        this.data.forEach((item) => {
          //  console.log(data);
            waitingDialog.hide();
            var nopo = item.nopo;
            var item_line = item.item_line;
            // console.log(nopo);

            axios({
            method: 'GET',
            url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/po-revisi/' + nopo + '/' + item_line,
            // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/po-revisi/3102014699/00030',

            // headers: {
                          // 'Content-Type': 'application/json'
                      // },
            data: {
                    '_token': $('input[name=_token]').val(),
                  },
            crossdomain: true,
            })
            .then(function (response) {
              this.data.forEach((item) => {
                waitingDialog.hide();
                // console.log(response);

                var nopo = response.data.podetail[0].EBELN;
                var item_line = response.data.podetail[0].EBELP;
                var inventory_code = response.data.podetail[0].MATNR;
                var quantity = response.data.podetail[0].MENGE;
                var deletation = response.data.podetail[0].LOEKZ;
                var uom = response.data.podetail[0].MEINS;
                var plant = response.data.pohead[0].WERKS;
                var store_loc = response.data.pohead[0].LGORT;
                var doc_date = response.data.pohead[0].BUDAT;
                var supplier = response.data.pohead[0].LIFNR;

                console.log('break---------------');
                console.log('nopo: ' + nopo);
                console.log('item_line: ' + item_line);
                console.log('inventory_code update to: ' + inventory_code);
                console.log('quantity update to: ' + quantity);
                console.log('deletation: ' + deletation);
                console.log('uom: ' + uom);
                console.log('plant: ' + plant);
                console.log('store_loc: ' + store_loc);
                console.log('doc_date: ' + doc_date);
                console.log('supplier: ' + supplier);

                axios({
                  method: 'post',
                  url: '../goods-receive/post-po-revision',
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'nopo': nopo,
                          'item_line': item_line,
                          'inventory_code': inventory_code,
                          'quantity': quantity,
                          'deletation': deletation,
                          'uom': uom,
                          'plant': plant,
                          'store_loc': store_loc,
                          'doc_date': doc_date,
                        },
                  })
                })
            })
            .catch(function (response) {
                //handle error
                console.log(response);
            });
          });
        // }
      })
    }


  $('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();



    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    /**
     * Module for displaying "Waiting for..." dialog using Bootstrap
     *
     * @author Eugene Maslovich <ehpc@em42.ru>
     */

      var waitingDialog = waitingDialog || (function ($) {
          'use strict';

        // Creating modal dialog's DOM
        var $dialog = $(
          '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
          '<div class="modal-dialog modal-m">' +
          '<div class="modal-content">' +
            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
            '<div class="modal-body">' +
              '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
          '</div></div></div>');

        return {
          /**
           * Opens our dialog
           * @param message Process...
           * @param options Custom options:
           *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
           *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
           */
          show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
              options = {};
            }
            if (typeof message === 'undefined') {
              message = 'Loading';
            }
            var settings = $.extend({
              dialogSize: 'm',
              progressType: '',
              onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
              $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
              $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                settings.onHide.call($dialog);
              });
            }
            // Opening dialog
            $dialog.modal();
          },
          /**
           * Closes dialog
           */
          hide: function () {
            $dialog.modal('hide');
          }
        };

      })(jQuery);
</script>
