@extends('layouts.main.template')
@section('title', 'Maintenance Flag')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'maintenance-flag')
@section('val_variable', 'maintenance_flag')
@section('content')
<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
      <a href="#" onclick="getTransactionData();" class="btn btn-link btn-float has-text text-size-small" style="color:red"><i class="icon-toggle" style="color:red"></i> <span>GET TRANSACTION DATA</span></a>
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>
      <a href="#" onClick="reload_table()" class="btn btn-link btn-float has-text text-size-small"><i class=" icon-database-refresh text-indigo-400"></i> <span>Refresh</span></a>
      <a href="#" class="btn btn-link btn-float has-text text-size-small" onClick="add()"><i class="icon-file-plus text-indigo-400"></i> <span>Add From Stock</span></a>
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header -->

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr> 
                <th>Action</th> 
                <th>Source</th> 
                <th>Number</th> 
                <th>Warehouse</th> 
                <th>Room</th> 
                <th>Bay</th> 
                <th>Rack</th> 
                <th>Pallet</th> 
                <th>Batch</th>
                <th>Inventory Code</th> 
                <th>Inventory Name</th> 
                <th>Unit</th>
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:80% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Stock</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Batch</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <th>Unit</th>
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">


var table_inventory;
var status = 0;
$(document).ready(function() {
  //datatable_inventorys
  table_inventory = $('#dtTableInventory').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
    processing: true,
    serverSide: true,
    lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
    language : {
      sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
      sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
      processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
  },
    ajax: {
        url: "{{ url('main/warehouse/maintenance-flag/data-modal') }}",
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            // d.code_transaction = $('input[name=code_transaction]').val();
            d.filter_status = $('input[name=filter_status]').val();
            d.keyword = $('input[name=keyword]').val();
            d.date_from = $('input[name=date_from]').val();
            d.date_to = $('input[name=date_to]').val();
        }
  },
    processing: true,
    deferRender: true,
    colReorder: true,
    ordering: true,
    autowidth: false,
    scrollX: true,
    dom: 'lfrtip',
    columns: [
    { data: 'action', name: 'action'},
    // { data: 'check', name: 'check', orderable: false, searchable: false },
    { data: 'warehouse_name', name: 'warehouse_name' },
    { data: 'room_name', name: 'room_name' },
    { data: 'bay_name', name: 'bay_name' },
    { data: 'rack_name', name: 'rack_name' },
    { data: 'pallet_number', name: 'pallet_number' },
    { data: 'batch', name: 'batch' },
    { data: 'inventory_code', name: 'inventory_code' },
    { data: 'inventory_name', name: 'inventory_name' },
    { data: 'unit', name: 'unit' },
    { data: 'quantity', name: 'quantity' },
    ],
    rowReorder: {
      dataSrc: 'action'
    }
  });
    $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});


</script>


@include('main.warehouse.maintenance_flag.script')
@include('scripts.change_status')
@stop
