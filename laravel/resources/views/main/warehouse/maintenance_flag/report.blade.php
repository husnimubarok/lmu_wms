@extends('layouts.main.template')
@section('title', 'Goods Receive History')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'goods-receive')   
@section('val_variable', 'goods_receive')   
@section('content') 
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button_report')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>  
                <th>PO Number</th> 
                <th>Receive Number</th>
                <th>Vendor</th> 
                <th>Container</th> 
                <th>Material</th> 
                <th>Material Description</th> 
                <th>Plant</th>
                <th>Batch</th>
                <th>UoM</th>
                <th>Qty Receide</th>
                <th>Date Receive</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

@include('main.warehouse.goods_receive.script_report')
@include('scripts.change_status')
@stop


