@extends('layouts.main.template')
@section('title', 'Goods Receive')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'goods_receive')
@section('val_variable', 'goods_receive')
@section('content')
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($goods_receive, array('route' => ['main.goods-receive.update', $goods_receive->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'GR WMS', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'GR WMS', 'id' => 'no_transaction', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                     <div class="form-group">
                      {{ Form::label('seq_no', 'Container No', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('container_no', old('container_no'), ['class' => 'form-control', 'placeholder' => 'Container No', 'id' => 'container_no', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'GR SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('nodoc', old('nodoc'), ['class' => 'form-control', 'placeholder' => 'GR SAP', 'id' => 'nodoc', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                     <div class="form-group">
                      {{ Form::label('seq_no', 'PO Number', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('nopo', old('nopo'), ['class' => 'form-control', 'placeholder' => 'PO Number', 'id' => 'nopo', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      ADDITIONAL INFORMATION
                    </legend>
                     <!-- <div class="form-group">
                      {{ Form::label('seq_no', 'Warehouse', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('warehouse_id', $warehouse_list, old('warehouse_id'), array('class' => 'select-search', 'placeholder' => 'Select Warehouse', 'id' => 'warehouse_id')) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Room', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('room_id', $room_list, old('room_id'), array('class' => 'select-search', 'placeholder' => 'Select Room', 'id' => 'room_id')) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Bay', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('bay_id', $bay_list, old('bay_id'), array('class' => 'select-search', 'placeholder' => 'Select Bay', 'id' => 'bay_id')) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Rack', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('rack_id', $rack_list, old('rack_id'), array('class' => 'select-search', 'placeholder' => 'Select Rack', 'id' => 'rack_id')) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Pallet', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                      {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number']) }}
                      </div>
                    </div> -->
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Doc Year', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('tahundoc', old('tahundoc'), ['class' => 'form-control', 'placeholder' => 'Doc Year', 'id' => 'tahundoc', 'readonly' => 'readonly']) }}
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Posting Date', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($goods_receive))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($goods_receive->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Posting Date', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Posting Date', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Header Text', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Header Text', 'id' => 'description', 'rows' => '3', 'cols' => '7']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>
                        <!-- <th>PO Date</th> -->
                        <th>Action</th>
                        <th>Item</th>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>UOM</th>
                        <th>Batch</th>
                        <th>Warehouse Name</th>
                        <th>Room</th>
                        <th>Bay</th>
                        <th>Rack</th>
                        <th>Pallet Number</th>
                        <th>Plant</th>
                        <th>Store Loc</th>

                        <th>PO Number</th>
                        <th>Vendor</th>
                        <th>Vendor Name</th>
                        <th>Doc Date</th>
                        <!-- <th>Revision</th> -->
                        <!-- <th>Action</th>  -->
                      </tr>
                    </thead>
                    @if(strlen($goods_receive->nodoc)==0)
                    <thead>
                      <tr>
                      <td>
                        <a href="#" class="btn btn-primary  btn-float btn-xs" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                        <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a>
                        <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                      </td>
                      <td>
                        {{ Form::text('item_line', old('item_line'), ['class' => 'form-control', 'id' => 'item_line', 'readonly']) }}
                      </td>
                      <td>
                          {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }}
                          {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }}
                          @if($goods_receive->code_transaction == 'GRNC')
                            <input type="text" class="form-control" name="inventory_code" id="inventory_code" readonly>
                          @else
                            <input type="text" class="form-control" name="inventory_code" id="inventory_code" style="background-color:#b1e7bf" placeholder="Tap ..."  onClick="openModalMaster();  return false;" readonly>
                          @endif
                      </td>
                      <td>
                        @if($goods_receive->code_transaction == 'GRNC')
                          <input type="text" class="form-control" name="inventory_name" id="inventory_name" readonly>
                        @else
                          <input type="text" class="form-control" name="inventory_name" id="inventory_name" style="background-color:#b1e7bf" placeholder="Tap ..."  onClick="openModalMaster();  return false;" readonly>
                        @endif
                      </td>
                      <td>
                        {{ Form::number('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity']) }}
                      </td>
                      <td>
                        {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code']) }}
                      </td>
                      <td>
                        {{ Form::text('batch', old('batch'), ['class' => 'form-control', 'id' => 'batch']) }}
                      </td>
                      <td>
                        {{ Form::text('warehouse_name', old('warehouse_name'), ['class' => 'form-control', 'id' => 'warehouse_name', 'readonly']) }}
                        {{ Form::hidden('warehouse_id', old('warehouse_id'), ['class' => 'form-control', 'id' => 'warehouse_id', 'readonly']) }}
                        {{ Form::hidden('inventory_receive_detail_id', old('inventory_receive_detail_id'), ['class' => 'form-control', 'id' => 'inventory_receive_detail_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('room_name', old('room_name'), ['class' => 'form-control', 'id' => 'room_name', 'readonly']) }}
                        {{ Form::hidden('room_id', old('room_id'), ['class' => 'form-control', 'id' => 'room_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('bay_name', old('bay_name'), ['class' => 'form-control', 'id' => 'bay_name', 'readonly']) }}
                        {{ Form::hidden('bay_id', old('bay_id'), ['class' => 'form-control', 'id' => 'bay_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('rack_name', old('rack_name'), ['class' => 'form-control', 'id' => 'rack_name', 'readonly']) }}
                        {{ Form::hidden('rack_id', old('rack_id'), ['class' => 'form-control', 'id' => 'rack_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number', 'readonly']) }}
                      </td>

                      <td>
                        {{ Form::text('plant', old('plant'), ['class' => 'form-control', 'id' => 'plant', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('store_loc', old('store_loc'), ['class' => 'form-control', 'id' => 'store_loc', 'readonly']) }}
                      </td>
                      <td>
                          {{ Form::text('po_number', old('po_number'), ['class' => 'form-control', 'id' => 'po_number', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('supplier_id', old('supplier_id'), ['class' => 'form-control', 'id' => 'supplier_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('doc_date', old('doc_date'), ['class' => 'form-control', 'id' => 'doc_date', 'readonly']) }}
                      </td>
                      <!-- <td></td>  -->
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              <!-- <a href="#" onclick="getSrc()"> Test </a> |  -->
              @if($goods_receive->code_transaction == 'GRNC')
              <a href="#"  class="btn btn-danger" id="cancel_gr" onclick="cancelGR();"> Cancel GR <i class="icon-cancel-circle2 position-right"></i></a>
              @endif
              @if(strlen($goods_receive->nodoc)==0 || $goods_receive->nodoc== '' || $goods_receive->nodoc== NULL)
                @if($goods_receive->code_transaction == 'GRNC')
                  <a href="#"  class="btn btn-primary" id="save_gr" onclick="postSAP();"> Save GR<i class="icon-arrow-right14 position-right"></i></a>
                @else
                  <!-- <a href="#"  class="btn btn-primary" id="save_gr" onclick="postGR();"> Save GR Manual<i class="icon-arrow-right14 position-right"></i></a> -->
                  <button type="submit" class="btn btn-primary">Save GR Manual <i class="icon-arrow-right14 position-right"></i></button>
                @endif
              @endif

              <!-- <a href="#"  class="btn btn-primary" id="save_gr" onclick="syncSAP();"> Sync SAP<i class="icon-arrow-right14 position-right"></i></a> -->
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:70% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Inventory Receive</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                    <th>Item</th>
                    <th>Material</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>UOM</th>
                    <th>Warehouse Name</th>
                    <th>Room</th>
                    <th>Bay</th>
                    <th>Rack</th>
                    <th>Pallet Number</th>
                    <th>Plant</th>
                    <th>Store Loc</th>
                    <th>PO Number</th>
                    <th>Vendor</th>
                    <th>Doc Date</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal"><i class="icon-close2"></i> Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
        <button onclick="pick();"  class="btn btn-primary legitRipple" type="button"><i class="icon-touch"></i> Pick</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_item" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:38% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Master Inventory</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$goods_receive->code_transaction}}">
          <table id="dtTableInventoryMaster" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Code</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory_master();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    revision = @if($revision > 0) 1 @else 0 @endif;
    // alert(revision);
    // $( "#save_gr" ).prop( "disabled", true );

    // if(revision == 1)
    // {
    //   $("#cancel_gr").show();
    //   $("#save_gr").hide();
    // };

    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
      ajax: {
         url: "{{ url('main/warehouse/goods-receive/get-detail') }}/{{$goods_receive->id}}",
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val();
        }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
      // "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      //   // alert(aData.revision);
      //   if ( aData.revision == "1" )
      //     {
      //         $('td', nRow).css('background-color', '#f4a8a8');
      //     }
      // },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'item_number', name: 'item_number' },
     { data: 'material_number', name: 'material_number' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'po_unit', name: 'po_unit' },
     { data: 'batch', name: 'batch' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'plant', name: 'plant' },
     { data: 'store_loc', name: 'store_loc' },
     { data: 'po_number', name: 'po_number' },
     { data: 'supplier_id', name: 'supplier_id' },
     { data: 'supplier_name', name: 'supplier_name' },
     { data: 'doc_date', name: 'doc_date' },
    //  { data: 'revision', name: 'revision' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });

  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     pageLength: 5,
     scrollX: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language: {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch:  "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },

    ajax: {
          url: "{{ url('main/warehouse/goods-receive/data-modal') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.filter_status = $('input[name=filter_status]').val();
              d.code_transaction = $('input[name=code_transaction]').val();
              d.keyword = $('input[name=keyword]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     dom: 'lBfrtip',
    //  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    //     // alert(aData.revision);
    //     if ( aData.revision == "1" )
    //       {
    //           $('td', nRow).css('background-color', '#f4a8a8');
    //       }
    //   },
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
    //  { data: 'inventory_id', name: 'inventory_id' },
     { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'item_line', name: 'item_line' },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'uom', name: 'uom' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'plant', name: 'plant' },
     { data: 'store_location', name: 'store_location' },
     { data: 'po_number', name: 'po_number' },
     { data: 'supplier_code', name: 'supplier_code' },
     { data: 'doc_date', name: 'doc_date' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  var table_inventory_master;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory_master = $('#dtTableInventoryMaster').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ URL::route('main.inventory.data-modal-transfer') }}",
          type : "GET",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' }
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

 function pick()
 {
  var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    // alert(list_id);

    if (list_id.length > 0) {
       var date_transaction = $('#date_transaction').val();
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to pick '+list_id.length+' data?',
            type: 'green',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'YES PICK',
                    btnClass: 'btn-green',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                                'date_transaction': date_transaction.replace("-","").replace("-",""),
                            },
                            url: "../../goods-receive/pick/{{$goods_receive->id}}",
                            type: "POST",
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = {
                                        "positionClass": "toast-bottom-right",
                                        "timeOut": 1000,
                                    };
                                    toastr.success('Success pick data!', 'Success Alert', options);
                                    reload_table();
                                    reload_table_inventory();
                                    $('#modal_form').modal('toggle');

                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'Pick data failed!',
                                    });
                                    reload_table();
                                    reload_table_inventory();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Pick data failed!',
                                });
                                reload_table();
                                reload_table_inventory();
                            }
                        });
                        reload_table();
                        reload_table_inventory();
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
    reload_table();
    reload_table_inventory();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory_master()
  {
    table_inventory_master.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
    $("#btnSave").attr("onclick","save()");
    $("#btnSaveAdd").attr("onclick","saveadd()");

    $('.errorGoods ReceiveName').addClass('hidden');

    save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal

    reload_table_inventory();
    reload_table();
  }

  function addValue(str, warehouse_id, bay_id, room_id, rack_id, inventory_id, id_detail){
    clearDetail();
    // var inventory_id = id;
    // var inventory_id = $(str).closest('tr').find('td:eq(0)').text();
    var po_number = $(str).closest('tr').find('td:eq(14)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(3)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(5)').text();
    var supplier_id = $(str).closest('tr').find('td:eq(15)').text();
    var plant = $(str).closest('tr').find('td:eq(12)').text();
    var store_loc = $(str).closest('tr').find('td:eq(13)').text();
    var item_line = $(str).closest('tr').find('td:eq(2)').text();
    var doc_date = $(str).closest('tr').find('td:eq(16)').text();
    var quantity = $(str).closest('tr').find('td:eq(4)').text();

    var warehouse_name = $(str).closest('tr').find('td:eq(7)').text();
    var room_name = $(str).closest('tr').find('td:eq(8)').text();
    var bay_name = $(str).closest('tr').find('td:eq(9)').text();
    var rack_name = $(str).closest('tr').find('td:eq(10)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(11)').text();

    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);

    $("#warehouse_id").val(warehouse_id);
    $("#room_id").val(room_id);
    $("#bay_id").val(bay_id);
    $("#rack_id").val(rack_id);
    $("#inventory_receive_detail_id").val(id_detail);

    $("#inventory_id").val(inventory_id);
    $("#po_number").val(po_number);
    $("#inventory_name").val(inventory_name);
    $("#quantity").val(quantity);
    $("#plant").val(plant);
    $("#store_loc").val(store_loc);
    $("#item_line").val(item_line);
    $("#doc_date").val(doc_date);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    $("#supplier_id").val(supplier_id);
    setBtnAdd();
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var inventory_name = $('#inventory_name').val();
     var quantity =   $('#quantity').val();

     if(inventory_name == ''){
      alert("Error validarion!");
     }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../goods-receive/post-detail/{{$goods_receive->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'po_number': $('#po_number').val(),
          'quantity': $('#quantity').val(),
          'batch': $('#batch').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'item_line': $('#item_line').val(),
          'doc_date': $('#doc_date').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'supplier_id': $('#supplier_id').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'inventory_receive_detail_id': $('#inventory_receive_detail_id').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(inventory_id);

    var warehouse_name = $(str).closest('tr').find('td:eq(7)').text();
    var room_name = $(str).closest('tr').find('td:eq(8)').text();
    var bay_name = $(str).closest('tr').find('td:eq(9)').text();
    var rack_name = $(str).closest('tr').find('td:eq(10)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(11)').text();


    var po_number = $(str).closest('tr').find('td:eq(14)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(3)').text();
    var batch = $(str).closest('tr').find('td:eq(6)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(5)').text();
    var supplier_id = $(str).closest('tr').find('td:eq(15)').text();
    var plant = $(str).closest('tr').find('td:eq(12)').text();
    var store_location = $(str).closest('tr').find('td:eq(13)').text();
    var item_line = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_code = $(str).closest('tr').find('td:eq(2)').text();
    var doc_date = $(str).closest('tr').find('td:eq(16)').text();
    var quantity = $(str).closest('tr').find('td:eq(4)').text();

    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);

    $("#id_detail").val(id_detail);
    $("#po_number").val(po_number);
    $("#inventory_id").val(inventory_id);
    $("#inventory_name").val(inventory_name);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    $("#supplier_id").val(supplier_id);
    $("#plant").val(plant);
    $("#store_loc").val(store_location);
    $("#item_line").val(item_line);
    $("#doc_date").val(doc_date);
    $("#quantity").val(quantity);
    $("#id_detail").val(id_detail);
    $("#inventory_code").val(inventory_code);
    // $("#inventory_id").val(inventory_id);
    $("#batch").val(batch);
    // $('#unit_of_measure_code').val(unit_of_measure_code).trigger('change.select2');

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =   $('#quantity').val();


    if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../goods-receive/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'item_line': $('#item_line').val(),
          'doc_date': $('#doc_date').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'batch': $('#batch').val(),
          'supplier_id': $('#supplier_id').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#po_number').val(''),
    $('#batch').val(''),
    $('#pallet_number').val(''),
    $('#plant').val(''),
    $('#store_loc').val(''),
    $('#item_line').val(''),
    $('#doc_date').val(''),
    $('#unit_of_measure_code').val(''),
    $('#supplier_id').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#inventory_receive_detail_id').val('')
  }

  function addItem(str, id, inventory_name){
    var inventory_id_transfer = id;
    var inventory_code = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(2)').text();

    console.log(inventory_code);
    console.log(inventory_name);

    $("#inventory_id").val(inventory_id_transfer);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);

    // setBtnAdd();
    $('#modal_form_item').modal('toggle');

  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../goods-receive/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function getSrc() {
    axios.get('../data-detail/3')
      .then(response => {
        // console.log(response);
        // this.data = response.data;
        // alert(response);
        console.log(response.data.header.detail);
        // this.data.forEach((item) => {
        //   console.log(item.inventory_id)
        //   console.log(item.flag)
        // });
      })
      .catch(error => {
        console.log(error);
      });
   };

   function testCancel(){
      axios({
        method: 'post',
        url: '../cancel/{{$goods_receive->id}}',
        data: {
                '_token': $('input[name=_token]').val(),
              },
      })
   };

   function cancelGR(){
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to cancel {{$goods_receive->no_transaction}} number?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: 'YES',
          btnClass: 'btn-red',
          action: function () {
            waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
            axios({
              method: 'post',
                  url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgr',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'notransaction': "{{$goods_receive->no_transaction}}",
                          'nodoc': "{{$goods_receive->nodoc}}",
                          'tahundoc': "{{$goods_receive->tahundoc}}",
                          'nopo': "{{$goods_receive->nopo}}",
                      },
                  // crossdomain: true,
              })
              .then(function (response) {
                  axios({
                    method: 'post',
                    url: '../cancel/{{$goods_receive->id}}',
                    data: {
                            '_token': $('input[name=_token]').val(),
                          },
                  })

                  //handle success
                  console.log(response);
                  if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                  {
                    $.alert({
                      type: 'green',
                      icon: 'fa fa-green', // glyphicon glyphicon-heart
                      title: 'Success',
                      content: 'Cancel data SAP Success!',
                    });
                  };

                  if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                  {
                    $.alert({
                      type: 'red',
                      icon: 'fa fa-red', // glyphicon glyphicon-heart
                      title: 'Cancel Error!',
                      content: 'Message SAP : ' + response.data.T_DATA[0].MESSAGE,
                    });
                  };
                  waitingDialog.hide();
                  // window.location.assign('../../goods-receive');
              })
              .catch(function (response) {
                  //handle error
                  console.log(response);
                  alert('Post data SAP Error!');

              });
            }
        },
      }
   });
  }

  function syncSAP(){
    setTimeout(function () {
    axios({
      method: 'post',
          url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exegr',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
      data: {
              '_token': $('input[name=_token]').val(),
              'notransaction': "{{$goods_receive->no_transaction}}",
          },
      crossdomain: true,
      })
      .then(function (response) {
            axios({
              method: 'post',
              url: '../post-response/{{$goods_receive->id}}',
              data: {
                      '_token': $('input[name=_token]').val(),
                      'notransaction': response.data.T_DATA[0].NO_TRANSACTION,
                      'nodoc': response.data.T_DATA[0].MBLNR,
                      'tahundoc': response.data.T_DATA[0].MJAHR,
                      'nopo': response.data.T_DATA[0].EBELN,
                      'description': $('#description').val(),
                      'date_transaction': $('#date_transaction').val(),
                    },
          })
          waitingDialog.hide();

          if(response.data.T_DATA[0].STATUS == 'SUCCESS')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../goods-receive');


          };

          if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.T_DATA[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          waitingDialog.hide();
          // window.location.assign('../../goods-receive');
      })
      .catch(function (response) {
          //handle error
          // alert('Sync data SAP Error!');
          $.alert({
          type: 'red',
          icon: 'fa fa-red', // glyphicon glyphicon-heart
          title: 'Error',
          // content: response.msg,
          content: 'Synchronize data to SAP Error!',
          });

    }, 3000)
    });
  };

  function postSAP(){
    var desc_req =  $('#description').val();
    if(desc_req == '')
    {
      // alert('description cannot null');
      $.alert({
          title: 'Warning!',
          content: 'Header Text field is required!',
      });
    }else{

      axios.get('../data-header/{{$goods_receive->id}}')
        .then(response_header => {

            // console.log('transaction_id : ' + response_header.data.transaction_id);

            if(response_header.data.counter > 1)
              {
                $.alert({
                    title: 'Warning!',
                    content: 'Cannot submit more than one PO number!',
                });

              }else{
                waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});

                axios.get('../data-detail/{{$goods_receive->id}}')
                  .then(response => {
                    this.data = response.data;
                    // var time_delay = 3000;

                    // var i = 0;
                    // function myLoop() {
                    // setTimeout(function () {
                    this.data.forEach((item, i) => {

                        var dt = new Date();
                        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                        // alert('Item Line: ' + item.item_number + 'Time WMS: ' + time);

                        var options = {
                            "positionClass": "toast-bottom-right",
                            "timeOut": 3000,
                        };
                        toastr.success('Get data: ' + item.item_number + ' at: ' + time, 'Success Alert', options);

                        console.log(response);

                      // var ii = 0;
                      // function myLoopSAP() {
                      // setTimeout(function () {
                      // ----
                        axios({
                          method: 'post',
                          url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-grpod',
                          // timeout: 10000000,
                          // headers: {
                                        // 'Content-Type': 'application/json',
                                        // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                                    // },
                          data: {
                                  '_token': $('input[name=_token]').val(),
                                  'notransaction': item.notransaction,
                                  'purchasing_number': item.purchasing_number,
                                  'item_number': item.item_number,
                                  'material_number':  item.material_number,
                                  'plant':  item.plant,
                                  'storage_loc': item.storage_loc,
                                  'po_qty': item.po_qty,
                                  'po_unit': item.po_unit,
                                  'po_date': item.po_date,
                                  'posting_date': item.posting_date,
                                  'vendor_number': item.vendor_number,
                                  'batch': item.batch,
                                  'doc_header': $('#description').val(),
                                  // 'doc_header': item.doc_header,
                                  'grrcpt': item.grrcpt,
                                  'total_items': item.total_items.toString(),
                                },
                            crossdomain: true,
                          })
                          .then(function (response) {
                              //handle success
                              console.log(response);
                              var dtq = new Date();
                              var timeq = dtq.getHours() + ":" + dtq.getMinutes() + ":" + dtq.getSeconds();

                              // alert('Item Line Save SAP: ' + item.item_number + 'Time success to SAP: ' + timeq);
                              // console.log(response.data);

                              // if(item.flag == 'X'){

                                console.log('no trans: ' + response.data.T_DATA[0].NO_TRANSACTION);
                                console.log('status: ' + response.data.T_DATA[0].STATUS);

                                if(response.data.T_DATA[0].STATUS == 'NOT OK')
                                {
                                  // alert(messages);
                                  $.alert({
                                      title: 'Error!',
                                      content: response.data.T_DATA[0].MESSAGES,
                                  });
                                  axios({
                                  method: 'post',
                                  url: '../post-response-detail/{{$goods_receive->id}}',
                                  data: {
                                          '_token': $('input[name=_token]').val(),
                                          'nopo': response.data.T_DATA[0].EBELN,
                                        },
                                  })
                                };

                                if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                                {
                                  $.alert({
                                    type: 'red',
                                    icon: 'fa fa-red', // glyphicon glyphicon-heart
                                    title: 'Error',
                                    content: response.data.T_DATA[0].MESSAGE,
                                  });
                                };

                                if(response.data.T_DATA[0].STATUS == 'Success')
                                {
                                  // syncSAP();
                                  // alert('Post data SAP Success!');

                                  // $.alert({
                                  //   type: 'green',
                                  //   icon: 'fa fa-green', // glyphicon glyphicon-heart
                                  //   title: 'Success',
                                  //   content: 'Post data SAP Success!',
                                  // });
                                  var options = {
                                      "positionClass": "toast-bottom-right",
                                      "timeOut": 5000,
                                  };
                                  toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);



                                    console.log('end');
                                    // waitingDialog.hide();
                                    // window.location.assign('../../goods-receive');
                                    // axios({
                                    //   method: 'post',
                                    //       url: 'http://webapp.lmu.co.id:3000/m/v1/wms/exegr',
                                    //       headers: {
                                    //                   'Content-Type': 'application/json',
                                    //                   'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                                    //               },
                                    //   data: {
                                    //           '_token': $('input[name=_token]').val(),
                                    //           'notransaction': "{{$goods_receive->notransaction}}",
                                    //       },
                                    //   })
                                    //   .then(function (response) {
                                    //       if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                                    //       {
                                    //         axios({
                                    //           method: 'post',
                                    //           url: '../post-response/{{$goods_receive->id}}',
                                    //           data: {
                                    //                   '_token': $('input[name=_token]').val(),
                                    //                   'notransaction': response.data.T_DATA[0].NO_TRANSACTION,
                                    //                   'nodoc': response.data.T_DATA[0].MBLNR,
                                    //                   'tahundoc': response.data.T_DATA[0].MJAHR,
                                    //                   'nopo': response.data.T_DATA[0].EBELN,
                                    //                   'description': $('#description').val(),
                                    //                   'date_transaction': $('#date_transaction').val(),
                                    //                 },
                                    //           })

                                              // $.alert({
                                              //   type: 'green',
                                              //   icon: 'fa fa-green', // glyphicon glyphicon-heart
                                              //   title: 'Success',
                                              //   content: 'Synchronize data SAP Success!',
                                              // });
                                      //   $.confirm({
                                      //     title: 'Confirm!',
                                      //     content: 'post to SAP {{$goods_receive->no_transaction}} number?',
                                      //     type: 'green',
                                      //     typeAnimated: true,
                                      //     buttons: {
                                      //         cancel: {
                                      //         action: function () {
                                      //         }
                                      //         },
                                      //         confirm: {
                                      //         text: 'YES',
                                      //         btnClass: 'btn-green',
                                      //         action: function () {
                                      //         waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});

                                                  syncSAP();

                                      //         }
                                      //       },
                                      //     }
                                      // });
                                    //       };

                                    //       if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                                    //       {
                                    //         $.alert({
                                    //           type: 'red',
                                    //           icon: 'fa fa-red', // glyphicon glyphicon-heart
                                    //           title: 'Error',
                                    //           content: response.data.T_DATA[0].MESSAGE,
                                    //         });
                                    //       };
                                    //       //handle success
                                    //       console.log(response);
                                    //       waitingDialog.hide();
                                    //       // window.location.assign('../../goods-receive');
                                    //   })
                                    //   .catch(function (response) {
                                    //       //handle error
                                    //       // alert('Sync data SAP Error!');
                                    //       $.alert({
                                    //       type: 'red',
                                    //       icon: 'fa fa-red', // glyphicon glyphicon-heart
                                    //       title: 'Error',
                                    //       // content: response.msg,
                                    //       content: 'Synchronize data to SAP Error!',
                                    //       });

                                    // });


                                  // }
                                // };
                                // throw BreakException;
                              // }else{
                              //   $.alert({
                              //       type: 'red',
                              //       icon: 'fa fa-red', // glyphicon glyphicon-heart
                              //       title: 'Error',
                              //       content: response.data.T_DATA[0].MESSAGE,
                              //     });

                              }

                              // alert(response.data.T_DATA[0].STATUS);
                              // return false;
                          })
                          .catch(function (response) {
                              //handle error
                              console.log(response);
                              // alert(response.msg);
                              // alert('Post data SAP Error!');
                              $.alert({
                                type: 'red',
                                icon: 'fa fa-red', // glyphicon glyphicon-heart
                                title: 'Error',
                                // content: response.msg,
                                content: 'Post SAP Error!',
                              });

                          });
                          // ii++;

                          // }, 3000 * ii)
                        // }
                        // myLoopSAP();

                      // loop
                });

                  // i++;

                // })
              // }
              // myLoop();



              })

            };
          })
      };
    }

    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 's',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);


  function openModalMaster()
  {
    // console.log('tes');
    $('#modal_form_item').modal('show'); // show bootstrap modal

    reload_table_inventory_master();
    reload_table_inventory();
    reload_table();
  }
</script>
@stop
