@extends('layouts.main.template')
@section('title', 'Goods Receive Outstanding')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'goods-receive')   
@section('val_variable', 'goods_receive')   
@section('content') 
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat"> 
            <ul class="nav nav-tabs nav-tabs-highlight">  
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
            </li>
            <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a> 
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print Table</span></a>
            </li>
          </ul>  
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>  
                <th>PO Number</th>
                <th>Material</th> 
                <th>Description</th> 
                <th>Quantity</th>
                <th>Item</th>
                <th>UoM</th>
                <th>Vendor Code</th>
                <th>Vendor Name</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

@include('main.warehouse.goods_receive.script_report_outstanding_summary')
@include('scripts.change_status')
@stop


