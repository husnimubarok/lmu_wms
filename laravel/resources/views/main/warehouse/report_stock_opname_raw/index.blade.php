@extends('layouts.report.template')
@section('title', 'Report Stock Opname Raw')
@section('caption', 'Report for stock ending!')
@section('url_variable', 'report-stock-opname-raw')
@section('val_variable', 'report_stock_opname_raw')
@section('content')
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th>Status</th>
                <th>No SO</th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Batch</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <th>Unit</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- End Bootstrap modal -->
@include('main.warehouse.report_stock_opname_raw.script')
@include('scripts.change_status')
@stop
