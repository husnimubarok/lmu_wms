@extends('layouts.main.template')
@section('title', 'Stock Backup')
@section('caption', 'This menu for stock backup')
@section('url_variable', 'user')
@section('val_variable', 'user')
@section('content')

<!-- Page header -->
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title">
      <center>
      <h4>
        <i class="icon-arrow-right6 position-left"></i>
        <span class="text-semibold">Home</span> - @yield('title')
        <small class="display-block">@yield('title'), @yield('caption')</small>
      </h4>
    </center>
    </div>
  </div>
</div>
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Column rendering -->
        <div class="row">
          <div class="col-lg-3 col-md-3">
          </div>
            <div class="col-md-6">
              <div class="panel panel-flat">
                <div class="panel-heading">
                  <h6 class="panel-title">Daily Data for Stock Backup</h6>
                  <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="export-stock" class="btn btn-link btn-float has-text text-size-small" style="color:red"><i class="icon-toggle" style="color:red"></i> <span>EXPORT STOCK MANUAL</span></a>
                    </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="alpaca-checkbox-styled"></div>
                  <a href="#"><i class="icon-folder position-left"></i> Folder Output &rarr;</a>
                  <div class="mt-10" id="alpaca-checkbox-styled-source">
                    <pre class="language-javascript">@foreach($output as $data_outputs) <i class="icon-file-excel position-left"></i>File Name: {{$data_outputs['basename']}} <a href="{{ url('stock') }}/{{$data_outputs['basename']}}"> <i class="icon-download position-left"></i>Download</a> | <a href="delete-stock/{{$data_outputs['basename']}}"> <i class="icon-trash position-left"></i>Delete</a><br>@endforeach
                      <code>
                    </code>
                  </pre>
                  </div>
                </div>
              </div>
            </div>
    		</div>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@stop
