@extends('layouts.report.template_blank')
@section('title', 'Report Stock')   
@section('caption', 'Report for stock ending!')   
@section('url_variable', 'stock-transfer')   
@section('val_variable', 'stock_transfer')   
@section('content') 

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  @foreach($warehouse as $warehouses)
  ['{{ $warehouses->warehouse_name }} : {{ $warehouses->rack_capacity }}', {{ $warehouses->rack_capacity }}],
  @endforeach 
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Warehouse Capacity Comparasion', 'width':600, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Warehouse', 'Capacity', 'Stock', 'Available %'],
          @foreach($warehouse as $warehouses)
          @php
            $available_capacity = 100 - (($warehouses->quantity_rack / $warehouses->rack_capacity) * 100);
          @endphp
          ['{{ $warehouses->warehouse_name }}', {{ $warehouses->rack_capacity }}, {{ $warehouses->quantity_rack }}, @php echo $available_capacity; @endphp],
          @endforeach  
        ]);

        var options = {
          chart: {
            title: 'Warehouse Stock Comparasion',
            subtitle: 'Warehouse, Capacity, Stock, and Available Stock In %',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Palette colors -->
				<h6 class="content-group-sm text-semibold">
					WMS Dashboard
					<small class="display-block">Warehouse dashboard for comparasion stock and capacity</small>
				</h6>
        <div class="row">

        <input type="hidden" name="filter_rack" id="filter_rack">  
        <input type="hidden" name="filter_rack_detail" id="filter_rack_detail">  

        @foreach($warehouse as $warehouses)
        @php
          $available_capacity = 100 - (($warehouses->quantity_rack / $warehouses->rack_capacity) * 100);
        @endphp
        <div class="col-sm-4 col-lg-2" onclick="open_modal({{ $warehouses->id }});">
          <div class="panel">
            @if($available_capacity > 50)
            <div class="bg-green-700 demo-color"><span>Available : @php echo number_format($available_capacity,2); @endphp %</span></div>
            @elseif($available_capacity < 50 && $available_capacity > 30 )
            <div class="bg-orange-700 demo-color"><span>Available : @php echo number_format($available_capacity,2); @endphp %</span></div>
            @else
            <div class="bg-danger-700 demo-color"><span>Available : @php echo number_format($available_capacity,2); @endphp %</span></div>
            @endif
            <div class="p-15">
              <div class="media-body">
                <strong>{{ $warehouses->warehouse_name }}</strong>
                <div class="text-muted mt-5">Capacity: {{ $warehouses->rack_capacity }} Rack</div>
                <div class="text-muted mt-5">Stock: {{ $warehouses->quantity_rack }} Rack</div>
                <div class="text-muted mt-5">Available: {{ $warehouses->rack_capacity - $warehouses->quantity_rack }} Rack</div>
              </div>

              <div class="media-right">
                <ul class="icons-list">
                  <li><a href="#" onclick="open_modal({{ $warehouses->id }})"><i class="icon-three-bars"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        @endforeach
 
        </div>
        <!-- /palette colors -->

        <!-- Blue modal -->
				<div id="modal_blue" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header bg-blue">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">Blue header</h6>
							</div>

							<div class="modal-body">
								<h6 class="text-semibold mt-5">Text in a modal</h6>
								<p>Friendship contrasted solicitude insipidity in introduced literature it. He seemed denote except as oppose do spring my. Between any may mention evening age shortly can ability regular. He shortly sixteen of colonel colonel evening cordial to.</p>

								<hr>

								<h6 class="text-semibold">Another paragraph</h6>
								<p>Comfort reached gay perhaps chamber his six detract besides add. Moonlight newspaper up he it enjoyment agreeable depending. Timed voice share led his widen noisy young.</p>
								<p>Up attempt offered ye civilly so sitting to. She new course get living within elinor joy. She her rapturous suffering concealed.</p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
								<button type="button" class="btn bg-blue">Save changes</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /blue modal -->


	      <!-- Blue 800 -->
				<div id="blue_800" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Data Stock by Rack </h5>
							</div>

							<div class="modal-body">
                WAREHOUSE Rack Position
							</div>

							<div class="table-responsive content-group">
								<table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
                  <thead>
                    <tr>
                    <th>Warehouse</th> 
                    <th>Room</th> 
                    <!-- <th>Bay</th>  -->
                    <th>Rack</th>  
                    <th>Batch</th>
                    <th>Stock</th>
                    <th>Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
								</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-link btn-xs text-uppercase text-semibold" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /blue 800 -->


        <!-- Blue 800 -->
				<div id="blue_900" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Data Stock by Item </h5>
							</div>

							<div class="modal-body">
                TOTAL Item Position
							</div>

							<div class="table-responsive content-group">
								<table id="dtTableDetail" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
                  <thead>
                    <tr>
                    <th>Item Code</th> 
                    <th>Item Name</th> 
                    <th>UoM</th>  
                    <th>Stock</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
								</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-link btn-xs text-uppercase text-semibold" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /blue 800 -->


			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->


<div class="row">
  <div class="col-md-6">
    <!-- Basic pie chart -->
    <div class="panel panel-flat">
      <div class="panel-heading">
        <h5 class="panel-title">Warehouse Capacity Comparasion</h5>
        <div class="heading-elements">
          <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
            <li><a data-action="reload"></a></li>
            <li><a data-action="close"></a></li>
          </ul>
        </div>
      </div>
      <div class="panel-body">
        <!-- <div class="chart-container has-scroll"> -->
          <div class="chart has-fixed-height has-minimum-width" id="piechart"></div>
        <!-- </div> -->
      </div>
    </div>
    <!-- /bacis pie chart -->
  </div>
  <!-- /main content -->

  <div class="col-md-6">

    <!-- Basic pie chart -->
    <div class="panel panel-flat">
      <div class="panel-heading">
        <h5 class="panel-title">Warehouse Stock Comparasion</h5>
        <div class="heading-elements">
          <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
            <li><a data-action="reload"></a></li>
            <li><a data-action="close"></a></li>
          </ul>
        </div>
      </div>
      <div class="panel-body">
        <div class="chart-container has-scroll">
          <div class="chart has-fixed-height has-minimum-width" id="barchart_material"></div>
        </div>
      </div>
    </div>
    <!-- /bacis pie chart -->
  </div>
  <!-- /main content -->
</div>
<!-- /page container -->
<script>

var table_detail;
$(document).ready(function() {
  //datatables
  table_detail = $('#dtTableDetail').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        url: "{{ url('/main/warehouse/data-stream-layout/data-detail') }}", 
        data: function (d) {
            d.filter_rack_detail = $('input[name=filter_rack_detail]').val();  
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   scrollX: 150,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
             ? '.buttons-excel'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [   
    { data: 'inventory_code', name: 'inventory_code' },  
    { data: 'inventory_name', name: 'inventory_name' },  
    { data: 'unit', name: 'unit' },  
    { data: 'quantity', name: 'quantity' }, 
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });


  $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });
</script>

<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        url: "{{ url('/main/warehouse/data-stream-layout/data') }}", 
        data: function (d) {
            d.filter_rack = $('input[name=filter_rack]').val();  
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   scrollX: 150,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
             ? '.buttons-excel'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [   
   { data: 'warehouse_name', name: 'warehouse_name' }, 
   { data: 'room_name', name: 'room_name' }, 
  //  { data: 'bay_name', name: 'bay_name' }, 
   { data: 'rack_name', name: 'rack_name' },  
   { data: 'batch', name: 'batch' },  
   { data: 'quantity', name: 'quantity' },  
   { data: 'action', name: 'action' },  
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });


  $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });

  function open_modal(id)
  { 
    $("#filter_rack").val(id);

    reload_table();
    $('#blue_800').modal('show'); 
    reload_table();

  }

  function open_modal_detail(id)
  { 
    $("#filter_rack_detail").val(id);

    reload_table_detail();
    $('#blue_900').modal('show'); 
    reload_table_detail();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function reload_table_detail()
  {
    table_detail.ajax.reload(null,false); //reload datatable ajax 
  }


  function filter_rack(id)
  {
    $("#filter_rack").val('0');
    reload_table();
  }
</script>
@stop


