@extends('layouts.main.template')
@section('title', 'Stock Adjustment')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'stock_adjustment')
@section('val_variable', 'stock_adjustment')
@section('content')
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($stock_adjustment, array('route' => ['main.stock-adjustment.update', $stock_adjustment->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">

              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'No Transaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'no_transaction', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Date Trancaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($stock_adjustment))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($stock_adjustment->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '7', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      ADDITIONAL INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'No SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('nodoc', old('nodoc'), ['class' => 'form-control', 'placeholder' => 'No SAP', 'id' => 'nodoc', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Year', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('year', old('year'), ['class' => 'form-control', 'placeholder' => 'Posting Year', 'id' => 'year', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>
                         <th>Action</th>
                         <th>Material</th>
                         <th>Description</th>
                         <th>Quantity Stock</th>
                         <th>Quantity Adjustment</th>
                         <th>UOM</th>
                         <th>Batch</th>
                         <th>Warehouse Name</th>
                         <th>Room</th>
                         <th>Bay</th>
                         <th>Rack</th>
                         <th>Pallet Number</th>
                         <th>Plant</th>
                         <th>Store Loc</th>
                      </tr>
                    </thead>
                    <thead>
                      <tr>
                        <td>
                          <a href="#" class="btn btn-primary  btn-float btn-xs" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                          <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a>
                          <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                        </td>
                        <td>
                            {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }}
                            {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }}
                            {{ Form::text('inventory_code', old('inventory_code'), ['class' => 'form-control', 'id' => 'inventory_code', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'id' => 'inventory_name']) }}
                        </td>
                        <td>
                          {{ Form::number('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::number('quantity_adjustment', old('quantity_adjustment'), ['class' => 'form-control', 'id' => 'quantity_adjustment']) }}
                        </td>
                        <td>
                          {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('batch', old('batch'), ['class' => 'form-control', 'id' => 'batch', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('warehouse_name', old('warehouse_name'), ['class' => 'form-control', 'id' => 'warehouse_name', 'readonly']) }}
                          {{ Form::hidden('warehouse_id', old('warehouse_id'), ['class' => 'form-control', 'id' => 'warehouse_id', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('room_name', old('room_name'), ['class' => 'form-control', 'id' => 'room_name', 'readonly']) }}
                          {{ Form::hidden('room_id', old('room_id'), ['class' => 'form-control', 'id' => 'room_id', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('bay_name', old('bay_name'), ['class' => 'form-control', 'id' => 'bay_name', 'readonly']) }}
                          {{ Form::hidden('bay_id', old('bay_id'), ['class' => 'form-control', 'id' => 'bay_id', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('rack_name', old('rack_name'), ['class' => 'form-control', 'id' => 'rack_name', 'readonly']) }}
                          {{ Form::hidden('rack_id', old('rack_id'), ['class' => 'form-control', 'id' => 'rack_id', 'readonly']) }}
                        </td>
                        <td>
                          {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number', 'readonly']) }}
                        </td>
                        <td>
                           <select class="select-search" id="plant" name="plant">
                             @foreach($plant_list as $plant_lists)
                             <option value="{{$plant_lists->store_location_code}}">{{$plant_lists->store_location_name}}</option>
                             @endforeach
                           </select>
                        </td>
                        <td>
                           <select class="select-search" id="store_loc" name="store_loc">
                             @foreach($store_loc_list as $store_loc_lists)
                             <option value="{{$store_loc_lists->store_location_code}}">{{$store_loc_lists->store_location_name}}</option>
                             @endforeach
                           </select>
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              <!-- @if(!isset($stock_adjustment->nodoc))
              <a href="#"  class="btn btn-primary" onclick="postSAP();"> SAVE <i class="icon-arrow-right14 position-right"></i></a>
              @else
              <a href="#"  class="btn btn-danger" id="cancel_gr" onclick="cancelPosting();"> CANCEL <i class="icon-cancel-circle2 position-right"></i></a>
            @endif-->


              @if($stock_adjustment->code_transaction == 'SADM')
                <a href="#"  class="btn btn-danger" onclick="cancelMinus();"> Cancel Adjusment minus <i class="icon-cancel-circle2 position-right"></i></a>
                @if(strlen($stock_adjustment->nodoc)==0)
                  <a href="#"  class="btn btn-primary" onclick="postSAPMINUS();"> SAVE Adjusment minus <i class="icon-arrow-right14 position-right"></i></a>
                @endif

                @else
                  <a href="#"  class="btn btn-danger" onclick="cancelPlus();"> CANCEL Adjusment plus <i class="icon-cancel-circle2 position-right"></i></a>
                @if(strlen($stock_adjustment->nodoc)==0)
                  <a href="#"  class="btn btn-primary" onclick="postSAPPLUS();"> SAVE Adjusment plus <i class="icon-arrow-right14 position-right"></i></a>
                @endif
              @endif
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:80% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Stock</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$stock_adjustment->code_transaction}}">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Batch</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <th>Unit</th>
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
    ajax: {
      url: "{{ url('main/warehouse/stock-adjustment/get-detail') }}/{{$stock_adjustment->id}}",
      type : "POST",
      headers : {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
     },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'material_number', name: 'material_number' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'quantity', name: 'quantity' },
     { data: 'quantity_adjustment', name: 'quantity_adjustment' },
     { data: 'po_unit', name: 'po_unit' },
     { data: 'batch', name: 'batch' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'plant', name: 'plant' },
     { data: 'store_loc', name: 'store_loc' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });


  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ url('main/warehouse/stock-adjustment/data-modal') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.code_transaction = $('input[name=code_transaction]').val();
              d.keyword = $('input[name=keyword]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
      { data: 'warehouse_name', name: 'warehouse_name' },
      { data: 'room_name', name: 'room_name' },
      { data: 'bay_name', name: 'bay_name' },
      { data: 'rack_name', name: 'rack_name' },
      { data: 'pallet_number', name: 'pallet_number' },
      { data: 'batch', name: 'batch' },
      { data: 'inventory_code', name: 'inventory_code' },
      { data: 'inventory_name', name: 'inventory_name' },
      { data: 'unit', name: 'unit' },
      { data: 'quantity', name: 'quantity' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorStock AdjustmentName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
  }

  function addValue(str, warehouse_id, bay_id, room_id, rack_id, inventory_id, id_detail){
    clearDetail();
    var inventory_id = inventory_id;
    var inventory_code = $(str).closest('tr').find('td:eq(7)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(8)').text();
    var quantity = $(str).closest('tr').find('td:eq(10)').text();

    var warehouse_name = $(str).closest('tr').find('td:eq(1)').text();
    var room_name = $(str).closest('tr').find('td:eq(2)').text();
    var bay_name = $(str).closest('tr').find('td:eq(3)').text();
    var rack_name = $(str).closest('tr').find('td:eq(4)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(5)').text();
    var batch = $(str).closest('tr').find('td:eq(6)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(9)').text();


    console.log(unit_of_measure_code);


    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);

    $("#warehouse_id").val(warehouse_id);
    $("#room_id").val(room_id);
    $("#bay_id").val(bay_id);
    $("#rack_id").val(rack_id);

    $("#inventory_id").val(inventory_id);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#quantity").val(quantity);
    $("#batch").val(batch);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    setBtnAdd();
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var quantity_adjustment = $('#quantity_adjustment').val();
     var inventory_id = $('#inventory_id').val();
     var unit_of_measure_code =   $('#unit_of_measure_code').val();
     var quantity =   $('#quantity').val();
     var quantity_transfer =   $('#quantity_transfer').val();

     if(inventory_id == '' || quantity_adjustment == ''){
      // alert("Error validarion!");
      $.alert({
          title: 'Error!',
          type: 'red',
          content: 'All data is required!',
      });
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-adjustment/post-detail/{{$stock_adjustment->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'inventory_code': $('#inventory_code').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'quantity_adjustment': $('#quantity_adjustment').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'batch': $('#batch').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id, inventory_id_transfer)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(inventory_id);

    var inventory_code = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(2)').text();
    var quantity = $(str).closest('tr').find('td:eq(3)').text();
    var quantity_adjustment = $(str).closest('tr').find('td:eq(4)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(5)').text();
    var batch = $(str).closest('tr').find('td:eq(6)').text();
    var warehouse_name = $(str).closest('tr').find('td:eq(7)').text();
    var room_name = $(str).closest('tr').find('td:eq(8)').text();
    var bay_name = $(str).closest('tr').find('td:eq(9)').text();
    var rack_name = $(str).closest('tr').find('td:eq(10)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(11)').text();
    var plant = $(str).closest('tr').find('td:eq(12)').text();
    var store_loc = $(str).closest('tr').find('td:eq(13)').text();


    $("#inventory_id").val(inventory_id);
    $("#id_detail").val(id_detail);

    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#store_loc").val(store_loc);
    $("#plant").val(plant);
    $("#quantity").val(quantity);
    $("#quantity_adjustment").val(quantity_adjustment);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#batch").val(batch);
    $("#unit_of_measure_code").val(unit_of_measure_code);

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =  $('#quantity').val();

    if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-adjustment/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'quantity_adjustment': $('#quantity_adjustment').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };

  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_code').val(''),
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#pallet_number').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#batch').val(''),
    $('#quantity_adjustment').val(''),
    $('#unit_of_measure_code').val('')
  };

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-adjustment/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function cancelMinus(){
   $.confirm({
       title: 'Confirm!',
       content: 'Are you sure to cancel {{$stock_adjustment->no_transaction}} number?',
       type: 'red',
       typeAnimated: true,
       buttons: {
         cancel: {
          action: function () {
          }
        },
        confirm: {
         text: 'YES',
         btnClass: 'btn-red',
         action: function () {
           waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
           axios({
             method: 'post',
                 url: 'https://wms.lmu.co.id/main/warehouse/goods-receive/sap-cancelgr',
                 // headers: {
                               // 'Content-Type': 'application/json',
                               // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                           // },
             data: {
                     '_token': $('input[name=_token]').val(),
                     'notransaction': "{{$stock_adjustment->no_transaction}}",
                     'nodoc': "{{$stock_adjustment->nodoc}}",
                     'tahundoc': "{{$stock_adjustment->year}}",
                     'nopo': "111111",
                 },
              // crossdomain: true,
             })
             .then(function (response) {
                 axios({
                   method: 'post',
                   url: '../cancel/{{$stock_adjustment->id}}',
                   data: {
                           '_token': $('input[name=_token]').val(),
                         },
                 })

                 //handle success
                 console.log(response);
                 if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                 {
                   $.alert({
                     type: 'green',
                     icon: 'fa fa-green', // glyphicon glyphicon-heart
                     title: 'Success',
                     content: 'Cancel data SAP Success!',
                   });
                 };

                 if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                 {
                   $.alert({
                     type: 'red',
                     icon: 'fa fa-red', // glyphicon glyphicon-heart
                     title: 'Cancel Error!',
                     content: 'Message SAP : ' + response.data.T_DATA[0].MESSAGE,
                   });
                 };
                 waitingDialog.hide();
                 window.location.assign('../../stock-adjustment');
                 // window.location.assign('../../goods-receive');
             })
             .catch(function (response) {
                 //handle error
                 console.log(response);
                 alert('Post data SAP Error!');

             });
           }
       },
     }
  });
 }

 function cancelPlus(){
   $.confirm({
       title: 'Confirm!',
       content: 'Are you sure to cancel {{$stock_adjustment->no_transaction}} number?',
       type: 'red',
       typeAnimated: true,
       buttons: {
         cancel: {
          action: function () {
          }
        },
        confirm: {
         text: 'YES',
         btnClass: 'btn-red',
         action: function () {
           waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
           axios({
             method: 'post',
                 // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-cancelgr',
                 url: 'https://wms.lmu.co.id/main/warehouse/goods-receive/sap-cancelgr',
                 // headers: {
                               // 'Content-Type': 'application/json',
                               // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                           // },
             data: {
                     '_token': $('input[name=_token]').val(),
                     'notransaction': "{{$stock_adjustment->no_transaction}}",
                     'nodoc': "{{$stock_adjustment->nodoc}}",
                     'tahundoc': "{{$stock_adjustment->year}}",
                     'nopo': "111111",
                 },
              // crossdomain: true,
             })
             .then(function (response) {
                 axios({
                   method: 'post',
                   url: '../cancel/{{$stock_adjustment->id}}',
                   data: {
                           '_token': $('input[name=_token]').val(),
                         },
                 })

                 //handle success
                 console.log(response);
                 if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                 {
                   $.alert({
                     type: 'green',
                     icon: 'fa fa-green', // glyphicon glyphicon-heart
                     title: 'Success',
                     content: 'Cancel data SAP Success!',
                   });
                 };

                 if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                 {
                   $.alert({
                     type: 'red',
                     icon: 'fa fa-red', // glyphicon glyphicon-heart
                     title: 'Cancel Error!',
                     content: 'Message SAP : ' + response.data.T_DATA[0].MESSAGE,
                   });
                 };
                 waitingDialog.hide();
                window.location.assign('../../stock-adjustment');

                 // window.location.assign('../../goods-receive');
             })
             .catch(function (response) {
                 //handle error
                 console.log(response);
                 alert('Post data SAP Error!');

             });
           }
       },
     }
  });
 }


  function syncSAPMINUS(){
    setTimeout(function () {
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-exeadjustminus',
          url: '../sap-exeadjustminus',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
      data: {
              '_token': $('input[name=_token]').val(),
              'notrx': $('#no_transaction').val(),
          },
      // crossdomain: true,
      })
      .then(function (response) {
            axios({
              method: 'post',
              url: '../post-response/{{$stock_adjustment->id}}',
              data: {
                      '_token': $('input[name=_token]').val(),
                      'nodoc': response.data.T_GRAJM[0].MBLNR,
                      'year': response.data.T_GRAJM[0].MJAHR,
                      'description': $('#description').val(),
                    },
          })
          waitingDialog.hide();

          if(response.data.T_GRAJM[0].STATUS == 'SUCCESS')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../stock-adjustment');

          };

          if(response.data.T_GRAJM[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.T_GRAJM[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          // waitingDialog.hide();
          // window.location.assign('../../goods-receive');
      })
      .catch(function (response) {
          //handle error
          // alert('Sync data SAP Error!');
          $.alert({
          type: 'red',
          icon: 'fa fa-red', // glyphicon glyphicon-heart
          title: 'Error',
          // content: response.msg,
          content: 'Synchronize data to SAP Error!',
          });

    }, 3000)
    });
  };

  function postSAPMINUS(){
    // alert('test');
    var desc_req =  $('#description').val();
    if(desc_req == '')
    {
      $.alert({
          title: 'Warning!',
          type: 'red',
          content: 'Header Text field is required!',
      });
    }else{
        waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});

        axios({
          method: 'post',
          url: '../save-header/{{$stock_adjustment->id}}',
          data: {
                  '_token': $('input[name=_token]').val(),
                  'description': $('#description').val(),
                  'date_transactin': $('#date_transactin').val(),
                },
        })

        axios.get('../data-detail/{{$stock_adjustment->id}}')
          .then(response => {
            this.data = response.data;

            this.data.forEach((item, i) => {

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                var options = {
                    "positionClass": "toast-bottom-right",
                    "timeOut": 3000,
                };
                toastr.success('Get data: ' + item.material_from + ' at: ' + time, 'Success Alert', options);

                console.log(response);
                console.log($('#description').val());
                axios({
                  method: 'post',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-adjustminus',
                  url: '../sap-adjustminus',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'notrx': item.no_transaction,
                          'item': item.item_line.toString(),
                          'matrial': item.matrial,
                          'plant': item.plant,
                          'storloc': item.storage,
                          'qty': item.quantity_adjustment,
                          'unit': item.unit_of_measure_code,
                          'docdate': item.doc_date,
                          'postdate': item.post_date,
                          'batch': item.batch,
                          // 'batch': item.batch,
                          'textheader': $('#description').val(),
                          'user': item.user_name,
                          'lines': item.total_items.toString(),
                        },
                    // crossdomain: true,
                  })
                  .then(function (response) {

                      console.log('notrx' + item.no_transaction);
                      console.log('item' + item.item_line.toString());
                      console.log('matrial' + item.matrial);
                      console.log('plant' + item.plant);
                      console.log('storloc' + item.storage);
                      console.log('qty' + item.quantity_adjustment);
                      console.log('unit' + item.unit_of_measure_code);
                      console.log('docdate' + item.doc_date);
                      console.log('postdate' + item.post_date);
                      console.log('batch' + item.batch);
                      console.log('textheader' + $('#description').val());
                      console.log('user' + item.user_name);
                      console.log('lines' + item.total_items);
                      console.log(data);
                      var dtq = new Date();
                      var timeq = dtq.getHours() + ":" + dtq.getMinutes() + ":" + dtq.getSeconds();

                          if(response.data.T_GRAJM[0].STATUS == 'Success')
                          {
                            syncSAPMINUS();
                            var options = {
                                "positionClass": "toast-bottom-right",
                                "timeOut": 5000,
                            };
                            toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                            console.log('end');
                        }
                        // waitingDialog.hide();
                  })
                  .catch(function (response) {
                      //handle error
                      waitingDialog.hide();
                      console.log(response);
                      // alert(response.msg);
                      // alert('Post data SAP Error!');
                      $.alert({
                        type: 'red',
                        icon: 'fa fa-red', // glyphicon glyphicon-heart
                        title: 'Error',
                        content: response.MESSAGE,
                        // content: 'Post SAP Error!',
                      });
                  });
              })
          })
      };
    }

 function syncSAPPLUS(){
    setTimeout(function () {
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-exeadjustplus',
          url: '../sap-exeadjustplus',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
          data: {
                  '_token': $('input[name=_token]').val(),
                  'notrx': $('#no_transaction').val(),
              },
          crossdomain: true,
      })
      .then(function (response) {
            axios({
              method: 'post',
              url: '../post-response/{{$stock_adjustment->id}}',
              data: {
                      '_token': $('input[name=_token]').val(),
                      'nodoc': response.data.T_GRAJS[0].MBLNR,
                      'year': response.data.T_GRAJS[0].MJAHR,
                      'description': $('#description').val(),
                    },
          })
          waitingDialog.hide();

          if(response.data.T_GRAJS[0].STATUS == 'SUCCESS')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../stock-adjustment');

          };

          if(response.data.T_GRAJS[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.T_TFDATA[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          // waitingDialog.hide();

          // window.location.assign('../../goods-receive');
      })
      .catch(function (response) {
          //handle error
          // alert('Sync data SAP Error!');
          // $.alert({
          // type: 'red',
          // icon: 'fa fa-red', // glyphicon glyphicon-heart
          // title: 'Error',
          // content: response.msg,
          // content: 'Synchronize data to SAP Error!',
          // });

    }, 3000)
    });
  };

  function postSAPPLUS(){
    // alert('test');
    var desc_req =  $('#description').val();
    if(desc_req == '')
    {
      $.alert({
          title: 'Warning!',
          type: 'red',
          content: 'Header Text field is required!',
      });
    }else{
        waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});

        axios({
          method: 'post',
          url: '../save-header/{{$stock_adjustment->id}}',
          data: {
                  '_token': $('input[name=_token]').val(),
                  'description': $('#description').val(),
                  'date_transactin': $('#date_transactin').val(),
                },
        })

        axios.get('../data-detail/{{$stock_adjustment->id}}')
          .then(response => {
            this.data = response.data;

            this.data.forEach((item, i) => {

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                var options = {
                    "positionClass": "toast-bottom-right",
                    "timeOut": 3000,
                };
                toastr.success('Get data: ' + item.material_from + ' at: ' + time, 'Success Alert', options);

                console.log(response);
                console.log($('#description').val());
                axios({
                  method: 'post',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-adjustplus',
                  url: '../sap-adjustplus',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'notrx': item.no_transaction,
                          'item': item.item_line.toString(),
                          'matrial': item.matrial,
                          'plant': item.plant,
                          'storloc': item.storage,
                          'qty': item.quantity_adjustment,
                          'unit': item.unit_of_measure_code,
                          'docdate': item.doc_date,
                          'postdate': item.post_date,
                          'batch': item.batch,
                          // 'batch': item.batch,
                          'textheader': $('#description').val(),
                          'user': item.user_name,
                          'lines': item.total_items.toString(),
                        },
                    // crossdomain: true,
                  })
                  .then(function (response) {

                      console.log('notrx' + item.no_transaction);
                      console.log('item' + item.item_line.toString());
                      console.log('matrial' + item.matrial);
                      console.log('plant' + item.plant);
                      console.log('storloc' + item.storage);
                      console.log('qty' + item.quantity_adjustment);
                      console.log('unit' + item.unit_of_measure_code);
                      console.log('docdate' + item.doc_date);
                      console.log('postdate' + item.post_date);
                      console.log('batch' + item.batch);
                      console.log('textheader' + $('#description').val());
                      console.log('user' + item.user_name);
                      console.log('lines' + item.total_items);
                      console.log(data);
                      var dtq = new Date();
                      var timeq = dtq.getHours() + ":" + dtq.getMinutes() + ":" + dtq.getSeconds();

                          if(response.data.T_GRAJS[0].STATUS == 'Success')
                          {
                            syncSAPPLUS();
                            var options = {
                                "positionClass": "toast-bottom-right",
                                "timeOut": 5000,
                            };
                            toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                            console.log('end');
                        }
                        // waitingDialog.hide();
                  })
                  .catch(function (response) {
                      //handle error
                      waitingDialog.hide();
                      console.log(response);
                      // alert(response.msg);
                      // alert('Post data SAP Error!');
                      $.alert({
                        type: 'red',
                        icon: 'fa fa-red', // glyphicon glyphicon-heart
                        title: 'Error',
                        content: response.MESSAGE,
                        // content: 'Post SAP Error!',
                      });
                  });
              })
          })
      };
    }

      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });

      /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);

</script>
@stop
