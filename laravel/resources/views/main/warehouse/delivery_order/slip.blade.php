<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>LMU WMS - Surat Jalan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="https://www.prepbootstrap.com/Content/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://www.prepbootstrap.com/Content/font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="https://www.prepbootstrap.com/Content/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="https://www.prepbootstrap.com/Content/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<style>
  body {
         width: 100%;
         height: 100%;
         margin: 0;
         padding: 0;
       }
       * {
           box-sizing: border-box;
           -moz-box-sizing: border-box;
       }

       .page {
           width: 210mm;
           padding: 5mm;
           margin: 5mm auto;
           border: 1px #D3D3D3 solid;
           border-radius: 5px;
           box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
           background-size: 210mm auto;
           font-size: 7pt;
           position: relative;
       }


       @media print {
          html, body {
              width: 210mm;
          }
          .page {
              margin: 0;
              border: initial;
              border-radius: initial;
              width: initial;
              min-height: initial;
              box-shadow: initial;
              background: initial;
              page-break-after: always;
              /*background: url('invoice_matrix_alt2.jpg');*/
            background-size: 100% auto;
          }
          #print{display: none;}
          .page .detail{
          background-color: #f3f3f3 !important;
        }
      }

      .panel-body {
          padding: 8px !important;
      }
</style>

<div class="container">


<!-- Simple Invoice - START -->
<div class="container">
   <div class="page">
    <div class="row">
        <div class="row">
            <div class="col-xs-5 col-md-5 col-lg-5 pull-left">
                <div class="panel-body">
                    <img src="{{url('images') }}/logo_report.png" alt="Logo" height="35" width="25"> <br>
                    <strong>PT. LARIS MANIS UTAMA</strong><br>
                    <p>Jl. Raya Bekasi km 21,5 No 168 <br/> rw Terate, Cakung, Jakarta Timur 13920</p><br>
                    <p>NPWP : {{ $preference->pkp_no }} <br/> Tgl PKP : {{ $preference->pkp_date }}</p><br>

                    <strong>Pembeli : </strong>
                    <p>
                      @if(isset($delivery_order))
                        {{ $delivery_order->customer_name }} <br>
                        {!! $delivery_order->customer_address !!} 
                      @endif
                      <br>
                    </p>
                </div>
            </div>
            <div class="col-xs-2 col-md-2 col-lg-2">
                  <center>
                    <h6> <strong>SURAT JALAN </strong> </h6>
                    <p>
                    @if(isset($delivery_order))
                    {{ $delivery_order->no_transaction }}
                    @endif
                    </p>
                  </center>
            </div>
            <div class="col-xs-5 col-md-5 col-lg-5">
                <div class="panel-body" style="margin-top: -30px !important">
                  <h1 align="right"> SJ </h1>
                  <p align="right"> {{ $preference->php_date }} </p>
                  <p align="right">
                    <strong>Kirim Dari : </strong><br>
                    Jalan Raya Bekasi Km 21,5 no 168 <br>
                    JAKARTA TIMUR 13920 <br>
                    Telp. (021) 224 62726 <br>
                    <!-- Kirim Ke : HERO DC CIBITUNG <br> -->
                    <!-- JL. INDO FARMA RT. 001 / RW. 007 DESA SUKA DANAU, BEKASI 00000 -->
                  </p>

                  <p align="right">
                    <strong>Kirim Ke : </strong> <br>
                    @if(isset($delivery_order))
                    {!! str_replace("</p>","", str_replace("<p>", "", $delivery_order->send_to)) !!}
                    @endif
                  </p>
                </div>
            </div>
            
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" style="margin-bottom: -2px !important">
                            <thead>
                                <tr>
                                    <td class="text-center"><strong>NO</strong></td>
                                    <td class="text-center"><strong>NAMA DAN SIZE BARANG</strong></td>
                                    <td class="text-center"><strong>QTY</strong></td>
                                    <td class="text-right"><strong>UOM</strong></td>
                                    <td class="text-right"><strong>KETERANGAN</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($delivery_order_detail as $delivery_order_details)
                                <tr>
                                    <td class="text-center">@php echo $i++ @endphp</td>
                                    <td class="text-left">{{ $delivery_order_details->inventory_name }}</td>
                                    <td class="text-right">{{ $delivery_order_details->quantity }}</td>
                                    <td class="text-right">{{ $delivery_order_details->unit_of_measure_code }}</td>
                                    <td class="text-right">{{ $delivery_order_details->notes_item }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="row">
       <div class="col-xs-12 col-md-12 col-lg-12 pull-left">
          <div class="panel-body">
            <p>Notes : {{$delivery_order->description}} </p>
          </div>
        </div>
        <div class="col-xs-12 col-md-12 col-lg-12 pull-left">
          <div class="panel-body">
            <p>BARANG SUDAH DITERIMA DALAM KEADAAN BAIK DAN CUKUP oleh: </p>
          </div>
        </div>

        <div class="col-xs-4 col-md-4 col-lg-4 pull-left">
          <div class="panel-body">
            <center>
              <strong>PENERIMA / PEMBELI </strong>
              <br>
              <br>
              <br>
              <br>
              <br>
              <p>(_____________________________)</p>
            </center>
            <p>Nama Jelas : <br>
            Tanggal : </p>
          </div>
        </div>

        <div class="col-xs-4 col-md-4 col-lg-4 pull-left">
          <div class="panel-body">
            <center>
              <strong>DIBUAT </strong>
              <br>
              <br>
              <br>
              <br>
              <br>
              <p>(_____________________________)</p>
            </center>
          </div>
        </div>

        <div class="col-xs-4 col-md-4 col-lg-4 pull-left">
          <div class="panel-body">
            <center>
              <strong>SUPIR </strong>
              <br>
              <br>
              <br>
              <br>
              <br>
              <p>(_____________________________)</p>
            </center>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5 col-md-5 col-lg-5 pull-left">
            <div class="panel-body">
              <div class="panel panel-default">
                  <strong style="font-size: 14px; padding: 20px">For Internal Use </strong>
              </div>
            </div>
        </div>
        <div class="col-xs-2 col-md-2 col-lg-2">
              <center>
                <h6> <strong>SURAT JALAN </strong> </h6>
                <p>
                  @if(isset($delivery_order))
                  {{ $delivery_order->no_transaction }}
                  @endif
                </p>
              </center>
        </div>
        <div class="col-xs-5 col-md-5 col-lg-5">
            <div class="panel-body" style="margin-top: -30px !important">
              <h1 align="right"> SJ </h1>
              <p align="right">   
                @if(isset($delivery_order))
                {{ $delivery_order->date_transaction }}
                @endif
              </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" style="margin-bottom: -2px !important">
                            <thead>
                                <tr>
                                    <td class="text-center"><strong>SPP</strong></td>
                                    <td class="text-center"><strong>SALES ORDER</strong></td>
                                    <td class="text-center"><strong>DELIVERY</strong></td>
                                    <td class="text-center"><strong>JAM KIRIM</strong></td>
                                    <td class="text-center"><strong>NAMA SUPIR</strong></td>
                                    <td class="text-center"><strong>TELP SUPIR</strong></td>
                                    <td class="text-center"><strong>NO. POL</strong></td>
                                    <td class="text-center"><strong>BAG.GUDANG</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                              @if(isset($delivery_order))
                                <tr>
                                    <td class="text-center">{{ $delivery_order->spp }}</td>
                                    <td class="text-center">{{ $delivery_order->sales_order }}</td>
                                    <td class="text-center">{{ $delivery_order->delivery }}</td>
                                    <td class="text-center">{{ $delivery_order->jam_kirim }}</td>
                                    <td class="text-center">{{ $delivery_order->driver }}</td>
                                    <td class="text-center">{{ $delivery_order->tlp_driver }}</td>
                                    <td class="text-center">{{ $delivery_order->no_pol }}</td>
                                    <td class="text-center">{!! $delivery_order->bag_gudang !!}</td>
                                </tr>
                              @else
                              <tr>
                                    <td class="text-center" colspan="8">
                                      <strong><p style="color: red">Tidak ada barang diinput </p></strong>
                                    </td>
                                </tr>
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-condensed" style="margin-bottom: -5px !important">
                              <thead>
                                  <tr>
                                      <td class="text-center"><strong>SPP</strong></td>
                                      <td class="text-center"><strong>SALES ORDER</strong></td>
                                      <td class="text-center"><strong>DELIVERY</strong></td>
                                  </tr>
                              </thead>
                              <tbody>
                                @if(isset($delivery_order))
                                  <tr>
                                      <td class="text-center">
                                        @if(isset($delivery_order->spp))
                                        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" /> <br> <br>
                                        <strong>{{ $delivery_order->spp }}</strong>
                                        @endif
                                      </td>
                                      <td class="text-center">
                                       @if(isset($delivery_order->sales_order))
                                        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('12', 'C39')}}" alt="barcode" /> <br> <br>
                                        <strong>{{ $delivery_order->sales_order }}</strong>
                                        @endif
                                      </td>
                                      <td class="text-center">
                                       @if(isset($delivery_order->sales_order))
                                        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('13', 'C39')}}" alt="barcode" /> <br> <br>
                                        <strong>{{ $delivery_order->delivery }}</strong>
                                        @endif
                                      </td>
                                  </tr>
                                @else
                                <tr>
                                      <td class="text-center" colspan="3">
                                        <strong><p style="color: red">Tidak ada barang diinput </p></strong>
                                      </td>
                                  </tr>
                                @endif
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
</div>

<style>
.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
</style>

<!-- Simple Invoice - END -->

</div>

</body>
</html>
