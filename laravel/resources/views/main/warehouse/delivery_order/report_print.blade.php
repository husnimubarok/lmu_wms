@extends('layouts.main.template')
@section('title', 'Goods Issue History')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'goods-receive')   
@section('val_variable', 'goods_receive')   
@section('content') 
@include('layouts.main.report_form.index_button_print')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content"> 

  <!-- Main content -->
  <div class="content-wrapper">

      <!-- Invoice template -->
      <div class="panel panel-white"> 

        <div class="panel-body no-padding-bottom">
          <div class="row">
            <div class="col-md-4 col-lg-4 content-group">
              <ul class="list-condensed list-unstyled">
                <li id="titlePrint" ><h6>Report - Goods Issue History</h6></li> 
                <li>Period: {{ Request::segment(6) }} - {{ Request::segment(7) }}</li> 
              </ul>
            </div>

            <div class="col-md-4 col-lg-4 content-group"> 
            </div>
            <div class="col-md-4 col-lg-4 content-group">
            </div>
          </div>
        </div>

        <div class="table-responsive" >
            <table class="table table-lg">
                <thead>
                    <tr>
                        <th class="col-sm-1">No Transaction</th>
                        <th class="col-sm-1">Date Transaction</th>
                        <th class="col-sm-1">Warehouse</th>
                        <th class="col-sm-1">Doc No</th>
                        <th class="col-sm-1">Doc Year</th>
                        <th class="col-sm-1">PO No</th>
                        <th class="col-sm-1">Description</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($itemdata AS $itemdatas)
                    <tr>
                        <td>{{ $itemdatas->no_transaction }}</td>
                        <td>{{ $itemdatas->date_transaction }}</td>
                        <td>{{ $itemdatas->warehouse_name }}</td>
                        <td>{{ $itemdatas->nodoc }}</td>
                        <td>{{ $itemdatas->tahundoc }}</td>
                        <td>{{ $itemdatas->nopo }}</td>
                        <td><span class="text-semibold">{{ $itemdatas->description }}</span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="panel-body">
          <div class="row invoice-payment">
            <div class="col-sm-7">
              <div class="content-group">
                <div class="mb-15 mt-15">
                  <img src="assets/images/signature.png" class="display-block" style="width: 150px;" alt="">
                </div>
              </div>
            </div>
            <div class="col-sm-5">
            </div>
          </div>
        </div>
      </div>
      <!-- /invoice template -->  
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@include('main.warehouse.goods_receive.script')
@include('scripts.change_status')
@stop


