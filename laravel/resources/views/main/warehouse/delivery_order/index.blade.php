@extends('layouts.main.template')
@section('title', 'Goods Issue')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'delivery-order')
@section('val_variable', 'delivery_order')
@section('content')
<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>

<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
      @actionStart('goods_issue', ['delete'])
      <a href="#" onclick="bulk_change_status();" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-minus2 text-indigo-400"></i> <span>archive</span></a>
      @actionEnd
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>
      <a href="#" onClick="reload_table()" class="btn btn-link btn-float has-text text-size-small"><i class=" icon-database-refresh text-indigo-400"></i> <span>Refresh</span></a>
      @actionStart('goods_issue', ['create'])
      <a href="#" class="btn btn-link btn-float has-text text-size-small" onClick="add()"><i class="icon-file-plus text-indigo-400"></i> <span>Create</span></a>
      @actionEnd
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          <ul class="nav nav-tabs nav-tabs-highlight">
            <input type="hidden" name="filter_status" id="filter_status">
            <!-- <li><a href="{{ URL::route('main.delivery-order.open.edit', '1') }}"><i class="icon-folder position-left"></i> Open</a></li> -->
            <li class="active" onclick="active_status();"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-folder-check position-left"></i> Active</a></li>
            <li onclick="inactive_status();"><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-folder-minus2 position-left"></i> Inactive</a></li>
            </li>
            <li>
            <div class="input-group" style="margin-left:10px !important">
              {{ Form::text('date_from', date('d-m-Y',strtotime("-30 days")), ['class' => 'form-control datepicker', 'placeholder' => 'Date From', 'id' => 'date_from', 'onChange' => 'filter_data();']) }}
            </div>
          </li>
          <li>
            <div class="input-group" style="margin-left:10px !important">
              {{ Form::text('date_to', date('d-m-Y', strtotime(now())), ['class' => 'form-control datepicker', 'placeholder' => 'Date To', 'id' => 'date_to', 'onChange' => 'filter_data();']) }}
            </div>
          </li>
          <li>
          &nbsp;
          </li>
          <li>
            <button class="btn btn-success btn-xs pull-left" type="button" onclick="filter_data();" data-dismiss="modal"><i class="icon-filter3"></i> Filter</button>
          </li>
            <li class="pull-right">
              <a tabindex="0" href="#" target="_blank"><i class="icon-question4"></i> <span>Help</span></a>
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
            </li>
            <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a>
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print</span></a>
            </li>
          </ul>
          <table id="dtTable" class="table details-tabl datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th style="width:5%">
                </th>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th>
                <th>GI WMS</th>
                <th>Date Transaction</th>
               <!-- <th>Warehouse</th> -->
                <th>GI SAP</th>
                <th>Notes (Surat Jalan)</th>
                <th>Picking No</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <script id="details-template" type="text/x-handlebars-template">
            <div class="label label-info">Picking Number's Posts</div>
            <table class="table details-table" id="posts-1">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Picking Number</th>
                </tr>
                </thead>
            </table>
        </script>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:30% !important">
    <div class="modal-content">
      {!! Form::open(array('route' => 'main.delivery-order.store', 'files' => 'true', 'id' => 'form', 'class' => 'form-horizontal'))!!}
      {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Select Transaction</h5>
       </div>
       <div class="modal-body">
        <div>
          <select class="select" name="code_transaction" id="code_transaction">
             <option value="GDIS/">GOODS ISSUE</option>
             <option value="GDMN/">GOODS ISSUE MANUAL</option>
           </select>
        </div>
      </div>
      <div class="modal-footer">
        @include('layouts.main.transaction_form.modal_button')
      </div>
    {!! Form::close() !!}
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@include('main.warehouse.delivery_order.script')
@include('scripts.change_status')
@stop
