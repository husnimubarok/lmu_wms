@extends('layouts.main.template')
@section('title', 'Goods Issue History')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'delivery-order')   
@section('val_variable', 'delivery_order')   
@section('content') 
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button_report')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>   
                <th>SJ Number</th>
                <th>Material</th> 
                <th>Material Description</th> 
                <th>Plant</th>
                <th>Batch</th>
                <th>UoM</th>
                <th>Qty Delivered</th>
                <th>Date Delivered</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

@include('main.warehouse.delivery_order.script_report')
@include('scripts.change_status')
@stop


