@extends('layouts.main.template')
@section('title', 'Goods Issue')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'delivery_order')
@section('val_variable', 'delivery_order')
@section('content')

@include('layouts.main.transaction_form.form_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($delivery_order, array('route' => ['main.delivery-order.update', $delivery_order->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
                  <!-- <a href="#" onclick="postSAPNew();"> s </a> -->
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                      <input name="pick_counter" id="pick_counter" type="hidden" value="@if(isset($detail_max->picking_number)){{$detail_max->picking_number}}@endif">
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'GI WMS', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::hidden('code_transaction', old('code_transaction'), ['class' => 'form-control', 'id' => 'code_transaction']) }}
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'GI WMS', 'id' => 'no_transaction', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Actual GI Date', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($delivery_order))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($delivery_order->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Actual GI Date', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Actual GI Date', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                     {{ Form::label('seq_no', 'GI SAP', ['class' => 'col-lg-3 control-label']) }}
                     <div class="col-lg-9">
                       {{ Form::text('nodo', old('nodo'), ['class' => 'form-control', 'placeholder' => 'GI SAP', 'id' => 'nopo', 'readonly' => 'readonly']) }}
                     </div>
                   </div>

                   <div class="form-group">
                     {{ Form::label('seq_no', 'Picking No', ['class' => 'col-lg-3 control-label']) }}
                     <div class="col-lg-9">
                      @if(isset($picking_det))
                      <input class="form-control" placeholder="Picking No" readonly="readonly" type="text" value="{{ $picking_det->picking_number }}">
                      @else
                      <input class="form-control" placeholder="Picking No" readonly="readonly" type="text">
                      @endif
                     </div>
                   </div>
                   @if($delivery_order->code_transaction == 'GDIS/')
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Notes (Surat Jalan) *', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <textarea id="description" name="description" rows="3" cols="20">
                           @if(isset($delivery_order))
                             {{$delivery_order->description}}
                           @endif
                        </textarea>
                      </div>
                    </div>
                    @endif
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      ADDITIONAL INFORMATION
                  </legend>
                     <!-- <div class="form-group">
                      {{ Form::label('seq_no', 'Warehouse', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('warehouse_id', $warehouse_list, old('warehouse_id'), array('class' => 'select', 'placeholder' => 'Select Warehouse', 'id' => 'warehouse_id')) }}
                      </div>
                    </div> -->

                    @if($delivery_order->code_transaction == 'GDIS/')
                   <div class="form-group">
                      {{ Form::label('seq_no', 'Customer Name', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                      @if(isset($picking_det))
                      <input class="form-control" placeholder="Customer Name" readonly="readonly" type="text" value="{{ $picking_det->customer }}">
                      @else
                      <input class="form-control" placeholder="Customer Name" readonly="readonly" type="text">
                      @endif
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Marketing', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                      @if(isset($picking_det))
                        <input class="form-control" placeholder="Marketing" readonly="readonly" type="text" value="{{ $picking_det->marketing }}">
                      @else
                        <input class="form-control" placeholder="Marketing" readonly="readonly" type="text">
                      @endif
                      </div>
                    </div>
                    @endif

                    @if($delivery_order->code_transaction == 'GDMN/')

                    <div class="form-group">
                      {{ Form::label('seq_no', 'NO SJ', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_sj', old('no_sj'), ['class' => 'form-control', 'placeholder' => 'NO SJ', 'id' => 'no_sj', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Customer Name', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                       {{ Form::text('customer_name', old('customer_name'), ['class' => 'form-control', 'placeholder' => 'Customer Name', 'id' => 'customer_name']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Customer Address', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                       {{ Form::text('customer_address', old('customer_address'), ['class' => 'form-control', 'placeholder' => 'Customer Address', 'id' => 'customer_address']) }}
                        <!-- <textarea id="customer_address" name="customer_address" style="height: 20px !important">
                           @if(isset($delivery_order))
                             {{$delivery_order->customer_address}}
                           @endif
                        </textarea> -->
                      </div>
                    </div>


                    <div class="form-group">
                      {{ Form::label('seq_no', 'Send To', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('send_to', old('send_to'), ['class' => 'form-control', 'placeholder' => 'Send To', 'id' => 'send_to']) }}
                        <!-- <textarea id="send_to" row="4" name="send_to">
                           @if(isset($delivery_order))
                             {{$delivery_order->send_to}}
                           @endif
                        </textarea> -->
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Notes', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                       {{ Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Notes', 'id' => 'description']) }}
                        <!-- <textarea id="description" row="4" name="description">
                           @if(isset($delivery_order))
                             {{$delivery_order->notes}}
                           @endif
                        </textarea> -->
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Created At', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-3">
                        @if(isset($delivery_order))
                        {{ Form::text('created_at', date('d-m-Y', strtotime($delivery_order->created_at)), ['class' => 'form-control datepicker', 'placeholder' => 'Actual GI Date', 'id' => 'created_at']) }}
                        @else
                        {{ Form::text('created_at', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Actual GI Date', 'id' => 'created_at']) }}
                        @endif
                      </div>
                    </div>
                    @endif

                  </fieldset>
                </div>

                @if($delivery_order->code_transaction == 'GDMN/')
                <div class="col-md-12">
                  <fieldset>
                    <legend class="text-semibold">
                      SLIP CONTENT DETAIL
                    </legend>
                    <div class="form-group">
                      <div class="col-lg-12">
                        <div class="row">
                          <div class="col-md-1">
                            {{ Form::text('spp', old('spp'), ['class' => 'form-control', 'placeholder' => 'SPP', 'id' => 'spp']) }}
                            <span class="label label-block label-info text-left">SPP</span>
                          </div>

                          <div class="col-md-2">
                            {{ Form::text('sales_order', old('sales_order'), ['class' => 'form-control', 'placeholder' => 'SALES ORDER', 'id' => 'sales_order']) }}
                            <span class="label label-block label-info">SALES ORDER</span>
                          </div>

                          <div class="col-md-2">
                            {{ Form::text('delivery', old('delivery'), ['class' => 'form-control', 'placeholder' => 'DELIVERY', 'id' => 'delivery']) }}
                            <span class="label label-block label-info text-right">DELIVERY</span>
                          </div>
                          <div class="col-md-1">
                            {{ Form::text('jam_kirim', old('jam_kirim'), ['class' => 'form-control', 'placeholder' => 'JAM KIRIM', 'id' => 'jam_kirim']) }}
                            <span class="label label-block label-info text-right">JAM KIRIM</span>
                          </div>
                          <div class="col-md-1">
                            {{ Form::text('driver', old('driver'), ['class' => 'form-control', 'placeholder' => 'NAMA SUPIR', 'id' => 'driver']) }}
                            <span class="label label-block label-info text-right">NAMA SUPIR</span>
                          </div>
                          <div class="col-md-2">
                            {{ Form::text('tlp_driver', old('tlp_driver'), ['class' => 'form-control', 'placeholder' => 'TELP SUPIR', 'id' => 'tlp_driver']) }}
                            <span class="label label-block label-info text-right">TELP SUPIR</span>
                          </div>
                          <div class="col-md-1">
                            {{ Form::text('no_pol', old('no_pol'), ['class' => 'form-control', 'placeholder' => 'NO. POL', 'id' => 'no_pol']) }}
                            <span class="label label-block label-info text-right">NO. POL</span>
                          </div>
                          <div class="col-md-2">
                          {{ Form::text('bag_gudang', old('bag_gudang'), ['class' => 'form-control', 'placeholder' => 'BAG. GUDANG', 'id' => 'bag_gudang']) }}
                            <span class="label label-block label-info text-right">BAG. GUDANG</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
                @endif
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                       <tr>
                        <th>Action</th>
                        <th>Item</th>
                        <th>Material</th>
                        <th>Descrition</th>
                        <th>Pick Qty</th>
                        <th>Quantity</th>
                        <th>UOM</th>
                        <th>Warehouse Name &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Room Name &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Bay</th>
                        <th>Rack</th>
                        <th>Pallet Number</th>
                        <th>Picking Number</th>
                        <th>Batch</th>
                        <th>Notes Item</th>
                      </tr>
                    </thead>
                    @if(strlen($delivery_order->nodo)==0)
                    <thead>
                      <tr>
                      <td>
                        <a href="#" class="btn btn-primary btn-xs btn-float legitRipple" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                        <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a>
                        <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                      </td>
                      <td>
                        {{ Form::text('item_line', old('item_line'), ['class' => 'form-control', 'id' => 'item_line']) }}
                      </td>
                      <td>
                        {{ Form::text('inventory_code', old('inventory_code'), ['class' => 'form-control', 'id' => 'inventory_code']) }}
                      </td>
                      <td>
                          {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }}
                          {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }}
                          {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'id' => 'inventory_name', 'readonly']) }}
                         </div>
                      </td>
                      <td>
                        {{ Form::number('pick_qty', old('pick_qty'), ['class' => 'form-control', 'id' => 'pick_qty']) }}
                      </td>
                      <td>
                        {{ Form::number('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity']) }}
                      </td>
                      <td>
                        <!-- {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code', 'readonly']) }} -->
                        @if($delivery_order->code_transaction == 'GDMN/')
                          {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code', 'readonly' => 'readonly']) }}
                        @else
                          <select class="select" id="unit_of_measure_code" name="unit_of_measure_code">
                            @foreach($uom_list as $uom_lists)
                            <option value="{{$uom_lists->unit_of_measure_code}}">{{$uom_lists->unit_of_measure_code}}</option>
                            @endforeach
                          </select>
                        @endif
                      </td>
                      <td>
                        <div class="input-group">
                          <!-- <span class="input-group-btn"> -->
                            <!-- <a href="#" class="btn btn-primary btn-xs btn-float legitRipple" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a> -->
                          <!-- </span> -->
                          {{ Form::text('warehouse_name', old('warehouse_name'), ['class' => 'form-control', 'id' => 'warehouse_name', 'readonly']) }}
                          {{ Form::hidden('warehouse_id', old('warehouse_id'), ['class' => 'form-control', 'id' => 'warehouse_id']) }}
                          {{ Form::hidden('picking_detail_id', old('picking_detail_id'), ['class' => 'form-control', 'id' => 'picking_detail_id']) }}
                        </div>
                      </td>
                      <td>
                        <div class="input-group">
                          {{ Form::text('room_name', old('room_name'), ['class' => 'form-control', 'id' => 'room_name', 'readonly']) }}
                          {{ Form::hidden('room_id', old('room_id'), ['class' => 'form-control', 'id' => 'room_id']) }}
                        </div>
                      </td>
                      <td>
                        {{ Form::text('bay_name', old('bay_name'), ['class' => 'form-control', 'id' => 'bay_name', 'readonly']) }}
                        {{ Form::hidden('bay_id', old('bay_id'), ['class' => 'form-control', 'id' => 'bay_id']) }}
                      </td>
                      <td>
                        {{ Form::text('rack_name', old('rack_name'), ['class' => 'form-control', 'id' => 'rack_name', 'readonly']) }}
                        {{ Form::hidden('rack_id', old('rack_id'), ['class' => 'form-control', 'id' => 'rack_id']) }}
                      </td>
                      <td>
                        {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('picking_number', old('picking_number'), ['class' => 'form-control', 'id' => 'picking_number', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('batch', old('batch'), ['class' => 'form-control', 'id' => 'batch', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('notes_item', old('notes_item'), ['class' => 'form-control', 'id' => 'notes_item']) }}
                      </td>
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              @if($delivery_order->code_transaction =='GDIS/' )
              <a href="#"  class="btn btn-danger" onclick="cancelDO();"> Cancel GI <i class="icon-arrow-right14 position-right"></i></a>
              @endif
              @if(strlen($delivery_order->nodo)==0)
              @if($delivery_order->code_transaction =='GDIS/' )
                <a href="#"  class="btn btn-primary" onclick="postSAPNew();"> Save GI <i class="icon-arrow-right14 position-right"></i></a>
              @else
                @if($delivery_order->save_manual != 0 || $delivery_order->save_manual == null)
                <button type="submit" class="btn btn-primary">Save GI Manual <i class="icon-arrow-right14 position-right"></i></button>
                @endif
                <!-- <a href="{{url('main/warehouse/delivery-order/slip/1')}}" target="_blank" class="btn btn-warning"> Print <i class="icon-printer2 position-right"></i></a> -->
              @endif
              @endif

            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:70% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Inventory</h5>
       </div>
       <div class="modal-body">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Item</th>
                <th>Material</th>
                <th>Description</th>
                <th>Pick Qty</th>
                <th>Quantity</th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet Number</th>
                <th>Picking Number</th>
                <th>Picking Date</th>
                <th>Batch</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal"><i class="icon-close2"></i> Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
        <button onclick="pick();"  class="btn btn-primary legitRipple" type="button"><i class="icon-touch"></i> Pick</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
      ajax: {
            url: "{{ url('main/warehouse/delivery-order/get-detail') }}/{{$delivery_order->id}}",
            type : "POST",
            headers : {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
            data: function (d) {
                d.filter_status = $('input[name=filter_status]').val();
            }
      },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'item_line', name: 'item_line' },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'pick_qty', name: 'pick_qty' },
     { data: 'quantity', name: 'quantity' },
     { data: 'inventory_unit_name', name: 'inventory_unit_name' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'picking_number', name: 'picking_number' },
     { data: 'batch', name: 'batch' },
     { data: 'notes_item', name: 'notes_item' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });


  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     pageLength: 5,
     scrollX: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language: {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
      ajax: {
            url: "{{ url('main/warehouse/delivery-order/data-modal') }}",
            type : "POST",
            headers : {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
            data: function (d) {
                d.filter_status = $('input[name=filter_status]').val();
                d.code_transaction = $('input[name=code_transaction]').val();
                d.keyword = $('input[name=keyword]').val();
            }
      },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
    //  { data: 'action', name: 'action' },
    { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'item_line', name: 'item_line' },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'pick_qty', name: 'pick_qty' },
     { data: 'quantity', name: 'quantity' },
     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'picking_number', name: 'picking_number' },
     { data: 'doc_date', name: 'doc_date' },
     { data: 'batch', name: 'batch' },
    //  { data: 'check', name: 'check'},
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
    $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  function pick()
 {
  var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    // alert(list_id);

    if (list_id.length > 0) {
       var date_transaction = $('#date_transaction').val();
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to pick '+list_id.length+' data?',
            type: 'green',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'YES PICK',
                    btnClass: 'btn-green',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                                'date_transaction': date_transaction.replace("-","").replace("-",""),
                            },
                            url: "../../delivery-order/pick/{{$delivery_order->id}}",
                            type: "POST",
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = {
                                        "positionClass": "toast-bottom-right",
                                        "timeOut": 1000,
                                    };
                                    toastr.success('Success pick data!', 'Success Alert', options);
                                    reload_table();
                                    reload_table_inventory();
                                    $('#modal_form').modal('toggle');

                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'Pick data failed!',
                                    });
                                    reload_table();
                                    reload_table_inventory();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Pick data failed!',
                                });
                                reload_table();
                                reload_table_inventory();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
   reload_table();
   reload_table_inventory();

   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorGoods IssueName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
  }

  function addValue(str, id, inventory_id, warehouse_id, room_id, bay_id, rack_id, picking_detail_id){
    clearDetail();
    var inventory_id = inventory_id;
    var inventory_name = $(str).closest('tr').find('td:eq(9)').text();
    var picking_number = $(str).closest('tr').find('td:eq(1)').text();
    var note = $(str).closest('tr').find('td:eq(1)').text();
    var item_line = $(str).closest('tr').find('td:eq(10)').text();
    var pick_qty = $(str).closest('tr').find('td:eq(11)').text();
    var quantity = $(str).closest('tr').find('td:eq(12)').text();
    var batch = $(str).closest('tr').find('td:eq(13)').text();

    var warehouse_name = $(str).closest('tr').find('td:eq(3)').text();
    var room_name = $(str).closest('tr').find('td:eq(4)').text();
    var bay_name = $(str).closest('tr').find('td:eq(5)').text();
    var rack_name = $(str).closest('tr').find('td:eq(6)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(7)').text();


    $("#inventory_id").val(inventory_id);
    $("#inventory_name").val(inventory_name);
    $("#quantity").val(quantity);
    $("#item_line").val(item_line);
    $("#picking_number").val(picking_number);
    $("#pick_qty").val(pick_qty);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#warehouse_id").val(warehouse_id);
    $("#room_id").val(room_id);
    $("#bay_id").val(bay_id);
    $("#rack_id").val(rack_id);
    $("#picking_detail_id").val(picking_detail_id);
    $("#pallet_number").val(pallet_number);
    $("#batch").val(batch);

    setBtnAdd();
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var inventory_id = $('#inventory_id').val();
     var unit_of_measure_code =   $('#unit_of_measure_code').val();
     var quantity =   $('#quantity').val();

     if(quantity == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../delivery-order/post-detail/{{$delivery_order->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'item_line': $('#item_line').val(),
          'picking_number': $('#picking_number').val(),
          'quantity': $('#quantity').val(),
          'pick_qty': $('#pick_qty').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'batch': $('#batch').val(),
          'notes_item': $('#notes_item').val(),
          'picking_detail_id': $('#picking_detail_id').val(),
        },
        success: function(data) {
          reload_table();
          var picking = $('#picking_number').val();
          $('#pick_counter').val(picking);
          clearDetail();
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(inventory_id);

    var warehouse_name = $(str).closest('tr').find('td:eq(7)').text();
    var room_name = $(str).closest('tr').find('td:eq(8)').text();
    var bay_name = $(str).closest('tr').find('td:eq(9)').text();
    var rack_name = $(str).closest('tr').find('td:eq(10)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(11)').text();
    var inventory_code = $(str).closest('tr').find('td:eq(2)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(3)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(6)').text();
    var item_line = $(str).closest('tr').find('td:eq(1)').text();
    var picking_number = $(str).closest('tr').find('td:eq(12)').text();
    var pick_qty = $(str).closest('tr').find('td:eq(4)').text();
    var quantity = $(str).closest('tr').find('td:eq(5)').text();
    var notes_item = $(str).closest('tr').find('td:eq(14)').text();

    $("#id_detail").val(id_detail);
    $("#inventory_id").val(inventory_id);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#item_line").val(item_line);
    $("#picking_number").val(picking_number);
    $("#pick_qty").val(pick_qty);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    // $('#unit_of_measure_code').val(unit_of_measure_code).trigger('change.select2');
    $("#quantity").val(quantity);
    $("#notes_item").val(notes_item);

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =   $('#quantity').val();

    if(inventory_name == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../delivery-order/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'inventory_id': $('#inventory_id').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'item_line': $('#item_line').val(),
          'picking_number': $('#picking_number').val(),
          'pick_qty': $('#pick_qty').val(),
          'notes_item': $('#notes_item').val(),
          'save_detail': $('#save_detail').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#item_line').val(''),
    $('#unit_of_measure_code').val(''),
    $('#pallet_number').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#room_id').val(''),
    $('#picking_number').val(''),
    $('#pick_qty').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#picking_detail_id').val(''),
    $('#batch').val(''),
    $('#notes_item').val(''),
    $('#rack_id').val('')
  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../delivery-order/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function cancelDO(){
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to cancel?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: 'CANCEL',
          btnClass: 'btn-red',
          action: function () {
            waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
            axios({
              method: 'post',
                  url: '../sap-cancelgido',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgido',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
              data: {
                      '_token': $('input[name=_token]').val(),
                      'nodo': $('#pick_counter').val(),
                  },
              // crossdomain: true,
              })
              .then(function (response) {
                  axios({
                    method: 'post',
                    url: '../cancel/{{$delivery_order->id}}',
                    data: {
                            '_token': $('input[name=_token]').val(),
                          },
                  })
                  //handle success
                  console.log(response);
                  waitingDialog.hide();

                  $.alert({
                      type: 'green',
                      icon: 'fa fa-success', // glyphicon glyphicon-heart
                      title: 'Alert',
                      content: 'Post Data SAP Success!',
                  });
                  window.location.assign('../../delivery-order');
              })
              .catch(function (response) {
                  //handle error
                  console.log(response);

              });
            }
        },
      }
   });
  }


  function syncSAP(){
    setTimeout(function () {
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-exegido',
          url: '../sap-exegido',
          timeout: 60 * 10 * 1000, // Let's say you want to wait at least 10 mins
          headers: {
                      'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  },
      data: {
              '_token': $('input[name=_token]').val(),
              'giwms': "{{$delivery_order->no_transaction}}",
          },
      crossdomain: true,
      })
      .then(function (response) {
          axios({
            method: 'post',
            url: '../post-response/{{$delivery_order->id}}',
            timeout: 60 * 3 * 1000, // Let's say you want to wait at least 4 mins
            data: {
                    '_token': $('input[name=_token]').val(),
                    'nodo': response.data.T_GIDATA[0].PGINM,
                    'description': $('#description').val(),
                    'date_transaction': $('#date_transaction').val(),
                  },
          })
          waitingDialog.hide();

          if(response.data.T_GIDATA[0].STATUS == 'Success')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../delivery-order');

          };

          if(response.data.T_GIDATA[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.T_GIDATA[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          waitingDialog.hide();
          // window.location.assign('../../goods-receive');
      })
      .catch(function (response) {
          //handle error
          // alert('Sync data SAP Error!');
          $.alert({
          type: 'red',
          icon: 'fa fa-red', // glyphicon glyphicon-heart
          title: 'Error',
          // content: response.msg,
          content: 'Post data SAP Error, check your connection or call your IT administrator!',
          });
          waitingDialog.hide();

    }, 3000)
    });
  };

  function postSAP(){
    var desc = $('#description').val();

    // alert(desc);
    waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
    axios({
      method: 'post',
      url: '../post-response-header/{{$delivery_order->id}}',
      timeout: 60 * 3 * 1000, // Let's say you want to wait at least 4 mins
      data: {
              '_token': $('input[name=_token]').val(),
              'description': desc,
              'date_transaction': $('#date_transaction').val(),
            },
    });
    axios.get('../data-detail/{{$delivery_order->id}}')
      .then(response => {
        this.data = response.data;
        this.data.forEach((item) => {

            // start loop
              var i = 0;
              function myLoop() {
              setTimeout(function () {
            // ----

            axios({
            method: 'post',
            // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-gido',
            url: '../sap-gido',
            timeout: 60 * 4 * 1000, // Let's say you want to wait at least 4 mins
            headers: {
                          'Content-Type': 'application/json',
                          // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                      },
            data: {
                    'giwms': item.giwms,
                    'nodo': item.nodo,
                    'itemdo': item.itemdo,
                    'matrial':  item.matrial,
                    'qty':  item.qty,
                    'qtypicking': item.qtypicking,
                    'notes': desc,
                    'total_items': item.total_items.toString(),
                    'post_date': item.post_date,
                  },
            crossdomain: true,
            })
            .then(function (response) {
                console.log(item.giwms);
                console.log(item.nodo);
                console.log(item.itemdo);
                console.log(item.matrial);
                console.log(item.qty);
                console.log(item.qtypicking);
                console.log(item.bendera);

                // alert(item.itemdo);

                //handle success
                console.log(response);

                // if(item.bendera == 'X'){
                  // console.log('end');

                  console.log(response.data.T_GIDATA[0].PGINM);

                  // axios({
                  // method: 'post',
                  // url: '../post-response/{{$delivery_order->id}}',
                  // data: {
                  //         '_token': $('input[name=_token]').val(),
                  //         'nodo': response.data.T_GIDATA[0].PGINM,
                  //         'description': $('#description').val(),
                  //         'date_transaction': $('#date_transaction').val(),
                  //       },
                  // })

                  // waitingDialog.hide();
                  // $.alert({
                  //     type: 'green',
                  //     icon: 'fa fa-success', // glyphicon glyphicon-heart
                  //     title: 'Alert',
                  //     content: 'Post Data SAP Success!',
                  // });

                  if(response.data.T_GIDATA[0].STATUS == 'NOT SUCCESS')
                  {
                    $.alert({
                      type: 'red',
                      icon: 'fa fa-red', // glyphicon glyphicon-heart
                      title: 'Error',
                      content: response.data.T_GIDATA[0].MESSAGE,
                    });
                    waitingDialog.hide();
                  };

                  if(response.data.T_GIDATA[0].STATUS == 'Continue')
                  {
                    var options = {
                                      "positionClass": "toast-bottom-right",
                                      "timeOut": 5000,
                                  };
                    toastr.success('Post Line SAP Success', 'Success Alert', options);
                  };

                  if(response.data.T_GIDATA[0].STATUS == 'Success')
                  {
                      var options = {
                          "positionClass": "toast-bottom-right",
                          "timeOut": 5000,
                      };
                      toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                      console.log('end');
                      syncSAP();
                  };

                  // window.location.assign('../../delivery-order');
                // }
            })
            .catch(function (response) {
                //handle error
                console.log(response);
                $.alert({
                  type: 'red',
                  icon: 'fa fa-red', // glyphicon glyphicon-heart
                  title: 'Error',
                  // content: response.msg,
                  content: 'Post data SAP Error, check your connection or call your IT administrator!',
                });

                waitingDialog.hide();
            });
            // ----
                i++;
                // if (i < 4) {
                //     myLoop();
                // }
            }, 15000)
          }
               myLoop();
            // loop
          });
      })
    }


    function postSAPNew(){
        var desc = $('#description').val();

        waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
        axios({
          method: 'post',
          url: '../post-response-header/{{$delivery_order->id}}',
          data: {
                  '_token': $('input[name=_token]').val(),
                  'description': desc,
                  'date_transaction': $('#date_transaction').val(),
                },
        });
        
        axios({
          method: 'post',
          url: '../sap-gido-new/{{ $delivery_order->id }}',
          headers: {
                        'Content-Type': 'application/json',
                    },
        })
        .then(function (response) {

            if(response.data.T_GIDATA[0].STATUS == 'Success')
            {
                axios({
                  method: 'post',
                  url: '../post-response/{{$delivery_order->id}}',
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'nodo': response.data.T_GIDATA[0].PGINM,
                          'description': $('#description').val(),
                          'date_transaction': $('#date_transaction').val(),
                        },
                })
                
                $.alert({
                  type: 'green',
                  icon: 'fa fa-green', // glyphicon glyphicon-heart
                  title: 'Success',
                  content: 'Synchronize data SAP Success!',
                });

                waitingDialog.hide();

                window.location.assign('../../delivery-order');
            };



            if(response.data.T_GIDATA[0].STATUS == 'NOT SUCCESS')
            {
              console.log(response.data);
              $.alert({
                type: 'red',
                icon: 'fa fa-red', // glyphicon glyphicon-heart
                title: 'Error',
                content: response.data.T_GIDATA[0].MESSAGE,
              });
              waitingDialog.hide();
            };
        })
        .catch(error => {
            //handle error
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              // content: response.msg,
              content: 'Post data SAP Error, check your connection or call your IT administrator!',
            });

            waitingDialog.hide();
        });
    }



  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);
</script>

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/basic/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('plugins') }}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.


    CKEDITOR.editorConfig = function( config )
    {
        config.height = '10px';
    };

    @if($delivery_order->code_transaction == 'GDMN/')
    // CKEDITOR.replace('description', {height: 300}, {removePlugins: 'toolbar'});
    // @else
    // CKEDITOR.replace('description', {height: 80}, {removePlugins: 'toolbar'});
    @endif
    CKEDITOR.replace('description_sap', {height: 80}, {removePlugins: 'toolbar'});

    // CKEDITOR.replace('customer_address', {height: 80}, {removePlugins: 'toolbar'});
    CKEDITOR.replace('notes', {height: 80}, {removePlugins: 'toolbar'});
    CKEDITOR.replace('send_to', {height: 80}, {removePlugins: 'toolbar'});
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });

  $('#date_transaction').datepicker({ dateFormat: 'dd-mm-yy' }).val();

</script>
@stop
