@extends('layouts.main.template')
@section('title', 'Posting Transfer')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'posting-transfer')
@section('val_variable', 'posting_transfer')
@section('content')
@include('layouts.main.transaction_form.form_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($posting_transfer, array('route' => ['main.posting-transfer.update', $posting_transfer->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">

              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Transfer WMS', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'Posting Transfer WMS', 'id' => 'no_transaction', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Actual Transfer Date', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($delivery_order))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($delivery_order->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Actual Transfer Date', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Actual Transfer Date', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>


                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold" id="div_add">
                      ADDITIONAL INFORMATION
                    </legend>
                        <div class="form-group">
                          {{ Form::label('seq_no', 'No SAP', ['class' => 'col-lg-3 control-label']) }}
                          <div class="col-lg-9">
                            {{ Form::text('nodoc', old('nodoc'), ['class' => 'form-control', 'placeholder' => 'No SAP', 'id' => 'nodoc', 'disabled' => 'disabled']) }}
                          </div>
                        </div>

                        <div class="form-group">
                          {{ Form::label('seq_no', 'Year', ['class' => 'col-lg-3 control-label']) }}
                          <div class="col-lg-9">
                            {{ Form::text('year', old('year'), ['class' => 'form-control', 'placeholder' => 'Posting Year', 'id' => 'year', 'disabled' => 'disabled']) }}
                          </div>
                        </div>
                       <div class="form-group">
                         {{ Form::label('seq_no', 'Header Text *', ['class' => 'col-lg-3 control-label']) }}
                         <div class="col-lg-9">
                         {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Header Text *', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                         </div>
                       </div>
                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                  <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                    <thead>
                      <tr>
                       <th rowspan="2">Action</th>
                       <th colspan="5" style="background-color:#dc5d5d"><center>Item Stock</center></th>
                       <th colspan="5" style="background-color:#5ddc7f" width="20%"><center>Item Transfer</center></th>
                       <th rowspan="2">Warehouse Name</th>
                       <th rowspan="2">Room</th>
                       <th rowspan="2">Bay</th>
                       <th rowspan="2">Rack</th>
                       <th rowspan="2">Pallet Number</th>
                       <th rowspan="2">Batch</th>
                       <th rowspan="2">Unit</th>
                     </tr>
                       <tr>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Store Location</th>
                        <th>Plant Name</th>
                        <th>Quantity</th>

                        <th>Material to Transfer</th>
                        <th>Description to Transfer</th>
                        <th>Store Location &nbsp; &nbsp;</th>
                        <th>Plant Name &nbsp; &nbsp;</th>
                        <th>Quantity</th>
                      </tr>
                    </thead>
                    @if(strlen($posting_transfer->grnum)==0)
                    <thead>
                      <tr>
                      <td>
                        <a href="#" class="btn btn-primary btn-xs btn-float legitRipple" type="button" onClick="openModal();  return false;"><i class="icon-folder"></i> </a>
                        <a type="button" id="btn_add_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="saveDetail(); return false;" title="Edit"> <i class="icon-add"></i> </a>
                        <a type="button" id="btn_update_detail" class="btn btn-info btn-float btn-xs" href="#" onclick="updateDetail(); return false;" title="Edit"> <i class="icon-pencil"></i> </a>
                      </td>
                      <td>
                        {{ Form::text('inventory_code', old('inventory_code'), ['class' => 'form-control', 'id' => 'inventory_code', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'id' => 'inventory_name', 'readonly']) }}
                      </td>
                      <td>
                        <select class="select-search" id="store_loc" name="store_loc">
                          @foreach($store_loc_list as $store_loc_lists)
                          <option value="{{$store_loc_lists->store_location_name}}">{{$store_loc_lists->store_location_name}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select-search" id="plant" name="plant">
                          @foreach($plant_list as $plant_lists)
                          <option value="{{$plant_lists->store_location_name}}">{{$plant_lists->store_location_name}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        {{ Form::number('quantity', old('quantity'), ['class' => 'form-control', 'id' => 'quantity', 'readonly']) }}
                      </td>
                      <td>
                        <input type="text" class="form-control" name="inventory_code_transfer" id="inventory_code_transfer" style="background-color:#b1e7bf" placeholder="Tap ..."  onClick="openModalMaster();  return false;" readonly>
                      </td>
                      <td>
                        <input type="text" class="form-control" name="inventory_name_transfer" id="inventory_name_transfer" style="background-color:#b1e7bf" placeholder="Tap ..."  onClick="openModalMaster();  return false;" readonly>
                        <input type="hidden" class="form-control" name="inventory_id_transfer" id="inventory_id_transfer" readonly>
                      </td>
                      <td>
                         <select class="select-search" id="store_loc_transfer" name="store_loc_transfer">
                           @foreach($store_loc_list as $store_loc_lists)
                           <option value="{{$store_loc_lists->store_location_code}}">{{$store_loc_lists->store_location_name}}</option>
                           @endforeach
                         </select>
                      </td>
                      <td>
                         <select class="select-search" id="plant_transfer" name="plant_transfer">
                           @foreach($plant_list as $plant_lists)
                           <option value="{{$plant_lists->store_location_code}}">{{$plant_lists->store_location_name}}</option>
                           @endforeach
                         </select>
                      </td>
                      <td>
                        <input type="number" class="form-control" name="quantity_transfer" id="quantity_transfer" placeholder="0">
                      </td>
                      <td>
                        {{ Form::text('warehouse_name', old('warehouse_name'), ['class' => 'form-control', 'id' => 'warehouse_name', 'readonly']) }}
                        {{ Form::hidden('warehouse_id', old('warehouse_id'), ['class' => 'form-control', 'id' => 'warehouse_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('room_name', old('room_name'), ['class' => 'form-control', 'id' => 'room_name', 'readonly']) }}
                        {{ Form::hidden('room_id', old('room_id'), ['class' => 'form-control', 'id' => 'room_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('bay_name', old('bay_name'), ['class' => 'form-control', 'id' => 'bay_name', 'readonly']) }}
                        {{ Form::hidden('bay_id', old('bay_id'), ['class' => 'form-control', 'id' => 'bay_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('rack_name', old('rack_name'), ['class' => 'form-control', 'id' => 'rack_name', 'readonly']) }}
                        {{ Form::hidden('rack_id', old('rack_id'), ['class' => 'form-control', 'id' => 'rack_id', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('pallet_number', old('pallet_number'), ['class' => 'form-control', 'id' => 'pallet_number', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('batch', old('batch'), ['class' => 'form-control', 'id' => 'batch', 'readonly']) }}
                      </td>
                      <td>
                        {{ Form::text('unit_of_measure_code', old('unit_of_measure_code'), ['class' => 'form-control', 'id' => 'unit_of_measure_code', 'readonly']) }}
                      </td>
                      {{ Form::hidden('inventory_id', old('inventory_id'), ['class' => 'form-control', 'id' => 'inventory_id']) }}
                      {{ Form::hidden('id_detail', old('id_detail'), ['class' => 'form-control', 'id' => 'id_detail']) }}
                      {{ Form::hidden('sales_return_detail_id', old('sales_return_detail_id'), ['class' => 'form-control', 'id' => 'sales_return_detail_id']) }}
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            <div class="text-right">
              @if(!isset($posting_transfer->nodoc))
              <a href="#"  class="btn btn-primary" onclick="postSAP();"> SAVE <i class="icon-arrow-right14 position-right"></i></a>
              @else
              <a href="#"  class="btn btn-danger" id="cancel_gr" onclick="cancelPosting();"> Cancel Posting <i class="icon-cancel-circle2 position-right"></i></a>
              @endif
            </div>
          </div>
          <!-- <a href="#"  class="btn btn-primary" onclick="syncSAP();"> SYNC <i class="icon-arrow-right14 position-right"></i></a> -->

        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:80% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Stock</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$posting_transfer->code_transaction}}">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Batch</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <th>Unit</th>
                <th>Stock</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_item" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:38% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Master Inventory</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$posting_transfer->code_transaction}}">
          <table id="dtTableInventoryMaster" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>Code</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_inventory_master();"  class="btn btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    var code_trans = '{{$posting_transfer->code_transaction}}';

    if(code_trans=='GPOD')
    {
      $('#div_return_reason_code').hide();
      $('#div_trdat').hide();
      $('#div_sonum').hide();
      $('#div_donum').hide();
      $('#div_grnum').hide();
      $('#div_mjahr').hide();
      $('#div_add').hide();
    }else{
      $('#div_description').hide();
    };

    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
    ajax: {
      url: "{{ url('main/warehouse/posting-transfer/get-detail') }}/{{$posting_transfer->id}}",
      type : "POST",
      headers : {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
     },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     sScrollX: "130%",
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' },
     { data: 'store_loc', name: 'store_loc' },
     { data: 'plant', name: 'plant' },
     { data: 'quantity', name: 'quantity' },

     { data: 'inventory_code_tf', name: 'inventory_code_tf' },
     { data: 'inventory_name_tf', name: 'inventory_name_tf' },
     { data: 'store_loc_transfer', name: 'store_loc_transfer' },
     { data: 'plant_transfer', name: 'plant_transfer' },
     { data: 'quantity_transfer', name: 'quantity_transfer' },

     { data: 'warehouse_name', name: 'warehouse_name' },
     { data: 'room_name', name: 'room_name' },
     { data: 'bay_name', name: 'bay_name' },
     { data: 'rack_name', name: 'rack_name' },
     { data: 'pallet_number', name: 'pallet_number' },
     { data: 'batch', name: 'batch' },
     { data: 'inventory_unit_name', name: 'inventory_unit_name' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });

  var table_inventory;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ url('main/warehouse/posting-transfer/data-modal') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.code_transaction = $('input[name=code_transaction]').val();
              d.keyword = $('input[name=keyword]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
      { data: 'warehouse_name', name: 'warehouse_name' },
      { data: 'room_name', name: 'room_name' },
      { data: 'bay_name', name: 'bay_name' },
      { data: 'rack_name', name: 'rack_name' },
      { data: 'pallet_number', name: 'pallet_number' },
      { data: 'batch', name: 'batch' },
      { data: 'inventory_code', name: 'inventory_code' },
      { data: 'inventory_name', name: 'inventory_name' },
      { data: 'unit', name: 'unit' },
      { data: 'quantity', name: 'quantity' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });


  var table_inventory_master;
  var status = 0;
  $(document).ready(function() {
    //datatable_inventorys
    table_inventory_master = $('#dtTableInventoryMaster').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ URL::route('main.inventory.data-modal-transfer') }}",
          type : "GET",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
     { data: 'inventory_code', name: 'inventory_code' },
     { data: 'inventory_name', name: 'inventory_name' }
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });


  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory()
  {
    table_inventory.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_inventory_master()
  {
    table_inventory_master.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorPosting TransferName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal

    reload_table_inventory();
    reload_table();
  }

  function openModalMaster()
  {
    // console.log('tes');
    $('#modal_form_item').modal('show'); // show bootstrap modal

    reload_table_inventory_master();
    reload_table_inventory();
    reload_table();
  }

  function addValue(str, warehouse_id, bay_id, room_id, rack_id, inventory_id, id_detail){
    clearDetail();
    var inventory_id = inventory_id;
    var inventory_code = $(str).closest('tr').find('td:eq(7)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(8)').text();
    var quantity = $(str).closest('tr').find('td:eq(10)').text();
    var quantity_transfer = $(str).closest('tr').find('td:eq(10)').text();

    var warehouse_name = $(str).closest('tr').find('td:eq(1)').text();
    var room_name = $(str).closest('tr').find('td:eq(2)').text();
    var bay_name = $(str).closest('tr').find('td:eq(3)').text();
    var rack_name = $(str).closest('tr').find('td:eq(4)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(5)').text();
    var batch = $(str).closest('tr').find('td:eq(6)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(9)').text();


    console.log(unit_of_measure_code);


    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#quantity_transfer").val(quantity_transfer);

    $("#warehouse_id").val(warehouse_id);
    $("#room_id").val(room_id);
    $("#bay_id").val(bay_id);
    $("#rack_id").val(rack_id);

    $("#inventory_id").val(inventory_id);
    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#quantity").val(quantity);
    $("#batch").val(batch);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    setBtnAdd();
    $('#modal_form').modal('toggle');

  }


  function addItem(str, id, inventory_name){
    var inventory_id_transfer = id;
    var inventory_code = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(2)').text();

    $("#inventory_id_transfer").val(inventory_id_transfer);
    $("#inventory_code_transfer").val(inventory_code);
    $("#inventory_name_transfer").val(inventory_name);

    setBtnAdd();
    $('#modal_form_item').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
     var inventory_id_transfer = $('#inventory_id_transfer').val();
     var inventory_id = $('#inventory_id').val();
     var unit_of_measure_code =   $('#unit_of_measure_code').val();
     var quantity =   $('#quantity').val();
     var quantity_transfer =   $('#quantity_transfer').val();

     if(inventory_id == '' || inventory_id_transfer == '' || inventory_id_transfer == ''){
      // alert("Error validarion!");
      $.alert({
          title: 'Error!',
          type: 'red',
          content: 'All data is required!',
      });
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../posting-transfer/post-detail/{{$posting_transfer->id}}' ,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'inventory_id': $('#inventory_id').val(),
          'inventory_code': $('#inventory_code').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'batch': $('#batch').val(),
          'inventory_id_transfer': $('#inventory_id_transfer').val(),
          'quantity_transfer': $('#quantity_transfer').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'plant_transfer': $('#plant_transfer').val(),
          'store_loc_transfer': $('#store_loc_transfer').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
        },
      })
    }
    return false;
  };

  function editDetail(str, id_detail, inventory_id, inventory_id_transfer)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(inventory_id);

    var inventory_code = $(str).closest('tr').find('td:eq(1)').text();
    var inventory_name = $(str).closest('tr').find('td:eq(2)').text();
    var store_loc = $(str).closest('tr').find('td:eq(3)').text();
    var plant = $(str).closest('tr').find('td:eq(4)').text();
    var quantity = $(str).closest('tr').find('td:eq(5)').text();
    var inventory_code_transfer = $(str).closest('tr').find('td:eq(6)').text();
    var inventory_name_transfer = $(str).closest('tr').find('td:eq(7)').text();
    var store_loc_transfer = $(str).closest('tr').find('td:eq(8)').text();
    var plant_transfer = $(str).closest('tr').find('td:eq(9)').text();
    var quantity_transfer = $(str).closest('tr').find('td:eq(10)').text();
    var warehouse_name = $(str).closest('tr').find('td:eq(11)').text();
    var room_name = $(str).closest('tr').find('td:eq(12)').text();
    var bay_name = $(str).closest('tr').find('td:eq(13)').text();
    var rack_name = $(str).closest('tr').find('td:eq(14)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(15)').text();
    var batch = $(str).closest('tr').find('td:eq(16)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(17)').text();


    $("#inventory_id").val(inventory_id);
    $("#inventory_id_transfer").val(inventory_id_transfer);
    $("#id_detail").val(id_detail);

    $("#inventory_code").val(inventory_code);
    $("#inventory_name").val(inventory_name);
    $("#store_loc").val(store_loc);
    $("#plant").val(plant);
    $("#quantity").val(quantity);
    $("#inventory_code_transfer").val(inventory_code_transfer);
    $("#inventory_name_transfer").val(inventory_name_transfer);
    $("#store_loc_transfer").val(store_loc_transfer);
    $("#plant_transfer").val(plant_transfer);
    $("#quantity_transfer").val(quantity_transfer);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#batch").val(batch);
    $("#unit_of_measure_code").val(unit_of_measure_code);

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var inventory_id = $('#inventory_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =  $('#quantity').val();

    if(inventory_id == '' || unit_of_measure_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../posting-transfer/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'inventory_id': $('#inventory_id').val(),
          'inventory_code': $('#inventory_code').val(),
          'unit_of_measure_code': $('#unit_of_measure_code').val(),
          'quantity': $('#quantity').val(),
          'warehouse_id': $('#warehouse_id').val(),
          'room_id': $('#room_id').val(),
          'bay_id': $('#bay_id').val(),
          'rack_id': $('#rack_id').val(),
          'pallet_number': $('#pallet_number').val(),
          'batch': $('#batch').val(),
          'inventory_id_transfer': $('#inventory_id_transfer').val(),
          'quantity_transfer': $('#quantity_transfer').val(),
          'plant': $('#plant').val(),
          'store_loc': $('#store_loc').val(),
          'plant_transfer': $('#plant_transfer').val(),
          'store_loc_transfer': $('#store_loc_transfer').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };
  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#inventory_code').val(''),
    $('#inventory_name').val(''),
    $('#inventory_id').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#pallet_number').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#batch').val(''),
    $('#inventory_id_transfer').val(''),
    $('#quantity_transfer').val(''),
    $('#inventory_id_transfer').val(''),
    $('#unit_of_measure_code').val(''),
    $('#inventory_code_transfer').val(''),
    $('#inventory_name_transfer').val('')
  }

  function delete_detail(id_detail, inventory_name)
  {

    var inventory_name = inventory_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'DELETE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + inventory_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../posting-transfer/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
          reload_table();
         }
       },
     }
   });
  };

  function cancelPosting(){
   $.confirm({
       title: 'Confirm!',
       content: 'Are you sure to cancel {{$posting_transfer->no_transaction}} number?',
       type: 'red',
       typeAnimated: true,
       buttons: {
         cancel: {
          action: function () {
          }
        },
        confirm: {
         text: 'YES',
         btnClass: 'btn-red',
         action: function () {
           waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
           axios({
             method: 'post',
                 url: 'https://wms.lmu.co.id/main/warehouse/goods-receive/sap-cancelgr',
                 // headers: {
                               // 'Content-Type': 'application/json',
                               // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                           // },
             data: {
                     '_token': $('input[name=_token]').val(),
                     'notransaction': "{{$posting_transfer->no_transaction}}",
                     'nodoc': "{{$posting_transfer->nodoc}}",
                     'tahundoc': "{{$posting_transfer->year}}",
                     'nopo': "111111",
                 },
              crossdomain: true,
             })
             .then(function (response) {
                 axios({
                   method: 'post',
                   url: '../cancel/{{$posting_transfer->id}}',
                   data: {
                           '_token': $('input[name=_token]').val(),
                         },
                 })

                 //handle success
                 console.log(response);
                 if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                 {
                   $.alert({
                     type: 'green',
                     icon: 'fa fa-green', // glyphicon glyphicon-heart
                     title: 'Success',
                     content: 'Cancel data SAP Success!',
                   });
                 };

                 if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                 {
                   $.alert({
                     type: 'red',
                     icon: 'fa fa-red', // glyphicon glyphicon-heart
                     title: 'Cancel Error!',
                     content: 'Message SAP : ' + response.data.T_DATA[0].MESSAGE,
                   });
                 };
                 waitingDialog.hide();
                 // window.location.assign('../../goods-receive');
             })
             .catch(function (response) {
                 //handle error
                 console.log(response);
                 $.alert({
                  type: 'red',
                  icon: 'fa fa-red', // glyphicon glyphicon-heart
                  title: 'Error',
                  // content: response.msg,
                  content: 'Post data SAP Error, check your connection or call your IT administrator!',
                });

                waitingDialog.hide();
                //  alert('Post data SAP Error!');

             });
           }
       },
     }
  });
 }



  function syncSAP(){
    setTimeout(function () {
    axios({
      method: 'post',
          // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-exetrfposting',
          url: '../sap-exetrfposting',
          // headers: {
                      // 'Content-Type': 'application/json',
                      // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                  // },
      data: {
              '_token': $('input[name=_token]').val(),
              'notrx': $('#no_transaction').val(),
          },
      crossdomain: true,
      })
      .then(function (response) {
            axios({
              method: 'post',
              url: '../post-response/{{$posting_transfer->id}}',
              data: {
                      '_token': $('input[name=_token]').val(),
                      'nodoc': response.data.T_TFDATA[0].MBLNR,
                      'year': response.data.T_TFDATA[0].YEAR,
                    },
          })
          waitingDialog.hide();

          if(response.data.T_TFDATA[0].STATUS == 'SUCCESS')
          {
            $.alert({
              type: 'green',
              icon: 'fa fa-green', // glyphicon glyphicon-heart
              title: 'Success',
              content: 'Synchronize data SAP Success!',
            });

            window.location.assign('../../goods-receive');

          };

          if(response.data.T_TFDATA[0].STATUS == 'NOT SUCCESS')
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              content: response.data.T_TFDATA[0].MESSAGE,
            });
          };
          //handle success
          console.log(response);
          waitingDialog.hide();
          // window.location.assign('../../goods-receive');
      })
      .catch(function (response) {
          //handle error
          // alert('Sync data SAP Error!');
          $.alert({
            type: 'red',
            icon: 'fa fa-red', // glyphicon glyphicon-heart
            title: 'Error',
            // content: response.msg,
            content: 'Post data SAP Error, check your connection or call your IT administrator!',
          });

          waitingDialog.hide();

    }, 3000)
    });
  };

  function postSAP(){
    // alert('test');
    var desc_req =  $('#description').val();
    if(desc_req == '')
    {
      $.alert({
          title: 'Warning!',
          type: 'red',
          content: 'Header Text field is required!',
      });
    }else{
        waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});

        axios({
          method: 'post',
          url: '../save-header/{{$posting_transfer->id}}',
          data: {
                  '_token': $('input[name=_token]').val(),
                  'description': $('#description').val(),
                  'date_transactin': $('#date_transactin').val(),
                },
        })

        axios.get('../data-detail/{{$posting_transfer->id}}')
          .then(response => {
            this.data = response.data;

            this.data.forEach((item, i) => {

                // alert('test');

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                var options = {
                    "positionClass": "toast-bottom-right",
                    "timeOut": 3000,
                };
                toastr.success('Get data: ' + item.material_from + ' at: ' + time, 'Success Alert', options);

                console.log(response);
                console.log($('#description').val());
                axios({
                  method: 'post',
                  // url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/sap-trfposting',
                  url: '../sap-trfposting',
                  // headers: {
                                // 'Content-Type': 'application/json',
                                // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                            // },
                  data: {
                          '_token': $('input[name=_token]').val(),
                          'notrx': item.no_transaction,
                          'item': item.item_line.toString(),
                          'material_from': item.material_from.toString(),
                          'matrial_to':  item.matrial_to.toString(),
                          'plant_from':  item.plant_from.toString(),
                          'plant_to': item.plant_to,
                          'storage_from': item.storage_from,
                          'storage_to': item.storage_to,
                          'quantity': item.quantity,
                          'unit': item.unit_of_measure_code,
                          'user_name': item.user_name,
                          'text_header': $('#description').val(),
                          'text_detail': $('#description').val(),
                          'doc_date': item.doc_date,
                          'post_date': item.post_date,
                          'total_items': item.total_items.toString(),
                        },
                    crossdomain: true,
                  })
                  .then(function (response) {
                      //handle success
                      // console.log("no" + item.no_transaction.toString());
                      // console.log("material_from" + item.material_from.toString());
                      // console.log("plant_from" + item.plant_from).toString();
                      // console.log("plant_to" + item.plant_to.toString());
                      // console.log("storage_from" + item.storage_from.toString());
                      // console.log("storage_to" + item.storage_to.toString());
                      // console.log("quantity" + item.quantity.toString());
                      // console.log("unit" + item.unit.toString());
                      // console.log("user_name" + item.user_name).toString();
                      // console.log("text_header" + item.text_header);
                      // console.log("doc_date" + item.doc_date.toString());
                      // console.log("description" + $('#description').val());
                      // console.log("total item" + item.total_items.toString());

                      console.log(data);
                      var dtq = new Date();
                      var timeq = dtq.getHours() + ":" + dtq.getMinutes() + ":" + dtq.getSeconds();

                        // console.log('no trans: ' + response.data.T_TFDATA[0].NO_TRANSACTION);
                        // console.log('status: ' + response.data.T_TFDATA[0].STATUS);

                        // console.log('test' + response.data.code);

                        // if(response.data.code == '0')
                        // {
                        //   // alert(messages);
                        //   $.alert({
                        //       title: 'Error!',
                        //       content: response.data.msg,
                        //   });
                        // }

                        // if(response.data.T_TFDATA[0].STATUS == 'NOT OK')
                        //   {
                        //     // alert(messages);
                        //     $.alert({
                        //         title: 'Error!',
                        //         content: response.data.T_TFDATA[0].MESSAGES,
                        //     });
                        //     axios({
                        //     method: 'post',
                        //     url: '../post-response-detail/{{$posting_transfer->id}}',
                        //     data: {
                        //             '_token': $('input[name=_token]').val(),
                        //             'nopo': response.data.T_TFDATA[0].EBELN,
                        //           },
                        //     })
                        //   };
                        //
                        //   if(response.data.T_TFDATA[0].STATUS == 'NOT SUCCESS')
                        //   {
                        //     $.alert({
                        //       type: 'red',
                        //       icon: 'fa fa-red', // glyphicon glyphicon-heart
                        //       title: 'Error',
                        //       content: response.data.T_TFDATA[0].MESSAGE,
                        //     });
                        //   };

                        // syncSAP();

                        // alert('test');


                          if(response.data.T_TFDATA[0].STATUS == 'Success')
                          {
                            // alert('success');
                            // axios({
                            //   method: 'post',
                            //   url: 'http://webapp.lmu.co.id:3000/m/v1/wms/exetrfposting',
                            //   data: {
                            //           '_token': $('input[name=_token]').val(),
                            //           'notrx': $('#no_transaction').val(),
                            //         },
                            // })

                            syncSAP();


                            var options = {
                                "positionClass": "toast-bottom-right",
                                "timeOut": 5000,
                            };
                            toastr.success('Please wait, synchronize data from WMS to SAP', 'Success Alert', options);
                            console.log('end');
                        }
                        waitingDialog.hide();
                  })
                  .catch(function (res) {
                      //handle error
                      waitingDialog.hide();
                      console.log('Test ' + res.msg);
                      // alert(response.msg);
                      // alert('Post data SAP Error!');
                      $.alert({
                        type: 'red',
                        icon: 'fa fa-red', // glyphicon glyphicon-heart
                        title: 'Error',
                        content: response.MESSAGE,
                        // content: 'Post SAP Error!',
                      });
                  });
              })
          })
      };
    }

  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  /**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);
</script>
@stop
