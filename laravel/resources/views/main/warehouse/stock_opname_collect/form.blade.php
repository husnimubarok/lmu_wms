@extends('layouts.main.template')
@section('title', 'Stock Opname Collect')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'stock_opname_collect')
@section('val_variable', 'stock_opname_collect')
@section('content')
@include('layouts.main.transaction_form.form_button')




<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          {!! Form::model($stock_opname_collect, array('route' => ['main.stock-decreasing.update', $stock_opname_collect->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          {{ csrf_field() }}
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">

              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-4">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'No Transaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::hidden('stock_opname_id', old('stock_opname_id'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'stock_opname_id', 'readonly' => 'readonly']) }}
                        {{ Form::text('no_transaction', old('no_transaction'), ['class' => 'form-control', 'placeholder' => 'No Transaction', 'id' => 'no_transaction', 'readonly' => 'readonly']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Date Trancaction', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        <div class="input-group">
                          @if(isset($stock_opname_collect))
                          {{ Form::text('date_transaction', date('d-m-Y', strtotime($stock_opname_collect->date_transaction)), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @else
                          {{ Form::text('date_transaction', old('date_transactin'), ['class' => 'form-control datepicker', 'placeholder' => 'Date Trancaction', 'id' => 'date_transaction']) }}
                          @endif
                        </div>
                      </div>
                    </div>

                  </fieldset>
                </div>

                <div class="col-md-4">
                  <fieldset>
                    <legend class="text-semibold">
                      PRIMARY INFORMATION
                    </legend>


                    <div class="form-group">
                      {{ Form::label('seq_no', 'No SAP', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('nodoc', old('nodoc'), ['class' => 'form-control', 'placeholder' => 'No SAP', 'id' => 'nodoc', 'disabled' => 'disabled']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Year', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('year', old('year'), ['class' => 'form-control', 'placeholder' => 'Posting Year', 'id' => 'year', 'disabled' => 'disabled']) }}
                      </div>
                    </div>
                   <div class="form-group">
                     {{ Form::label('seq_no', 'Header Text *', ['class' => 'col-lg-3 control-label']) }}
                     <div class="col-lg-9">
                     {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Header Text *', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                     </div>
                   </div>
                  </fieldset>
                </div>


                <div class="col-md-4">
                  <fieldset>
                    <legend class="text-semibold" id="div_add">
                      STOCK OPNAME NUMBER
                    </legend>
                    <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state">
                      <thead>
                         <tr>
                           <th>Action</th>
                           <th>SO Number</th>
                           <th>SO Date</th>
                           <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    <a href="#"  class="btn btn-sm btn-primary" onclick="openModal();"> <i class="icon-folder position-right"></i> Open SO</a>
                  </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
              </div>
            <div class="text-right">
              @if(!isset($stock_opname_collect->nodoc))
              <a href="#"  class="btn btn-primary" onclick="postSAPNew();"> SAVE <i class="icon-arrow-right14 position-right"></i></a>
              @else
              <a href="#"  class="btn btn-danger" id="cancel_gr" onclick="cancelPosting();"> CANCEL <i class="icon-cancel-circle2 position-right"></i></a>
              @endif
            </div>
          </div>
        {!! Form::close() !!}
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Stock Opname</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$stock_opname_collect->code_transaction}}">
          <table id="dtTableInventory" class="table datatable-scroller-buttons datatable-colvis-state">
            <thead>
               <tr>
                <!-- <th>Action</th>  -->
                <tr>
                <th style="width:5%">
                  #
                </th>
                <th>SO No</th>
                <th>DO Date</th>
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_stock_opname_id();"  class="btn btn-sm btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->




<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_item" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:60% !important">
    <div class="modal-content">
       <div class="modal-header bg-orange">
         <h5 class="modal-title">Data Stock Opname</h5>
       </div>
       <div class="modal-body">
        <input type="hidden" name="code_transaction" id="code_transaction" value="{{$stock_opname_collect->code_transaction}}">
          <table id="dtTableStockOpnameDetail" class="table datatable-scroller-buttons datatable-colvis-state">
                <thead>
                  <tr>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">Material</th>
                    <th rowspan="2">Description</th>
                    <th colspan="3" style="background-color:#dc5d5d"><center>Qty Opname</center></th>
                    <th rowspan="2">UOM</th>
                    <th rowspan="2">Batch</th>
                    <th rowspan="2">Warehouse Name</th>
                    <th rowspan="2">Room</th>
                    <th rowspan="2">Bay</th>
                    <th rowspan="2">Rack</th>
                    <th rowspan="2">Pallet Number</th>
                    <th rowspan="2">Plant</th>
                    <th rowspan="2">Store Loc</th>
                </tr>
                <tr>
                  <th>Quantity Stock</th>
                  <th>Quantity Actual</th>
                  <th>Quantity Adjustment</th>

                </tr>
              </thead>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
        <button onclick="reload_table_stock_opname_id();"  class="btn btn-sm btn-success legitRipple" type="button"><i class="icon-database-refresh"></i> Refresh</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
  var table;
  var status = 0;
  $(document).ready(function() {
    setBtnAdd();
    //datatables
    table = $('#dtTable').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
          sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
          sSearch: "Search: &nbsp;&nbsp;",
          processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
      },
      ajax: {
        url: "{{ url('main/warehouse/stock-opname-collect/get-detail') }}/{{$stock_opname_collect->id}}",
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
       },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     sScrollX: "130%",
     scrollX: true,
     dom: 'lBfrtip',
     columnDefs: [
        { targets: 'action', orderable: false }
      ],
     colVis: {
          "buttonText": "Change columns"
      },
      buttons: [
          'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
      ],
      initComplete: function() {
         var $buttons = $('.dt-buttons').hide();
         $('#btnCopy').on('click', function() {
            var btnClass = 'copy'
               ? '.buttons-copy'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnPrint').on('click', function() {
            var btnClass = 'print'
               ? '.buttons-print'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnCsv').on('click', function() {
            var btnClass = 'csv'
               ? '.buttons-csv'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
         $('#btnColvis').on('click', function() {
            var btnClass = 'colvis'
               ? '.buttons-colvis'
               : null;
            if (btnClass) $buttons.find(btnClass).click();
         })
       },
     columns: [
     { data: 'action', name: 'action'},
     { data: 'no_transaction', name: 'no_transaction' },
     { data: 'date_transaction', name: 'date_transaction' },
     { data: 'description', name: 'description' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
  });


  var table_stock_opname_id;
  var status = 0;
  $(document).ready(function() {
    //datatable_stock_opname_ids
    table_stock_opname_id = $('#dtTableInventory').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ url('main/warehouse/stock-opname-collect/data-modal') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.code_transaction = $('input[name=code_transaction]').val();
              d.keyword = $('input[name=keyword]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: false,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
     { data: 'action', name: 'action'},
     // { data: 'check', name: 'check', orderable: false, searchable: false },
      { data: 'no_transaction', name: 'no_transaction' },
      { data: 'date_transaction', name: 'date_transaction' },
      { data: 'description', name: 'description' },
      { data: 'mstatus', name: 'mstatus' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });


  var table_stock_opname_item;
  var status = 0;
  $(document).ready(function() {
    //datatable_stock_opname_items
    table_stock_opname_item = $('#dtTableStockOpnameDetail').DataTable({
    dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
    "<'row'<'col-xs-12't>>"+
    "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
     processing: true,
     serverSide: true,
     lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
     language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
      ajax: {
          url: "{{ url('main/warehouse/stock-opname-collect/data-modal-item') }}",
          type : "POST",
          headers : {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
          data: function (d) {
              d.keyword = $('input[name=keyword]').val();
              d.stock_opname_id = $('input[name=stock_opname_id]').val();
          }
    },
     processing: true,
     deferRender: true,
     colReorder: true,
     ordering: true,
     autowidth: true,
     scrollX: true,
     dom: 'lfrtip',
     columns: [
      { data: 'mstatus', name: 'mstatus'},
      { data: 'material_number', name: 'material_number' },
      { data: 'inventory_name', name: 'inventory_name' },
      { data: 'quantity', name: 'quantity' },
      { data: 'quantity_actual', name: 'quantity_actual' },
      { data: 'quantity_adjustment', name: 'quantity_adjustment' },
      { data: 'po_unit', name: 'po_unit' },
      { data: 'batch', name: 'batch' },
      { data: 'warehouse_name', name: 'warehouse_name' },
      { data: 'room_name', name: 'room_name' },
      { data: 'bay_name', name: 'bay_name' },
      { data: 'rack_name', name: 'rack_name' },
      { data: 'pallet_number', name: 'pallet_number' },
      { data: 'plant', name: 'plant' },
      { data: 'store_loc', name: 'store_loc' },
     ],
     rowReorder: {
        dataSrc: 'action'
     }
    });
      $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_stock_opname_id()
  {
    table_stock_opname_id.ajax.reload(null,false); //reload datatable ajax
  }

  function reload_table_stock_opname_item()
  {
    table_stock_opname_item.ajax.reload(null,false); //reload datatable ajax
  }

  function openModal()
  {
    reload_table_stock_opname_id();
    reload_table();

   $("#btnSave").attr("onclick","save()");
   $("#btnSaveAdd").attr("onclick","saveadd()");

   $('.errorStock DecreasingName').addClass('hidden');

   save_method = 'add';
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
  }

  function openModalItem(id)
  {
    $("#stock_opname_id").val(id);

    // reload_table();
    reload_table_stock_opname_item();

    $('#modal_form_item').modal('show'); // show bootstrap modal
  }

  function addValue(str, warehouse_id, bay_id, room_id, rack_id, stock_opname_id_id, id_detail){
     
    $('#modal_form').modal('toggle');

  }

  function setBtnAdd() {
    $('#btn_add_detail').show(100);
    $('#btn_update_detail').hide(100);
  }

  function setBtnUpdate() {
    $('#btn_add_detail').hide(100);
    $('#btn_update_detail').show(100);
  }


  function saveDetail(id)
  {
    //Ajax Load data from ajax
    $.ajax({
      url: '../../stock-opname-collect/post-detail/{{$stock_opname_collect->id}}' ,
      type: "POST",
      data: {
        'stock_opname_id': id,
        '_token': $('input[name=_token]').val(),
      },
      success: function(data) {
        reload_table_stock_opname_id();
        reload_table();
      },
    })
    return false;
  };

  function editDetail(str, id_detail, stock_opname_id_id, stock_opname_id_id_transfer)
   {
    clearDetail();
    console.log(str);
    console.log(id_detail);
    console.log(stock_opname_id_id);

    var stock_opname_id_code = $(str).closest('tr').find('td:eq(1)').text();
    var stock_opname_id_name = $(str).closest('tr').find('td:eq(2)').text();
    var quantity = $(str).closest('tr').find('td:eq(3)').text();
    var quantity_decreasing = $(str).closest('tr').find('td:eq(4)').text();
    var unit_of_measure_code = $(str).closest('tr').find('td:eq(5)').text();
    var batch = $(str).closest('tr').find('td:eq(6)').text();
    var warehouse_name = $(str).closest('tr').find('td:eq(7)').text();
    var room_name = $(str).closest('tr').find('td:eq(8)').text();
    var bay_name = $(str).closest('tr').find('td:eq(9)').text();
    var rack_name = $(str).closest('tr').find('td:eq(10)').text();
    var pallet_number = $(str).closest('tr').find('td:eq(11)').text();
    var plant = $(str).closest('tr').find('td:eq(12)').text();
    var store_loc = $(str).closest('tr').find('td:eq(13)').text();
    var account_code = $(str).closest('tr').find('td:eq(14)').text();
    var cost_center_code = $(str).closest('tr').find('td:eq(15)').text();


    $("#stock_opname_id_id").val(stock_opname_id_id);
    $("#id_detail").val(id_detail);

    $("#stock_opname_id_code").val(stock_opname_id_code);
    $("#stock_opname_id_name").val(stock_opname_id_name);
    $("#store_loc").val(store_loc);
    $("#plant").val(plant);
    $("#quantity").val(quantity);
    $("#quantity_decreasing").val(quantity_decreasing);
    $("#warehouse_name").val(warehouse_name);
    $("#room_name").val(room_name);
    $("#bay_name").val(bay_name);
    $("#rack_name").val(rack_name);
    $("#pallet_number").val(pallet_number);
    $("#batch").val(batch);
    $("#unit_of_measure_code").val(unit_of_measure_code);
    $("#account_code").val(account_code);
    $("#cost_center_code").val(cost_center_code);

    setBtnUpdate();
  };


  function updateDetail(id)
  {
    var id_detail = $('#id_detail').val();
    var stock_opname_id_id = $('#stock_opname_id_id').val();
    var unit_of_measure_code =   $('#unit_of_measure_code').val();
    var quantity =  $('#quantity').val();

    if(stock_opname_id_id == '' || account_code == ''){
      alert("Error validarion!");
    }else{
      save_method = 'update';

      //Ajax Load data from ajax
      $.ajax({
        url: '../../stock-decreasing/put-detail/' + id_detail ,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(),
          'id_detail': $('#id_detail').val(),
          'quantity_decreasing': $('#quantity_decreasing').val(),
          'account_code': $('#account_code').val(),
          'cost_center_code': $('#cost_center_code').val(),
        },
        success: function(data) {
          reload_table();
          clearDetail();
          setBtnAdd();
        },
      })
    }
  };

  function reload_table_detail()
  {
      table_detail.ajax.reload(null, false); //reload datatable ajax
  }

  function clearDetail()
  {
    $('#stock_opname_id_code').val(''),
    $('#stock_opname_id_name').val(''),
    $('#stock_opname_id_id').val(''),
    $('#quantity').val(''),
    $('#warehouse_name').val(''),
    $('#room_name').val(''),
    $('#bay_name').val(''),
    $('#rack_name').val(''),
    $('#pallet_number').val(''),
    $('#warehouse_id').val(''),
    $('#room_id').val(''),
    $('#bay_id').val(''),
    $('#rack_id').val(''),
    $('#batch').val(''),
    $('#quantity_decreasing').val(''),
    // $('#account_code').val(''),
    // $('#cost_center_code').val(''),
    $('#unit_of_measure_code').val('')
  };

  function delete_detail(id_detail, stock_opname_id_name)
  {

    var stock_opname_id_name = stock_opname_id_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete ' + stock_opname_id_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '../../stock-opname-collect/delete-detail/' + id_detail,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully delete data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  };

  function cancelPosting(){
     $.confirm({
         title: 'Confirm!',
         content: 'Are you sure to cancel {{$stock_opname_collect->no_transaction}} number?',
         type: 'red',
         typeAnimated: true,
         buttons: {
           cancel: {
            action: function () {
            }
          },
          confirm: {
           text: 'YES',
           btnClass: 'btn-red',
           action: function () {
             waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
             axios({
               method: 'post',
                   url: 'https://o530ivwnpi.execute-api.ap-southeast-1.amazonaws.com/wms/sap-cancelgr',
                   // headers: {
                                 // 'Content-Type': 'application/json',
                                 // 'key': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZWdfYXBpX2tleSI6ImxhcmlzX21hbmlzX3dtc19hdXRoX2tleSIsImlhdCI6MTU1MzIyMDQ3NH0.6rSJSJR_XwVrt6LpLjHOZsw-VTCKq23t009pcE3hLwk',
                             // },
               data: {
                       '_token': $('input[name=_token]').val(),
                       'notransaction': "{{$stock_opname_collect->no_transaction}}",
                       'nodoc': "{{$stock_opname_collect->nodoc}}",
                       'tahundoc': "{{$stock_opname_collect->year}}",
                       'nopo': "111111",
                   },
                // crossdomain: true,
               })
               .then(function (response) {
                   axios({
                     method: 'post',
                     url: '../cancel/{{$stock_opname_collect->id}}',
                     data: {
                             '_token': $('input[name=_token]').val(),
                           },
                   })

                   //handle success
                   console.log(response);
                   if(response.data.T_DATA[0].STATUS == 'SUCCESS')
                   {
                     $.alert({
                       type: 'green',
                       icon: 'fa fa-green', // glyphicon glyphicon-heart
                       title: 'Success',
                       content: 'Cancel data SAP Success!',
                     });
                   };

                   if(response.data.T_DATA[0].STATUS == 'NOT SUCCESS')
                   {
                     $.alert({
                       type: 'red',
                       icon: 'fa fa-red', // glyphicon glyphicon-heart
                       title: 'Cancel Error!',
                       content: 'Message SAP : ' + response.data.T_DATA[0].MESSAGE,
                     });
                   };
                   waitingDialog.hide();
                   // window.location.assign('../../goods-receive');
               })
               .catch(function (response) {
                   //handle error
                   console.log(response);
                   alert('Post data SAP Error!');

               });
             }
         },
       }
    });
   }
 
    function postSAPNew(){
        var desc = $('#description').val();

        waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
        
        axios({
          method: 'post',
          url: '../sap-so-new/{{ $stock_opname_collect->id }}',
          headers: {
                        'Content-Type': 'application/json',
                    },
        })
        .then(function (response) {

            if(response.data.T_GIDATA[0].STATUS == 'Success')
            {
                $.alert({
                  type: 'green',
                  icon: 'fa fa-green', // glyphicon glyphicon-heart
                  title: 'Success',
                  content: 'Synchronize data SAP Success!',
                });

                waitingDialog.hide();

                window.location.assign('../../stock-opname-collect');
            };

            if(response.data.T_GIDATA[0].STATUS == 'NOT SUCCESS')
            {
              console.log(response.data);
              $.alert({
                type: 'red',
                icon: 'fa fa-red', // glyphicon glyphicon-heart
                title: 'Error',
                content: response.data.T_GIDATA[0].MESSAGE,
              });
              waitingDialog.hide();
            };
        })
        .catch(error => {
            //handle error
            $.alert({
              type: 'red',
              icon: 'fa fa-red', // glyphicon glyphicon-heart
              title: 'Error',
              // content: response.msg,
              content: 'Post data SAP Error, check your connection or call your IT administrator!',
            });

            waitingDialog.hide();
        });
    }



      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });

      /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);

</script>

<style>
.dataTables_filter input {
    outline: 0;
    width: 100px !important;
    height: 38px;
    padding: 8px 0;
    padding-right: 24px;
    font-size: 13px;
    line-height: 1.5384616;
    color: #333333;
    background-color: transparent;
    border: 1px solid transparent;
    border-width: 1px 0;
    border-bottom-color: #ddd;
}
</style>
@stop
