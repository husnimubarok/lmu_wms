@extends('layouts.main.template')
@section('title', 'Stock Opname Collect')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'stock-opname-collect')
@section('val_variable', 'stock_opname-collect')
@section('content')
<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group">
      @actionStart('stock_opname', ['update'])
      <a href="#" onclick="bulk_change_status();" class="btn btn-link btn-float has-text text-size-small"><i class="icon-file-minus2 text-indigo-400"></i> <span>archive</span></a>
      @actionEnd
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a>
      <a href="#" onClick="reload_table()" class="btn btn-link btn-float has-text text-size-small"><i class=" icon-database-refresh text-indigo-400"></i> <span>Refresh</span></a>
      @actionStart('stock_opname', ['update'])
      <a href="#" class="btn btn-link btn-float has-text text-size-small" onClick="add()"><i class="icon-file-plus text-indigo-400"></i> <span>Create</span></a>
      @actionEnd
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.transaction_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th>
                <th>No Transaction</th>
                <th>Date Transaction</th>
                <th>No SAP</th>
                <th>Posting Year</th>
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:30% !important">
    <div class="modal-content">
      {!! Form::open(array('route' => 'main.stock-opname-collect.store', 'files' => 'true', 'id' => 'form', 'class' => 'form-horizontal'))!!}
      {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Select Transaction</h5>
       </div>
       <div class="modal-body">
         <div style="display:none">
          <select class="select" name="code_transaction" id="code_transaction">
             <option value="SOCL">STOCK OPNAME COLLECT</option>
           </select>
          </div>
      </div>
      <div class="modal-footer">
        @include('layouts.main.transaction_form.modal_button')
      </div>
    {!! Form::close() !!}
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@include('main.warehouse.stock_opname_collect.script')
@include('scripts.change_status')
@stop
