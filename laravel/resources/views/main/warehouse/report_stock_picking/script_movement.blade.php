<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
    ajax: {
         url: "{{ URL::route('main.report-stock-picking-movement.data') }}", 
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.date_from = $('input[name=date_from]').val();
            d.date_to = $('input[name=date_to]').val();
        }
   },
   processing: true,
   deferRender: true,
   colReorder: true,
   scrollX: 150,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
             ? '.buttons-excel'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
     },
   columns: [
   { data: 'warehouse_name', name: 'warehouse_name' },
   { data: 'room_name', name: 'room_name' },
   { data: 'bay_name', name: 'bay_name' },
   { data: 'rack_name', name: 'rack_name' },
   { data: 'pallet_number', name: 'pallet_number' },
   { data: 'batch', name: 'batch' },
   { data: 'inventory_code', name: 'inventory_code' },
   { data: 'inventory_name', name: 'inventory_name' },
   { data: 'unit', name: 'unit' },
   { data: 'goods_receive', name: 'goods_receive' },
   { data: 'gr_return', name: 'gr_return' },
   { data: 'picking', name: 'picking' },
   { data: 'stock_movement', name: 'stock_movement' },
   { data: 'nol', name: 'nol' },
   { data: 'stock', name: 'stock' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });


  function add()
  {

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

   function active_status()
  {
    $("#filter_status").val('0');
    reload_table();
  }

  function inactive_status()
  {
    $("#filter_status").val('1');
    reload_table();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function add()
  {
    $("#btnSave").attr("onclick","save()");
    $("#btnSaveAdd").attr("onclick","saveadd()");

    $('.errorCustomerTypeName').addClass('hidden');

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

  function change_status(id, no_transaction, status)
  {
      var no_transaction = no_transaction.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + no_transaction + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '@yield("url_variable")/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }

  $('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>
