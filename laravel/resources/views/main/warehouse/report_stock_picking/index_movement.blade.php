@extends('layouts.report.template')
@section('title', 'Report Stock Movement Booking')
@section('caption', 'Report for stock movement booking!')
@section('url_variable', 'stock-transfer')
@section('val_variable', 'stock_transfer')
@section('content')
@include('layouts.report.report_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.report_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th>Warehouse</th>
                <th>Room</th>
                <th>Bay</th>
                <th>Rack</th>
                <th>Pallet</th>
                <th>Batch</th>
                <th>Inventory Code</th>
                <th>Inventory Name</th>
                <th>Unit</th>
                <th>Goods Geceive</th>
                <th>GR Return</th>
                <th>Goods Issue</th>
                <th>Movement</th>
                <th>Adjustment</th>
                <th>Ending</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- End Bootstrap modal -->
@include('main.warehouse.report_stock_picking.script_movement')
@include('scripts.change_status')
@stop
