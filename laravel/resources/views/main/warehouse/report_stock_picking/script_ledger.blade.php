<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
  //  ajax: {
  //       url: "{{ URL::route('main.report-stock-ledger.data') }}",
  //       data: function (d) {
  //           d.filter_status = $('input[name=filter_status]').val();
  //           d.keyword = $('input[name=keyword]').val();
  //       }
  //   },
  ajax: {
         url: "{{ URL::route('main.report-stock-picking-ledger.data') }}", 
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.date_from = $('input[name=date_from]').val();
            d.date_to = $('input[name=date_to]').val();
        }
   },
   processing: true,
   deferRender: true,
   scrollX: 150,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
             ? '.buttons-excel'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
     },
   columns: [
   { data: 'warehouse_name', name: 'warehouse_name' },
   { data: 'room_name', name: 'room_name' },
   { data: 'bay_name', name: 'bay_name' },
   { data: 'rack_name', name: 'rack_name' },
   { data: 'pallet_number', name: 'pallet_number' },
   { data: 'batch', name: 'batch' },
   { data: 'inventory_code', name: 'inventory_code' },
   { data: 'inventory_name', name: 'inventory_name' },
   { data: 'unit', name: 'unit' },
   { data: 'no_transaction', name: 'no_transaction' },
   { data: 'date_transaction', name: 'date_transaction' },
   { data: 'type_transaction', name: 'type_transaction' },
   { data: 'quantity', name: 'quantity' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

function filter_data()
{
   var date_from = $('input[name=date_from]').val();
   var date_to = $('input[name=date_to]').val();
   if(date_from == '' || date_to == '')
   {
   $.alert({
      title: 'Warning!',
      type_field: 'red',
      content: 'Filter from is required!',
   });
   }else{
   reload_table();

   };
}

function reload_table()
{
   table.ajax.reload(null,false); //reload datatable ajax
}

$('#search-form').on('submit', function(e) {
   table.draw();
   e.preventDefault();
});

$('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
$('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>
