<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        // url: "{{ URL::route('main.bay.data') }}", 
        url: "@yield("url_variable")/data", 
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val(); 
            d.keyword = $('input[name=keyword]').val(); 
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       }) 
        $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
              ? '.buttons-excel'
              : null;
          if (btnClass) $buttons.find(btnClass).click(); 
        })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [  
   { data: 'check', name: 'check', orderable: false, searchable: false },
   { data: 'action', name: 'action'}, 
   { data: '@yield("val_variable")_code', name: '@yield("val_variable")_code' },
   { data: '@yield("val_variable")_name', name: '@yield("val_variable")_name' },
   { data: 'room_name', name: 'room_name' },
   { data: 'capacity', name: 'capacity' },
   { data: 'pallet_capacity', name: 'pallet_capacity' },
   { data: 'description', name: 'description' },
   { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
});

 function active_status()
{
  $("#filter_status").val('0');
  reload_table();
}

function inactive_status()
{
  $("#filter_status").val('1');
  reload_table();
}

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

function add()
{
  $("#btnSave").attr("onclick","save()");
  $("#btnSaveAdd").attr("onclick","saveadd()");

  $('.errorCustomerTypeName').addClass('hidden');

  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
}


function save()
{   
  var url;
  url = "{{ URL::route('main.bay.store') }}";
  // url: "@yield("url_variable")" + "/create", 
  
  $.ajax({
    type: 'POST',
    url: url,
    data: {
      '_token': $('input[name=_token]').val(), 
      '@yield("url_variable")_code': $('#@yield("url_variable")_code').val(), 
      '@yield("url_variable")_name': $('#@yield("url_variable")_name').val(), 
      'room_id': $('#room_id').val(), 
      'capacity': $('#capacity').val(), 
      'pallet_capacity': $('#pallet_capacity').val(), 
      'description': $('#description').val(), 
      'status': $('#status').val()
    },
    success: function(data) { 

      $('.errorCustomerTypeName').addClass('hidden');

      if ((data.errors)) {
        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.error('Data is required!', 'Error Validation', options);
        
        if (data.errors.bay_name) {
          $('.errorCustomerTypeName').removeClass('hidden');
          $('.errorCustomerTypeName').text(data.errors.bay_name);
        }
      } else {

        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.success('Successfully added data!', 'Success Alert', options);
        $('#modal_form').modal('hide');
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function saveadd()
{   
 $.ajax({
    type: 'POST',
    // url: "{{ URL::route('main.bay.store') }}",
    url: "@yield("url_variable")/create", 
    data: {
      '_token': $('input[name=_token]').val(), 
      '@yield("val_variable")_code': $('#@yield("val_variable")_code').val(), 
      '@yield("val_variable")_name': $('#@yield("val_variable")_name').val(),
      'room_id': $('#room_id').val(), 
      'capacity': $('#capacity').val(), 
      'pallet_capacity': $('#pallet_capacity').val(), 
      'description': $('#description').val(),  
      'status': $('#status').val()
    },
    success: function(data) {  
        $('.errorCustomerTypeName').addClass('hidden');

        if ((data.errors)) {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Data is required!', 'Error Validation', options);
        
          if (data.errors.bay_name) {
            $('.errorCustomerTypeName').removeClass('hidden');
            $('.errorCustomerTypeName').text(data.errors.bay_name);
          }
        } else {
      var options = { 
        "positionClass": "toast-bottom-right", 
        "timeOut": 1000, 
      };
      toastr.success('Successfully added data!', 'Success Alert', options);
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function edit(id)
 { 
  $('.errorCustomerTypeName').addClass('hidden');
  $("#btnSave").attr("onclick","update("+id+")");
  $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

  save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
      url : '@yield("url_variable")/edit/' + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="id_key"]').val(data.id); 
        $('[name="@yield("val_variable")_code"]').val(data.@yield("val_variable")_code);
        $('[name="@yield("val_variable")_name"]').val(data.@yield("val_variable")_name);
        $('[name="room_id"]').val(data.room_id);
        $('[name="capacity"]').val(data.capacity);
        $('[name="pallet_capacity"]').val(data.pallet_capacity);
        $('[name="description"]').val(data.description);
        $('[name="status"]').val(data.status);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit @yield("title")'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function update(id)
  {
    save_method = 'update'; 
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: '@yield("url_variable")/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          '@yield("val_variable")_code': $('#@yield("val_variable")_code').val(), 
          '@yield("val_variable")_name': $('#@yield("val_variable")_name').val(),
          'room_id': $('#room_id').val(), 
          'capacity': $('#capacity').val(), 
          'pallet_capacity': $('#pallet_capacity').val(), 
          'description': $('#description').val(),  
          'status': $('#status').val()
        },
        success: function(data) {  
          $('.errorCustomerTypeName').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Data is required!', 'Error Validation', options);
           
            if (data.errors.bayname) {
              $('.errorCustomerTypeName').removeClass('hidden');
              $('.errorCustomerTypeName').text(data.errors.bayname);
            }
          } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#modal_form').modal('hide');
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
        } 
      },
    })
  };

  function updateadd(id)
  {
      save_method = 'update'; 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url: '@yield("url_variable")/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          '@yield("val_variable")_code': $('#@yield("val_variable")_code').val(), 
          '@yield("val_variable")_name': $('#@yield("val_variable")_name').val(), 
          'status': $('#status').val()
        },
        success: function(data) { 
          if ((data.errors)) {
           swal("Error!", "Gat data failed!", "error")
         } else { 
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
          $("#btnSave").attr("onclick","save()");
          $("#btnSaveAdd").attr("onclick","saveadd()");
        } 
      },
    })
  };

  function change_status(id, bay_name, status)
  {
      var bay_name = bay_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + bay_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () { 
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '@yield("url_variable")/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val() 
            },
            success: function(data)
            { 
              reload_table();
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }
</script> 



