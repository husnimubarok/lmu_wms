@extends('layouts.main.template')
@section('title', 'Preference')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'preference')
@section('val_variable', 'preference')
@section('content')
<style>
.nav-tabs > li > a {
    margin-right: 2px;
    line-height: 1.384616;
    border-radius: 3px 3px 0 0;
}
.nav-tabs > li > a.legitRipple {
    margin-bottom: 0px;
}
</style>
<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
</div>
</div>
<!-- /page header

 Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          <div class="col-md-12">
						<div class="panel panel-flat">
              {!! Form::model($preference, array('route' => ['main.preference.update', $preference->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
							<div class="panel-body">
								<div class="tabbable">
									<ul class="nav nav-tabs">
                    <li class="active"><a href="#basic-general" data-toggle="tab">General</a></li>
                    <li><a href="#basic-information" data-toggle="tab">Information</a></li>
                    <!-- <li><a href="#basic-flag" data-toggle="tab">Maintenance Flag</a></li> -->
                    <li><a href="#basic-systeminfo" data-toggle="tab">System Info</a></li>
										<li class="pull-right">
                      <button type="submit" class="btn btn-primary" data-toggle=""><i class="icon-file-check2 position-left text-white-400"></i> Save </button>
                    </li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="basic-general">
                      <div class="panel panel-flat">
                        <div class="panel-heading">
                          <h5 class="panel-title"></h5>
                          <div class="heading-elements">
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('seq_no', 'Company Name', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('company_name', old('company_name'), ['class' => 'form-control', 'placeholder' => 'Company Name', 'id' => 'company_name']) }}
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Logo *', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::file('logo', old('logo'), ['class' => 'form-control', 'placeholder' => 'Logo', 'id' => 'logo']) }}
                                    <br>
                                    <div class="col-md-6">
                                      <img src="{{$preference->file_path}}/preference/{{$preference->logo}}" alt="Logo" height="40" width="25">
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Address', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address', 'id' => 'address']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Web', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('web', old('web'), ['class' => 'form-control', 'placeholder' => 'Web', 'id' => 'web']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Email', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'id' => 'email']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Telp', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('telp', old('telp'), ['class' => 'form-control', 'placeholder' => 'Telp', 'id' => 'telp']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Fax', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('fax', old('fax'), ['class' => 'form-control', 'placeholder' => 'fax', 'id' => 'fax']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'PKP NO', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('pkp_no', old('pkp_no'), ['class' => 'form-control', 'placeholder' => 'PKP NO', 'id' => 'pkp_no']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                {{ Form::label('seq_no', 'PKP Date', ['class' => 'col-lg-3 control-label']) }}
                                <div class="col-lg-9">
                                  <div class="input-group">
                                    {{ Form::text('pkp_date', old('pkp_date'), ['class' => 'form-control', 'placeholder' => 'PKP Date', 'id' => 'pkp_date']) }}
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Branch Code', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => 'Branch Code', 'id' => 'branch_code']) }}
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'File Path', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    {{ Form::text('file_path', old('file_path'), ['class' => 'form-control', 'placeholder' => 'File Path', 'id' => 'file_path']) }}
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
    								</div>

										<div class="tab-pane" id="basic-information">

                        <div class="panel panel-flat">
                          <div class="panel-heading">
                            <h5 class="panel-title"></h5>
                            <div class="heading-elements">
                            </div>
                          </div>
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                    {{ Form::label('role_name', 'Backup Database', ['class' => 'col-lg-3 control-label']) }}
                                    <div class="col-lg-9">
                                      <a href="{{ URL::route('main.preference.backup_database') }}"> Backup Database </a>
                                    </div>
                                  </div>
                              </div>

                            </div>
                          </div>
                        </div>
										</div>

										<div class="tab-pane" id="basic-flag">
                      <div class="panel panel-flat">
                        <div class="panel-heading">
                          <h5 class="panel-title"></h5>
                          <div class="heading-elements">
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('seq_no', 'Branch Item', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                  										<input type="checkbox" name="branch_item" checked>
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
										</div>

										<div class="tab-pane" id="basic-systeminfo">
                      <div class="panel panel-flat">
                        <div class="panel-heading">
                          <h5 class="panel-title"></h5>
                          <div class="heading-elements">
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('seq_no', 'Laravel Version', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                   <p> {{ App::VERSION() }} </p>
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Language Code', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    <p> id </p>

                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'Extensions & Modules', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                      <!-- <p> » GD: Enabled ✓ </p>
                                      <p> » BC Math: Enabled ✓ </p>
                                      <p> » INTL: Enabled ✓ </p>
                                      <p> » OpenSSL: Enabled ✓ </p>
                                      <p> » MBString: Enabled ✓ </p>
                                      <p> » Curl: Enabled ✓ </p> -->
                                      @php
                                         $ext = get_loaded_extensions();
                                         foreach ($ext as $key => $val) {
                                            echo "» "; echo $val; echo " : Enabled ✓";
                                            echo "<br/>";
                                          }
                                       @endphp
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'User Settings', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                    <p> .Server Software: LiteSpeed </p>
                                    <p>
                                      <?php
                                      // prints e.g. 'Current PHP version: 4.1.1'
                                      echo '.Current PHP version: ' . phpversion();
                                      // prints e.g. '2.0' or nothing if the extension isn't enabled
                                      echo phpversion('tidy');
                                      ?>
                                    </p>
                                    <p> .DB Version: 5.5.5-10.2.25-MariaDB </p>
                                    <p> .Server Port: 8001 </p>
                                    <p> .OS: Linux 3.10.0-962.3.2.lve1.5.25.6.el7.x86_64 #1 SMP Thu Apr 18 06:40:26 EDT 2019 x86_64 </p>
                                  </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  {{ Form::label('role_name', 'File Permissions', ['class' => 'col-lg-3 control-label']) }}
                                  <div class="col-lg-9">
                                  <p>  » [laravel/storage/logs:] - 0755 | Writable ✓ </p>
                                  <p>  » [public/uploads:] - 0755 | Writable ✓ </p>
                                  <p>  » [public/uploads/item_pics:] - 0755 | Writable ✓ </p>
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->

<!-- <script>
$('#pkp_date').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script> -->
@include('main.preference.preference.script')
@stop
