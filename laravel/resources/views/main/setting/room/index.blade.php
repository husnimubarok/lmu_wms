@extends('layouts.main.template')
@section('title', 'Room')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'room')
@section('val_variable', 'room')
@section('content')
@include('layouts.main.master_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.master_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th>
                <th>Code</th>
                <th>Name</th>
                <!-- <th>Warehouse</th> -->
                <!-- <th>Temprature</th> -->
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         <h5 class="modal-title">@yield('title') Form</h5>
       </div>
       <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-2">Code</label>
          <div class="col-md-9">
            <input name="room_id_filter" id="room_id_filter" class="form-control" type="hidden">
            <input name="room_code" id="room_code" class="form-control text-uppercase" type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">Name</label>
          <div class="col-md-9">
            <input name="room_name" id="room_name" class="form-control" type="text">
            <small class="errorCustomerTypeName hidden alert-danger"></small>
          </div>
        </div>
        <!-- <div class="form-group">
          <label class="control-label col-md-2">Warehouse</label>
          <div class="col-md-9">
            {{ Form::select('warehouse_id', $warehouse_list, old('warehouse_id'), array('class' => 'select-search', 'placeholder' => 'Select Warehouse', 'id' => 'warehouse_id')) }}
          </div>
        </div> -->
        <!-- <div class="form-group">
          <label class="control-label col-md-2">Temprature</label>
          <div class="col-md-9">
            {{ Form::select('temprature_id', $temprature_list, old('temprature_id'), array('class' => 'select-search', 'placeholder' => 'Select Temprature', 'id' => 'temprature_id')) }}
          </div>
        </div> -->
        <div class="form-group">
          <label class="control-label col-md-2">Description</label>
          <div class="col-md-9">
            <textarea rows="5" cols="5" name="description" id="description" class="form-control" placeholder="Description"></textarea>
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      @include('layouts.main.master_form.modal_button')
    </div>
  </div><!-- /.modal-content -->
</div>
</div>
</div><!-- /.modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_detail" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:60% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
       <div class="modal-header bg-orange" style="height: 60px">
        <h5 class="modal-title">Data Warehouse</h5>
      </div>
       <div class="modal-body">
         <div class="form-body">
          <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
              <div class="form-group">
                <label class="control-label col-md-3">Room Code</label>
                <div class="col-md-8">
                  <input name="room_id_filter" id="room_id_filter" class="form-control" type="hidden" readonly>
                  <input name="room_code_filter" id="room_code_filter" class="form-control" type="text" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Room Name</label>
                <div class="col-md-8">
                  <input name="room_name_filter" id="room_name_filter" class="form-control" type="text" readonly>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="form-group">
                  <div class="col-md-5">
                    <label class="control-label col-md-3">Warehouse</label>
                    <select class="form-control select2" style="width: 100%;" name="warehouse_id"  id="warehouse_id">
                       @foreach($warehouse_list as $warehouse_lists)
                        <option value="{{$warehouse_lists->id}}">{{$warehouse_lists->warehouse_name}}</option>
                       @endforeach
                     </select>
                  </div>
                  <div class="col-md-5">
                    <label class="control-label col-md-3">Temprature</label>
                    <select class="form-control select2" style="width: 100%;" name="temprature_id"  id="temprature_id">
                       @foreach($temprature_list as $temprature_lists)
                        <option value="{{$temprature_lists->id}}">{{$temprature_lists->temprature_name}}</option>
                       @endforeach
                     </select>
                  </div>
                  <div class="col-md-1">
                      <label class="control-label col-md-3">Action</label>
                      <button type="button" id="btnAdd"  class="btn btn-primary btn-xs pull-right" style="margin-left:5px !important" onclick="store_detail();">  <i class="icon-plus-circle2"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            <div class="col-md-12">
            <div class="col-md-1">
            &nbsp;&nbsp;
            &nbsp;&nbsp;
            </div>
            <div class="col-md-12">
            <table id="dtTableDetail" class="table table-bordered table-hover stripe" style="width:100%">
              <thead>
                <tr>
                  <th>Warehouse Code</th>
                  <th>Warehouse Name</th>
                  <th>Temperature Code</th>
                  <th>Temperature Name</th>
                  <!-- <th>Description</th> -->
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <a href="#" onClick="reload_table_detail();" class="btn btn-success pull-right"><i class="fa fa-close"></i> Refresh</a>
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- End Bootstrap modal -->
@include('main.setting.room.script')
@include('scripts.change_status')
@stop
