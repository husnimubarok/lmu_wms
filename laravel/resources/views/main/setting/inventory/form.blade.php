@extends('layouts.main.template')
@section('title', 'Inventory')
@section('caption', 'Caption for this menu!')
@section('url_variable', 'inventory')
@section('val_variable', 'inventory')
@section('content')
@include('layouts.main.master_def_form.form_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form -->
          @if(isset($inventory))
            {!! Form::model($inventory, array('route' => ['main.inventory.update', $inventory->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @else
            {!! Form::open(array('route' => 'main.inventory.store', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @endif
          {{ csrf_field() }}
          <div class="panel panel-flat">

            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
                 <div class="form-group pull-right">
                    <label for="real_name" class="col-sm-4 control-label">Status: &nbsp; &nbsp; </label>
                    <div class="col-sm-8 pull-right">
                      <select class="select" name="status" id="status">
                       <option value="0">Active</option>
                       <option value="1">Inactive</option>
                     </select>
                   </div>
                 </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> Inventory details</legend>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Inventory Code', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('inventory_code', old('inventory_code'), ['class' => 'form-control', 'placeholder' => 'Inventory Code', 'id' => 'inventory_code']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Inventory Name', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('inventory_name', old('inventory_name'), ['class' => 'form-control', 'placeholder' => 'Inventory Name', 'id' => 'inventory_name']) }}
                      </div>
                    </div>

                   <div class="form-group">
                      {{ Form::label('seq_no', 'Unit Of Measure', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('unit_of_measure_id', $unit_of_measure_list, old('unit_of_measure_id'), array('class' => 'select', 'placeholder' => 'Select Unit Of Measure', 'id' => 'unit_of_measure_id')) }}
                      </div>
                    </div>
                  </fieldset>
                </div>

               <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"><i class="icon-truck position-left"></i>
                      Additional Info
                    </legend>
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Inventory Group', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::select('inventory_group_id', $inventory_group_list, old('inventory_group_id'), array('class' => 'select', 'placeholder' => 'Select Inventory Group', 'id' => 'inventory_group_id')) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Volume', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-5">
                        {{ Form::number('volume', old('volume'), ['class' => 'form-control', 'placeholder' => 'Volume', 'id' => 'volume']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Volume Unit', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('volume_unit', old('volume_unit'), ['class' => 'form-control', 'placeholder' => 'Volume Unit', 'id' => 'volume_unit']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
          </div>
        </form>
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.setting.inventory.script')
@include('scripts.change_status')
@stop
