<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: &nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        url: "{{ URL::route('main.monthly-rate.data') }}", 
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val(); 
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   dom: 'lBfrtip',
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [  
   { data: 'check', name: 'check', orderable: false, searchable: false },
   { data: 'action', name: 'action'}, 
   { data: 'monthly_rate_code', name: 'monthly_rate_code' },
   { data: 'monthly_rate_name', name: 'monthly_rate_name' },
   { data: 'description', name: 'description' },
   { data: 'rate_amount', name: 'rate_amount' },
   { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
  }
 });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
});

 function active_status()
{
  $("#filter_status").val('0');
  reload_table();
}

function inactive_status()
{
  $("#filter_status").val('1');
  reload_table();
}

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

function add()
{
  $("#btnSave").attr("onclick","save()");
  $("#btnSaveAdd").attr("onclick","saveadd()");

  $('.errorCustomerTypeName').addClass('hidden');

  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Monthly Rate'); // Set Title to Bootstrap modal title
}


function save()
{   
  var url;
  url = "{{ URL::route('main.monthly-rate.store') }}";
  
  $.ajax({
    type: 'POST',
    url: url,
    data: {
      '_token': $('input[name=_token]').val(), 
      'monthly_rate_code': $('#monthly_rate_code').val(), 
      'monthly_rate_name': $('#monthly_rate_name').val(), 
      'description': $('#description').val(), 
      'rate_amount': $('#rate_amount').val(), 
      'status': $('#status').val()
    },
    success: function(data) { 

      $('.errorCustomerTypeName').addClass('hidden');

      if ((data.errors)) {
        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.error('Data is required!', 'Error Validation', options);
        
        if (data.errors.monthly_rate_name) {
          $('.errorCustomerTypeName').removeClass('hidden');
          $('.errorCustomerTypeName').text(data.errors.monthly_rate_name);
        }
      } else {

        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.success('Successfully added data!', 'Success Alert', options);
        $('#modal_form').modal('hide');
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function saveadd()
{   
 $.ajax({
    type: 'POST',
    url: "{{ URL::route('main.monthly-rate.store') }}",
    data: {
      '_token': $('input[name=_token]').val(), 
      'monthly_rate_code': $('#monthly_rate_code').val(), 
      'monthly_rate_name': $('#monthly_rate_name').val(), 
      'description': $('#description').val(), 
      'rate_amount': $('#rate_amount').val(), 
      'status': $('#status').val()
    },
    success: function(data) {  
        $('.errorCustomerTypeName').addClass('hidden');

        if ((data.errors)) {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Data is required!', 'Error Validation', options);
        
          if (data.errors.monthly_rate_name) {
            $('.errorCustomerTypeName').removeClass('hidden');
            $('.errorCustomerTypeName').text(data.errors.monthly_rate_name);
          }
        } else {
      var options = { 
        "positionClass": "toast-bottom-right", 
        "timeOut": 1000, 
      };
      toastr.success('Successfully added data!', 'Success Alert', options);
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function edit(id)
 { 
  $('.errorCustomerTypeName').addClass('hidden');
  $("#btnSave").attr("onclick","update("+id+")");
  $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

  save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
      url : 'monthly-rate/edit/' + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="id_key"]').val(data.id); 
        $('[name="monthly_rate_code"]').val(data.monthly_rate_code);
        $('[name="monthly_rate_name"]').val(data.monthly_rate_name);
        $('[name="description"]').val(data.description);
        $('[name="rate_amount"]').val(data.rate_amount);
        $('[name="status"]').val(data.status);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Monthly Rate'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function update(id)
  {
    save_method = 'update'; 
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: 'monthly-rate/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'monthly_rate_code': $('#monthly_rate_code').val(), 
          'monthly_rate_name': $('#monthly_rate_name').val(), 
          'description': $('#description').val(), 
          'rate_amount': $('#rate_amount').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
          $('.errorCustomerTypeName').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Data is required!', 'Error Validation', options);
           
            if (data.errors.monthly_ratename) {
              $('.errorCustomerTypeName').removeClass('hidden');
              $('.errorCustomerTypeName').text(data.errors.monthly_ratename);
            }
          } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#modal_form').modal('hide');
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
        } 
      },
    })
  };

  function updateadd(id)
  {
      save_method = 'update'; 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url: 'monthly-rate/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'monthly_ratename': $('#monthly_ratename').val(), 
          'status': $('#status').val()
        },
        success: function(data) { 
          if ((data.errors)) {
           swal("Error!", "Gat data failed!", "error")
         } else { 
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
          $("#btnSave").attr("onclick","save()");
          $("#btnSaveAdd").attr("onclick","saveadd()");
        } 
      },
    })
  };

  function bulk_change_status() {
    var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    if (list_id.length > 0) {
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to archive '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'ARCHIVE',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                            },
                            url: "monthly-rate/bulk-change-status",
                            type: "POST", 
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = { 
                                        "positionClass": "toast-bottom-right", 
                                        "timeOut": 1000, 
                                    };
                                    toastr.success('Success archive!', 'Success Alert', options);
                                    reload_table();
                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'archive failed!',
                                    });
                                    reload_table();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'archive failed!',
                                });
                                reload_table();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
  }
</script> 




