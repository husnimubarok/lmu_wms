@extends('layouts.main.template')
@section('title', 'Vendor')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'vendor')   
@section('val_variable', 'supplier')   
@section('content') 
@include('layouts.main.master_def_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.master_def_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr> 
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th> 
                <th>Code</th> 
                <th>Name</th> 
                <th>Country</th>
                <th>City</th> 
                <th>District</th>
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.setting.vendor.script')
@include('scripts.change_status')
@stop


