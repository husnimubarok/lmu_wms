@extends('layouts.main.template')
@section('title', 'Vendor')   
@section('caption', 'Caption for this menu!')   
@section('url_variable', 'vendor')   
@section('val_variable', 'supplier')   
@section('content') 
@include('layouts.main.master_def_form.form_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
          @if(isset($vendor))
            {!! Form::model($vendor, array('route' => ['main.vendor.update', $vendor->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @else
            {!! Form::open(array('route' => 'main.vendor.store', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @endif
          {{ csrf_field() }}
          <div class="panel panel-flat">

            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
                 <div class="form-group pull-right">
                    <label for="real_name" class="col-sm-4 control-label">Status: &nbsp; &nbsp; </label>
                    <div class="col-sm-8 pull-right">
                      <select class="select" name="status" id="status">
                       <option value="0">Active</option>
                       <option value="1">Inactive</option>
                     </select>
                   </div>
                 </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> Vendor details</legend>

                    <div class="form-group"> 
                      {{ Form::label('seq_no', 'Vendor Code', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::text('supplier_code', old('supplier_code'), ['class' => 'form-control', 'placeholder' => 'Vendor Code', 'id' => 'supplier_code']) }}
                      </div>
                    </div>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Vendor Name', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('supplier_name', old('supplier_name'), ['class' => 'form-control', 'placeholder' => 'Vendor Name', 'id' => 'supplier_name']) }}
                      </div>
                    </div>

                    <div class="form-group"> 
                      {{ Form::label('seq_no', 'Country', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::select('country_id', $country_group_list, old('country_id'), array('class' => 'select', 'placeholder' => 'Select Country', 'id' => 'country_id')) }} 
                      </div>
                    </div>
 
                  </fieldset>
                </div>

                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> Vendor details</legend>

                     <div class="form-group"> 
                      {{ Form::label('seq_no', 'City', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::select('city_id', $city_group_list, old('city_id'), array('class' => 'select', 'placeholder' => 'Select City', 'id' => 'city_id')) }} 
                      </div>
                    </div>

                     <div class="form-group"> 
                      {{ Form::label('seq_no', 'District', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::select('district_id', $district_group_list, old('district_id'), array('class' => 'select', 'placeholder' => 'Select District', 'id' => 'district_id')) }} 
                      </div>
                    </div>

      
                    <div class="form-group">
                      {{ Form::label('seq_no', 'Description', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'id' => 'description', 'rows' => '5', 'cols' => '5']) }}
                      </div>
                    </div>
                  </fieldset>
                </div>
 
              </div>
              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
          </div>
        </form>
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.setting.vendor.script')
@include('scripts.change_status')
@stop


