@extends('layouts.main.template')
@section('title', 'Deferral Profile Maintenance')   
@section('content') 
<style type="text/css">
.btn-xs,
.btn-group-xs > .btn {
  padding: 1px 13px;
}
</style>
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          <ul class="nav nav-tabs nav-tabs-highlight">
            <input type="hidden" name="filter_status" id="filter_status">  
            <li class="active" onclick="active_status();"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-folder-check position-left"></i> Active</a></li>
            <li onclick="inactive_status();"><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-folder-minus2 position-left"></i> Inactive</a></li>
            </li> 
            <li class="pull-right">
              <a tabindex="0" href="#" target="_blank"><i class="icon-question4"></i></a>
            </li> 
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
            </li>
            <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCsv"><i class="icon-file-excel"></i> <span>Export to CSV</span></a> 
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print</span></a>
            </li>
          </ul>  
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr> 
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" class="styled"/>
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th> 
                <th>Code</th> 
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status: &nbsp; &nbsp; </label>
            <div class="col-sm-8 pull-right">
              <select class="select" name="status" id="status">
               <option value="0">Active</option>
               <option value="1">Inactive</option>
             </select>
           </div>
         </div>
         <h5 class="modal-title">Deferral Profile Maintenance Form</h5>
       </div>
       <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-2">Code</label>
          <div class="col-md-9">
            <input name="deferral_profile_maintenance_code" id="deferral_profile_maintenance_code" class="form-control text-uppercase" type="text">
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-md-2">Name</label>
          <div class="col-md-9">
            <input name="deferral_profile_maintenance_name" id="deferral_profile_maintenance_name" class="form-control" type="text">
            <small class="errorCustomerTypeName hidden alert-danger"></small> 
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-md-2">Description</label>
          <div class="col-md-9">
            {{-- <input name="description" id="description" class="form-control" type="text"> --}}
            <textarea rows="5" cols="5" name="description" id="description" class="form-control" placeholder="Default textarea"></textarea>
          </div>
        </div> 
      </div>
    </form>
    <div class="modal-footer"> 
      <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Close</button>
      <button id="btnSaveAdd"  class="btn btn-warning legitRipple" type="button">Save & Add</button>
      <button id="btnSave" class="btn btn-warning legitRipple" type="button">Save & Close</button>
    </div>
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   "lengthMenu": [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        "sSearch": "Search: &nbsp;&nbsp;"
    },
   ajax: {
        url: "{{ url('main/deferral-profile-maintenance/data') }}", 
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val(); 
        }
    },
   processing: true,
   language: {
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-danger" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   deferRender: true,
   colReorder: true,
   dom: 'lBfrtip',
    "colVis": {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [  
   { data: 'check', name: 'check', orderable: false, searchable: false },
   { data: 'action', name: 'action'}, 
   { data: 'deferral_profile_maintenance_code', name: 'deferral_profile_maintenance_code' },
   { data: 'deferral_profile_maintenance_name', name: 'deferral_profile_maintenance_name' },
   { data: 'description', name: 'description' },
   { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
  }
 });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
});

 function active_status()
{
  $("#filter_status").val('0');
  reload_table();
}

function inactive_status()
{
  $("#filter_status").val('1');
  reload_table();
}

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

function add()
{
  $("#btnSave").attr("onclick","save()");
  $("#btnSaveAdd").attr("onclick","saveadd()");

  $('.errorCustomerTypeName').addClass('hidden');

  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Deferral Profile Maintenance'); // Set Title to Bootstrap modal title
}


function save()
{   
  var url;
  url = "{{ URL::route('main.deferral-profile-maintenance.store') }}";
  
  $.ajax({
    type: 'POST',
    url: url,
    data: {
      '_token': $('input[name=_token]').val(), 
      'deferral_profile_maintenance_code': $('#deferral_profile_maintenance_code').val(), 
      'deferral_profile_maintenance_name': $('#deferral_profile_maintenance_name').val(), 
      'description': $('#description').val(), 
      'status': $('#status').val()
    },
    success: function(data) { 

      $('.errorCustomerTypeName').addClass('hidden');

      if ((data.errors)) {
        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.error('Data is required!', 'Error Validation', options);
        
        if (data.errors.deferral_profile_maintenance_name) {
          $('.errorCustomerTypeName').removeClass('hidden');
          $('.errorCustomerTypeName').text(data.errors.deferral_profile_maintenance_name);
        }
      } else {

        var options = { 
          "positionClass": "toast-bottom-right", 
          "timeOut": 1000, 
        };
        toastr.success('Successfully added data!', 'Success Alert', options);
        $('#modal_form').modal('hide');
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function saveadd()
{   
 $.ajax({
    type: 'POST',
    url: "{{ URL::route('main.deferral-profile-maintenance.store') }}",
    data: {
      '_token': $('input[name=_token]').val(), 
      'deferral_profile_maintenance_code': $('#deferral_profile_maintenance_code').val(), 
      'deferral_profile_maintenance_name': $('#deferral_profile_maintenance_name').val(), 
      'description': $('#description').val(), 
      'status': $('#status').val()
    },
    success: function(data) {  
        $('.errorCustomerTypeName').addClass('hidden');

        if ((data.errors)) {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Data is required!', 'Error Validation', options);
        
          if (data.errors.deferral_profile_maintenance_name) {
            $('.errorCustomerTypeName').removeClass('hidden');
            $('.errorCustomerTypeName').text(data.errors.deferral_profile_maintenance_name);
          }
        } else {
      var options = { 
        "positionClass": "toast-bottom-right", 
        "timeOut": 1000, 
      };
      toastr.success('Successfully added data!', 'Success Alert', options);
        $('#form')[0].reset(); // reset form on modals
        reload_table(); 
      } 
    },
  })
};

function edit(id)
 { 
  $('.errorCustomerTypeName').addClass('hidden');
  $("#btnSave").attr("onclick","update("+id+")");
  $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

  save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
      url : 'deferral-profile-maintenance/edit/' + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="id_key"]').val(data.id); 
        $('[name="deferral_profile_maintenance_code"]').val(data.deferral_profile_maintenance_code);
        $('[name="deferral_profile_maintenance_name"]').val(data.deferral_profile_maintenance_name);
        $('[name="description"]').val(data.description);
        $('[name="status"]').val(data.status);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Deferral Profile Maintenance'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function update(id)
  {
    save_method = 'update'; 
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: 'deferral-profile-maintenance/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'deferral_profile_maintenance_code': $('#deferral_profile_maintenance_code').val(), 
          'deferral_profile_maintenance_name': $('#deferral_profile_maintenance_name').val(), 
          'description': $('#description').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
          $('.errorCustomerTypeName').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Data is required!', 'Error Validation', options);
           
            if (data.errors.deferral_profile_maintenancename) {
              $('.errorCustomerTypeName').removeClass('hidden');
              $('.errorCustomerTypeName').text(data.errors.deferral_profile_maintenancename);
            }
          } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#modal_form').modal('hide');
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
        } 
      },
    })
  };

  function updateadd(id)
  {
      save_method = 'update'; 
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url: 'deferral-profile-maintenance/edit/' + id,
        type: "PUT",
        data: {
          '_token': $('input[name=_token]').val(), 
          'deferral_profile_maintenancename': $('#deferral_profile_maintenancename').val(), 
          'status': $('#status').val()
        },
        success: function(data) { 
          if ((data.errors)) {
           swal("Error!", "Gat data failed!", "error")
         } else { 
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully updated data!', 'Success Alert', options);
          $('#form')[0].reset(); // reset form on modals
          reload_table(); 
          $("#btnSave").attr("onclick","save()");
          $("#btnSaveAdd").attr("onclick","saveadd()");
        } 
      },
    })
  };

  function change_status(id, deferral_profile_maintenance_name)
  {
      var deferral_profile_maintenance_name = deferral_profile_maintenance_name.bold();
      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + deferral_profile_maintenance_name + ' data?',
        type: 'red',
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () { 
           }
         },
         confirm: {
          text: 'ARCHIVE',
          btnClass: 'btn-red',
          action: function () {
           $.ajax({
            url : 'deferral-profile-maintenance/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val() 
            },
            success: function(data)
            { 
              reload_table();
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }

  function bulk_change_status() {
    var list_id = [];
    $(".data-check:checked").each(function() {
        list_id.push(this.value);
    });

    if (list_id.length > 0) {
        $.confirm({
            title: 'Confirm!',
            content: 'Do you want to archive '+list_id.length+' data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'ARCHIVE',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'ids': list_id,
                            },
                            url: "deferral-profile-maintenance/bulk-change-status",
                            type: "POST", 
                            dataType: "JSON",
                            success: function(data) {
                                if(data.status) {
                                    var options = { 
                                        "positionClass": "toast-bottom-right", 
                                        "timeOut": 1000, 
                                    };
                                    toastr.success('Success archive!', 'Success Alert', options);
                                    reload_table();
                                } else {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'archive failed!',
                                    });
                                    reload_table();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'archive failed!',
                                });
                                reload_table();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $.alert({
            type: 'orange',
            icon: 'fa fa-warning', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'No data selected!',
        });
    }
  }
</script> 
@stop



