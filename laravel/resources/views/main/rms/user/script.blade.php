<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        url: "{{ URL::route('main.user.data') }}", 
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val(); 
            d.keyword = $('input[name=keyword]').val(); 
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       }) 
        $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
              ? '.buttons-excel'
              : null;
          if (btnClass) $buttons.find(btnClass).click(); 
        })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [  
   { data: 'check', name: 'check', orderable: false, searchable: false },
   { data: 'action', name: 'action'}, 
   { data: 'username', name: 'username' },
   { data: 'role_name', name: 'role_name' }, 
   { data: 'type_name', name: 'type_name' }, 
   { data: 'mstatus', name: 'mstatus' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });

   function active_status()
  {
    $("#filter_status").val('0');
    reload_table();
  }

  function inactive_status()
  {
    $("#filter_status").val('1');
    reload_table();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function add()
  {
    $("#btnSave").attr("onclick","save()");
    $("#btnSaveAdd").attr("onclick","saveadd()");

    $('.errorCustomerTypeName').addClass('hidden');

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

  function change_status(id, user_name, status)
  {
      var user_name = user_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + user_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () { 
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '@yield("url_variable")/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val() 
            },
            success: function(data)
            { 
              reload_table();
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }

  $('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script> 



