@extends('layouts.main.template')
@section('title', 'User')   
@section('caption', 'Caption for this user!')   
@section('url_variable', 'user')   
@section('val_variable', 'user')   
@section('content') 

<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group"> 
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a> 
      <a href="{{ URL::route('main.user.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-close2 text-indigo-400"></i> <span>Close</span></a>
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header 
Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
          @if(isset($user))
            {!! Form::model($user, array('route' => ['main.user.update', $user->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @else
            {!! Form::open(array('route' => 'main.user.store', 'files' => 'true', 'class' => 'form-horizontal'))!!}
          @endif
          {{ csrf_field() }}
          <div class="panel panel-flat">

            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset>
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> User details</legend>

                    <div class="form-group">
                      {{ Form::label('seq_no', 'Username', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9">
                        {{ Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => 'Username', 'id' => 'username']) }}
                      </div>
                    </div> 
 
                  </fieldset>
                </div>

                <div class="col-md-6"> 
                   <fieldset>
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> Role </legend>
                    <div class="form-group">
                      {{ Form::label('role_name', 'Role *', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                       {{ Form::select('role_id', $role_list, old('role_id'), array('class' => 'form-control select2', 'placeholder' => 'Select Role', 'id' => 'salesman_name', 'required' => 'true')) }}
                      </div>
                    </div> 
                </div>
              </fieldset>
            </div>


            <div class="col-md-6"> 
                   <fieldset>
                    <div class="form-group">
                      {{ Form::label('role_name', 'User Type *', ['class' => 'col-lg-3 control-label']) }}
                      <div class="col-lg-9"> 
                        <select class="select" name="type_name" id="type_name">
                          <option value="Mobile">Mobile</option>
                          <option value="WMS">WMS</option>
                        </select>
                      </div>
              </fieldset>
            </div>
              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
          </div>
        </form>
        <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.rms.user.script')
@include('scripts.change_status')
@stop


