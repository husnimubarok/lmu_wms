@extends('layouts.main.template')
@section('title', 'Role')   
@section('caption', 'Caption for role!')   
@section('url_variable', 'role')   
@section('val_variable', 'role')   
@section('content') 
@include('layouts.main.master_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.master_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr> 
                <th style="width:5%">
                  <label class="control control--checkbox">
                    <input type="checkbox" id="check-all" />
                    <div class="control__indicator"></div>
                  </label>
                </th>
                <th>Action</th> 
                <th>Name</th> 
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" id="modal_backdrop" class="modal fade" data-backdrop="false">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header bg-orange" style="height: 60px">
         @include('layouts.main.master_form.form_status')
         <h5 class="modal-title">@yield('title') Form</h5>
       </div>
       <div class="modal-body"> 
        <div class="form-group">
          <label class="control-label col-md-2">Name</label>
          <div class="col-md-9">
            <input name="name" id="name" class="form-control" type="text">
            <small class="errorCustomerTypeName hidden alert-danger"></small> 
          </div>
        </div>  
      </div>
    </form>
    <div class="modal-footer"> 
      @include('layouts.main.master_form.modal_button')
    </div>
  </div><!-- /.modal-content -->
</div>
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
@include('main.rms.role.script')
@include('scripts.change_status')
@stop


