@extends('layouts.main.template')
@section('title', 'User')   
@section('caption', 'Caption for this user!')   
@section('url_variable', 'user')   
@section('val_variable', 'user')   
@section('content')

<!-- Page header -->
<div class="page-header">
<div class="page-header-content">
  <div class="page-title">
    <h4>
      <i class="icon-arrow-right6 position-left"></i>
      <span class="text-semibold">Home</span> - @yield('title')
      <small class="display-block">@yield('title'), @yield('caption')</small>
    </h4>
  </div>
  @php
    $main = Request::segment(2);
  @endphp
  @if(isset($main))
  <div class="heading-elements">
    <div class="heading-btn-group"> 
      <a href="#" onClick="history.back()" class="btn btn-link btn-float has-text text-size-small"><i class="icon-history text-indigo-400"></i> <span>Back</span></a> 
      <a href="{{ URL::route('main.user.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-close2 text-indigo-400"></i> <span>Close</span></a>
    </div>
  </div>
  @endif
</div>
</div>
<!-- /page header 
Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- 2 columns form --> 
           

            <div class="panel panel-flat">

            <div class="panel-heading">
              <h5 class="panel-title"></h5>
              <div class="heading-elements">
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-4">
                <fieldset>
                

                    @if(isset($role))
                      {!! Form::model($role, array('route' => ['main.role-permission.update', $role->id], 'method' => 'PUT'))!!}
                    @else
                     {!! Form::open(array('route' => 'main.role-permission.store'))!!}
                    @endif
                    <legend class="text-semibold"><i class="icon-reading position-left"></i> User details</legend>
                    {{ Form::label('name', 'Name *') }}
                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Type here..', 'required' => 'required']) }}
                    <br>

                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Type here..']) }}
                    <br>
                    @if(isset($users) && count($users) !== 0)
                        {{ Form::label('user', 'Selected User') }}
                        <table class="table table-sm">
                        @foreach ($users as $index => $user)
                            @if($index % 2 == 0)
                                <tr>
                            @endif
                                <td>{{$index+1}}. {{$user->name}}</td>
                            @if($index % 2 == 1 || $index === count($users) - 1)                        
                                </tr>
                            @endif
                        @endforeach
                        </table>
                    @endif
                    </fieldset>
                </div>
                <div class="col-md-8">
                 <fieldset>
                  <legend class="text-semibold"><i class="icon-key position-left"></i> Permissions</legend>
                    @if(isset($allPermissions))

                        {{ Form::label('permission', 'All Permissions') }}

                        <table class="table table-sm">
                        <!-- <input type="checkbox" name="suppliera_create" value="0"> Read<br> -->
                        @foreach($allPermissions as $index => $permission)
                            <tr>
                                <td><b>{{$index+1}}. {{ $permission->modulename }}</b></td>
                                <td><b>{{ $permission->name }}</b></td>
                                <td>
                                    <input type="checkbox" name="{{$permission->keyname . '_read'}}" value="@if(isset($permission->read) && $permission->read === true) {{$permission->read}} @else 0 @endif" @if(isset($permission->read) && $permission->read === true) checked @endif> Read<br>
                                </td>
                                <td>
                                    <input type="checkbox" name="{{$permission->keyname . '_create'}}" value="@if(isset($permission->add) && $permission->add === true) {{$permission->add}} @else 0 @endif" @if(isset($permission->add) && $permission->add === true) checked @endif> Create<br>
                                </td>
                                <td>
                                    <input type="checkbox" name="{{$permission->keyname . '_update'}}" value=" @if(isset($permission->update) && $permission->update === true) {{$permission->update}} @else 0 @endif" @if(isset($permission->update) && $permission->update === true)checked @endif> Update<br>
                                </td>
                                <td>
                                    <input type="checkbox" name="{{$permission->keyname . '_delete'}}" value="@if(isset($permission->delete) && $permission->delete === true) {{$permission->delete}} @else 0 @endif" @if(isset($permission->delete) && $permission->delete === true) checked @endif> Delete<br>
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    @endif
                    </fieldset>
                    <hr/>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Save</button>
                <a href="{{URL::route('main.role-permission.index')}}" class="btn btn-default pull-right">
                    <i class="fa fa-arrow-circle-o-left"></i> Back
                </a> &nbsp;
        {!! Form::close() !!}
         <!-- /2 columns form -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@stop

@section('scripts')
<script></script>
@stop