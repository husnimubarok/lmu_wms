@extends('layouts.editor.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1>
                <i class="fa fa-sitemap"></i>&nbsp;{{$role->name}}
            </h1>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td><b>Name</b></td>
                            <td>{{$role->name}}</td>
                        </tr>
                        <tr>
                            <td><b>Module Name</b></td>
                            <td><b>Description</b></td>
                            <td>{{$role->description}}</td>
                        </tr>
                        <tr>
                            <td><b>Selected User</b></td>
                            <td>
                                <table>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                @endforeach
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Permissions</b></td>
                            <td>
                                <table class="table table-bordered">
                                
                                @foreach ($rolePermissions as $rp)
                                    <tr>
                                        <td>{{$rp->permission->modulename}}</td>
                                        <td>{{$rp->permission->name}}</td>
                                        <td>{{$rp->action}}</td>
                                    </tr>
                                @endforeach
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td><b>@if($role->status) {{$role->status}} @else {{'ACTIVE'}} @endif</b></td>
                        </tr>
                    </tbody>
                </table>
                <a href="{{URL::route('role-permission.index')}}" class="btn btn-default">
                    <i class="fa fa-arrow-circle-o-left"></i> Back
                </a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts') 
<script type="text/javascript"></script>
@stop