@extends('layouts.main.template')
@section('title', 'User')   
@section('caption', 'Caption for this role_permission!')   
@section('url_variable', 'role-permission')   
@section('val_variable', 'role_permission')   
@section('content') 
@include('layouts.main.master_def_form.index_button')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          @include('layouts.main.master_def_form.table_button')
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>  
                <th>Action</th>  
                <th>Name</th>
                <th>Description</th>
                <th>User Selected</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.rms.roles_permission.script')
@include('scripts.change_status')
@stop


