<script>
var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({ 
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
   ajax: {
        url: "{{ URL::route('main.role-permission.data') }}", 
        type : "POST",
        headers : {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
        data: function (d) {
            d.filter_status = $('input[name=filter_status]').val(); 
            d.keyword = $('input[name=keyword]').val(); 
        }
    },
   processing: true, 
   deferRender: true,
   colReorder: true,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       }) 
         $('#btnExcel').on('click', function() {
            var btnClass = 'excel'
               ? '.buttons-excel'
               : null;
            if (btnClass) $buttons.find(btnClass).click(); 
         })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click(); 
       })
     },
   columns: [  
    { data: 'action', name: 'action'}, 
    { data: 'name', name: 'name'},
    { data: 'description', name: 'description'},
    { data: 'countEmp', name: 'countEmp'},
    { data: 'status', name: 'status' }
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });
});

 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });

   function active_status()
  {
    $("#filter_status").val('0');
    reload_table();
  }

  function inactive_status()
  {
    $("#filter_status").val('1');
    reload_table();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }
 
</script> 



