@extends('layouts.main.template')
@section('title', 'User Log')
@section('caption', 'Caption for this user log!')
@section('url_variable', 'user')
@section('val_variable', 'user')
@section('content')
@include('layouts.report.report_form.index_button')

<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Column rendering -->
        <div class="panel panel-flat">
          <ul class="nav nav-tabs nav-tabs-highlight">
            <input type="hidden" name="filter_status" id="filter_status">
            <!-- <li class="pull-right">
              <a tabindex="0" href="#" target="_blank"><i class="icon-question4"></i> <span>Help</span></a>
            </li>  -->
            <li>
              <div class="input-group" style="margin-left:10px !important">
                {{ Form::text('date_from', date('d-m-Y',strtotime("-10 days")), ['class' => 'form-control datepicker', 'placeholder' => 'Date From', 'id' => 'date_from', 'onChange' => 'filter_data();']) }}

              </div>
            </li>
            <li>
              <div class="input-group" style="margin-left:10px !important">
                {{ Form::text('date_to', date('d-m-Y', strtotime("+1 days")), ['class' => 'form-control datepicker', 'placeholder' => 'Date To', 'id' => 'date_to', 'onChange' => 'filter_data();']) }}
              </div>
            </li>
            <li>
            &nbsp;
            </li>
            <li>
              <button class="btn btn-success btn-xs pull-left" type="button" onclick="filter_data();" data-dismiss="modal"><i class="icon-filter3"></i> Filter</button>
            </li>
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnCopy"><i class="icon-copy3"></i> <span>Copy</span></a>
            </li>
            <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to Excel</span></a>
            </li>
            <!-- <li class="pull-right">
                <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnExcel"><i class="icon-file-excel"></i> <span>Export to </span></a>
            </li> -->
            <li class="pull-right">
              <a tabindex="0" aria-controls="DataTables_Table_1" href="#" id="btnPrint"><i class="icon-printer2"></i> <span>Print</span></a>
            </li>
          </ul>
          <table id="dtTable" class="table datatable-scroller-buttons datatable-colvis-state" width="100%">
            <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Description</th>
                <th>Transaction Type</th>
                <th>Transaction No</th>
                <th>Action</th>
                <th>Source</th>
                <th>Transaction Date</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
               <tr>
                 <th>#</th>
                 <th>Username</th>
                 <th>Description</th>
                 <th>Transaction Type</th>
                 <th>Transaction No</th>
                 <th>Action</th>
                 <th>Source</th>
                 <th>Transaction Date</th>
               </tr>
          </tfoot>
          </table>
      </div>
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
<!-- End Bootstrap modal -->
@include('main.rms.user_log.script')
@stop
