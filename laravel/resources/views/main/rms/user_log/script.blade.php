<script>

$('#date_from').datepicker({ dateFormat: 'dd-mm-yy' }).val();
$('#date_to').datepicker({ dateFormat: 'dd-mm-yy' }).val();

var table;
var status = 0;
$(document).ready(function() {
  //datatables
  table = $('#dtTable').DataTable({
  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
  "<'row'<'col-xs-12't>>"+
  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
   processing: true,
   serverSide: true,
   lengthMenu: [[10, 25, 50, 100, -1], [10,   25, 50, 100, "All"]],
   language : {
        sLengthMenu: "Show: &nbsp; _MENU_ &nbsp;&nbsp;&nbsp;&nbsp;",
        sSearch: "Search: <input type='hidden' id='keyword' name='keyword'> _INPUT_&nbsp;&nbsp;",
        processing: '<button type="button" style="position: relative; z-index: 10000 !important" class="btn btn-default" id="spinner-dark-9"><i class="icon-sync spinner position-left"></i> Processing...</button>'
    },
    ajax: {
          url: "{{ URL::route('main.user-log.data') }}",
         type : "POST",
         headers : {
           'X-CSRF-TOKEN': '{{ csrf_token() }}'
           },
         data: function (d) {
             d.keyword = $('input[name=keyword]').val();
             d.date_from = $('input[name=date_from]').val();
             d.date_to = $('input[name=date_to]').val();
         }
    },
   processing: true,
   deferRender: true,
   order: [[ 6, "desc" ]],
   colReorder: true,
   dom: 'lBfrtip',
   columnDefs: [
      { targets: 'action', orderable: false }
    ],
   colVis: {
        "buttonText": "Change columns"
    },
    buttons: [
        'copy', 'csv', 'excel', 'print','pageLength', 'colvis'
    ],
    initComplete: function() {
       var $buttons = $('.dt-buttons').hide();
       $('#btnCopy').on('click', function() {
          var btnClass = 'copy'
             ? '.buttons-copy'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnPrint').on('click', function() {
          var btnClass = 'print'
             ? '.buttons-print'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
       $('#btnCsv').on('click', function() {
          var btnClass = 'csv'
             ? '.buttons-csv'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
        $('#btnExcel').on('click', function() {
          var btnClass = 'excel'
              ? '.buttons-excel'
              : null;
          if (btnClass) $buttons.find(btnClass).click();
        })
       $('#btnColvis').on('click', function() {
          var btnClass = 'colvis'
             ? '.buttons-colvis'
             : null;
          if (btnClass) $buttons.find(btnClass).click();
       })
     },
   columns: [
   { data: 'id', name: 'id' },
   { data: 'username', name: 'username' },
   { data: 'description', name: 'description' },
   { data: 'scope', name: 'scope' },
   { data: 'transaction_number', name: 'transaction_number' },
   { data: 'action', name: 'action' },
   { data: 'source', name: 'source' },
   { data: 'created_at', name: 'created_at' },
   ],
   rowReorder: {
      dataSrc: 'action'
   }
  });

  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });

  table.columns().every( function () {
      var that = this;

      $( 'input', this.footer() ).on( 'keyup change clear', function () {
          if ( that.search() !== this.value ) {
              that
                  .search( this.value )
                  .draw();
          }
      } );
  } );
});

  // Setup - add a text input to each footer cell
   $('#dtTable tfoot th').each( function () {
       var title = $(this).text();
       $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
   } );

   // var table = $('#dtTable').DataTable();

    // Apply the search


  // $('#dtTable thead tr').clone(true).appendTo( '#dtTable thead' );
  // $('#dtTable thead tr:eq(1) th').each( function (i) {
  //     var title = $(this).text();
  //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
  //
  //     $( 'input', this ).on( 'keyup change', function () {
  //         if ( table.column(i).search() !== this.value ) {
  //             table
  //                 .column(i)
  //                 .search( this.value )
  //                 .draw();
  //         }
  //     } );
  // } );

 
 $('#search-form').on('submit', function(e) {
    table.draw();
    e.preventDefault();
  });

   function active_status()
  {
    $("#filter_status").val('0');
    reload_table();
  }

  function inactive_status()
  {
    $("#filter_status").val('1');
    reload_table();
  }

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }

  function add()
  {
    $("#btnSave").attr("onclick","save()");
    $("#btnSaveAdd").attr("onclick","saveadd()");

    $('.errorCustomerTypeName').addClass('hidden');

    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add @yield("title")'); // Set Title to Bootstrap modal title
  }

  function change_status(id, user_name, status)
  {
      var user_name = user_name.bold();

      if(status == 0){
        btn = 'btn-red';
        text_field = 'INACTIVE';
        type_field = 'red';
      }else{
        btn = 'btn-green';
        text_field = 'ACTIVE';
        type_field = 'green';
      };

      $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to archive ' + user_name + ' data?',
        type: type_field,
        typeAnimated: true,
        buttons: {
          cancel: {
           action: function () {
           }
         },
         confirm: {
          text: text_field,
          btnClass: btn,
          action: function () {
           $.ajax({
            url : '@yield("url_variable")/delete/' + id,
            type: "DELETE",
            data: {
              '_token': $('input[name=_token]').val()
            },
            success: function(data)
            {
              reload_table();
              var options = {
                "positionClass": "toast-bottom-right",
                "timeOut": 1000,
              };
              toastr.success('Successfully archive data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              $.alert({
                type: 'red',
                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Error archive data!',
              });
            }
          });
         }
       },
     }
   });
  }

  function filter_data()
  {
    var date_from = $('input[name=date_from]').val();
    var date_to = $('input[name=date_to]').val();
    if(date_from == '' || date_to == '')
    {
    $.alert({
        title: 'Warning!',
        type_field: 'red',
        content: 'Filter from is required!',
    });
    }else{
    reload_table();

    };
  }

</script>
