@extends('layouts.main.template')
@section('title', 'Dasboard')
@section('content')
<!-- Page container -->
  <div class="page-container">
    <!-- Page content -->
    <div class="page-content">
      <!-- Main content -->
      <div class="content-wrapper">

      	<!-- Palette colors -->
				<h4 class="content-group-sm text-semibold">
          @php
            /* This sets the $time variable to the current hour in the 24 hour clock format */
            $time = date("H");
            /* Set the $timezone variable to become the current timezone */
            $timezone = date("e");
            /* If the time is less than 1200 hours, show good morning */
            if ($time < "12") {
                echo "Good Morning";
            } else
            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
            if ($time >= "12" && $time < "17") {
                echo "Good Afternoon";
            } else
            /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
            if ($time >= "17" && $time < "19") {
                echo "Good Evening";
            } else
            /* Finally, show good night if the time is greater than or equal to 1900 hours */
            if ($time >= "19") {
                echo "Good Night";
            }
          @endphp

        <br>
					<small class="display-block">Welcome to Warehouse Management System Application</small>
					<small class="display-block">You have some notifications bellow</small>
				</h4>
        <!-- Dashboard content -->
        <div class="row">



        </div>
        <!-- /dashboard content -->
      </div>
      <!-- /main content -->
    </div>
    <!-- /page content -->
  </div>
  <!-- /page container -->
@stop
