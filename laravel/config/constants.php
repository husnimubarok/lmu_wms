<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => '/lmu_wms/uploads',
        'bootstrap' => '/lmu_wms/laravel/bootstrap',
        'css' => '/lmu_wms/assets/css',
        'scss' => '/lmu_wms/assets/lte_sass/build/scss',
        'img' => '/lmu_wms/assets/img',
        'images' => '/lmu_wms/assets/images',
        'js' => '/lmu_wms/assets/js',
        'plugin' => '/lmu_wms/assets/plugins',
        'swf' => '/lmu_wms/assets/swf',
    ],

];